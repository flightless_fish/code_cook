# Qt说明与学习资料



## 1. 如何安装

* [Qt安装说明](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/qt/README.md)



## 2. 学习资料

Qt的学习资料可以在如下的地址找到：

* [Qt Books](https://gitee.com/pi-lab/resources/tree/master/books/qt)
* [《Qt 学习之路》](https://www.w3cschool.cn/learnroadqt/) : 适合初学
* [《QT基本操作（简易教程）》](https://blog.csdn.net/qq_43333395/article/details/90814951)
* [《QT学习教程（全面）》](https://blog.csdn.net/perfectguyipeng/article/details/84611569)



## 3. References

* [《Qt编程指南》](https://qtguide.ustclug.org/) : 比较详细，但是不适合初学
* [《Qt5编程入门教程》](http://c.biancheng.net/qt/)

