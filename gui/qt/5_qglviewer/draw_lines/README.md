# 简单的QGLViewer示例程序

本程序演示了如何使用QGLViewer画线段。

![cover image](images/draw_lines.png)

## 需要的库

```
sudo apt install build-essential cmake git
sudo apt install gcc g++
```

Install Qt5:
```
# QT5
sudo apt install qt5-default qt5-qmake-bin
sudo apt install libqt5network5 libqt5sql5 libqt5svg5 libqt5svg5-dev 
```

Install Qt4 (If you use Qt4):
```
sudo apt install libqt4-dev qt4-qmake libqt4-opengl-dev libqt4-sql libqt4-sql-sqlite
```

## 编译
```
mkdir build
cd build
qmake ../draw_lines.pro
make

# 执行
./draw_lines
```

