/****************************************************************************

 Copyright (C) 2002-2008 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.3.4.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

#include <math.h>
#include <stdlib.h> // RAND_MAX

#include "draw_widget.h"


using namespace qglviewer;
using namespace std;

///////////////////////   V i e w e r  ///////////////////////
void Viewer::init()
{
    restoreStateFromFile();
    glDisable(GL_LIGHTING);

    nLineSeg = 600;

    glPointSize(2.0);
    glLineWidth(2);

    setGridIsDrawn();

    // set scene visualization range is 200, this value need fit to draw range
    setSceneRadius(200);

    help();
}

void Viewer::draw()
{
    double x, y, z;
    double R, deg_ratio, z_inc;

    R = 100;
    deg_ratio = 0.05;
    z_inc = 0.1;

    glBegin(GL_LINE_STRIP);

    for(int i=0; i<nLineSeg; i++)
    {
        x = R*cos(i*deg_ratio);
        y = R*sin(i*deg_ratio);
        z = i*z_inc;

        glColor3ub(0xFF, 0x00, 0x00);
        glVertex3d(x, y, z);
    }

    glEnd();
}

QString Viewer::helpString() const
{
    QString text("<h2>Draw Line</h2>");
    text += "Demo of use GL_LINE_STRIP to draw continue lines";
    return text;
}
