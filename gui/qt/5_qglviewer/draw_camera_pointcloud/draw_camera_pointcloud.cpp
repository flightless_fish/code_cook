
#include "viewer_widget.h"
#include "GSLAM/core/Svar.h"
#include <qapplication.h>

int main(int argc, char** argv)
{
    svar.ParseMain(argc, argv);

    QApplication application(argc,argv);

    Viewer viewer;

#if QT_VERSION < 0x040000
    application.setMainWidget(&viewer);
#else
    viewer.setWindowTitle("Draw Camera & PointCloud");
#endif

    viewer.show();

    return application.exec();
}
