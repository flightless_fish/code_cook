
#include <QGLViewer/qglviewer.h>
#include <QKeyEvent>
#include <GSLAM/core/SPtr.h>

class ViewerData;

class Viewer : public QGLViewer
{
protected :
    virtual void draw();
    virtual void init();

    virtual QString helpString() const;
    virtual void keyPressEvent(QKeyEvent *ke);

private:
    SPtr<ViewerData>    m_data;
};
