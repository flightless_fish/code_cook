# 可视化相机、点云示例程序

本程序演示了如何使用QGLViewer可视化相机、点云。

![cover image](images/camera_pointcloud.png)

## 需要的库

```
sudo apt install build-essential cmake git
sudo apt install gcc g++
```

Install Qt5:
```
# QT5
sudo apt install qt5-default qt5-qmake-bin
sudo apt install libqt5network5 libqt5sql5 libqt5svg5 libqt5svg5-dev 
```

Install Qt4 (If you use Qt4):
```
# QT4
sudo apt install libqt4-dev qt4-qmake libqt4-opengl-dev libqt4-sql libqt4-sql-sqlite
```

## 编译
```
# 编译
mkdir build
cd build
qmake ../draw_camera_pointcloud.pro
make

# 执行
./draw_camera_pointcloud
```

