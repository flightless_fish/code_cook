
#include <vector>
#include <iostream>
#include <fstream>
#include <ios>

#include <math.h>
#include <stdlib.h>

#include <Eigen/Eigen>
#include <GSLAM/core/SE3.h>
#include <GSLAM/core/Point.h>
#include <GSLAM/core/Svar.h>

#include "viewer_widget.h"
#include "tinyformat.h"

using namespace qglviewer;
using namespace std;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct PointClound
{
    pi::Point3d     pt;
    pi::Point3ub    ptc;
};

class ViewerData
{
public:
    ViewerData() {}
    virtual ~ViewerData() {}

    int loadData(const std::string& fname) {
        int N, M;

        std::cout << tinyformat::format("Load file: %s\n", fname);

        std::ifstream   ifs;
        ifs.open(fname, std::ios::in);
        if( !ifs.is_open() ) {
            std::cerr << "ERR: can not open file!\n";
            return -1;
        }

        // read camera, point number
        ifs >> N >> M;
        std::cout << tinyformat::format("cam: %6d, pts: %6d\n", N, M);

        arr_cameras.resize(N);
        arr_points.resize(M);

        // read cameras
        pi::SE3d cam;
        for(int i=0; i<N; i++) {
            ifs >> cam;
            arr_cameras[i] = cam;
            std::cout << tinyformat::format("cam[%4d]: ", i) << cam << "\n";
        }

        // read points
        for(int i=0; i<M; i++) {
            PointClound p;
            double p1[3];
            int p2[3];

            ifs >> p1[0] >> p1[1] >> p1[2] >> p2[0] >> p2[1] >> p2[2];
            std::cout << tinyformat::format("pt[%6d]: %12f %12f %12f %4d %4d %4d\n",
                                            i,
                                            p1[0], p1[1], p1[2],
                                            p2[0], p2[1], p2[2]);

            p.pt  = pi::Point3d(p1[0], p1[1], p1[2]);
            p.ptc = pi::Point3ub(p2[0], p2[1], p2[2]);
            arr_points[i] = p;
        }
    }

public:
    std::vector<pi::SE3d>       arr_cameras;
    std::vector<PointClound>    arr_points;
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Viewer::init()
{
    m_data = SPtr<ViewerData>(new ViewerData);

    restoreStateFromFile();
    glDisable(GL_LIGHTING);

    glPointSize(2.0);
    glLineWidth(2);

    setGridIsDrawn();

    // set scene visualization range is 200, this value need fit to draw range
    setSceneRadius(2000);
}

void Viewer::draw()
{
    if( !m_data->arr_cameras.size() ) return;

    //////////////////////////////////////
    /// draw points
    //////////////////////////////////////
    {
        double pointsize = svar.GetDouble("OpenGL.pointSize", 3.0);
        glPointSize(pointsize);

        std::vector<PointClound> &arrp = m_data->arr_points;
        int np = arrp.size();

        glBegin(GL_POINTS);

        for(int i=0; i<np; i++) {
            PointClound &p = arrp[i];
            glColor3ub(p.ptc.x, p.ptc.y, p.ptc.z);
            glVertex3f(p.pt.x, p.pt.y, p.pt.z);
        }

        glEnd();
    }

    //////////////////////////////////////
    /// draw camera
    //////////////////////////////////////
    {
        double camSize = svar.GetDouble("OpenGL.camSize", 8.0);

        glLineWidth(2.0);
        glBegin(GL_LINES);

        double cam[12], _R[9], _t[3];
        std::vector<pi::SE3d> &ac = m_data->arr_cameras;

        for(int i=0; i<ac.size(); i++) {
            pi::SE3d &c = ac[i];
            c.getMatrix(cam);

            for(int jj=0; jj<3; jj++) {
                for(int ii=0; ii<3; ii++) {
                    _R[jj*3+ii] = cam[jj*4+ii];
                }
                _t[jj] = cam[jj*4 + 3];
            }

            Eigen::Matrix3d R(_R);
            Eigen::Vector3d t1(_t), t, pxyz;

            t = -R * t1;

            Eigen::Vector3d vright   =  R.col(0).normalized() * camSize;
            Eigen::Vector3d vup      =  R.col(1).normalized() * camSize;
            Eigen::Vector3d vforward =  R.col(2).normalized() * camSize;

            // x-axis
            pxyz = t + vright;

            glColor3ub(255, 0, 0);
            glVertex3f(t(0), t(1), t(2));
            glVertex3f(pxyz(0), pxyz(1), pxyz(2));

            // y-axis
            pxyz = t + vup;

            glColor3ub(0, 255, 0);
            glVertex3f(t(0), t(1), t(2));
            glVertex3f(pxyz(0), pxyz(1), pxyz(2));

            // z-axis
            pxyz = t + vforward;

            glColor3ub(0, 0, 255);
            glVertex3f(t(0), t(1), t(2));
            glVertex3f(pxyz(0), pxyz(1), pxyz(2));
        }

        glEnd();
    }
}

QString Viewer::helpString() const
{
    QString text("<h2>Draw Camera & Point Cloud</h2>");
    return text;
}

void Viewer::keyPressEvent(QKeyEvent *ke)
{
    if( ke->key() == Qt::Key_L ) {
        std::string fname = svar.GetString("fn_in", "../data/img_sfm_1s-cam_pc.txt");
        m_data->loadData(fname);

        update();
    } else if ( ke->key() == Qt::Key_Q ) {
        close();
    } else {
        QGLViewer::keyPressEvent(ke);
    }
}
