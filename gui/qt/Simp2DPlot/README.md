# Simple 2D Plot

这个程序演示如何使用呢`QCustomPlot`画二维图形。具体的效果如下图所示：

![demo image](figures/Simple2DPlot.png)



程序的主题文件是：`main.cpp`，生成绘图窗口、仿真线程

主窗口是：`gui_mainwindow.h`, `gui_mainwindow.cpp`，主要包括`QCustomPlot`对象，以及绘图的函数

仿真线程：`thread_sim.h`, `thread_sim.cpp`，模拟一个旋转半径不断增加的圆弧运动 



## 需要的库

* Qt4 (sudo apt-get install libqt4-core libqt4-dev)
* QCustomPlot (included, webpage: http://www.workslikeclockwork.com/) 



## Compile

```
qmake-qt4 Simp2DPlot.pro
make
```



## Usage
```
./build/Simp2DPlot
```
