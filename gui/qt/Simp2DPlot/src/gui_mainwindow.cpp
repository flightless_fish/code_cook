#include <QtGui>
#include <QDebug>
#include <QDesktopWidget>
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>


#include "gui_mainwindow.h"


PlotWindow::PlotWindow(QWidget *parent) :
    QMainWindow(parent)
{
    muxData = new QMutex(QMutex::NonRecursive);

    customPlot = new QCustomPlot(this);
    connect(customPlot, SIGNAL(beforeReplot()),     this, SLOT(plotBegin()));
    connect(customPlot, SIGNAL(afterReplot()),      this, SLOT(plotEnd()));

    setCentralWidget(customPlot);

    setupInitData();
    setupCanvas();

    // window title
    setWindowTitle("2D Plot");

    // status bar
    msgString1 = "";
    msgString2 = "";
    statusBar()->clearMessage();

    customPlot->replot();

    //startTimer(30);
}

PlotWindow::~PlotWindow()
{
    delete muxData;
}

void PlotWindow::keyPressEvent(QKeyEvent *event)
{
    int key;
    int cmd = -1;

    key  = event->key();

    if( key == Qt::Key_Up )     cmd = 1;
    if( key == Qt::Key_Down )   cmd = 2;
    if( key == Qt::Key_Left )   cmd = 3;
    if( key == Qt::Key_Right )  cmd = 4;


    QCPRange    rng_x, rng_y;
    rng_x = customPlot->xAxis->range();
    rng_y = customPlot->yAxis->range();

    // Pan
    if( key == Qt::Key_A ) {
        customPlot->xAxis->moveRange(-rng_x.size()/8.0);
        plot();
    }

    if( key == Qt::Key_D ) {
        customPlot->xAxis->moveRange(rng_x.size()/8.0);
        plot();
    }

    if( key == Qt::Key_W ) {
        customPlot->yAxis->moveRange(rng_y.size()/8.0);
        plot();
    }

    if( key == Qt::Key_S ) {
        customPlot->yAxis->moveRange(-rng_y.size()/8.0);
        plot();
    }

    // zoom
    if( key == Qt::Key_Comma) {
        customPlot->xAxis->scaleRange(0.8, rng_x.center());
        customPlot->yAxis->scaleRange(0.8, rng_y.center());
        plot();
    }

    if( key == Qt::Key_Period) {
        customPlot->xAxis->scaleRange(1.25, rng_x.center());
        customPlot->yAxis->scaleRange(1.25, rng_y.center());
        plot();
    }

    if( cmd > 0 ) emit commandSend(cmd);
}

void PlotWindow::mousePressEvent(QMouseEvent *event)
{
    // 1 - left
    // 2 - right
    // 4 - middle
    printf("window pressed, %d, %d, %d\n", event->button(), event->pos().x(), event->pos().y());

    if( event->button() == 1 ) {

    }
}

void PlotWindow::resizeEvent(QResizeEvent *event)
{
    customPlot->xAxis->setScaleRatio(customPlot->yAxis, 1.0);
}

void PlotWindow::timerEvent(QTimerEvent *event)
{
    //plot();
}

void PlotWindow::canvasMouseMoveEvent(QMouseEvent *event)
{
    double      fx, fy;
    QString     msg;

    fx = customPlot->xAxis->pixelToCoord(event->pos().x());
    fy = customPlot->yAxis->pixelToCoord(event->pos().y());

    msgString2.sprintf("pos: %7.3f %7.3f\n", fx, fy);

    msg = msgString1 + " | " + msgString2;
    statusBar()->showMessage(msg);
}


void PlotWindow::canvsMousePressEvent(QMouseEvent *event)
{
#if 0
    // 1 - left
    // 2 - right
    // 4 - middle
    if( event->button() != 2 ) return;

    double      fx, fy;

    fx = customPlot->xAxis->pixelToCoord(event->pos().x());
    fy = customPlot->yAxis->pixelToCoord(event->pos().y());

    // add to estimated position
    arrEstPos_x.push_back(fx);
    arrEstPos_y.push_back(fy);
    curvEstPos->setData(arrEstPos_x, arrEstPos_y);

    // generate particles
    int n_particles, i;
    double  x1, y1;
    n_particles = 10;
    arrParticles_x.clear();
    arrParticles_y.clear();
    for(i=0; i<n_particles; i++) {
        x1 = fx + 2.0*(rand()%10000)/10000.0-1.0;
        y1 = fy + 2.0*(rand()%10000)/10000.0-1.0;

        arrParticles_x.push_back(x1);
        arrParticles_y.push_back(y1);
    }
    plotParticles->setData(arrParticles_x, arrParticles_y);

    // draw car
    if( arrEstPos_x.size() > 1 ) {
        int         n;
        double      x1, y1, x2, y2, dx, dy;
        n = arrEstPos_x.size();

        x1 = arrEstPos_x[n-2];
        y1 = arrEstPos_y[n-2];
        x2 = arrEstPos_x[n-1];
        y2 = arrEstPos_y[n-1];

        dx = x2 - x1;
        dy = y2 - y1;
        parmCarModel[2] = atan2(dy, dx);
    }
    parmCarModel[0] = fx;
    parmCarModel[1] = fy;
    drawCar();

    // replot canvas
    customPlot->replot();
#endif
}


void PlotWindow::canvasReplot(void)
{
    plot();    
}

void PlotWindow::plotBegin(void)
{
    muxData->lock();
}

void PlotWindow::plotEnd(void)
{
    muxData->unlock();
}


void PlotWindow::canvasShowMessage(QString msg)
{
    showMessage(msg);
}



void PlotWindow::setupInitData(void)
{
    if ( 0 ) {
        double      t, x, y, x1, y1;
        int         t_step = 60;
        double      dt, R=0, dR=0.2;
        int         i;

        // generate waypoints
        dt = 2*M_PI/t_step;
        for(i = 0; i<t_step; i++) {
            t = i*dt;
            x = R*cos(t);
            y = R*sin(t);
            arrWaypoint_x.push_back(x);
            arrWaypoint_y.push_back(y);

            R = R + dR;
        }
    }

    // setupt car parameters
    parmCarModel[0] = 0;            // pos - x
    parmCarModel[1] = 0;            // pos - y
    parmCarModel[2] = 0;            // angle
    parmCarModel[3] = 1;            // size

}

void PlotWindow::setupCanvas(void)
{
    connect(customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(canvsMousePressEvent(QMouseEvent*)));
    connect(customPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(canvasMouseMoveEvent(QMouseEvent*)));

    customPlot->xAxis->setScaleRatio(customPlot->yAxis, 1.0);

    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    if( 0 ) {
        customPlot->setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom)); // period as decimal separator and comma as thousand separator
        customPlot->legend->setVisible(true);
        QFont legendFont = font();  // start out with PlotWindow's font..
        legendFont.setPointSize(9); // and make a bit smaller for legend
        customPlot->legend->setFont(legendFont);
        customPlot->legend->setBrush(QBrush(QColor(255,255,255,230)));

        // by default, the legend is in the inset layout of the main axis rect. So this is how we access it to change legend placement:
        customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignRight);
    }


    // setup waypoint plot
    curvWayPoint = new QCPCurve(customPlot->xAxis, customPlot->yAxis);
    customPlot->addPlottable(curvWayPoint);
    curvWayPoint->setPen(QPen(QColor(255, 0, 0)));
    //curvWayPoint->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, QColor(255, 0, 0), QColor(255, 0, 0), 7));
    curvWayPoint->setLineStyle(QCPCurve::lsLine);
    curvWayPoint->setName("Track");



    // real car position
    curvCar = new QCPCurve(customPlot->xAxis, customPlot->yAxis);
    customPlot->addPlottable(curvCar);
    curvCar->setPen(QPen(QColor(0, 0, 255)));
    curvCar->setLineStyle(QCPCurve::lsLine);
    curvCar->setBrush(QBrush(QColor(0, 0, 255, 80)));
    curvCar->setName("Real car position");



    // set ranges appropriate to show data:
    customPlot->xAxis->setRange(-1000, 1000);
    customPlot->yAxis->setRange(-1000, 1000);
    // set labels
    customPlot->xAxis->setLabel("X [m]");
    customPlot->yAxis->setLabel("Y [m]");
    // make ticks on both axis go outward:
    customPlot->xAxis->setTickLength(0, 5);
    customPlot->xAxis->setSubTickLength(0, 3);
    customPlot->yAxis->setTickLength(0, 5);
    customPlot->yAxis->setSubTickLength(0, 3);

    // configure right and top axis to show ticks but no labels:
    customPlot->xAxis2->setVisible(true);
    customPlot->xAxis2->setTickLabels(false);
    customPlot->yAxis2->setVisible(true);
    customPlot->yAxis2->setTickLabels(false);
    customPlot->xAxis2->setTickLength(0, 0);
    customPlot->xAxis2->setSubTickLength(0, 0);
    customPlot->yAxis2->setTickLength(0, 0);
    customPlot->yAxis2->setSubTickLength(0, 0);
}


void PlotWindow::setPath(QVector<double> &arrX, QVector<double> &arrY)
{
    muxData->lock();

    curvWayPoint->setData(arrX, arrY);

    muxData->unlock();
}



void PlotWindow::setCarPos(double x, double y, double t)
{
    muxData->lock();

    parmCarModel[0] = x;
    parmCarModel[1] = y;
    parmCarModel[2] = t;

    drawCar();

    muxData->unlock();
}

void PlotWindow::setCarSize(double s)
{
    muxData->lock();

    parmCarModel[3] = s;

    drawCar();

    muxData->unlock();
}

void PlotWindow::setCarModel(double *parm)
{
    muxData->lock();

    for(int i=0; i<4; i++) {
        parmCarModel[i] = parm[i];
    }

    drawCar();

    muxData->unlock();
}

void PlotWindow::setPlotRange(double xmin, double xmax, double ymin, double ymax)
{
    muxData->lock();

    // set ranges appropriate to show data:
    customPlot->xAxis->setRange(xmin, xmax);
    customPlot->yAxis->setRange(ymin, ymax);

    muxData->unlock();
}

void PlotWindow::showMessage(QString &msg)
{
    QString     _msg;

    msgString1 = msg;
    _msg = msgString1 + " | " + msgString2;
    statusBar()->showMessage(_msg);
}

void PlotWindow::setScreenShot_fname(std::string &fnBase)
{
    fnScreenShot_base = fnBase;
}


void PlotWindow::clear(void)
{
    muxData->lock();

    arrWaypoint_x.clear();
    arrWaypoint_y.clear();
    curvWayPoint->setData(arrWaypoint_x, arrWaypoint_y);

    muxData->unlock();
}

void PlotWindow::plot(void)
{
    static int  idx = 0;
    QString     fnOut;

    customPlot->replot();

    if( fnScreenShot_base.size() > 0 ) {
        fnOut.sprintf("%s_%05d.png", fnScreenShot_base.c_str(), idx++);
        customPlot->savePng(fnOut);
    }
}

void PlotWindow::drawCar(void)
{
    double  x, y, t, s;
    double  x1, y1, x2, y2;

    QVector<double> arrX, arrY;


    x = parmCarModel[0];
    y = parmCarModel[1];
    t = parmCarModel[2];
    s = parmCarModel[3];

    x2 = x+s/3*cos(M_PI/2.0+t);
    y2 = y+s/3*sin(M_PI/2.0+t);
    arrX.push_back(x2);
    arrY.push_back(y2);

    x1 = x+s/3*cos(3.0*M_PI/2.0+t);
    y1 = y+s/3*sin(3.0*M_PI/2.0+t);
    arrX.push_back(x1);
    arrY.push_back(y1);

    x1 = x+s*1.3*cos(t);
    y1 = y+s*1.3*sin(t);
    arrX.push_back(x1);
    arrY.push_back(y1);

    arrX.push_back(x2);
    arrY.push_back(y2);

    curvCar->setData(arrX, arrY);
}
