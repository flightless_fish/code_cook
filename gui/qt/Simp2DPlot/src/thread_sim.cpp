
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <QtGui>
#include <QDateTime>

#include "thread_sim.h"
#include "gui_mainwindow.h"

#include "utils.h"


ThreadSim::ThreadSim(QObject *parent) : QThread(parent)
{
    isAlive = 1;

    qRegisterMetaType<QString>("QString");

    PlotWindow *pw = PlotWindow::instance();

    connect(this,   SIGNAL(replot()),               pw, SLOT(canvasReplot()));
    connect(this,   SIGNAL(showMessage(QString)),   pw, SLOT(canvasShowMessage(QString)));

    connect(pw,     SIGNAL(commandSend(int)),       this, SLOT(commandRecv(int)));
}

ThreadSim::~ThreadSim()
{
    wait();
}

void ThreadSim:: stop(void)
{
    isAlive = 0;
}

void ThreadSim::commandRecv(int cmd)
{

}

void ThreadSim::getCommand(int *cmd)
{

}

void ThreadSim::run()
{
    PlotWindow *pw = PlotWindow::instance();
    qint64 dt, tnow, tlast = QDateTime::currentMSecsSinceEpoch();
    qint64 refreshDT = 200;

    QVector<double>     pathX, pathY;

    double  x, y, theta, R, Rinc;
    double  v = 40.0;

    R       = 400;
    Rinc    = 2;
    theta   = 0;
    x       = R*cos(theta);
    y       = R*sin(theta);

    // set car size
    pw->setCarSize(100);

    // set car pos
    pw->setCarPos(x, y, theta+M_PI/2.0);

    // set path
    pathX.append(x);
    pathY.append(y);
    pw->setPath(pathX, pathY);

    // replot
    emit replot();

    while( isAlive )
    {
        tnow = QDateTime::currentMSecsSinceEpoch();
        dt = tnow - tlast;

        if( dt > refreshDT )
        {
            R += Rinc*dt*0.001;

            theta += dt*0.001*v / R;
            x = R*cos(theta);
            y = R*sin(theta);

            // set car's position
            pw->setCarPos(x, y, theta+M_PI/2.0);

            // set path
            pathX.append(x);
            pathY.append(y);
            pw->setPath(pathX, pathY);

            // replot
            emit replot();

            tlast = tnow;
        }

        this->msleep(50);
    }
}
