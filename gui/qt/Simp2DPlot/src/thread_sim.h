#ifndef __THREAD_SIM_H__
#define __THREAD_SIM_H__

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <QtGui>

class ThreadSim : public QThread
{
    Q_OBJECT

public:
    ThreadSim(QObject *parent = 0);
    ~ThreadSim();

    void stop(void);

    void getCommand(int *cmd);          // 1 - Forward
                                        // 2 - Backward
                                        // 3 - Turn Left
                                        // 4 - Turn Right

signals:
    void replot();
    void showMessage(QString msg);

public slots:
    virtual void commandRecv(int cmd);

protected:
    virtual void run();

    int         isAlive;                // is finished?
};

#endif // __THREAD_SIM_H__
