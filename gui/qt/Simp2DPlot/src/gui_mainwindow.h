#ifndef __PLOTWINDOW_H__
#define __PLOTWINDOW_H__

#include <memory>
#include <QtCore>
#include <QtGui>
#include <QMainWindow>
#include <QTimer>

#include "qcustomplot.h"


class PlotWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PlotWindow(QWidget *parent = 0);
    ~PlotWindow();

    void setupCanvas(void);
    void setupInitData(void);

    void setPath(QVector<double> &arrX, QVector<double> &arrY);

    void setCarPos(double x, double y, double t);
    void setCarSize(double s);
    void setCarModel(double *parm);

    void setPlotRange(double xmin, double xmax, double ymin, double ymax);

    void clear(void);
    void plot(void);

    void setScreenShot_fname(std::string &fnBase);

    void showMessage(QString &msg);

    static PlotWindow* instance(void)
    {
        static std::shared_ptr<PlotWindow> global_handle(new PlotWindow);
        return global_handle.get();
    }

protected slots:
    void canvsMousePressEvent(QMouseEvent *event);
    void canvasMouseMoveEvent(QMouseEvent *event);

    void canvasReplot(void);
    void canvasShowMessage(QString msg);

    void plotBegin(void);                   // slot for QCustomPlot begin plot
    void plotEnd(void);                     // slot for QCustomPlot end plot

signals:
    void commandSend(int cmd);


protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
    void timerEvent(QTimerEvent *event);

private:
    QCustomPlot         *customPlot;

    QVector<double>     arrWaypoint_x, arrWaypoint_y;
    QCPCurve            *curvWayPoint;


    double              parmCarModel[4];        // 0 - pos x
                                                // 1 - pos y
                                                // 2 - theta
                                                // 3 - size
    QCPCurve            *curvCar;


    QString             msgString1, msgString2;

    std::string         fnScreenShot_base;

    QMutex              *muxData;

private:
    void drawCar(void);
};

#endif // __PLOTWINDOW_H__
