#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <QApplication>


#include "gui_mainwindow.h"
#include "thread_sim.h"
#include "utils.h"
#include "ringbuffer.h"
#include "Svar.h"

using namespace std;
using namespace GSLAM;


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
    int ret = 0;

    // parse command line arguments
    svar.ParseMain(argc, argv);
    dbg_stacktrace_setup();

    // Begin QT application
    QApplication a(argc, argv);

    // simp plot window
    PlotWindow *win = PlotWindow::instance();
    win->show();
    win->setGeometry(10, 10, 950, 768);
    win->plot();

    // simulation thread
    ThreadSim *ts = new ThreadSim;
    ts->start();

    // enter Qt main loop
    ret = a.exec();

    ts->stop();
    return ret;
}
