#include "udp.h"
#include <QUdpSocket>

UDP::UDP(RingBuffer<uint8_t>* ringbuffTemp,QObject *parent){

    this->udpSocket = new QUdpSocket;
    ringbuff = ringbuffTemp;
    std::cout<<ringbuff<<std::endl;
    this->udpSocket->bind(QHostAddress("192.168.1.32"),6677); //绑定ip地址和端口号
    connect(this->udpSocket, SIGNAL(readyRead()),this,SLOT(recv()));
}

UDP::~UDP(){
    this->wait();
}

void UDP::run(){
    while(1){
        /*if(ringbuff->used())
            std::cout<<"ringbuff"<<ringbuff->used()<<std::endl;*/
       // connect(this->udpSocket, SIGNAL(readyRead()),this,SLOT(recv()));
    }
}

void UDP::recv()
{
    char buf[1000] = {1};
    QString s;
    QHostAddress sender=QHostAddress("192.168.1.32");
    quint16 port=6688;
    quint16 nread = this->udpSocket->readDatagram(buf,32,&sender,&port);
    if(ringbuff->left()>0)
        ringbuff->write((uint8_t *)buf,nread);
}
