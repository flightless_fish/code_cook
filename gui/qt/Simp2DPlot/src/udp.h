#ifndef UDP_H
#define UDP_H

#include <QtGui>
#include <QUdpSocket>
#include "ringbuffer.h"
#include <QString>

class UDP:public QThread{
    Q_OBJECT
public:
    UDP(RingBuffer<uint8_t>* ringbuffTemp, QObject *parent = 0);

    QUdpSocket *udpSocket;
    ~UDP();
protected:
    virtual void run();

private slots:
    void recv();
private:
    RingBuffer<uint8_t>* ringbuff;
    QString path_;
};


#endif // UDP_H
