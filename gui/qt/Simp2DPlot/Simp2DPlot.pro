TARGET   = Simp2DPlot

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt

UI_DIR       = ./build
MOC_DIR      = ./build
OBJECTS_DIR  = ./build


HEADERS += \
    ./src/qcustomplot.h \
    ./src/gui_mainwindow.h \
    ./src/thread_sim.h \
    ./src/utils.h \
    ./src/Svar.h \
    ./src/ringbuffer.h  \
    ./src/utils_GPS.h

SOURCES += \
    ./src/qcustomplot.cpp \
    ./src/gui_mainwindow.cpp \
    ./src/thread_sim.cpp \
    ./src/main.cpp \
    ./src/utils.cpp \
    ./src/utils_GPS.cpp

OTHER_FILES = README.md

QMAKE_CFLAGS += -g
