# SFML demos

Install dependencies on Ubuntu:
```
sudo apt-get install libsfml-dev
```

Compile the code:
```
g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system
```
