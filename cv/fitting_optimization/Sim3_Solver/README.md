# Demo of SIM3 estimation

This program demonstrate the useage of SIM3 estimation.

## Requirements
* OpenCV: `sudo apt-get install libopencv-dev`

## Build
```
mkdir build
cd build
cmake ..
make 
```

