
// Demo for fitting vision tracking and IMU/GPS trackings


#include <iostream>
#include <math.h>
#include <stdlib.h>

//#include <windows.h>

#include "libs/Sim3Solver.h"

using namespace std;

int demo_sim3fitting(void)
{
    std::vector<pi::Point3d> TrackPoints;
    std::vector<pi::Point3d> GPSPoints;
    pi::SIM3d sim3;

    // 1. generate simulation data
    pi::SIM3d sim3_idea;

    pi::Point3d& sim3_t = sim3_idea.get_translation();
    sim3_t.x = 100;
    sim3_t.y = 200;
    sim3_t.z = 100;

    pi::SO3d& sim3_r = sim3_idea.get_rotation();
    sim3_r.FromEulerAngle(20, 10, 10);
    sim3_idea.get_scale() = 2;

    TrackPoints.resize(3);
    GPSPoints.resize(TrackPoints.size());

    pi::Point3d p_last, p_noise;

    for(int i=0; i<TrackPoints.size(); i++) {
        p_last.x += 1000.0*(rand()%1000 - 500)/500.0;
        p_last.y += 1000.0*(rand()%1000 - 500)/500.0;
        p_last.z += 1000.0*(rand()%1000 - 500)/500.0;

        p_noise.x = 5.0 * (rand()%1000 - 500)/500.0;
        p_noise.y = 5.0 * (rand()%1000 - 500)/500.0;
        p_noise.z = 5.0 * (rand()%1000 - 500)/500.0;

        if( 1 ) {
            TrackPoints[i] = p_last;
            GPSPoints[i]   = sim3_idea*p_last + p_noise;
            pi::Point3d& gp = GPSPoints[i];
            printf("TrackPoints[%2d] = %f %f %f\n", i, p_last.x, p_last.y, p_last.z);
            printf("GPSPoints[%2d]   = %f %f %f\n", i, gp.x, gp.y, gp.z);
        } else {
            GPSPoints[i] = p_last;
            TrackPoints[i] = sim3_idea*p_last;
            pi::Point3d& tp = TrackPoints[i];
            printf("GPSPoints[%2d]   = %f %f %f\n", i, p_last.x, p_last.y, p_last.z);
            printf("TrackPoints[%2d] = %f %f %f\n", i, tp.x, tp.y, tp.z);
        }
    }

    // 2. call sim3 solver (sim3: TrackPoints -> GPSPoints)
    Sim3Solver::getSim3Fast(TrackPoints, GPSPoints, sim3);


    cout << "sim3_idea: \n   rot=" << sim3_idea.get_rotation() << "\n   tr=" << sim3_idea.get_translation() << "\n   s=" << sim3_idea.get_scale() << "\n";
    cout << "sim3_est : \n   rot=" << sim3.get_rotation() << "\n   tr=" << sim3.get_translation() << "\n   s=" << sim3.get_scale() << "\n";

    // 3. apply sim3 to TrackPoints to GPSPoints
    std::vector<pi::Point3d> FitPoints(TrackPoints.size());
    pi::Point3d p_err;

    for(int i=0; i<TrackPoints.size(); i++) {
        FitPoints[i] = sim3*TrackPoints[i];
        p_err = p_err + GPSPoints[i] - FitPoints[i];

        pi::Point3d& fp = FitPoints[i];
        pi::Point3d& gp = GPSPoints[i];
        printf("FitPoints[%2d] = %f %f %f\n", i, fp.x, fp.y, fp.z);
        printf("GPSPoints[%2d] = %f %f %f\n", i, gp.x, gp.y, gp.z);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    demo_sim3fitting();

    return 0;
}
