#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;
using namespace cv;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int gaussian_blur()
{
    string  fn_in;
    Mat     img1, img2, img3;

    int     ksize;
    double  sigmaX, sigmaY;

    // get parameters
    fn_in = svar.GetString("fn", "../data/blocking_artificts.png");

    ksize = svar.GetInt("ksize", 3);
    sigmaX = svar.GetInt("sigmaX", 2);
    sigmaY = svar.GetInt("sigmaY", 2);

    // load image
    img1 = imread(fn_in);

    // test gaussian blur
    GaussianBlur(img1, img2, Size(ksize, ksize), sigmaX, sigmaY);

    // test bilatera filter
    bilateralFilter(img1, img3, 3, 3, 3);

    imshow("img ori", img1);
    imshow("gs", img2);
    imshow("bf", img3);

    cvvWaitKey(0);


    return 0;
}


int image_level()
{
    string fn = svar.GetString("fn", "../data/test.png");

    Mat img = imread(fn, CV_LOAD_IMAGE_ANYDEPTH);

    double min, max;
    cv::Point minLoc, maxLoc;
    cv::minMaxLoc(img, &min, &max, &minLoc, &maxLoc);

    cout << "image file: " << fn << "\n";
    cout << "min, max = " << min << ", " << max << "\n";

    return 0;
}

int images_level()
{
    string fn = svar.GetString("fn", "../data/test.png");
    string fnOut = svar.GetString("fnOut", "../data/test_out.png");

    StringArray fileList;
    path_lsdir(fn, fileList);

    double gmin, gmax;
    gmin = 65536; gmax = 0;

    for(auto f: fileList)
    {
        string fn_img = path_join(fn, f);

        Mat img;
        double min, max;
        cv::Point minLoc, maxLoc;

        // read image & detect scale
        img = imread(fn_img, CV_LOAD_IMAGE_ANYDEPTH);
        cv::minMaxLoc(img, &min, &max, &minLoc, &maxLoc);

        cout << f << "," << min << "," << max << "," << max-min << "\n";

        if( min < gmin ) gmin = min;
        if( max > gmax ) gmax = max;


        // convert scale
        Mat imgOut;
        imgOut.create(img.rows, img.cols, CV_8UC1);
        uint16_t *pSrc = (uint16_t*) img.data;
        uint8_t *pDst = imgOut.data;

        for(int i=0; i<img.rows*img.cols; i++)
        {
            pDst[i] = (pSrc[i] - min) * 255 / (max - min);
        }

        string fnOutPath = path_join(fnOut, f);
        imwrite(fnOutPath, imgOut);
    }

    cout << gmin << "," << gmax << "," << gmax-gmin << "\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "gaussian_blur");

    DEF_TEST(gaussian_blur);

    DEF_TEST(image_level);
    DEF_TEST(images_level);
}
