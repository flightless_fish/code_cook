#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;
using namespace cv;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_init()
{
    // 3x3 unit array (= cv::Mat::eye(3,3, CV_64F) )
    cv::Mat m = (cv::Mat_<double>(3,3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);
    // 2x2 rotate matrix
    double angle = 30, a = std::cos(angle*CV_PI/180), b = std::sin(angle*CV_PI/180);
    cv::Mat r = (cv::Mat_<double>(2,2) << a, -b, b, a);

    std::cout << "m=" << m << std::endl << std::endl;
    std::cout << "r=" << r << std::endl;

    return 0;
}

int mat_init_channel_depth()
{
    cv::Mat m1(3, 3, CV_64FC(10));                  // 3x3 double (10-channel)
    cv::Mat m2(3, 3, CV_MAKETYPE(CV_64F, 10));      // 3x3 double (10-channel)

    CV_Assert(m1.type() == m2.type() );
    std::cout << "mat1/mat2\n";
    std::cout << "   dims: " << m1.dims
              << ", depth(byte/channel): " << m1.elemSize()
              << ", channels :" << m1.channels()
              << std::endl;


    // 64F, channels=1, 2x2x3x4 array
    const int sizes[] = {2, 2, 3, 4};
    cv::Mat m3(sizeof(sizes)/sizeof(int), sizes, CV_64FC1);

    CV_Assert(m3.dims == sizeof(sizes)/sizeof(sizes[0]));
    CV_Assert(m3.rows == -1);
    CV_Assert(m3.cols == -1);

    std::cout << "mat3\n";
    std::cout << "   dims: " << m3.dims
              << ", depth(byte/channel): " << m3.elemSize()
              << ", channels: " << m3.channels()
              << std::endl;

    return 0;
}

int mat_init_c_array()
{
    float *data = new float[9] ;
    for(int i=0; i<9; ++i)  data[i] = i+1;

    {
        // init mat by user specified data
        //      it is necessary to release data array by user
        cv::Mat m1(3, 3, CV_32F, data);

        // reference count didn't work
        CV_Assert(m1.refcount == NULL);

        std::cout << "m1=" << m1 << std::endl;
    }

    // release user data
    delete[] data;

    return 0;
}

int mat_init_const()
{
    cv::Mat m1(3, 3, CV_32F, cv::Scalar(5));
    std::cout << "m1=" << m1 << std::endl;

    cv::Mat m2(3, 3, CV_32F);
    m2 = cv::Scalar(5);
    std::cout << "m2=" << m2 << std::endl;

    cv::Mat m3 = cv::Mat::ones(3, 3, CV_32F)*5;
    std::cout << "m3=" << m3 << std::endl;

    cv::Mat m4 = cv::Mat::zeros(3, 3, CV_32F)+5;
    std::cout << "m4=" << m4 << std::endl;

    return 0;
}

int mat_init_matlab()
{
    // 要素がすべて 3 の 5x5 行列
    cv::Mat mat1 = cv::Mat::ones(5, 5, CV_8U)*3;
    // 要素がすべて 0 の 5x5 行列
    cv::Mat mat2 = cv::Mat::zeros(5, 5, CV_8U);
    // 5x5 の単位行列
    cv::Mat mat3 = cv::Mat::eye(5, 5, CV_8U)*2;

    std::cout << "mat1=" << mat1 << std::endl << std::endl;
    std::cout << "mat2=" << mat2 << std::endl << std::endl;
    std::cout << "mat3=" << mat3 << std::endl;

    return 0;
}

int mat_init_rand()
{
    cv::Mat mat(3, 2, CV_8UC1);

    // 一様分布乱数，[0,256)
    cv::randu(mat, cv::Scalar(0), cv::Scalar(256));
    std::cout << mat << std::endl << std::endl;

    // 正規分布乱数，mean=128, stddev=10
    cv::randn(mat, cv::Scalar(128), cv::Scalar(10));
    std::cout << mat << std::endl << std::endl;

    // RNG初期化
    cv::RNG gen(cv::getTickCount());

    // 一様分布乱数，[0,256)
    gen.fill(mat, cv::RNG::UNIFORM, cv::Scalar(0), cv::Scalar(256));
    std::cout << mat << std::endl << std::endl;

    // 正規分布乱数，mean=128, stddev=10
    gen.fill(mat, cv::RNG::NORMAL, cv::Scalar(128), cv::Scalar(10));
    std::cout << mat << std::endl << std::endl;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_output()
{
    cv::Mat m1 = cv::Mat::eye(3,3,CV_32FC1);
    cv::Mat m2 = (cv::Mat_<cv::Vec2i>(2, 2) << cv::Vec2i(1,1), cv::Vec2i(2, 4), cv::Vec2i(3, 9), cv::Vec2i(4, 16));
    cv::Point_<int> p1(1,2);
    cv::Point3_<double> p2(1.5, 2.2, 3.9);

    // cv::Mat
    std::cout << "m1=" << m1 << std::endl << std::endl;
    // cv::Mat (multi-channel)
    std::cout << "m2=" << m2 << std::endl << std::endl;
    // cv::Point_, cv::Point3_
    std::cout << "p1=" << p1 << std::endl << std::endl;
    std::cout << "p2=" << p2 << std::endl << std::endl;
    // cv::MatExpr
    std::cout << "m1+m1+1=" << m1+m1+1 << std::endl;

    return 0;
}

int mat_output_fmt()
{
    cv::Mat m(4, 2, CV_8UC3);
    cv::randu(m, cv::Scalar::all(0), cv::Scalar::all(255));

    std::cout << "m (default) = " << m << ";" << std::endl << std::endl;
    std::cout << "m (python) = " << cv::format(m,"python") << ";" << std::endl << std::endl;
    std::cout << "m (numpy) = " << cv::format(m,"numpy") << ";" << std::endl << std::endl;
    std::cout << "m (csv) = " << cv::format(m,"csv") << ";" << std::endl << std::endl;
    std::cout << "m (c) = " << cv::format(m,"C") << ";" << std::endl << std::endl;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_properties()
{
    cv::Mat m1(3, 4, CV_64FC1);

    // rows
    std::cout << "rows:" << m1.rows <<std::endl;
    // cols
    std::cout << "cols:" << m1.cols << std::endl;
    // dimension
    std::cout << "dims:" << m1.dims << std::endl;
    // size (2-dimension)
    std::cout << "size[]:" << m1.size().width << "," << m1.size().height << std::endl;
    // depth (byte per element)
    std::cout << "depth (ID):" << m1.depth() << "(=" << CV_64F << ")" << std::endl;
    // channels
    std::cout << "channels:" << m1.channels() << std::endl;
    // element size (all channels) [byte]
    std::cout << "elemSize:" << m1.elemSize() << "[byte]" << std::endl;
    // element size (1 channel) [byte]
    std::cout << "elemSize1 (elemSize/channels):" << m1.elemSize1() << "[byte]" << std::endl;
    // total element number
    std::cout << "total:" << m1.total() << std::endl;
    // step (byte per row) [byte]
    std::cout << "step:" << m1.step << "[byte]" << std::endl;
    // step (element per row)
    std::cout << "step1 (step/elemSize1):" << m1.step1()  << std::endl;
    // data storage is continous ?
    std::cout << "isContinuous:" << (m1.isContinuous()?"true":"false") << std::endl;
    // is sub-matrix
    std::cout << "isSubmatrix:" << (m1.isSubmatrix()?"true":"false") << std::endl;
    // is empty
    std::cout << "empty:" << (m1.empty()?"true":"false") << std::endl;

    return 0;
}

int mat_properties_submatrix()
{
    cv::Mat m1(4, 5, CV_32FC(5));       // 5x4 x5
    cv::Rect roi_rect(0, 0, 3, 4);      // ROI 4x3
    cv::Mat r1(m1, roi_rect);

    // rows
    std::cout << "rows: " << r1.rows << std::endl;
    // cols
    std::cout << "cols: " << r1.cols << std::endl;
    // dimension
    std::cout << "dims: " << r1.dims << std::endl;
    // size (2-dimension case)
    std::cout << "size[]: " << r1.size().width << ", " << r1.size().height << std::endl;
    // channels
    std::cout << "channels: " << r1.channels() << std::endl;
    // element size (all channels) [byte]
    std::cout << "elemSize:" << r1.elemSize() << "[byte]" << std::endl;
    // element size (1 channel) [byte]
    std::cout << "elemSize1 (elemSize/channels):" << r1.elemSize1() << "[byte]" << std::endl;
    // total element number
    std::cout << "total:" << r1.total() << std::endl;
    // step (byte per row) [byte]
    std::cout << "step:" << r1.step << "[byte]" << std::endl;
    // step (element per row)
    std::cout << "step1 (step/elemSize1):" << r1.step1()  << std::endl;
    // data storage is continous ?
    std::cout << "isContinuous:" << (r1.isContinuous()?"true":"false") << std::endl;
    // is sub-matrix
    std::cout << "isSubmatrix:" << (r1.isSubmatrix()?"true":"false") << std::endl;
    // is empty
    std::cout << "empty:" << (r1.empty()?"true":"false") << std::endl;

    return 0;
}

int mat_properties_5dim()
{
    // 32S, channles=2, 2x3x3x4x6 array
    const int sizes[] = {2, 3, 3, 4, 6};
    cv::Mat m1(sizeof(sizes)/sizeof(int), sizes, CV_32SC2);

    // rows
    std::cout << "rows: " << m1.rows << std::endl;
    // cols
    std::cout << "cols: " << m1.cols << std::endl;
    // dimension
    std::cout << "dims: " << m1.dims << std::endl;
    // size (2-dimension case)
    std::cout << "size[]: ";
    for(int i=0; i<m1.dims; i++) std::cout << m1.size[i] << ", ";
    std::cout << std::endl;
    // depth
    std::cout << "depth (ID): " << m1.depth() << "(=" << CV_32S << ")" << std::endl;
    // channels
    std::cout << "channels: " << m1.channels() << std::endl;
    // element size (all channels) [byte]
    std::cout << "elemSize:" << m1.elemSize() << "[byte]" << std::endl;
    // element size (1 channel) [byte]
    std::cout << "elemSize1 (elemSize/channels):" << m1.elemSize1() << "[byte]" << std::endl;
    // total element number
    std::cout << "total:" << m1.total() << std::endl;
    // step (byte per row) [byte]
    std::cout << "step[] ";
    for(int i=0; i<m1.dims; i++) std::cout << m1.step[i] << ", ";
    std::cout << " [byte]" << std::endl;
    // step (element per row)
    std::cout << "step1 (step/elemSize1): " << m1.step1()  << std::endl;
    // data storage is continous ?
    std::cout << "isContinuous: " << (m1.isContinuous()?"true":"false") << std::endl;
    // is sub-matrix
    std::cout << "isSubmatrix: " << (m1.isSubmatrix()?"true":"false") << std::endl;
    // is empty
    std::cout << "empty: " << (m1.empty()?"true":"false") << std::endl;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_copy()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3,3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);

    // shallow copy
    cv::Mat m_shallow = m1;
    // deep copy (clone and copyTo)
    cv::Mat m_deep1 = m1.clone();
    cv::Mat m_deep2;
    m1.copyTo(m_deep2);

    std::cout << "m1=" << m1 << std::endl << std::endl;
    std::cout << "m_shallow=" << m_shallow << std::endl << std::endl;
    std::cout << "m_deep1=" << m_deep1 << std::endl << std::endl;
    std::cout << "m_deep2=" << m_deep2 << std::endl << std::endl;

    // m1 element at (0,0) is set to a value
    m1.at<double>(0,0) = 100;

    std::cout << "m1=" << m1 << std::endl << std::endl;
    std::cout << "m_shallow=" << m_shallow << std::endl << std::endl;
    std::cout << "m_deep1=" << m_deep1 << std::endl << std::endl;
    std::cout << "m_deep2=" << m_deep2 << std::endl << std::endl;

    return 0;
}

int mat_assign()
{
    // 1000x1000 x8 = 8(Mb) matrix create
    cv::Mat A(1000, 100, CV_64F);

    // create matrix B reference to A, share same data
    cv::Mat B = A;

    // create matrix C reference to B, only the 3-rd rows
    cv::Mat C = B.row(3);

    // create matrix D, create new data array
    cv::Mat D = B.clone();

    // copy B's 5 row to C
    B.row(5).copyTo(C);

    // set A reference to D
    // After this, modified A data reference to B & C
    A = D;

    // release B matrix, actuall original A's data
    B.release();

    // finally, complete copy C's data
    C = C.clone();

    return 0;
}

int mat_refcount()
{
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    int *refcount = m1.refcount;

    // the reference count is 1 when m1 is create
    std::cout << "ref_count=" << *(refcount) << std::endl;

    {
        cv::Mat m2 = m1;

        // m2 reference to m1, therefore, ref_count will be 2
        std::cout << "ref_count=" << *(refcount) << std::endl;
    }

    // after the {}, the reference count will back to 1
    std::cout << "ref_count=" << *(refcount) << std::endl;

    // clone m1->m3, and assign m1=m3, then ref_count will be 0
    cv::Mat m3 = m1.clone();
    m1 = m3;

    std::cout << "ref_count=" << *(refcount) << std::endl;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_convert()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 3.1, 3.2, 3.3);
    std::cout << "m1(CV_64FC1)" << std::endl << m1 << "\n\n";

    cv::Mat m2, m3;
    m1.convertTo(m2, CV_8U);
    std::cout << "m2(CV_8UC1)" << std::endl << m2 << "\n\n";

    m1.convertTo(m3, CV_8U, 2, 10);
    std::cout << "m3(CV_8UC1)" << std::endl << m3 << "\n\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_resize()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);

    std::cout << "m1 = " << m1 << "\n\n";

    // 2x3 matrix
    m1.resize(2);
    std::cout << "m1 = " << m1 << "\n\n";

    // 4x3 matrix
    m1.resize(4);
    std::cout << "m1 = " << m1 << "\n\n";

    // 5x3 matrix
    m1.resize(5, cv::Scalar(100));
    std::cout << "m1 = " << m1 << "\n\n";

    return 0;
}

int mat_reshape()
{
    // 3x4 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 4) << 1, 2, 3, 4, 6, 7, 8, 9, 11, 12, 13, 14);

    // 3x2 (2-channel) matrix
    cv::Mat m2 = m1.reshape(2);

    // 2x6 (1-channel) matrix
    cv::Mat m3 = m1.reshape(1, 2);

    std::cout << "m1 = " << m1 << "\nch = " << m1.channels() << "\n\n";
    std::cout << "m2 = " << m2 << "\nch = " << m2.channels() << "\n\n";
    std::cout << "m3 = " << m3 << "\nch = " << m3.channels() << "\n\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_extract_row_col()
{
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);

    // all rows
    std::cout << m1.rowRange(cv::Range::all()) << "\n\n";
    // [0, 2) rows
    std::cout << m1.rowRange(cv::Range(0, 2)) << "\n\n";
    // 0-th row
    std::cout << m1.row(0) << "\n\n";

    // all cols
    std::cout << m1.colRange(cv::Range::all()) << "\n\n";
    // [0, 2) rows
    std::cout << m1.colRange(cv::Range(0, 2)) << "\n\n";
    // 0-th row
    std::cout << m1.col(0) << "\n\n";

    return 0;
}

int mat_submatrix()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    std::cout << m1 << "\n\n";

    // rows [0, 2) cols [0, 2) submatrix
    std::cout << m1(cv::Range(0, 2), cv::Range(0, 2)) << "\n\n";
    // rows [0, 2) cols [0, 2) submatrix
    std::cout << m1(cv::Rect(0, 0, 2, 2)) << "\n\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_channel_merge()
{
    cv::Mat m1 = (cv::Mat_<double>(2, 2) << 1.0, 2.0, 3.0, 4.0);
    cv::Mat m2 = (cv::Mat_<double>(2, 2) << 1.1, 2.1, 3.1, 4.1);
    cv::Mat m3 = (cv::Mat_<double>(2, 2) << 1.2, 2.2, 3.2, 4.2);

    std::vector<cv::Mat> mv;
    mv.push_back(m1);
    mv.push_back(m2);
    mv.push_back(m3);

    cv::Mat m_merged;
    cv::merge(mv, m_merged);

    std::cout << "m_merged = " << m_merged << "\n\n";

    return 0;
}

int mat_channel_split()
{
    cv::Mat m0 = (cv::Mat_<int>(3, 6) << 1, 2, 3, 4, 5, 6,
                                         7, 8, 9, 10, 11, 12,
                                         13, 14, 15, 16, 17, 18);

    // channels=3, 3x2 matrix
    cv::Mat m1 = m0.reshape(3, 2);
    std::cout << "m1\n" << m1 << "\n\n";

    std::vector<cv::Mat> planes;

    // split each channel
    cv::split(m1, planes);
    std::vector<cv::Mat>::const_iterator it = planes.begin();
    for(; it != planes.end(); it++) {
        std::cout << *it << "\n\n";
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_reduce()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 5, 3, 4, 2, 6, 7, 8, 9);
    cv::Mat v1, v2, v3, v4;

    // 3x3 matrix -> 1 row
    cv::reduce(m1, v1, 0, CV_REDUCE_SUM);       // each col's sum
    cv::reduce(m1, v2, 0, CV_REDUCE_AVG);       // each col's average
    cv::reduce(m1, v3, 0, CV_REDUCE_MIN);       // each col's minimum
    cv::reduce(m1, v4, 0, CV_REDUCE_MAX);       // each col's maximum

    std::cout << "m1=" << m1 << "\n\n";
    std::cout << "v1(sum) = " << v1 << "\n";
    std::cout << "v2(avg) = " << v2 << "\n";
    std::cout << "v3(min) = " << v3 << "\n";
    std::cout << "v4(max) = " << v4 << "\n";

    cv::Mat_<int> m = v1;
    for(int i=0; i<3; i++)
        printf("%d\n", m(i));

    // 3x3 matrix -> 1 col
    cv::reduce(m1, v1, 1, CV_REDUCE_SUM);
    cv::reduce(m1, v2, 1, CV_REDUCE_AVG);
    cv::reduce(m1, v3, 1, CV_REDUCE_MIN);
    cv::reduce(m1, v4, 1, CV_REDUCE_MAX);

    std::cout << "\n";
    std::cout << "v1(sum) = " << v1 << "\n";
    std::cout << "v2(avg) = " << v2 << "\n";
    std::cout << "v3(min) = " << v3 << "\n";
    std::cout << "v4(max) = " << v4 << "\n";



    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_locateROI()
{
    // 3x3 matrix
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    std::cout << "m1\n" << m1 << "\n\n";

    // row [0, 2), cols [0, 2) sub-matrix
    cv::Mat m2 = m1(cv::Rect(0, 0, 2, 2));
    std::cout << "m2\n" << m2 << "\n\n";

    cv::Size wholeSize;
    cv::Point ofs;
    m2.locateROI(wholeSize, ofs);
    std::cout << "wholeSize: " << wholeSize.height << " x " << wholeSize.width << "\n";
    std::cout << "offset:    " << ofs.x << ", " << ofs.y << "\n\n";

    // adjustROI
    m2.adjustROI(0, 1, 0, 1);
    std::cout << "m2\n" << m2 << "\n\n";
    std::cout << "rows = " << m2.rows << ", cols = " << m2.cols << "\n";


    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_randShuffle()
{
    // 4x5 matrix
    cv::Mat m1 = (cv::Mat_<double>(4, 5) << 1, 2, 3, 4, 5,
                                            6, 7, 8, 9, 10,
                                            11, 12, 13, 14, 15,
                                            16, 17, 18, 19, 20);
    std::cout << "m1(original)\n" << m1 << "\n\n";

    cv::RNG gen(cv::getTickCount());
    // row/col shuffle
    cv::randShuffle(m1, 2.0, &gen);
    std::cout << "m1(shuffle)\n" << m1 << "\n\n";

    // sub-matrix shuffle
    cv::Mat m2 = m1(cv::Rect(1, 1, 3, 2));
    std::cout << "m2(sub-matrix)\n" << m2 << "\n\n";
    cv::randShuffle(m2, 2.0, &gen);
    std::cout << "m2(shuffle sub-matrix)\n" << m2 << "\n\n";

    std::cout << "m1\n" << m1 << "\n\n";

    return 0;
}

int mat_swap()
{
    cv::Mat eye  = cv::Mat::eye (3, 3, CV_8UC1);
    cv::Mat ones = cv::Mat::ones(4, 4, CV_8UC1);

    std::cout << "eye  = " << eye << "\n";
    std::cout << "ones = " << ones << "\n";

    cv::swap(eye, ones);

    std::cout << "eye  = " << eye << "\n";
    std::cout << "ones = " << ones << "\n";

    return 0;
}

int mat_sort()
{
    cv::Mat mat(5, 5, CV_8UC1);

    // uniform rand [0, 255)
    cv::randu(mat, cv::Scalar(0), cv::Scalar(25));
    std::cout << mat << "\n\n";

    cv::Mat dst_mat;

    // each row sort (ascending)
    cv::sort(mat, dst_mat, CV_SORT_EVERY_ROW | CV_SORT_ASCENDING);
    std::cout << "ROW|ASCENDING\n" << dst_mat << "\n";
    cv::sortIdx(mat, dst_mat, CV_SORT_EVERY_ROW | CV_SORT_ASCENDING);
    std::cout << "index\n" << dst_mat << "\n\n";

    // each row sort (ascending)
    cv::sort(mat, dst_mat, CV_SORT_EVERY_ROW | CV_SORT_DESCENDING);
    std::cout << "ROW|DESCENDING\n" << dst_mat << "\n";
    cv::sortIdx(mat, dst_mat, CV_SORT_EVERY_ROW | CV_SORT_DESCENDING);
    std::cout << "index\n" << dst_mat << "\n\n";


    // each col sort (ascending)
    cv::sort(mat, dst_mat, CV_SORT_EVERY_COLUMN | CV_SORT_ASCENDING);
    std::cout << "COL|ASCENDING\n" << dst_mat << "\n";
    cv::sortIdx(mat, dst_mat, CV_SORT_EVERY_COLUMN | CV_SORT_ASCENDING);
    std::cout << "index\n" << dst_mat << "\n\n";

    // each col sort (ascending)
    cv::sort(mat, dst_mat, CV_SORT_EVERY_COLUMN | CV_SORT_DESCENDING);
    std::cout << "COL|DESCENDING\n" << dst_mat << "\n";
    cv::sortIdx(mat, dst_mat, CV_SORT_EVERY_COLUMN | CV_SORT_DESCENDING);
    std::cout << "index\n" << dst_mat << "\n\n";

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int mat_access()
{
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);
    cv::Mat_<double> m2 = m1.clone();
    cv::Mat_<double> m3 = m1;

    // direct access
    std::cout << "m1(1, 1) = " << m1.at<double>(1, 1) << "\n";
    std::cout << "m2(1, 1) = " << m2(1, 1) << "\n";
    std::cout << "m3(1, 1) = " << m3(1, 1) << "\n";

    // pointer access
    std::cout << "m1(1, 2) = " << m1.ptr<double>(1)[2] << "\n";
    std::cout << "m2[1, 2) = " << m2[1][2] << "\n";
    std::cout << "m3(1, 2) = " << m3[1][2] << "\n";

    return 0;
}

int matx_usage()
{
    // float 1x2 matrix
    cv::Matx21f m1(1, 2);
    // double 3x3 matrix
    cv::Matx33d m2(1, 3, 5, 2, 4, 6, 7, 8, 9);
    // double 5x1 matrix
    cv::Matx<double, 5, 1> m3(10, 11, 12, 13, 14);

    // float 2x11 matrix
    cv::Matx<float, 2, 11> m4;

    for(int j=0; j<2; j++)
        for(int i=0; i<11; i++)
            m4(j, i) = i+j;

    // convert to Mat, output
    std::cout << "m1 = " << cv::Mat(m1) << "\n\n";
    std::cout << "m2 = " << cv::Mat(m2) << "\n\n";
    std::cout << "m3 = " << cv::Mat(m3) << "\n\n";
    std::cout << "m4 = " << cv::Mat(m4) << "\n\n";

    // Matx -> Mat
    cv::Mat mat1(m2, false);        // share data
    cv::Mat mat2(m2);               // copy data

    // Mat -> Matx
    cv::Matx33d mx1 = m2;           // share data

    std::cout << "mat1 = " << mat1 << "\n\n";
    std::cout << "mat2 = " << mat2 << "\n\n";
    std::cout << "mx1  = " << cv::Mat(mx1) << "\n\n";

    return 0;
}

int vec_usage()
{
    // 300x300 3-channel color image
    cv::Mat_<cv::Vec3b> img(300, 300, cv::Vec3b(0, 200, 0));

    // draw white line at diagnol
    for(int i=0; i<img.cols; i++)
        img(i, i) = cv::Vec3b(255, 255, 255);

    cv::namedWindow("image", CV_WINDOW_AUTOSIZE|CV_WINDOW_FREERATIO);
    cv::imshow("image", img);
    cv::waitKey(0);

    return 0;
}

int mat_stl_vector()
{
    cv::Mat m1 = (cv::Mat_<double>(3, 3) << 1, 2, 0, 3, 1, 4, 6, 7, 8);
    std::vector<double> v1;

    // Mat -> stl vector
    m1 = m1.reshape(0, 1); // change to 1-row matrix
    m1.copyTo(v1);

    std::cout << "m1 = " << m1 << "\n\n";
    for(std::vector<double>::iterator it=v1.begin(); it!=v1.end(); it++)
        std::cout << *it << ", ";
    std::cout << "\n";

    // vector -> Mat
    cv::Mat m2(v1);         // share data
    cv::Mat m3(v1, true);   // copy data

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "mat_init");

    DEF_TEST(mat_init);
    DEF_TEST(mat_init_channel_depth);
    DEF_TEST(mat_init_c_array);
    DEF_TEST(mat_init_const);
    DEF_TEST(mat_init_matlab);
    DEF_TEST(mat_init_rand);

    DEF_TEST(mat_output);
    DEF_TEST(mat_output_fmt);

    DEF_TEST(mat_properties);
    DEF_TEST(mat_properties_submatrix);
    DEF_TEST(mat_properties_5dim);

    DEF_TEST(mat_copy);
    DEF_TEST(mat_assign);
    DEF_TEST(mat_refcount);

    DEF_TEST(mat_convert);
    DEF_TEST(mat_resize);
    DEF_TEST(mat_reshape);

    DEF_TEST(mat_extract_row_col);
    DEF_TEST(mat_submatrix);

    DEF_TEST(mat_channel_merge);
    DEF_TEST(mat_channel_split);

    DEF_TEST(mat_reduce);

    DEF_TEST(mat_locateROI);

    DEF_TEST(mat_randShuffle);
    DEF_TEST(mat_swap);
    DEF_TEST(mat_sort);

    DEF_TEST(mat_access);
    DEF_TEST(matx_usage);
    DEF_TEST(vec_usage);
    DEF_TEST(mat_stl_vector);
}
