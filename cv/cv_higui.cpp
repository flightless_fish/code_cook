#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "GSLAM/core/Svar.h"
#include "GSLAM/core/Glog.h"

#include "utils.h"

using namespace std;
using namespace cv;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int draw_text()
{
    string              fname;
    string              text;
    Mat                 img1;

    int                 x, y;
    int                 r, g, b;
    int                 fontFace, fontScale, thickness, lineType;

    // get parameters
    fname = svar.GetString("fn", "../data/lena.jpg");
    text = svar.GetString("text", "Hello world!");

    x = svar.GetInt("x", 100);
    y = svar.GetInt("y", 100);
    r = svar.GetInt("r", 255);
    g = svar.GetInt("g", 0);
    b = svar.GetInt("b", 0);

    fontFace = svar.GetInt("fontFace", FONT_HERSHEY_PLAIN);
    fontScale = svar.GetInt("fontScale", 2);
    thickness = svar.GetInt("thickness", 1);
    lineType = svar.GetInt("lineType", 1);

    // load image
    img1 = imread(fname);

    // draw text
    putText(img1, text, Point(x, y),
            fontFace, fontScale,
            Scalar(b, g, r),
            thickness, lineType);

    imshow("draw text", img1);
    cvvWaitKey(0);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int video_read()
{
    string              fname;
    VideoCapture        vcap;
    Mat                 img0;

    int                 key;
    int                 i;

    // get video filename
    fname = svar.GetString("fv", "../data/video_01.3gp");

    // open video stream
    if( !vcap.open(fname) ) {
        LOG(ERROR) << "can not open video file: " << fname;
        return -1;
    }

    // play video
    i = 0;
    while(1) {
        vcap >> img0;
        if( img0.empty() ) break;

        imshow("video", img0);

        key = cvWaitKey(10);
        if( key == 27 ) break;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int print_camera_settings(VideoCapture &cap)
{
    printf("==================== Camera Settings ===========================\n");
    printf("FrameWidth  = %f\n", cap.get(CV_CAP_PROP_FRAME_WIDTH));
    printf("FrameHeight = %f\n", cap.get(CV_CAP_PROP_FRAME_HEIGHT));
    printf("FPS         = %f\n", cap.get(CV_CAP_PROP_FPS));
    printf("Format      = %f\n", cap.get(CV_CAP_PROP_FORMAT));
    printf("Brightness  = %f\n", cap.get(CV_CAP_PROP_BRIGHTNESS));
    printf("Contrast    = %f\n", cap.get(CV_CAP_PROP_CONTRAST));
    printf("Saturation  = %f\n", cap.get(CV_CAP_PROP_SATURATION));
    printf("Hue         = %f\n", cap.get(CV_CAP_PROP_HUE));
    printf("Gain        = %f\n", cap.get(CV_CAP_PROP_GAIN));
    printf("Exposure    = %f\n", cap.get(CV_CAP_PROP_EXPOSURE));
    //printf("WB          = %f\n", cap.get(CV_CAP_PROP_WHITE_BALANCE));
    printf("================================================================\n");

}

int video_capture()
{
    int                 debug = 0;

    int                 vcap_dev;
    int                 vw, vh;
    int                 fps;

    VideoCapture        vcap;
    Mat                 img0;

    int                 key;
    int                 i;

    double              ts;

    uint64_t            t1, t2, t3;

    // get configures
    debug = svar.GetInt("debug", 0);

    vcap_dev = svar.GetInt("dev", 0);
    vw = svar.GetInt("vw", 1280);
    vh = svar.GetInt("vh", 720);
    fps = svar.GetInt("fps", 30);

    // open video stream
    if( !vcap.open(vcap_dev) ) {
        LOG(ERROR) << "can not open camera device: " << vcap_dev;
        return -1;
    }

    print_camera_settings(vcap);

    // set parameters
    vcap.set(CV_CAP_PROP_FRAME_WIDTH,  vw);
    vcap.set(CV_CAP_PROP_FRAME_HEIGHT, vh);
    vcap.set(CV_CAP_PROP_FPS, fps);

    print_camera_settings(vcap);


    // play video
    i = 0;
    while(1) {
        t1 = tm_get_millis();
        vcap.grab();
        t2 = tm_get_millis();

        vcap.retrieve(img0);

        ts = vcap.get(CV_CAP_PROP_POS_MSEC);

        t3 = tm_get_millis();

        if( debug )
            printf("t1 = %3d, t2 = %3d, t_all = %3d, ts = %f\n",
                   t2-t1, t3-t2, t3-t1,
                   ts);


        if( img0.empty() ) break;

        imshow("video", img0);

        key = cvWaitKey(10);
        if( key == 27 ) break;

        i++;
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "draw_text");

    DEF_TEST(draw_text);

    DEF_TEST(video_read);
    DEF_TEST(video_capture);
}
