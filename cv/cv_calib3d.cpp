#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;
using namespace cv;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

double computeReprojectionErrors(
        const vector<vector<Point3f> >& objectPoints,
        const vector<vector<Point2f> >& imagePoints,
        const vector<Mat>& rvecs, const vector<Mat>& tvecs,
        const Mat& cameraMatrix, const Mat& distCoeffs,
        vector<float>& perViewErrors )
{
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); i++ )
    {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float)std::sqrt(err*err/n);
        totalErr += err*err;
        totalPoints += n;
    }

    return std::sqrt(totalErr/totalPoints);
}

void calcChessboardCorners(Size boardSize, float squareSize,
                           vector<Point3f>& corners)
{
    corners.resize(0);

    for( int i = 0; i < boardSize.height; i++ )
        for( int j = 0; j < boardSize.width; j++ )
            corners.push_back(Point3f(float(j*squareSize),
                                      float(i*squareSize),
                                      0));
}

static void saveCameraParams( const string &filename,
                              Size imageSize, Size boardSize,
                              float squareSize, float aspectRatio,
                              int flags,
                              const Mat& cameraMatrix, const Mat& distCoeffs,
                              const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                              const vector<float> &reprojErrs,
                              const vector<vector<Point2f> > &imagePoints,
                              double totalAvgErr )
{
    FileStorage fs( filename, FileStorage::WRITE );

    time_t tt;
    time( &tt );
    struct tm *t2 = localtime( &tt );
    char buf[1024];

    strftime( buf, sizeof(buf)-1, "%c", t2 );
    fs << "calibration_time" << buf;

    if( !rvecs.empty() || !reprojErrs.empty() )
        fs << "nframes" << (int)std::max(rvecs.size(), reprojErrs.size());
    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    fs << "board_width" << boardSize.width;
    fs << "board_height" << boardSize.height;
    fs << "square_size" << squareSize;

    if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        fs << "aspectRatio" << aspectRatio;

    if( flags != 0 )
    {
        sprintf( buf, "flags: %s%s%s%s",
                 flags & CV_CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
                 flags & CV_CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
                 flags & CV_CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
                 flags & CV_CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "" );
        cvWriteComment( *fs, buf, 0 );
    }

    fs << "flags" << flags;

    fs << "camera_matrix" << cameraMatrix;
    fs << "distortion_coefficients" << distCoeffs;

    fs << "avg_reprojection_error" << totalAvgErr;
    if( !reprojErrs.empty() )
        fs << "per_view_reprojection_errors" << Mat(reprojErrs);

    if( !rvecs.empty() && !tvecs.empty() )
    {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for( int i = 0; i < (int)rvecs.size(); i++ )
        {
            Mat r = bigmat(Range(i, i+1), Range(0,3));
            Mat t = bigmat(Range(i, i+1), Range(3,6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "extrinsic_parameters" << bigmat;
    }

    if( !imagePoints.empty() )
    {
        Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
        for( int i = 0; i < (int)imagePoints.size(); i++ )
        {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "image_points" << imagePtMat;
    }
}


bool runCalibration( vector<vector<Point2f> > imagePoints,
                     Size imageSize, Size boardSize,
                     float squareSize, float aspectRatio,
                     int flags,
                     Mat& cameraMatrix, Mat& distCoeffs,
                     vector<Mat>& rvecs, vector<Mat>& tvecs,
                     vector<float>& reprojErrs,
                     double& totalAvgErr)
{
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        cameraMatrix.at<double>(0,0) = aspectRatio;

    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcChessboardCorners(boardSize, squareSize, objectPoints[0]);

    objectPoints.resize(imagePoints.size(), objectPoints[0]);

    double rms = calibrateCamera(objectPoints,
                                 imagePoints, imageSize,
                                 cameraMatrix,
                                 distCoeffs, rvecs, tvecs,
                                 flags);
    // |CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);
    // *|CV_CALIB_FIX_K3*/|CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);
    printf("RMS error reported by calibrateCamera: %g\n", rms);

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                            rvecs, tvecs,
                                            cameraMatrix, distCoeffs,
                                            reprojErrs);

    return ok;
}

bool runAndSave(const string& outputFilename,
                const vector<vector<Point2f> >& imagePoints,
                Size imageSize,
                Size boardSize, float squareSize,
                float aspectRatio, int flags,
                Mat& cameraMatrix, Mat& distCoeffs,
                bool writeExtrinsics, bool writePoints )
{
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(imagePoints,
                             imageSize,
                             boardSize, squareSize,
                             aspectRatio, flags,
                             cameraMatrix, distCoeffs,
                             rvecs, tvecs,
                             reprojErrs, totalAvgErr);
    printf("%s. avg reprojection error = %.2f\n",
           ok ? "Calibration succeeded" : "Calibration failed",
           totalAvgErr);

    if( ok )
        saveCameraParams( outputFilename,
                          imageSize,
                          boardSize, squareSize,
                          aspectRatio, flags,
                          cameraMatrix, distCoeffs,
                          writeExtrinsics ? rvecs : vector<Mat>(),
                          writeExtrinsics ? tvecs : vector<Mat>(),
                          writeExtrinsics ? reprojErrs : vector<float>(),
                          writePoints ? imagePoints : vector<vector<Point2f> >(),
                          totalAvgErr );
    return ok;
}

int camera_calib()
{

    Size        boardSize, imageSize;
    float       squareSize = 1.0f, aspectRatio = 1.0f;

    Mat         cameraMatrix, distCoeffs;
    vector<vector<Point2f> >  imagePoints;

    bool        writeExtrinsics = false, writePoints = false;
    int         flags = 0;

    string      fn_fl;
    string      outputFileName;
    StringArray fl;

    int         delay = 0;

    int         i;
    char        txtMsg[300];
    int         key;

    // get parameters
    boardSize.width = svar.GetInt("bw", 9);
    boardSize.height = svar.GetInt("bw", 6);

    squareSize = svar.GetDouble("s", 0.025);

    aspectRatio = svar.GetDouble("a", 1.0);
    delay = svar.GetInt("d", 0);

    outputFileName = svar.GetString("o", "cam_calib.out");
    fn_fl = svar.GetString("i", "filelist.txt");

    // read filelist
    readlines(fn_fl, fl);

    // for each image
    for(i=0; i<fl.size(); i++) {
        Mat                 view, viewGray;
        vector<Point2f>     pointbuf;
        bool                found;

        view = imread(fl[i]);
        imageSize = view.size();

        if( view.channels() > 1 )
            cvtColor(view, viewGray, CV_BGR2GRAY);
        else
            view.copyTo(viewGray);

        // find corners
        found = findChessboardCorners(view, boardSize,
                                      pointbuf,
                                      CV_CALIB_CB_ADAPTIVE_THRESH |
                                      CV_CALIB_CB_FAST_CHECK |
                                      CV_CALIB_CB_NORMALIZE_IMAGE);

        if( found ) {
            // find sub-pixel
            cornerSubPix( viewGray, pointbuf, Size(11,11),
                          Size(-1,-1),
                          TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

            // add to point list
            imagePoints.push_back(pointbuf);

            // draw chessboard corners
            drawChessboardCorners( view, boardSize, Mat(pointbuf), found );
        }

        // resize image
        {
            flip(view, view, 0);
            flip(view, view, 1);

            if( view.rows > 650 ) {
                Mat img1;
                float   scale;

                scale = 650.0/view.rows;
                resize(view, img1, Size(), scale, scale);
                img1.copyTo(view);
            }
        }

        // draw text message
        sprintf(txtMsg, "%d/%d", i, fl.size());
        putText(view, txtMsg, Point(10, 15), 1, 1, Scalar(255,0,0));

        // show image
        imshow("image", view);

        key = cvvWaitKey(0);
        if( key == 27 ) break;
    }

    // run calibration & save parameters
    runAndSave(outputFileName,
               imagePoints, imageSize,
               boardSize, squareSize, aspectRatio,
               flags,
               cameraMatrix, distCoeffs,
               writeExtrinsics, writePoints);

    // show undistorted
    {
        Mat view, rview, map1, map2;

        initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),
                                getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0),
                                imageSize, CV_16SC2,
                                map1, map2);

        for( i = 0; i < (int)fl.size(); i++ )
        {
            view = imread(fl[i], 1);
            if( view.empty() ) continue;

            //undistort( view, rview, cameraMatrix, distCoeffs, cameraMatrix );
            remap(view, rview, map1, map2, INTER_LINEAR);

            // resize image
            {
                flip(rview, rview, 0);

                if( rview.rows > 650 ) {
                    Mat img1;
                    float   scale;

                    scale = 650.0/rview.rows;
                    resize(rview, img1, Size(), scale, scale);
                    img1.copyTo(rview);
                }
            }

            // draw text message
            sprintf(txtMsg, "%d/%d", i, fl.size());
            putText(rview, txtMsg, Point(10, 15), 1, 1, Scalar(0,0,255));

            imshow("image", rview);

            key = cvvWaitKey(0);
            if( key == 27 ) break;
        }
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "camera_calib");

    DEF_TEST(camera_calib);
}
