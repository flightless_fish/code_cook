import multiprocessing 
import time

def print_cube(stop_signal): 
    i = 0
    while True:
        i += 1
        print("{} Cube".format(i)) 
        time.sleep(1)

        # master process send stop signal then quit
        if stop_signal.value == 1:
            break

def print_square(stop_signal): 
    i = 0
    while True:
        i += 1
        print("{} Square".format(i)) 
        time.sleep(1)

        # master process send stop signal then quit
        if stop_signal.value == 1:
            break


if __name__ == "__main__": 

    stop_signal = multiprocessing.Value('i', 0)

    # creating processes 
    p1 = multiprocessing.Process(target=print_square, args=(stop_signal, )) 
    p2 = multiprocessing.Process(target=print_cube, args=(stop_signal, )) 

    # starting process 1&2
    p1.start() 
    p2.start() 

    # sleep 3 seconds & send stop signal to slave processes
    time.sleep(3)
    stop_signal.value = 1

    # wait until process 1&2 is finished 
    p1.join() 
    p2.join() 

    # both processes finished 
    print("Done!")