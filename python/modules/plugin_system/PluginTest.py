
import os
from pluginbase import PluginBase

p_here = os.path.split(os.path.abspath(__file__))[0]

p1 = PluginBase.load_plugin(p_here+"/plugins/PluginA/PluginA.py")

r = p1.call("add", [1, 2])
print("res = %d" % r)

