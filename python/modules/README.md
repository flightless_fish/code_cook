# Module & Plugin demo for Python



## PVFS demo

这个demo演示如何使用统一的接口来实现不同的插件。本例子演示的是一个虚拟文件系统，为了存储大量碎文件，操作系统的文件系统存在存储效率低、访问速度慢的问题。使用HDF或者mongodb能够提高读写的效率，并集成了zstd压缩算法，能够显著降低存储容量的消耗。本示例程序演示如何通过Python的插件化设计实现一个统一的API接口能够操作文件系统、HDF、MongoDB三种实现的虚拟文件系统，让上层不关心底层如何存储具体的实现。


接口和对象生成的实现在：`pvfs.py`中，三种实现继承PVFS接口并实现各自的读写函数等。如何加载任意一个目录的程序，可以参考`pvfs.py`中`PVFS.create_pvfs`的实现，本例子中加载本目录中的`pvfs_[name].py`的python程序。



使用的示例程序为：`tool_pvfs.py`，具体的使用方法为：

1. 导入文件到虚拟文件系统：
```
python tool_pvfs.py --act import --db test.hdf --path /home/youpath/datafile
```

2. 导出虚拟文件系统到指定目录
```
python tool_pvfs.py --act export --db test.hdf --path /home/youpath/datafile_export
```

3. 列出所有文件
```
python tool_pvfs.py --act ls_all --db test.hdf 
```

其他的用法请研读`tool_pvfs.py`代码。





## References

* [A simple but flexible plugin system for Python](https://github.com/mitsuhiko/pluginbase) - PluginBase is a module for Python that enables the development of flexible plugin systems in Python. 可以参考这个研究如何实现插件的实现。