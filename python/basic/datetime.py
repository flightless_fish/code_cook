# coding=utf-8
import os,sys
import time, datetime


def getUTC_timestamp():
    t_l   = time.time()
    t_utc = time.gmtime(t_l)
    t_loc = time.localtime(t_l)
    t_offset = t_loc.tm_hour - t_utc.tm_hour
    ts_utc   = t_l - t_offset*3600
    
    print("t_offset  : ", t_offset)
    print("local time: ", t_l)
    print("utc       : ", t_utc)
    
    print("ts_utc    : ", ts_utc)
    
    return ts_utc

if __name__ == "__main__":
    ts = getUTC_timestamp()
    print("timestamp: %f" % ts)