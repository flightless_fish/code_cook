class easydict(dict):
    def __getattr__(self,name):
        if name in self:
            return self[name]
        n=easydict()
        super().__setitem__(name, n)
        return n
        
    def __getitem__(self,name):
        if name not in self:
            super().__setitem__(name, easydict())
        return super().__getitem__(name)
        
    def __setattr__(self,name,value):
        super().__setitem__(name,value)


class easyaccessdict(dict):
    def __getattr__(self, name):
        return self[name]
        
    def __setattr__(self, name, value):
        super().__setitem__(name,value)
        
    def __missing__(self, name):
        super().__setitem__(name, easyaccessdict())
        return super().__getitem__(name)
