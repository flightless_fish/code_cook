#!/usr/bin/env python

import os, sys
from optparse import OptionParser


def parse_optarg():
    parser = OptionParser(usage="usage: %prog [options] filename",
                          version="%prog 1.0")
    parser.add_option("-x", "--xhtml",
                      action="store_true",
                      dest="xhtml_flag",
                      default=False,
                      help="create a XHTML template instead of HTML")
    parser.add_option("-c", "--cssfile",
                      action="store",  # optional because action defaults to "store"
                      dest="cssfile",
                      default="style.css",
                      help="CSS file to link",)
    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("wrong number of arguments")

    print("---------------------------------------------------------")
    print("opts = %s" % str(options))
    print("args = %s" % str(args))
    print("---------------------------------------------------------")
    print("")

    return (options, args)

if __name__ == '__main__':
    # parse command opts & args
    (opts, args) = parse_optarg()

    # print opts
    print("xhtml_flag = %s" % str(opts.xhtml_flag))
    print("cssfile    = %s" % str(opts.cssfile))

    # print args
    print("args       = %s" % str(args))
