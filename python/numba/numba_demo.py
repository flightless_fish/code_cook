from numba import jit

"""
References:
    加速python运行-numba https://www.jianshu.com/p/69d9d7e37bc5
    Python · numba 的基本应用 https://zhuanlan.zhihu.com/p/27152060
"""


@jit
def f(count=1000):
    total = 0
    for i in range(count):
        total += i
    return total

