# Demo for python

更多库的使用方法 [Python参考](doc/references/python)



## 0. 使用方法

需要安装一些依赖，最好使用Python3

```
pip install -r requirements.txt
```



关于如何使用`virtualenv`和`virtualenvwrapper`请参考`requirements.txt`中的注释说明。



## 1. Basic usage

* [datatime & UTC timestamp](basic/datetime.py)
* [user implement dict](basic/easydict.py)



## 2. Config utils

* [python config](pconf/README.md)



## 3. Module Programming

* [PVFS demo](modules/README.md)


