import sys
import tty
import termios
import threading
from multiprocessing import Process, Queue

def readchar():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch
    
def readkey(getchar_fn=None):
    getchar = getchar_fn or readchar
    c1 = getchar()
    if ord(c1) != 0x1b:
        return c1
    c2 = getchar()
    if ord(c2) != 0x5b:
        return c1
    c3 = getchar()
    return chr(0x10 + ord(c3) - 65)

def thread_readkey(keyQueue):
    while True:
        key = readkey()
        keyQueue.put(key)
        if key == 'q':
            break
           
if __name__ == "__main__":
    keyQueue = Queue(10)

    # create key detect thread
    t_readkey = threading.Thread(target = thread_readkey, args=(keyQueue,))
    t_readkey.start()

    while True:
        if keyQueue.qsize() > 0:
            key = keyQueue.get()
            print("key = %s" % key.strip())
            if key == 'q':
                print("exit")
                break

    t_readkey.join()
