import sys
import collections

import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
from PyQt5.QtWidgets import (QApplication, QWidget, QListWidget, QListWidgetItem)

class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        #definition of colors and brushes (for colored backgrounds)
        layers = collections.OrderedDict(
            {1:('greenItem','g'),
             2:('yellowItem','y'),
             3:('mixedItem','m')})
        brushes = {'y': QtGui.QColor(255,200,0),
                   'g': QtGui.QColor(0,  160,0)}
        texSIG = QtGui.QPixmap(200,1)
        texSIG.fill(brushes['g'])
        paint = QtGui.QPainter()
        paint.begin(texSIG)
        paint.fillRect(80, 0, 40, 1, brushes['y'])
        paint.end()
        brushes['m'] = texSIG

        #create list
        self.lst = QListWidget(self)
        self.lst.setStyleSheet( """
                                QListWidget:item:selected:active {
                                     background-color:red;
                                }
                                """
                                )
        #comment on styling:
        #background-color on selection is plain red
        #something like "selection-color: solid rgba(255, 0, 0, 50%);" shows no effect

        self.lst.setSortingEnabled(False)
        for idx, d in layers.items():
            newItem = QListWidgetItem('{:s}'.format(d[0]))
            newItem.setBackground(QtGui.QBrush( brushes[d[1]] ))
            newItem.setTextAlignment(QtCore.Qt.AlignVCenter)
            self.lst.addItem(newItem)
        #self.lst.setSelectionMode(QtGui.QAbstractItemView.SelectionMode(QtGui.QAbstractItemView.ExtendedSelection))
        #self.lst.setDragDropMode(self.lst.InternalMove)
        
        #setup and show window
        self.setMinimumWidth(200)
        self.setMinimumHeight(200)
        self.show()

#create gui
app = QApplication(sys.argv)
#create and display main window
main = MainWindow()
main.show()
#return exit code
sys.exit(app.exec_())