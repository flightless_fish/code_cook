import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtCore import pyqtSlot, Qt

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 table - pythonspot.com'
        self.left = 0
        self.top = 0
        self.width = 300
        self.height = 200
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.createTable()

        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget) 
        self.setLayout(self.layout) 

        # Show widget
        self.show()

    def createTable(self):
       # Create table
        self.tableWidget = QTableWidget()
        
        nLab = 5
        self.tableWidget.setRowCount(nLab)
        self.tableWidget.setColumnCount(2)

        for j in range(nLab):
            i1 = QTableWidgetItem("")
            i1.setBackground(QColor(255-j*30, 0, 0))
            self.tableWidget.setItem(j, 0, i1)

            i2 = QTableWidgetItem("lab %d" % j)
            self.tableWidget.setItem(j, 1, i2)
        
        # set column width
        self.tableWidget.setColumnWidth(0, 30)
        self.tableWidget.setColumnWidth(1, 160)

        # hide horizontal header
        self.tableWidget.horizontalHeader().setVisible(False)

        # disable selection
        #self.tableWidget.setFocusPolicy(Qt.NoFocus)
        #self.tableWidget.setSelectionMode()

        # table selection change
        #self.tableWidget.doubleClicked.connect(self.on_click)

        

    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())  