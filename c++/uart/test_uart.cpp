

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "UART.h"
#include "Svar.h"


int test_uart(void)
{
    pi::UART uart;
    std::string port;

    char buf[2048];
    int n, nr;


    port = svar.GetString("port", "/dev/ttyACM0");

    // open port
    if( 0 != uart.open(port, 38400) ) {
        printf("Can not open UART port: %s\n", port.c_str());
        return -1;
    }

    // read data
    while( 1 ) {
        // n  - desired byte number
        // nr - actually readed byte number
        n = 128;
        nr = uart.read(buf, n);

        printf("read [%4d/%4d] bytes from UART\n", nr, n);
    }

}

int main(int argc, char *argv[])
{
    svar.ParseMain(argc, argv);

    return test_uart();
}

