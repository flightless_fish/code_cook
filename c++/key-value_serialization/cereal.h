#ifndef __CEREAL_IN_ONE_H__
#define __CEREAL_IN_ONE_H__



/*

## Include orders:

#include "cereal/macros.hpp"
#include "cereal/details/helpers.hpp"
#include "cereal/access.hpp"
#include "cereal/details/traits.hpp"
#include "cereal/details/static_object.hpp"
#include "cereal/details/polymorphic_impl_fwd.hpp"
#include "cereal/types/base_class.hpp"
#include "cereal/cereal.hpp"
#include "cereal/types/common.hpp"

#include "cereal/archives/binary.hpp"
#include "cereal/archives/portable_binary.hpp"

#include "cereal/types/concepts/pair_associative_container.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/complex.hpp"
#include "cereal/types/array.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/utility.hpp"
#include "cereal/types/tuple.hpp"
#include "cereal/types/deque.hpp"
#include "cereal/types/list.hpp"
#include "cereal/types/set.hpp"
#include "cereal/types/stack.hpp"
#include "cereal/types/functional.hpp"
#include "cereal/types/queue.hpp"


*/





/*! \file macros.hpp
    \brief Preprocessor macros that can customise the cereal library

    By default, cereal looks for serialization functions with very
    specific names, that is: serialize, load, save, load_minimal,
    or save_minimal.

    This file allows an advanced user to change these names to conform
    to some other style or preference.  This is implemented using
    preprocessor macros.

    As a result of this, in internal cereal code you will see macros
    used for these function names.  In user code, you should name
    the functions like you normally would and not use the macros
    to improve readability.
    \ingroup utility */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



#ifndef CEREAL_THREAD_SAFE
//! Whether cereal should be compiled for a threaded environment
/*! This macro causes cereal to use mutexes to control access to
    global internal state in a thread safe manner.

    Note that even with this enabled you must still ensure that
    archives are accessed by only one thread at a time; it is safe
    to use multiple archives in paralel, but not to access one archive
    from many places simultaneously. */
#define CEREAL_THREAD_SAFE 0
#endif // CEREAL_THREAD_SAFE

#ifndef CEREAL_SIZE_TYPE
//! Determines the data type used for size_type
/*! cereal uses size_type to ensure that the serialized size of
    dynamic containers is compatible across different architectures
    (e.g. 32 vs 64 bit), which may use different underlying types for
    std::size_t.

    More information can be found in cereal/details/helpers.hpp.

    If you choose to modify this type, ensure that you use a fixed
    size type (e.g. uint32_t). */
#define CEREAL_SIZE_TYPE uint64_t
#endif // CEREAL_SIZE_TYPE

// ######################################################################
#ifndef CEREAL_SERIALIZE_FUNCTION_NAME
//! The serialization/deserialization function name to search for.
/*! You can define @c CEREAL_SERIALIZE_FUNCTION_NAME to be different assuming
    you do so before this file is included. */
#define CEREAL_SERIALIZE_FUNCTION_NAME serialize
#endif // CEREAL_SERIALIZE_FUNCTION_NAME

#ifndef CEREAL_LOAD_FUNCTION_NAME
//! The deserialization (load) function name to search for.
/*! You can define @c CEREAL_LOAD_FUNCTION_NAME to be different assuming you do so
    before this file is included. */
#define CEREAL_LOAD_FUNCTION_NAME load
#endif // CEREAL_LOAD_FUNCTION_NAME

#ifndef CEREAL_SAVE_FUNCTION_NAME
//! The serialization (save) function name to search for.
/*! You can define @c CEREAL_SAVE_FUNCTION_NAME to be different assuming you do so
    before this file is included. */
#define CEREAL_SAVE_FUNCTION_NAME save
#endif // CEREAL_SAVE_FUNCTION_NAME

#ifndef CEREAL_LOAD_MINIMAL_FUNCTION_NAME
//! The deserialization (load_minimal) function name to search for.
/*! You can define @c CEREAL_LOAD_MINIMAL_FUNCTION_NAME to be different assuming you do so
    before this file is included. */
#define CEREAL_LOAD_MINIMAL_FUNCTION_NAME load_minimal
#endif // CEREAL_LOAD_MINIMAL_FUNCTION_NAME

#ifndef CEREAL_SAVE_MINIMAL_FUNCTION_NAME
//! The serialization (save_minimal) function name to search for.
/*! You can define @c CEREAL_SAVE_MINIMAL_FUNCTION_NAME to be different assuming you do so
    before this file is included. */
#define CEREAL_SAVE_MINIMAL_FUNCTION_NAME save_minimal
#endif // CEREAL_SAVE_MINIMAL_FUNCTION_NAME

// ######################################################################
//! Defines the CEREAL_NOEXCEPT macro to use instead of noexcept
/*! If a compiler we support does not support noexcept, this macro
    will detect this and define CEREAL_NOEXCEPT as a no-op
    @internal */
#if !defined(CEREAL_HAS_NOEXCEPT)
  #if defined(__clang__)
    #if __has_feature(cxx_noexcept)
      #define CEREAL_HAS_NOEXCEPT
    #endif
  #else // NOT clang
    #if defined(__GXX_EXPERIMENTAL_CXX0X__) && __GNUC__ * 10 + __GNUC_MINOR__ >= 46 || \
        defined(_MSC_FULL_VER) && _MSC_FULL_VER >= 190023026
      #define CEREAL_HAS_NOEXCEPT
    #endif // end GCC/MSVC check
  #endif // end NOT clang block

  #ifndef CEREAL_NOEXCEPT
    #ifdef CEREAL_HAS_NOEXCEPT
      #define CEREAL_NOEXCEPT noexcept
    #else
      #define CEREAL_NOEXCEPT
    #endif // end CEREAL_HAS_NOEXCEPT
  #endif // end !defined(CEREAL_HAS_NOEXCEPT)
#endif // ifndef CEREAL_NOEXCEPT









/*! \file helpers.hpp
    \brief Internal helper functionality
    \ingroup Internal */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <type_traits>
#include <cstdint>
#include <utility>
#include <memory>
#include <unordered_map>
#include <stdexcept>


namespace cereal
{
  // ######################################################################
  //! An exception class thrown when things go wrong at runtime
  /*! @ingroup Utility */
  struct Exception : public std::runtime_error
  {
    explicit Exception( const std::string & what_ ) : std::runtime_error(what_) {}
    explicit Exception( const char * what_ ) : std::runtime_error(what_) {}
  };

  // ######################################################################
  //! The size type used by cereal
  /*! To ensure compatability between 32, 64, etc bit machines, we need to use
      a fixed size type instead of size_t, which may vary from machine to
      machine.

      The default value for CEREAL_SIZE_TYPE is specified in cereal/macros.hpp */
  using size_type = CEREAL_SIZE_TYPE;

  // forward decls
  class BinaryOutputArchive;
  class BinaryInputArchive;

  // ######################################################################
  namespace detail
  {
    struct NameValuePairCore {}; //!< Traits struct for NVPs
  }

  //! For holding name value pairs
  /*! This pairs a name (some string) with some value such that an archive
      can potentially take advantage of the pairing.

      In serialization functions, NameValuePairs are usually created like so:
      @code{.cpp}
      struct MyStruct
      {
        int a, b, c, d, e;

        template<class Archive>
        void serialize(Archive & archive)
        {
          archive( CEREAL_NVP(a),
                   CEREAL_NVP(b),
                   CEREAL_NVP(c),
                   CEREAL_NVP(d),
                   CEREAL_NVP(e) );
        }
      };
      @endcode

      Alternatively, you can give you data members custom names like so:
      @code{.cpp}
      struct MyStruct
      {
        int a, b, my_embarrassing_variable_name, d, e;

        template<class Archive>
        void serialize(Archive & archive)
        {
          archive( CEREAL_NVP(a),
                   CEREAL_NVP(b),
                   cereal::make_nvp("var", my_embarrassing_variable_name) );
                   CEREAL_NVP(d),
                   CEREAL_NVP(e) );
        }
      };
      @endcode

      There is a slight amount of overhead to creating NameValuePairs, so there
      is a third method which will elide the names when they are not used by
      the Archive:

      @code{.cpp}
      struct MyStruct
      {
        int a, b;

        template<class Archive>
        void serialize(Archive & archive)
        {
          archive( cereal::make_nvp<Archive>(a),
                   cereal::make_nvp<Archive>(b) );
        }
      };
      @endcode

      This third method is generally only used when providing generic type
      support.  Users writing their own serialize functions will normally
      explicitly control whether they want to use NVPs or not.

      @internal */
  template <class T>
  class NameValuePair : detail::NameValuePairCore
  {
    private:
      // If we get passed an array, keep the type as is, otherwise store
      // a reference if we were passed an l value reference, else copy the value
      using Type = typename std::conditional<std::is_array<typename std::remove_reference<T>::type>::value,
                                             typename std::remove_cv<T>::type,
                                             typename std::conditional<std::is_lvalue_reference<T>::value,
                                                                       T,
                                                                       typename std::decay<T>::type>::type>::type;

      // prevent nested nvps
      static_assert( !std::is_base_of<detail::NameValuePairCore, T>::value,
                     "Cannot pair a name to a NameValuePair" );

      NameValuePair & operator=( NameValuePair const & ) = delete;

    public:
      //! Constructs a new NameValuePair
      /*! @param n The name of the pair
          @param v The value to pair.  Ideally this should be an l-value reference so that
                   the value can be both loaded and saved to.  If you pass an r-value reference,
                   the NameValuePair will store a copy of it instead of a reference.  Thus you should
                   only pass r-values in cases where this makes sense, such as the result of some
                   size() call.
          @internal */
      NameValuePair( char const * n, T && v ) : name(n), value(std::forward<T>(v)) {}

      char const * name;
      Type value;
  };

  //! A specialization of make_nvp<> that simply forwards the value for binary archives
  /*! @relates NameValuePair
      @internal */
  template<class Archive, class T> inline
  typename
  std::enable_if<std::is_same<Archive, ::cereal::BinaryInputArchive>::value ||
                 std::is_same<Archive, ::cereal::BinaryOutputArchive>::value,
  T && >::type
  make_nvp( const char *, T && value )
  {
    return std::forward<T>(value);
  }

  //! A specialization of make_nvp<> that actually creates an nvp for non-binary archives
  /*! @relates NameValuePair
      @internal */
  template<class Archive, class T> inline
  typename
  std::enable_if<!std::is_same<Archive, ::cereal::BinaryInputArchive>::value &&
                 !std::is_same<Archive, ::cereal::BinaryOutputArchive>::value,
  NameValuePair<T> >::type
  make_nvp( const char * name, T && value)
  {
    return {name, std::forward<T>(value)};
  }

  //! Convenience for creating a templated NVP
  /*! For use in internal generic typing functions which have an
      Archive type declared
      @internal */
  #define CEREAL_NVP_(name, value) ::cereal::make_nvp<Archive>(name, value)

  // ######################################################################
  //! A wrapper around data that can be serialized in a binary fashion
  /*! This class is used to demarcate data that can safely be serialized
      as a binary chunk of data.  Individual archives can then choose how
      best represent this during serialization.

      @internal */
  template <class T>
  struct BinaryData
  {
    //! Internally store the pointer as a void *, keeping const if created with
    //! a const pointer
    using PT = typename std::conditional<std::is_const<typename std::remove_pointer<T>::type>::value,
                                         const void *,
                                         void *>::type;

    BinaryData( T && d, uint64_t s ) : data(std::forward<T>(d)), size(s) {}

    PT data;       //!< pointer to beginning of data
    uint64_t size; //!< size in bytes
  };

  // ######################################################################
  namespace detail
  {
    // base classes for type checking
    /* The rtti virtual function only exists to enable an archive to
       be used in a polymorphic fashion, if necessary.  See the
       archive adapters for an example of this */
    class OutputArchiveBase
    {
      public:
        OutputArchiveBase() = default;
        OutputArchiveBase( OutputArchiveBase && ) CEREAL_NOEXCEPT {}
        OutputArchiveBase & operator=( OutputArchiveBase && ) CEREAL_NOEXCEPT { return *this; }
        virtual ~OutputArchiveBase() CEREAL_NOEXCEPT = default;

      private:
        virtual void rtti() {}
    };

    class InputArchiveBase
    {
      public:
        InputArchiveBase() = default;
        InputArchiveBase( InputArchiveBase && ) CEREAL_NOEXCEPT {}
        InputArchiveBase & operator=( InputArchiveBase && ) CEREAL_NOEXCEPT { return *this; }
        virtual ~InputArchiveBase() CEREAL_NOEXCEPT = default;

      private:
        virtual void rtti() {}
    };

    // forward decls for polymorphic support
    template <class Archive, class T> struct polymorphic_serialization_support;
    struct adl_tag;

    // used during saving pointers
    static const int32_t msb_32bit  = 0x80000000;
    static const int32_t msb2_32bit = 0x40000000;
  }

  // ######################################################################
  //! A wrapper around size metadata
  /*! This class provides a way for archives to have more flexibility over how
      they choose to serialize size metadata for containers.  For some archive
      types, the size may be implicitly encoded in the output (e.g. JSON) and
      not need an explicit entry.  Specializing serialize or load/save for
      your archive and SizeTags allows you to choose what happens.

      @internal */
  template <class T>
  class SizeTag
  {
    private:
      // Store a reference if passed an lvalue reference, otherwise
      // make a copy of the data
      using Type = typename std::conditional<std::is_lvalue_reference<T>::value,
                                             T,
                                             typename std::decay<T>::type>::type;

      SizeTag & operator=( SizeTag const & ) = delete;

    public:
      SizeTag( T && sz ) : size(std::forward<T>(sz)) {}

      Type size;
  };

  // ######################################################################
  //! A wrapper around a key and value for serializing data into maps.
  /*! This class just provides a grouping of keys and values into a struct for
      human readable archives. For example, XML archives will use this wrapper
      to write maps like so:

      @code{.xml}
      <mymap>
        <item0>
          <key>MyFirstKey</key>
          <value>MyFirstValue</value>
        </item0>
        <item1>
          <key>MySecondKey</key>
          <value>MySecondValue</value>
        </item1>
      </mymap>
      @endcode

      \sa make_map_item
      @internal */
  template <class Key, class Value>
  struct MapItem
  {
    using KeyType = typename std::conditional<
      std::is_lvalue_reference<Key>::value,
      Key,
      typename std::decay<Key>::type>::type;

    using ValueType = typename std::conditional<
      std::is_lvalue_reference<Value>::value,
      Value,
      typename std::decay<Value>::type>::type;

    //! Construct a MapItem from a key and a value
    /*! @internal */
    MapItem( Key && key_, Value && value_ ) : key(std::forward<Key>(key_)), value(std::forward<Value>(value_)) {}

    MapItem & operator=( MapItem const & ) = delete;

    KeyType key;
    ValueType value;

    //! Serialize the MapItem with the NVPs "key" and "value"
    template <class Archive> inline
    void CEREAL_SERIALIZE_FUNCTION_NAME(Archive & archive)
    {
      archive( make_nvp<Archive>("key",   key),
               make_nvp<Archive>("value", value) );
    }
  };

  //! Create a MapItem so that human readable archives will group keys and values together
  /*! @internal
      @relates MapItem */
  template <class KeyType, class ValueType> inline
  MapItem<KeyType, ValueType> make_map_item(KeyType && key, ValueType && value)
  {
    return {std::forward<KeyType>(key), std::forward<ValueType>(value)};
  }

  namespace detail
  {
    //! Tag for Version, which due to its anonymous namespace, becomes a different
    //! type in each translation unit
    /*! This allows CEREAL_CLASS_VERSION to be safely called in a header file */
    namespace{ struct version_binding_tag {}; }

    // ######################################################################
    //! Version information class
    /*! This is the base case for classes that have not been explicitly
        registered */
    template <class T, class BindingTag = version_binding_tag> struct Version
    {
      static const std::uint32_t version = 0;
      // we don't need to explicitly register these types since they
      // always get a version number of 0
    };

    //! Holds all registered version information
    struct Versions
    {
      std::unordered_map<std::size_t, std::uint32_t> mapping;

      std::uint32_t find( std::size_t hash, std::uint32_t version )
      {
        const auto result = mapping.emplace( hash, version );
        return result.first->second;
      }
    }; // struct Versions
  } // namespace detail
} // namespace cereal








/*! \file access.hpp
    \brief Access control, default construction, and serialization disambiguation */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <type_traits>
#include <iostream>
#include <cstdint>
#include <functional>


namespace cereal
{
  // ######################################################################
  //! A class that allows cereal to load smart pointers to types that have no default constructor
  /*! If your class does not have a default constructor, cereal will not be able
      to load any smart pointers to it unless you overload LoadAndConstruct
      for your class, and provide an appropriate load_and_construct method.  You can also
      choose to define a member static function instead of specializing this class.

      The specialization of LoadAndConstruct must be placed within the cereal namespace:

      @code{.cpp}
      struct MyType
      {
        MyType( int x ); // note: no default ctor
        int myX;

        // Define a serialize or load/save pair as you normally would
        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( myX );
        }
      };

      // Provide a specialization for LoadAndConstruct for your type
      namespace cereal
      {
        template <> struct LoadAndConstruct<MyType>
        {
          // load_and_construct will be passed the archive that you will be loading
          // from as well as a construct object which you can use as if it were the
          // constructor for your type.  cereal will handle all memory management for you.
          template <class Archive>
          static void load_and_construct( Archive & ar, cereal::construct<MyType> & construct )
          {
            int x;
            ar( x );
            construct( x );
          }

          // if you require versioning, simply add a const std::uint32_t as the final parameter, e.g.:
          // load_and_construct( Archive & ar, cereal::construct<MyType> & construct, std::uint32_t const version )
        };
      } // end namespace cereal
      @endcode

      Please note that just as in using external serialization functions, you cannot get
      access to non-public members of your class by befriending cereal::access.  If you
      have the ability to modify the class you wish to serialize, it is recommended that you
      use member serialize functions and a static member load_and_construct function.

      load_and_construct functions, regardless of whether they are static members of your class or
      whether you create one in the LoadAndConstruct specialization, have the following signature:

      @code{.cpp}
      // generally Archive will be templated, but it can be specific if desired
      template <class Archive>
      static void load_and_construct( Archive & ar, cereal::construct<MyType> & construct );
      // with an optional last parameter specifying the version: const std::uint32_t version
      @endcode

      Versioning behaves the same way as it does for standard serialization functions.

      @tparam T The type to specialize for
      @ingroup Access */
  template <class T>
  struct LoadAndConstruct
  { };

  // forward decl for construct
  //! @cond PRIVATE_NEVERDEFINED
  namespace memory_detail{ template <class Ar, class T> struct LoadAndConstructLoadWrapper; }
  //! @endcond

  //! Used to construct types with no default constructor
  /*! When serializing a type that has no default constructor, cereal
      will attempt to call either the class static function load_and_construct
      or the appropriate template specialization of LoadAndConstruct.  cereal
      will pass that function a reference to the archive as well as a reference
      to a construct object which should be used to perform the allocation once
      data has been appropriately loaded.

      @code{.cpp}
      struct MyType
      {
        // note the lack of default constructor
        MyType( int xx, int yy );

        int x, y;
        double notInConstructor;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( x, y );
          ar( notInConstructor );
        }

        template <class Archive>
        static void load_and_construct( Archive & ar, cereal::construct<MyType> & construct )
        {
          int x, y;
          ar( x, y );

          // use construct object to initialize with loaded data
          construct( x, y );

          // access to member variables and functions via -> operator
          ar( construct->notInConstructor );

          // could also do the above section by:
          double z;
          ar( z );
          construct->notInConstructor = z;
        }
      };
      @endcode

      @tparam T The class type being serialized
      */
  template <class T>
  class construct
  {
    public:
      //! Construct and initialize the type T with the given arguments
      /*! This will forward all arguments to the underlying type T,
          calling an appropriate constructor.

          Calling this function more than once will result in an exception
          being thrown.

          @param args The arguments to the constructor for T
          @throw Exception If called more than once */
      template <class ... Args>
      void operator()( Args && ... args );
      // implementation deferred due to reliance on cereal::access

      //! Get a reference to the initialized underlying object
      /*! This must be called after the object has been initialized.

          @return A reference to the initialized object
          @throw Exception If called before initialization */
      T * operator->()
      {
        if( !itsValid )
          throw Exception("Object must be initialized prior to accessing members");

        return itsPtr;
      }

      //! Returns a raw pointer to the initialized underlying object
      /*! This is mainly intended for use with passing an instance of
          a constructed object to cereal::base_class.

          It is strongly recommended to avoid using this function in
          any other circumstance.

          @return A raw pointer to the initialized type */
      T * ptr()
      {
        return operator->();
      }

    private:
      template <class A, class B> friend struct ::cereal::memory_detail::LoadAndConstructLoadWrapper;

      construct( T * p ) : itsPtr( p ), itsEnableSharedRestoreFunction( [](){} ), itsValid( false ) {}
      construct( T * p, std::function<void()> enableSharedFunc ) : // g++4.7 ice with default lambda to std func
        itsPtr( p ), itsEnableSharedRestoreFunction( enableSharedFunc ), itsValid( false ) {}
      construct( construct const & ) = delete;
      construct & operator=( construct const & ) = delete;

      T * itsPtr;
      std::function<void()> itsEnableSharedRestoreFunction;
      bool itsValid;
  };

  // ######################################################################
  //! A class that can be made a friend to give cereal access to non public functions
  /*! If you desire non-public serialization functions within a class, cereal can only
      access these if you declare cereal::access a friend.

      @code{.cpp}
      class MyClass
      {
        private:
          friend class cereal::access; // gives access to the private serialize

          template <class Archive>
          void serialize( Archive & ar )
          {
            // some code
          }
      };
      @endcode
      @ingroup Access */
  class access
  {
    public:
      // ####### Standard Serialization ########################################
      template<class Archive, class T> inline
      static auto member_serialize(Archive & ar, T & t) -> decltype(t.CEREAL_SERIALIZE_FUNCTION_NAME(ar))
      { return t.CEREAL_SERIALIZE_FUNCTION_NAME(ar); }

      template<class Archive, class T> inline
      static auto member_save(Archive & ar, T const & t) -> decltype(t.CEREAL_SAVE_FUNCTION_NAME(ar))
      { return t.CEREAL_SAVE_FUNCTION_NAME(ar); }

      template<class Archive, class T> inline
      static auto member_save_non_const(Archive & ar, T & t) -> decltype(t.CEREAL_SAVE_FUNCTION_NAME(ar))
      { return t.CEREAL_SAVE_FUNCTION_NAME(ar); }

      template<class Archive, class T> inline
      static auto member_load(Archive & ar, T & t) -> decltype(t.CEREAL_LOAD_FUNCTION_NAME(ar))
      { return t.CEREAL_LOAD_FUNCTION_NAME(ar); }

      template<class Archive, class T> inline
      static auto member_save_minimal(Archive const & ar, T const & t) -> decltype(t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar))
      { return t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar); }

      template<class Archive, class T> inline
      static auto member_save_minimal_non_const(Archive const & ar, T & t) -> decltype(t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar))
      { return t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar); }

      template<class Archive, class T, class U> inline
      static auto member_load_minimal(Archive const & ar, T & t, U && u) -> decltype(t.CEREAL_LOAD_MINIMAL_FUNCTION_NAME(ar, std::forward<U>(u)))
      { return t.CEREAL_LOAD_MINIMAL_FUNCTION_NAME(ar, std::forward<U>(u)); }

      // ####### Versioned Serialization #######################################
      template<class Archive, class T> inline
      static auto member_serialize(Archive & ar, T & t, const std::uint32_t version ) -> decltype(t.CEREAL_SERIALIZE_FUNCTION_NAME(ar, version))
      { return t.CEREAL_SERIALIZE_FUNCTION_NAME(ar, version); }

      template<class Archive, class T> inline
      static auto member_save(Archive & ar, T const & t, const std::uint32_t version ) -> decltype(t.CEREAL_SAVE_FUNCTION_NAME(ar, version))
      { return t.CEREAL_SAVE_FUNCTION_NAME(ar, version); }

      template<class Archive, class T> inline
      static auto member_save_non_const(Archive & ar, T & t, const std::uint32_t version ) -> decltype(t.CEREAL_SAVE_FUNCTION_NAME(ar, version))
      { return t.CEREAL_SAVE_FUNCTION_NAME(ar, version); }

      template<class Archive, class T> inline
      static auto member_load(Archive & ar, T & t, const std::uint32_t version ) -> decltype(t.CEREAL_LOAD_FUNCTION_NAME(ar, version))
      { return t.CEREAL_LOAD_FUNCTION_NAME(ar, version); }

      template<class Archive, class T> inline
      static auto member_save_minimal(Archive const & ar, T const & t, const std::uint32_t version) -> decltype(t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar, version))
      { return t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar, version); }

      template<class Archive, class T> inline
      static auto member_save_minimal_non_const(Archive const & ar, T & t, const std::uint32_t version) -> decltype(t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar, version))
      { return t.CEREAL_SAVE_MINIMAL_FUNCTION_NAME(ar, version); }

      template<class Archive, class T, class U> inline
      static auto member_load_minimal(Archive const & ar, T & t, U && u, const std::uint32_t version) -> decltype(t.CEREAL_LOAD_MINIMAL_FUNCTION_NAME(ar, std::forward<U>(u), version))
      { return t.CEREAL_LOAD_MINIMAL_FUNCTION_NAME(ar, std::forward<U>(u), version); }

      // ####### Other Functionality ##########################################
      // for detecting inheritance from enable_shared_from_this
      template <class T> inline
      static auto shared_from_this(T & t) -> decltype(t.shared_from_this());

      // for placement new
      template <class T, class ... Args> inline
      static void construct( T *& ptr, Args && ... args )
      {
        new (ptr) T( std::forward<Args>( args )... );
      }

      // for non-placement new with a default constructor
      template <class T> inline
      static T * construct()
      {
        return new T();
      }

      template <class T> inline
      static std::false_type load_and_construct(...)
      { return std::false_type(); }

      template<class T, class Archive> inline
      static auto load_and_construct(Archive & ar, ::cereal::construct<T> & construct) -> decltype(T::load_and_construct(ar, construct))
      {
        T::load_and_construct( ar, construct );
      }

      template<class T, class Archive> inline
      static auto load_and_construct(Archive & ar, ::cereal::construct<T> & construct, const std::uint32_t version) -> decltype(T::load_and_construct(ar, construct, version))
      {
        T::load_and_construct( ar, construct, version );
      }
  }; // end class access

  // ######################################################################
  //! A specifier used in conjunction with cereal::specialize to disambiguate
  //! serialization in special cases
  /*! @relates specialize
      @ingroup Access */
  enum class specialization
  {
    member_serialize,            //!< Force the use of a member serialize function
    member_load_save,            //!< Force the use of a member load/save pair
    member_load_save_minimal,    //!< Force the use of a member minimal load/save pair
    non_member_serialize,        //!< Force the use of a non-member serialize function
    non_member_load_save,        //!< Force the use of a non-member load/save pair
    non_member_load_save_minimal //!< Force the use of a non-member minimal load/save pair
  };

  //! A class used to disambiguate cases where cereal cannot detect a unique way of serializing a class
  /*! cereal attempts to figure out which method of serialization (member vs. non-member serialize
      or load/save pair) at compile time.  If for some reason cereal cannot find a non-ambiguous way
      of serializing a type, it will produce a static assertion complaining about this.

      This can happen because you have both a serialize and load/save pair, or even because a base
      class has a serialize (public or private with friend access) and a derived class does not
      overwrite this due to choosing some other serialization type.

      Specializing this class will tell cereal to explicitly use the serialization type you specify
      and it will not complain about ambiguity in its compile time selection.  However, if cereal detects
      an ambiguity in specializations, it will continue to issue a static assertion.

      @code{.cpp}
      class MyParent
      {
        friend class cereal::access;
        template <class Archive>
        void serialize( Archive & ar ) {}
      };

      // Although serialize is private in MyParent, to cereal::access it will look public,
      // even through MyDerived
      class MyDerived : public MyParent
      {
        public:
          template <class Archive>
          void load( Archive & ar ) {}

          template <class Archive>
          void save( Archive & ar ) {}
      };

      // The load/save pair in MyDerived is ambiguous because serialize in MyParent can
      // be accessed from cereal::access.  This looks the same as making serialize public
      // in MyParent, making it seem as though MyDerived has both a serialize and a load/save pair.
      // cereal will complain about this at compile time unless we disambiguate:

      namespace cereal
      {
        // This struct specialization will tell cereal which is the right way to serialize the ambiguity
        template <class Archive> struct specialize<Archive, MyDerived, cereal::specialization::member_load_save> {};

        // If we only had a disambiguation for a specific archive type, it would look something like this
        template <> struct specialize<cereal::BinaryOutputArchive, MyDerived, cereal::specialization::member_load_save> {};
      }
      @endcode

      You can also choose to use the macros CEREAL_SPECIALIZE_FOR_ALL_ARCHIVES or
      CEREAL_SPECIALIZE_FOR_ARCHIVE if you want to type a little bit less.

      @tparam T The type to specialize the serialization for
      @tparam S The specialization type to use for T
      @ingroup Access */
  template <class Archive, class T, specialization S>
  struct specialize : public std::false_type {};

  //! Convenient macro for performing specialization for all archive types
  /*! This performs specialization for the specific type for all types of archives.
      This macro should be placed at the global namespace.

      @code{cpp}
      struct MyType {};
      CEREAL_SPECIALIZE_FOR_ALL_ARCHIVES( MyType, cereal::specialization::member_load_save );
      @endcode

      @relates specialize
      @ingroup Access */
  #define CEREAL_SPECIALIZE_FOR_ALL_ARCHIVES( Type, Specialization )                                \
  namespace cereal { template <class Archive> struct specialize<Archive, Type, Specialization> {}; }

  //! Convenient macro for performing specialization for a single archive type
  /*! This performs specialization for the specific type for a single type of archive.
      This macro should be placed at the global namespace.

      @code{cpp}
      struct MyType {};
      CEREAL_SPECIALIZE_FOR_ARCHIVE( cereal::XMLInputArchive, MyType, cereal::specialization::member_load_save );
      @endcode

      @relates specialize
      @ingroup Access */
  #define CEREAL_SPECIALIZE_FOR_ARCHIVE( Archive, Type, Specialization )               \
  namespace cereal { template <> struct specialize<Archive, Type, Specialization> {}; }

  // ######################################################################
  // Deferred Implementation, see construct for more information
  template <class T> template <class ... Args> inline
  void construct<T>::operator()( Args && ... args )
  {
    if( itsValid )
      throw Exception("Attempting to construct an already initialized object");

    ::cereal::access::construct( itsPtr, std::forward<Args>( args )... );
    itsEnableSharedRestoreFunction();
    itsValid = true;
  }
} // namespace cereal





/*! \file traits.hpp
    \brief Internal type trait support
    \ingroup Internal */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef __clang__
#if (__GNUC__ == 4 && __GNUC_MINOR__ <= 7)
#define CEREAL_OLDER_GCC
#endif // gcc 4.7 or earlier
#endif // __clang__

#include <type_traits>
#include <typeindex>



namespace cereal
{
  namespace traits
  {
    using yes = std::true_type;
    using no  = std::false_type;

    namespace detail
    {
      // ######################################################################
      //! Used to delay a static_assert until template instantiation
      template <class T>
      struct delay_static_assert : std::false_type {};

      // ######################################################################
      // SFINAE Helpers
      #ifdef CEREAL_OLDER_GCC // when VS supports better SFINAE, we can use this as the default
      template<typename> struct Void { typedef void type; };
      #endif // CEREAL_OLDER_GCC

      //! Return type for SFINAE Enablers
      enum class sfinae {};

      // ######################################################################
      // Helper functionality for boolean integral constants and Enable/DisableIf
      template <bool H, bool ... T> struct meta_bool_and : std::integral_constant<bool, H && meta_bool_and<T...>::value> {};
      template <bool B> struct meta_bool_and<B> : std::integral_constant<bool, B> {};

      template <bool H, bool ... T> struct meta_bool_or : std::integral_constant<bool, H || meta_bool_or<T...>::value> {};
      template <bool B> struct meta_bool_or<B> : std::integral_constant<bool, B> {};

      // workaround needed due to bug in MSVC 2013, see
      // http://connect.microsoft.com/VisualStudio/feedback/details/800231/c-11-alias-template-issue
      template <bool ... Conditions>
      struct EnableIfHelper : std::enable_if<meta_bool_and<Conditions...>::value, sfinae> {};

      template <bool ... Conditions>
      struct DisableIfHelper : std::enable_if<!meta_bool_or<Conditions...>::value, sfinae> {};
    } // namespace detail

    //! Used as the default value for EnableIf and DisableIf template parameters
    /*! @relates EnableIf
        @relates DisableIf */
    static const detail::sfinae sfinae = {};

    // ######################################################################
    //! Provides a way to enable a function if conditions are met
    /*! This is intended to be used in a near identical fashion to std::enable_if
        while being significantly easier to read at the cost of not allowing for as
        complicated of a condition.

        This will compile (allow the function) if every condition evaluates to true.
        at compile time.  This should be used with SFINAE to ensure that at least
        one other candidate function works when one fails due to an EnableIf.

        This should be used as the las template parameter to a function as
        an unnamed parameter with a default value of cereal::traits::sfinae:

        @code{cpp}
        // using by making the last template argument variadic
        template <class T, EnableIf<std::is_same<T, bool>::value> = sfinae>
        void func(T t );
        @endcode

        Note that this performs a logical AND of all conditions, so you will need
        to construct more complicated requirements with this fact in mind.

        @relates DisableIf
        @relates sfinae
        @tparam Conditions The conditions which will be logically ANDed to enable the function. */
    template <bool ... Conditions>
    using EnableIf = typename detail::EnableIfHelper<Conditions...>::type;

    // ######################################################################
    //! Provides a way to disable a function if conditions are met
    /*! This is intended to be used in a near identical fashion to std::enable_if
        while being significantly easier to read at the cost of not allowing for as
        complicated of a condition.

        This will compile (allow the function) if every condition evaluates to false.
        This should be used with SFINAE to ensure that at least one other candidate
        function works when one fails due to a DisableIf.

        This should be used as the las template parameter to a function as
        an unnamed parameter with a default value of cereal::traits::sfinae:

        @code{cpp}
        // using by making the last template argument variadic
        template <class T, DisableIf<std::is_same<T, bool>::value> = sfinae>
        void func(T t );
        @endcode

        This is often used in conjunction with EnableIf to form an enable/disable pair of
        overloads.

        Note that this performs a logical AND of all conditions, so you will need
        to construct more complicated requirements with this fact in mind.  If all conditions
        hold, the function will be disabled.

        @relates EnableIf
        @relates sfinae
        @tparam Conditions The conditions which will be logically ANDed to disable the function. */
    template <bool ... Conditions>
    using DisableIf = typename detail::DisableIfHelper<Conditions...>::type;

    // ######################################################################
    namespace detail
    {
      template <class InputArchive>
      struct get_output_from_input : no
      {
        static_assert( detail::delay_static_assert<InputArchive>::value,
            "Could not find an associated output archive for input archive." );
      };

      template <class OutputArchive>
      struct get_input_from_output : no
      {
        static_assert( detail::delay_static_assert<OutputArchive>::value,
            "Could not find an associated input archive for output archive." );
      };
    }

    //! Sets up traits that relate an input archive to an output archive
    #define CEREAL_SETUP_ARCHIVE_TRAITS(InputArchive, OutputArchive)  \
    namespace cereal { namespace traits { namespace detail {          \
      template <> struct get_output_from_input<InputArchive>          \
      { using type = OutputArchive; };                                \
      template <> struct get_input_from_output<OutputArchive>         \
      { using type = InputArchive; }; } } } /* end namespaces */

    // ######################################################################
    //! Used to convert a MAKE_HAS_XXX macro into a versioned variant
    #define CEREAL_MAKE_VERSIONED_TEST ,0

    // ######################################################################
    //! Creates a test for whether a non const member function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param name The name of the function to test for (e.g. serialize, load, save)
        @param test_name The name to give the test for the function being tested for (e.g. serialize, versioned_serialize)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #ifdef CEREAL_OLDER_GCC
    #define CEREAL_MAKE_HAS_MEMBER_TEST(name, test_name, versioned)                                                                         \
    template <class T, class A, class SFINAE = void>                                                                                        \
    struct has_member_##test_name : no {};                                                                                                  \
    template <class T, class A>                                                                                                             \
    struct has_member_##test_name<T, A,                                                                                                     \
      typename detail::Void< decltype( cereal::access::member_##name( std::declval<A&>(), std::declval<T&>() versioned ) ) >::type> : yes {}
    #else // NOT CEREAL_OLDER_GCC
    #define CEREAL_MAKE_HAS_MEMBER_TEST(name, test_name, versioned)                                                                     \
    namespace detail                                                                                                                    \
    {                                                                                                                                   \
      template <class T, class A>                                                                                                       \
      struct has_member_##name##_##versioned##_impl                                                                                     \
      {                                                                                                                                 \
        template <class TT, class AA>                                                                                                   \
        static auto test(int) -> decltype( cereal::access::member_##name( std::declval<AA&>(), std::declval<TT&>() versioned ), yes()); \
        template <class, class>                                                                                                         \
        static no test(...);                                                                                                            \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;                                                    \
      };                                                                                                                                \
    } /* end namespace detail */                                                                                                        \
    template <class T, class A>                                                                                                         \
    struct has_member_##test_name : std::integral_constant<bool, detail::has_member_##name##_##versioned##_impl<T, A>::value> {}
    #endif // NOT CEREAL_OLDER_GCC

    // ######################################################################
    //! Creates a test for whether a non const non-member function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper non-member function for the given archive. */
    #define CEREAL_MAKE_HAS_NON_MEMBER_TEST(test_name, func, versioned)                                                         \
    namespace detail                                                                                                            \
    {                                                                                                                           \
      template <class T, class A>                                                                                               \
      struct has_non_member_##test_name##_impl                                                                                  \
      {                                                                                                                         \
        template <class TT, class AA>                                                                                           \
        static auto test(int) -> decltype( func( std::declval<AA&>(), std::declval<TT&>() versioned ), yes());                  \
        template <class, class>                                                                                                 \
        static no test( ... );                                                                                                  \
        static const bool value = std::is_same<decltype( test<T, A>( 0 ) ), yes>::value;                                        \
      };                                                                                                                        \
    } /* end namespace detail */                                                                                                \
    template <class T, class A>                                                                                                 \
    struct has_non_member_##test_name : std::integral_constant<bool, detail::has_non_member_##test_name##_impl<T, A>::value> {}

    // ######################################################################
    // Member Serialize
    CEREAL_MAKE_HAS_MEMBER_TEST(serialize, serialize,);

    // ######################################################################
    // Member Serialize (versioned)
    CEREAL_MAKE_HAS_MEMBER_TEST(serialize, versioned_serialize, CEREAL_MAKE_VERSIONED_TEST);

    // ######################################################################
    // Non Member Serialize
    CEREAL_MAKE_HAS_NON_MEMBER_TEST(serialize, CEREAL_SERIALIZE_FUNCTION_NAME,);

    // ######################################################################
    // Non Member Serialize (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_TEST(versioned_serialize, CEREAL_SERIALIZE_FUNCTION_NAME, CEREAL_MAKE_VERSIONED_TEST);

    // ######################################################################
    // Member Load
    CEREAL_MAKE_HAS_MEMBER_TEST(load, load,);

    // ######################################################################
    // Member Load (versioned)
    CEREAL_MAKE_HAS_MEMBER_TEST(load, versioned_load, CEREAL_MAKE_VERSIONED_TEST);

    // ######################################################################
    // Non Member Load
    CEREAL_MAKE_HAS_NON_MEMBER_TEST(load, CEREAL_LOAD_FUNCTION_NAME,);

    // ######################################################################
    // Non Member Load (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_TEST(versioned_load, CEREAL_LOAD_FUNCTION_NAME, CEREAL_MAKE_VERSIONED_TEST);

    // ######################################################################
    #undef CEREAL_MAKE_HAS_NON_MEMBER_TEST
    #undef CEREAL_MAKE_HAS_MEMBER_TEST

    // ######################################################################
    //! Creates a test for whether a member save function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param test_name The name to give the test (e.g. save or versioned_save)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #ifdef CEREAL_OLDER_GCC
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_IMPL(test_name, versioned)                                                                  \
    namespace detail                                                                                                                \
    {                                                                                                                               \
    template <class T, class A>                                                                                                     \
    struct has_member_##test_name##_impl                                                                                            \
      {                                                                                                                             \
        template <class TT, class AA, class SFINAE = void> struct test : no {};                                                     \
        template <class TT, class AA>                                                                                               \
        struct test<TT, AA,                                                                                                         \
          typename detail::Void< decltype( cereal::access::member_save( std::declval<AA&>(),                                        \
                                                                        std::declval<TT const &>() versioned ) ) >::type> : yes {}; \
        static const bool value = test<T, A>();                                                                                     \
                                                                                                                                    \
        template <class TT, class AA, class SFINAE = void> struct test2 : no {};                                                    \
        template <class TT, class AA>                                                                                               \
        struct test2<TT, AA,                                                                                                        \
          typename detail::Void< decltype( cereal::access::member_save_non_const(                                                   \
                                            std::declval<AA&>(),                                                                    \
                                            std::declval<typename std::remove_const<TT>::type&>() versioned ) ) >::type> : yes {};  \
        static const bool not_const_type = test2<T, A>();                                                                           \
      };                                                                                                                            \
    } /* end namespace detail */
    #else /* NOT CEREAL_OLDER_GCC =================================== */
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_IMPL(test_name, versioned)                                                                  \
    namespace detail                                                                                                                \
    {                                                                                                                               \
    template <class T, class A>                                                                                                     \
    struct has_member_##test_name##_impl                                                                                            \
      {                                                                                                                             \
        template <class TT, class AA>                                                                                               \
        static auto test(int) -> decltype( cereal::access::member_save( std::declval<AA&>(),                                        \
                                                                        std::declval<TT const &>() versioned ), yes());             \
        template <class, class> static no test(...);                                                                                \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;                                                \
                                                                                                                                    \
        template <class TT, class AA>                                                                                               \
        static auto test2(int) -> decltype( cereal::access::member_save_non_const(                                                  \
                                              std::declval<AA &>(),                                                                 \
                                              std::declval<typename std::remove_const<TT>::type&>() versioned ), yes());            \
        template <class, class> static no test2(...);                                                                               \
        static const bool not_const_type = std::is_same<decltype(test2<T, A>(0)), yes>::value;                                      \
      };                                                                                                                            \
    } /* end namespace detail */
    #endif /* NOT CEREAL_OLDER_GCC */

    // ######################################################################
    // Member Save
    CEREAL_MAKE_HAS_MEMBER_SAVE_IMPL(save, )

    template <class T, class A>
    struct has_member_save : std::integral_constant<bool, detail::has_member_save_impl<T, A>::value>
    {
      typedef typename detail::has_member_save_impl<T, A> check;
      static_assert( check::value || !check::not_const_type,
        "cereal detected a non-const save. \n "
        "save member functions must always be const" );
    };

    // ######################################################################
    // Member Save (versioned)
    CEREAL_MAKE_HAS_MEMBER_SAVE_IMPL(versioned_save, CEREAL_MAKE_VERSIONED_TEST)

    template <class T, class A>
    struct has_member_versioned_save : std::integral_constant<bool, detail::has_member_versioned_save_impl<T, A>::value>
    {
      typedef typename detail::has_member_versioned_save_impl<T, A> check;
      static_assert( check::value || !check::not_const_type,
        "cereal detected a versioned non-const save. \n "
        "save member functions must always be const" );
    };

    // ######################################################################
    #undef CEREAL_MAKE_HAS_MEMBER_SAVE_IMPL

    // ######################################################################
    //! Creates a test for whether a non-member save function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper non-member function for the given archive.

        @param test_name The name to give the test (e.g. save or versioned_save)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #define CEREAL_MAKE_HAS_NON_MEMBER_SAVE_TEST(test_name, versioned)                                                       \
    namespace detail                                                                                                         \
    {                                                                                                                        \
      template <class T, class A>                                                                                            \
      struct has_non_member_##test_name##_impl                                                                               \
      {                                                                                                                      \
        template <class TT, class AA>                                                                                        \
        static auto test(int) -> decltype( CEREAL_SAVE_FUNCTION_NAME(                                                        \
                                              std::declval<AA&>(),                                                           \
                                              std::declval<TT const &>() versioned ), yes());                                \
        template <class, class> static no test(...);                                                                         \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;                                         \
                                                                                                                             \
        template <class TT, class AA>                                                                                        \
        static auto test2(int) -> decltype( CEREAL_SAVE_FUNCTION_NAME(                                                       \
                                              std::declval<AA &>(),                                                          \
                                              std::declval<typename std::remove_const<TT>::type&>() versioned ), yes());     \
        template <class, class> static no test2(...);                                                                        \
        static const bool not_const_type = std::is_same<decltype(test2<T, A>(0)), yes>::value;                               \
      };                                                                                                                     \
    } /* end namespace detail */                                                                                             \
                                                                                                                             \
    template <class T, class A>                                                                                              \
    struct has_non_member_##test_name : std::integral_constant<bool, detail::has_non_member_##test_name##_impl<T, A>::value> \
    {                                                                                                                        \
      using check = typename detail::has_non_member_##test_name##_impl<T, A>;                                                \
      static_assert( check::value || !check::not_const_type,                                                                 \
        "cereal detected a non-const type parameter in non-member " #test_name ". \n "                                       \
        #test_name " non-member functions must always pass their types as const" );                                          \
    };

    // ######################################################################
    // Non Member Save
    CEREAL_MAKE_HAS_NON_MEMBER_SAVE_TEST(save, )

    // ######################################################################
    // Non Member Save (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_SAVE_TEST(versioned_save, CEREAL_MAKE_VERSIONED_TEST)

    // ######################################################################
    #undef CEREAL_MAKE_HAS_NON_MEMBER_SAVE_TEST

    // ######################################################################
    // Minimal Utilities
    namespace detail
    {
      // Determines if the provided type is an std::string
      template <class> struct is_string : std::false_type {};

      template <class CharT, class Traits, class Alloc>
      struct is_string<std::basic_string<CharT, Traits, Alloc>> : std::true_type {};
    }

    // Determines if the type is valid for use with a minimal serialize function
    template <class T>
    struct is_minimal_type : std::integral_constant<bool,
      detail::is_string<T>::value || std::is_arithmetic<T>::value> {};

    // ######################################################################
    //! Creates implementation details for whether a member save_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param test_name The name to give the test (e.g. save_minimal or versioned_save_minimal)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #ifdef CEREAL_OLDER_GCC
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_IMPL(test_name, versioned)                                                                        \
    namespace detail                                                                                                                              \
    {                                                                                                                                             \
      template <class T, class A>                                                                                                                 \
      struct has_member_##test_name##_impl                                                                                                        \
      {                                                                                                                                           \
        template <class TT, class AA, class SFINAE = void> struct test : no {};                                                                   \
        template <class TT, class AA>                                                                                                             \
        struct test<TT, AA, typename detail::Void< decltype(                                                                                      \
            cereal::access::member_save_minimal( std::declval<AA const &>(),                                                                      \
                                                 std::declval<TT const &>() versioned ) ) >::type> : yes {};                                      \
                                                                                                                                                  \
        static const bool value = test<T, A>();                                                                                                   \
                                                                                                                                                  \
        template <class TT, class AA, class SFINAE = void> struct test2 : no {};                                                                  \
        template <class TT, class AA>                                                                                                             \
        struct test2<TT, AA, typename detail::Void< decltype(                                                                                     \
            cereal::access::member_save_minimal_non_const( std::declval<AA const &>(),                                                            \
                                                           std::declval<typename std::remove_const<TT>::type&>() versioned ) ) >::type> : yes {}; \
        static const bool not_const_type = test2<T, A>();                                                                                         \
                                                                                                                                                  \
        static const bool valid = value || !not_const_type;                                                                                       \
      };                                                                                                                                          \
    } /* end namespace detail */
    #else /* NOT CEREAL_OLDER_GCC =================================== */
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_IMPL(test_name, versioned)                     \
    namespace detail                                                                           \
    {                                                                                          \
      template <class T, class A>                                                              \
      struct has_member_##test_name##_impl                                                     \
      {                                                                                        \
        template <class TT, class AA>                                                          \
        static auto test(int) -> decltype( cereal::access::member_save_minimal(                \
              std::declval<AA const &>(),                                                      \
              std::declval<TT const &>() versioned ), yes());                                  \
        template <class, class> static no test(...);                                           \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;           \
                                                                                               \
        template <class TT, class AA>                                                          \
        static auto test2(int) -> decltype( cereal::access::member_save_minimal_non_const(     \
              std::declval<AA const &>(),                                                      \
              std::declval<typename std::remove_const<TT>::type&>() versioned ), yes());       \
        template <class, class> static no test2(...);                                          \
        static const bool not_const_type = std::is_same<decltype(test2<T, A>(0)), yes>::value; \
                                                                                               \
        static const bool valid = value || !not_const_type;                                    \
      };                                                                                       \
    } /* end namespace detail */
    #endif // NOT CEREAL_OLDER_GCC

    // ######################################################################
    //! Creates helpers for minimal save functions
    /*! The get_member_*_type structs allow access to the return type of a save_minimal,
        assuming that the function actually exists.  If the function does not
        exist, the type will be void.

        @param test_name The name to give the test (e.g. save_minimal or versioned_save_minimal)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_HELPERS_IMPL(test_name, versioned)                           \
    namespace detail                                                                                         \
    {                                                                                                        \
      template <class T, class A, bool Valid>                                                                \
      struct get_member_##test_name##_type { using type = void; };                                           \
                                                                                                             \
      template <class T, class A>                                                                            \
      struct get_member_##test_name##_type<T, A, true>                                                       \
      {                                                                                                      \
        using type = decltype( cereal::access::member_save_minimal( std::declval<A const &>(),               \
                                                                    std::declval<T const &>() versioned ) ); \
      };                                                                                                     \
    } /* end namespace detail */

    // ######################################################################
    //! Creates a test for whether a member save_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param test_name The name to give the test (e.g. save_minimal or versioned_save_minimal) */
    #define CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_TEST(test_name)                                                      \
    template <class T, class A>                                                                                      \
    struct has_member_##test_name : std::integral_constant<bool, detail::has_member_##test_name##_impl<T, A>::value> \
    {                                                                                                                \
      using check = typename detail::has_member_##test_name##_impl<T, A>;                                            \
      static_assert( check::valid,                                                                                   \
        "cereal detected a non-const member " #test_name ". \n "                                                     \
        #test_name " member functions must always be const" );                                                       \
                                                                                                                     \
      using type = typename detail::get_member_##test_name##_type<T, A, check::value>::type;                         \
      static_assert( (check::value && is_minimal_type<type>::value) || !check::value,                                \
        "cereal detected a member " #test_name " with an invalid return type. \n "                                   \
        "return type must be arithmetic or string" );                                                                \
    };

    // ######################################################################
    // Member Save Minimal
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_IMPL(save_minimal, )
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_HELPERS_IMPL(save_minimal, )
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_TEST(save_minimal)

    // ######################################################################
    // Member Save Minimal (versioned)
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_IMPL(versioned_save_minimal, CEREAL_MAKE_VERSIONED_TEST)
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_HELPERS_IMPL(versioned_save_minimal, CEREAL_MAKE_VERSIONED_TEST)
    CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_TEST(versioned_save_minimal)

    // ######################################################################
    #undef CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_IMPL
    #undef CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_HELPERS_IMPL
    #undef CEREAL_MAKE_HAS_MEMBER_SAVE_MINIMAL_TEST

    // ######################################################################
    //! Creates a test for whether a non-member save_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param test_name The name to give the test (e.g. save_minimal or versioned_save_minimal)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #define CEREAL_MAKE_HAS_NON_MEMBER_SAVE_MINIMAL_TEST(test_name, versioned)                                               \
    namespace detail                                                                                                         \
    {                                                                                                                        \
      template <class T, class A>                                                                                            \
      struct has_non_member_##test_name##_impl                                                                               \
      {                                                                                                                      \
        template <class TT, class AA>                                                                                        \
        static auto test(int) -> decltype( CEREAL_SAVE_MINIMAL_FUNCTION_NAME(                                                \
              std::declval<AA const &>(),                                                                                    \
              std::declval<TT const &>() versioned ), yes());                                                                \
        template <class, class> static no test(...);                                                                         \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;                                         \
                                                                                                                             \
        template <class TT, class AA>                                                                                        \
        static auto test2(int) -> decltype( CEREAL_SAVE_MINIMAL_FUNCTION_NAME(                                               \
              std::declval<AA const &>(),                                                                                    \
              std::declval<typename std::remove_const<TT>::type&>() versioned ), yes());                                     \
        template <class, class> static no test2(...);                                                                        \
        static const bool not_const_type = std::is_same<decltype(test2<T, A>(0)), yes>::value;                               \
                                                                                                                             \
        static const bool valid = value || !not_const_type;                                                                  \
      };                                                                                                                     \
                                                                                                                             \
      template <class T, class A, bool Valid>                                                                                \
      struct get_non_member_##test_name##_type { using type = void; };                                                       \
                                                                                                                             \
      template <class T, class A>                                                                                            \
      struct get_non_member_##test_name##_type <T, A, true>                                                                  \
      {                                                                                                                      \
        using type = decltype( CEREAL_SAVE_MINIMAL_FUNCTION_NAME( std::declval<A const &>(),                                 \
                                                                  std::declval<T const &>() versioned ) );                   \
      };                                                                                                                     \
    } /* end namespace detail */                                                                                             \
                                                                                                                             \
    template <class T, class A>                                                                                              \
    struct has_non_member_##test_name : std::integral_constant<bool, detail::has_non_member_##test_name##_impl<T, A>::value> \
    {                                                                                                                        \
      using check = typename detail::has_non_member_##test_name##_impl<T, A>;                                                \
      static_assert( check::valid,                                                                                           \
        "cereal detected a non-const type parameter in non-member " #test_name ". \n "                                       \
        #test_name " non-member functions must always pass their types as const" );                                          \
                                                                                                                             \
      using type = typename detail::get_non_member_##test_name##_type<T, A, check::value>::type;                             \
      static_assert( (check::value && is_minimal_type<type>::value) || !check::value,                                        \
        "cereal detected a non-member " #test_name " with an invalid return type. \n "                                       \
        "return type must be arithmetic or string" );                                                                        \
    };

    // ######################################################################
    // Non-Member Save Minimal
    CEREAL_MAKE_HAS_NON_MEMBER_SAVE_MINIMAL_TEST(save_minimal, )

    // ######################################################################
    // Non-Member Save Minimal (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_SAVE_MINIMAL_TEST(versioned_save_minimal, CEREAL_MAKE_VERSIONED_TEST)

    // ######################################################################
    #undef CEREAL_MAKE_HAS_NON_MEMBER_SAVE_MINIMAL_TEST

    // ######################################################################
    // Load Minimal Utilities
    namespace detail
    {
      //! Used to help strip away conversion wrappers
      /*! If someone writes a non-member load/save minimal function that accepts its
          parameter as some generic template type and needs to perform trait checks
          on that type, our NoConvert wrappers will interfere with this.  Using
          the struct strip_minmal, users can strip away our wrappers to get to
          the underlying type, allowing traits to work properly */
      struct NoConvertBase {};

      //! A struct that prevents implicit conversion
      /*! Any type instantiated with this struct will be unable to implicitly convert
          to another type.  Is designed to only allow conversion to Source const &.

          @tparam Source the type of the original source */
      template <class Source>
      struct NoConvertConstRef : NoConvertBase
      {
        using type = Source; //!< Used to get underlying type easily

        template <class Dest, class = typename std::enable_if<std::is_same<Source, Dest>::value>::type>
        operator Dest () = delete;

        //! only allow conversion if the types are the same and we are converting into a const reference
        template <class Dest, class = typename std::enable_if<std::is_same<Source, Dest>::value>::type>
        operator Dest const & ();
      };

      //! A struct that prevents implicit conversion
      /*! Any type instantiated with this struct will be unable to implicitly convert
          to another type.  Is designed to only allow conversion to Source &.

          @tparam Source the type of the original source */
      template <class Source>
      struct NoConvertRef : NoConvertBase
      {
        using type = Source; //!< Used to get underlying type easily

        template <class Dest, class = typename std::enable_if<std::is_same<Source, Dest>::value>::type>
        operator Dest () = delete;

        #ifdef __clang__
        template <class Dest, class = typename std::enable_if<std::is_same<Source, Dest>::value>::type>
        operator Dest const & () = delete;
        #endif // __clang__

        //! only allow conversion if the types are the same and we are converting into a const reference
        template <class Dest, class = typename std::enable_if<std::is_same<Source, Dest>::value>::type>
        operator Dest & ();
      };

      //! A type that can implicitly convert to anything else
      struct AnyConvert
      {
        template <class Dest>
        operator Dest & ();

        template <class Dest>
        operator Dest const & () const;
      };
    } // namespace detail

    // ######################################################################
    //! Creates a test for whether a member load_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        Our strategy here is to first check if a function matching the signature more or less exists
        (allow anything like load_minimal(xxx) using AnyConvert, and then secondly enforce
        that it has the correct signature using NoConvertConstRef

        @param test_name The name to give the test (e.g. load_minimal or versioned_load_minimal)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #ifdef CEREAL_OLDER_GCC
    #define CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_IMPL(test_name, versioned)                                                    \
    namespace detail                                                                                                          \
    {                                                                                                                         \
      template <class T, class A, class SFINAE = void> struct has_member_##test_name##_impl : no {};                          \
      template <class T, class A>                                                                                             \
      struct has_member_##test_name##_impl<T, A, typename detail::Void< decltype(                                             \
          cereal::access::member_load_minimal( std::declval<A const &>(),                                                     \
                                               std::declval<T &>(), AnyConvert() versioned ) ) >::type> : yes {};             \
                                                                                                                              \
        template <class T, class A, class U, class SFINAE = void> struct has_member_##test_name##_type_impl : no {};          \
        template <class T, class A, class U>                                                                                  \
        struct has_member_##test_name##_type_impl<T, A, U, typename detail::Void< decltype(                                   \
            cereal::access::member_load_minimal( std::declval<A const &>(),                                                   \
                                                 std::declval<T &>(), NoConvertConstRef<U>() versioned ) ) >::type> : yes {}; \
    } /* end namespace detail */
    #else /* NOT CEREAL_OLDER_GCC =================================== */
    #define CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_IMPL(test_name, versioned)              \
    namespace detail                                                                    \
    {                                                                                   \
      template <class T, class A>                                                       \
      struct has_member_##test_name##_impl                                              \
      {                                                                                 \
        template <class TT, class AA>                                                   \
        static auto test(int) -> decltype( cereal::access::member_load_minimal(         \
              std::declval<AA const &>(),                                               \
              std::declval<TT &>(), AnyConvert() versioned ), yes());                   \
        template <class, class> static no test(...);                                    \
        static const bool value = std::is_same<decltype(test<T, A>(0)), yes>::value;    \
      };                                                                                \
      template <class T, class A, class U>                                              \
      struct has_member_##test_name##_type_impl                                         \
      {                                                                                 \
        template <class TT, class AA, class UU>                                         \
        static auto test(int) -> decltype( cereal::access::member_load_minimal(         \
              std::declval<AA const &>(),                                               \
              std::declval<TT &>(), NoConvertConstRef<UU>() versioned ), yes());        \
        template <class, class, class> static no test(...);                             \
        static const bool value = std::is_same<decltype(test<T, A, U>(0)), yes>::value; \
                                                                                        \
      };                                                                                \
    } /* end namespace detail */
    #endif // NOT CEREAL_OLDER_GCC

    // ######################################################################
    //! Creates helpers for minimal load functions
    /*! The has_member_*_wrapper structs ensure that the load and save types for the
        requested function type match appropriately.

        @param load_test_name The name to give the test (e.g. load_minimal or versioned_load_minimal)
        @param save_test_name The name to give the test (e.g. save_minimal or versioned_save_minimal,
                              should match the load name.
        @param save_test_prefix The name to give the test (e.g. save_minimal or versioned_save_minimal,
                              should match the load name, without the trailing "_minimal" (e.g.
                              save or versioned_save).  Needed because the preprocessor is an abomination.
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #define CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_HELPERS_IMPL(load_test_name, save_test_name, save_test_prefix, versioned) \
    namespace detail                                                                                                      \
    {                                                                                                                     \
      template <class T, class A, bool Valid>                                                                             \
      struct has_member_##load_test_name##_wrapper : std::false_type {};                                                  \
                                                                                                                          \
      template <class T, class A>                                                                                         \
      struct has_member_##load_test_name##_wrapper<T, A, true>                                                            \
      {                                                                                                                   \
        using AOut = typename detail::get_output_from_input<A>::type;                                                     \
                                                                                                                          \
        static_assert( has_member_##save_test_prefix##_minimal<T, AOut>::value,                                           \
          "cereal detected member " #load_test_name " but no valid member " #save_test_name ". \n "                       \
          "cannot evaluate correctness of " #load_test_name " without valid " #save_test_name "." );                      \
                                                                                                                          \
        using SaveType = typename detail::get_member_##save_test_prefix##_minimal_type<T, AOut, true>::type;              \
        const static bool value = has_member_##load_test_name##_impl<T, A>::value;                                        \
        const static bool valid = has_member_##load_test_name##_type_impl<T, A, SaveType>::value;                         \
                                                                                                                          \
        static_assert( valid || !value, "cereal detected different or invalid types in corresponding member "             \
            #load_test_name " and " #save_test_name " functions. \n "                                                     \
            "the paramater to " #load_test_name " must be a constant reference to the type that "                         \
            #save_test_name " returns." );                                                                                \
      };                                                                                                                  \
    } /* end namespace detail */

    // ######################################################################
    //! Creates a test for whether a member load_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        @param load_test_name The name to give the test (e.g. load_minimal or versioned_load_minimal)
        @param load_test_prefix The above parameter minus the trailing "_minimal" */
    #define CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_TEST(load_test_name, load_test_prefix)                                         \
    template <class T, class A>                                                                                                \
    struct has_member_##load_test_prefix##_minimal : std::integral_constant<bool,                                              \
      detail::has_member_##load_test_name##_wrapper<T, A, detail::has_member_##load_test_name##_impl<T, A>::value>::value> {};

    // ######################################################################
    // Member Load Minimal
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_IMPL(load_minimal, )
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_HELPERS_IMPL(load_minimal, save_minimal, save, )
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_TEST(load_minimal, load)

    // ######################################################################
    // Member Load Minimal (versioned)
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_IMPL(versioned_load_minimal, CEREAL_MAKE_VERSIONED_TEST)
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_HELPERS_IMPL(versioned_load_minimal, versioned_save_minimal, versioned_save, CEREAL_MAKE_VERSIONED_TEST)
    CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_TEST(versioned_load_minimal, versioned_load)

    // ######################################################################
    #undef CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_IMPL
    #undef CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_HELPERS_IMPL
    #undef CEREAL_MAKE_HAS_MEMBER_LOAD_MINIMAL_TEST

    // ######################################################################
    // Non-Member Load Minimal
    namespace detail
    {
      #ifdef CEREAL_OLDER_GCC
      void CEREAL_LOAD_MINIMAL_FUNCTION_NAME(); // prevents nonsense complaining about not finding this
      void CEREAL_SAVE_MINIMAL_FUNCTION_NAME();
      #endif // CEREAL_OLDER_GCC
    } // namespace detail

    // ######################################################################
    //! Creates a test for whether a non-member load_minimal function exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper member function for the given archive.

        See notes from member load_minimal implementation.

        @param test_name The name to give the test (e.g. load_minimal or versioned_load_minimal)
        @param save_name The corresponding name the save test would have (e.g. save_minimal or versioned_save_minimal)
        @param versioned Either blank or the macro CEREAL_MAKE_VERSIONED_TEST */
    #define CEREAL_MAKE_HAS_NON_MEMBER_LOAD_MINIMAL_TEST(test_name, save_name, versioned)                                    \
    namespace detail                                                                                                         \
    {                                                                                                                        \
      template <class T, class A, class U = void>                                                                            \
      struct has_non_member_##test_name##_impl                                                                               \
      {                                                                                                                      \
        template <class TT, class AA>                                                                                        \
        static auto test(int) -> decltype( CEREAL_LOAD_MINIMAL_FUNCTION_NAME(                                                \
              std::declval<AA const &>(), std::declval<TT&>(), AnyConvert() versioned ), yes() );                            \
        template <class, class> static no test( ... );                                                                       \
        static const bool exists = std::is_same<decltype( test<T, A>( 0 ) ), yes>::value;                                    \
                                                                                                                             \
        template <class TT, class AA, class UU>                                                                              \
        static auto test2(int) -> decltype( CEREAL_LOAD_MINIMAL_FUNCTION_NAME(                                               \
              std::declval<AA const &>(), std::declval<TT&>(), NoConvertConstRef<UU>() versioned ), yes() );                 \
        template <class, class, class> static no test2( ... );                                                               \
        static const bool valid = std::is_same<decltype( test2<T, A, U>( 0 ) ), yes>::value;                                 \
                                                                                                                             \
        template <class TT, class AA>                                                                                        \
        static auto test3(int) -> decltype( CEREAL_LOAD_MINIMAL_FUNCTION_NAME(                                               \
              std::declval<AA const &>(), NoConvertRef<TT>(), AnyConvert() versioned ), yes() );                             \
        template <class, class> static no test3( ... );                                                                      \
        static const bool const_valid = std::is_same<decltype( test3<T, A>( 0 ) ), yes>::value;                              \
      };                                                                                                                     \
                                                                                                                             \
      template <class T, class A, bool Valid>                                                                                \
      struct has_non_member_##test_name##_wrapper : std::false_type {};                                                      \
                                                                                                                             \
      template <class T, class A>                                                                                            \
      struct has_non_member_##test_name##_wrapper<T, A, true>                                                                \
      {                                                                                                                      \
        using AOut = typename detail::get_output_from_input<A>::type;                                                        \
                                                                                                                             \
        static_assert( detail::has_non_member_##save_name##_impl<T, AOut>::valid,                                            \
          "cereal detected non-member " #test_name " but no valid non-member " #save_name ". \n "                            \
          "cannot evaluate correctness of " #test_name " without valid " #save_name "." );                                   \
                                                                                                                             \
        using SaveType = typename detail::get_non_member_##save_name##_type<T, AOut, true>::type;                            \
        using check = has_non_member_##test_name##_impl<T, A, SaveType>;                                                     \
        static const bool value = check::exists;                                                                             \
                                                                                                                             \
        static_assert( check::valid || !check::exists, "cereal detected different types in corresponding non-member "        \
            #test_name " and " #save_name " functions. \n "                                                                  \
            "the paramater to " #test_name " must be a constant reference to the type that " #save_name " returns." );       \
        static_assert( check::const_valid || !check::exists,                                                                 \
            "cereal detected an invalid serialization type parameter in non-member " #test_name ".  "                        \
            #test_name " non-member functions must accept their serialization type by non-const reference" );                \
      };                                                                                                                     \
    } /* namespace detail */                                                                                                 \
                                                                                                                             \
    template <class T, class A>                                                                                              \
    struct has_non_member_##test_name : std::integral_constant<bool,                                                         \
      detail::has_non_member_##test_name##_wrapper<T, A, detail::has_non_member_##test_name##_impl<T, A>::exists>::value> {};

    // ######################################################################
    // Non-Member Load Minimal
    CEREAL_MAKE_HAS_NON_MEMBER_LOAD_MINIMAL_TEST(load_minimal, save_minimal, )

    // ######################################################################
    // Non-Member Load Minimal (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_LOAD_MINIMAL_TEST(versioned_load_minimal, versioned_save_minimal, CEREAL_MAKE_VERSIONED_TEST)

    // ######################################################################
    #undef CEREAL_MAKE_HAS_NON_MEMBER_LOAD_MINIMAL_TEST

    // ######################################################################
    //! Member load and construct check
    template<typename T, typename A>
    struct has_member_load_and_construct : std::integral_constant<bool,
      std::is_same<decltype( access::load_and_construct<T>( std::declval<A&>(), std::declval< ::cereal::construct<T>&>() ) ), void>::value>
    { };

    // ######################################################################
    //! Member load and construct check (versioned)
    template<typename T, typename A>
    struct has_member_versioned_load_and_construct : std::integral_constant<bool,
      std::is_same<decltype( access::load_and_construct<T>( std::declval<A&>(), std::declval< ::cereal::construct<T>&>(), 0 ) ), void>::value>
    { };

    // ######################################################################
    //! Creates a test for whether a non-member load_and_construct specialization exists
    /*! This creates a class derived from std::integral_constant that will be true if
        the type has the proper non-member function for the given archive. */
    #define CEREAL_MAKE_HAS_NON_MEMBER_LOAD_AND_CONSTRUCT_TEST(test_name, versioned)                                            \
    namespace detail                                                                                                            \
    {                                                                                                                           \
      template <class T, class A>                                                                                               \
      struct has_non_member_##test_name##_impl                                                                                  \
      {                                                                                                                         \
        template <class TT, class AA>                                                                                           \
        static auto test(int) -> decltype( LoadAndConstruct<TT>::load_and_construct(                                            \
                                           std::declval<AA&>(), std::declval< ::cereal::construct<TT>&>() versioned ), yes());  \
        template <class, class>                                                                                                 \
        static no test( ... );                                                                                                  \
        static const bool value = std::is_same<decltype( test<T, A>( 0 ) ), yes>::value;                                        \
      };                                                                                                                        \
    } /* end namespace detail */                                                                                                \
    template <class T, class A>                                                                                                 \
    struct has_non_member_##test_name : std::integral_constant<bool, detail::has_non_member_##test_name##_impl<T, A>::value> {};

    // ######################################################################
    //! Non member load and construct check
    CEREAL_MAKE_HAS_NON_MEMBER_LOAD_AND_CONSTRUCT_TEST(load_and_construct, )

    // ######################################################################
    //! Non member load and construct check (versioned)
    CEREAL_MAKE_HAS_NON_MEMBER_LOAD_AND_CONSTRUCT_TEST(versioned_load_and_construct, CEREAL_MAKE_VERSIONED_TEST)

    // ######################################################################
    //! Has either a member or non member load and construct
    template<typename T, typename A>
    struct has_load_and_construct : std::integral_constant<bool,
      has_member_load_and_construct<T, A>::value || has_non_member_load_and_construct<T, A>::value ||
      has_member_versioned_load_and_construct<T, A>::value || has_non_member_versioned_load_and_construct<T, A>::value>
    { };

    // ######################################################################
    #undef CEREAL_MAKE_HAS_NON_MEMBER_LOAD_AND_CONSTRUCT_TEST

    // ######################################################################
    // End of serialization existence tests
    #undef CEREAL_MAKE_VERSIONED_TEST

    // ######################################################################
    template <class T, class InputArchive, class OutputArchive>
    struct has_member_split : std::integral_constant<bool,
      (has_member_load<T, InputArchive>::value && has_member_save<T, OutputArchive>::value) ||
      (has_member_versioned_load<T, InputArchive>::value && has_member_versioned_save<T, OutputArchive>::value)> {};

    // ######################################################################
    template <class T, class InputArchive, class OutputArchive>
    struct has_non_member_split : std::integral_constant<bool,
      (has_non_member_load<T, InputArchive>::value && has_non_member_save<T, OutputArchive>::value) ||
      (has_non_member_versioned_load<T, InputArchive>::value && has_non_member_versioned_save<T, OutputArchive>::value)> {};

    // ######################################################################
    template <class T, class OutputArchive>
    struct has_invalid_output_versioning : std::integral_constant<bool,
      (has_member_versioned_save<T, OutputArchive>::value && has_member_save<T, OutputArchive>::value) ||
      (has_non_member_versioned_save<T, OutputArchive>::value && has_non_member_save<T, OutputArchive>::value) ||
      (has_member_versioned_serialize<T, OutputArchive>::value && has_member_serialize<T, OutputArchive>::value) ||
      (has_non_member_versioned_serialize<T, OutputArchive>::value && has_non_member_serialize<T, OutputArchive>::value) ||
      (has_member_versioned_save_minimal<T, OutputArchive>::value && has_member_save_minimal<T, OutputArchive>::value) ||
      (has_non_member_versioned_save_minimal<T, OutputArchive>::value &&  has_non_member_save_minimal<T, OutputArchive>::value)> {};

    // ######################################################################
    template <class T, class InputArchive>
    struct has_invalid_input_versioning : std::integral_constant<bool,
      (has_member_versioned_load<T, InputArchive>::value && has_member_load<T, InputArchive>::value) ||
      (has_non_member_versioned_load<T, InputArchive>::value && has_non_member_load<T, InputArchive>::value) ||
      (has_member_versioned_serialize<T, InputArchive>::value && has_member_serialize<T, InputArchive>::value) ||
      (has_non_member_versioned_serialize<T, InputArchive>::value && has_non_member_serialize<T, InputArchive>::value) ||
      (has_member_versioned_load_minimal<T, InputArchive>::value && has_member_load_minimal<T, InputArchive>::value) ||
      (has_non_member_versioned_load_minimal<T, InputArchive>::value &&  has_non_member_load_minimal<T, InputArchive>::value)> {};

    // ######################################################################
    namespace detail
    {
      //! Create a test for a cereal::specialization entry
      #define CEREAL_MAKE_IS_SPECIALIZED_IMPL(name)                                          \
      template <class T, class A>                                                            \
      struct is_specialized_##name : std::integral_constant<bool,                            \
        !std::is_base_of<std::false_type, specialize<A, T, specialization::name>>::value> {}

      CEREAL_MAKE_IS_SPECIALIZED_IMPL(member_serialize);
      CEREAL_MAKE_IS_SPECIALIZED_IMPL(member_load_save);
      CEREAL_MAKE_IS_SPECIALIZED_IMPL(member_load_save_minimal);
      CEREAL_MAKE_IS_SPECIALIZED_IMPL(non_member_serialize);
      CEREAL_MAKE_IS_SPECIALIZED_IMPL(non_member_load_save);
      CEREAL_MAKE_IS_SPECIALIZED_IMPL(non_member_load_save_minimal);

      #undef CEREAL_MAKE_IS_SPECIALIZED_IMPL

      //! Number of specializations detected
      template <class T, class A>
      struct count_specializations : std::integral_constant<int,
        is_specialized_member_serialize<T, A>::value +
        is_specialized_member_load_save<T, A>::value +
        is_specialized_member_load_save_minimal<T, A>::value +
        is_specialized_non_member_serialize<T, A>::value +
        is_specialized_non_member_load_save<T, A>::value +
        is_specialized_non_member_load_save_minimal<T, A>::value> {};
    } // namespace detail

    //! Check if any specialization exists for a type
    template <class T, class A>
    struct is_specialized : std::integral_constant<bool,
      detail::is_specialized_member_serialize<T, A>::value ||
      detail::is_specialized_member_load_save<T, A>::value ||
      detail::is_specialized_member_load_save_minimal<T, A>::value ||
      detail::is_specialized_non_member_serialize<T, A>::value ||
      detail::is_specialized_non_member_load_save<T, A>::value ||
      detail::is_specialized_non_member_load_save_minimal<T, A>::value>
    {
      static_assert(detail::count_specializations<T, A>::value <= 1, "More than one explicit specialization detected for type.");
    };

    //! Create the static assertion for some specialization
    /*! This assertion will fail if the type is indeed specialized and does not have the appropriate
        type of serialization functions */
    #define CEREAL_MAKE_IS_SPECIALIZED_ASSERT(name, versioned_name, print_name, spec_name)                      \
    static_assert( (is_specialized<T, A>::value && detail::is_specialized_##spec_name<T, A>::value &&           \
                   (has_##name<T, A>::value || has_##versioned_name<T, A>::value))                              \
                   || !(is_specialized<T, A>::value && detail::is_specialized_##spec_name<T, A>::value),        \
                   "cereal detected " #print_name " specialization but no " #print_name " serialize function" )

    //! Generates a test for specialization for versioned and unversioned functions
    /*! This creates checks that can be queried to see if a given type of serialization function
        has been specialized for this type */
    #define CEREAL_MAKE_IS_SPECIALIZED(name, versioned_name, spec_name)                     \
    template <class T, class A>                                                             \
    struct is_specialized_##name : std::integral_constant<bool,                             \
      is_specialized<T, A>::value && detail::is_specialized_##spec_name<T, A>::value>       \
    { CEREAL_MAKE_IS_SPECIALIZED_ASSERT(name, versioned_name, name, spec_name); };          \
    template <class T, class A>                                                             \
    struct is_specialized_##versioned_name : std::integral_constant<bool,                   \
      is_specialized<T, A>::value && detail::is_specialized_##spec_name<T, A>::value>       \
    { CEREAL_MAKE_IS_SPECIALIZED_ASSERT(name, versioned_name, versioned_name, spec_name); }

    CEREAL_MAKE_IS_SPECIALIZED(member_serialize, member_versioned_serialize, member_serialize);
    CEREAL_MAKE_IS_SPECIALIZED(non_member_serialize, non_member_versioned_serialize, non_member_serialize);

    CEREAL_MAKE_IS_SPECIALIZED(member_save, member_versioned_save, member_load_save);
    CEREAL_MAKE_IS_SPECIALIZED(non_member_save, non_member_versioned_save, non_member_load_save);
    CEREAL_MAKE_IS_SPECIALIZED(member_load, member_versioned_load, member_load_save);
    CEREAL_MAKE_IS_SPECIALIZED(non_member_load, non_member_versioned_load, non_member_load_save);

    CEREAL_MAKE_IS_SPECIALIZED(member_save_minimal, member_versioned_save_minimal, member_load_save_minimal);
    CEREAL_MAKE_IS_SPECIALIZED(non_member_save_minimal, non_member_versioned_save_minimal, non_member_load_save_minimal);
    CEREAL_MAKE_IS_SPECIALIZED(member_load_minimal, member_versioned_load_minimal, member_load_save_minimal);
    CEREAL_MAKE_IS_SPECIALIZED(non_member_load_minimal, non_member_versioned_load_minimal, non_member_load_save_minimal);

    #undef CEREAL_MAKE_IS_SPECIALIZED_ASSERT
    #undef CEREAL_MAKE_IS_SPECIALIZED

    // ######################################################################
    // detects if a type has any active minimal output serialization
    template <class T, class OutputArchive>
    struct has_minimal_output_serialization : std::integral_constant<bool,
      is_specialized_member_save_minimal<T, OutputArchive>::value ||
      ((has_member_save_minimal<T, OutputArchive>::value ||
        has_non_member_save_minimal<T, OutputArchive>::value ||
        has_member_versioned_save_minimal<T, OutputArchive>::value ||
        has_non_member_versioned_save_minimal<T, OutputArchive>::value) &&
       !(is_specialized_member_serialize<T, OutputArchive>::value ||
         is_specialized_member_save<T, OutputArchive>::value))> {};

    // ######################################################################
    // detects if a type has any active minimal input serialization
    template <class T, class InputArchive>
    struct has_minimal_input_serialization : std::integral_constant<bool,
      is_specialized_member_load_minimal<T, InputArchive>::value ||
      ((has_member_load_minimal<T, InputArchive>::value ||
        has_non_member_load_minimal<T, InputArchive>::value ||
        has_member_versioned_load_minimal<T, InputArchive>::value ||
        has_non_member_versioned_load_minimal<T, InputArchive>::value) &&
       !(is_specialized_member_serialize<T, InputArchive>::value ||
         is_specialized_member_load<T, InputArchive>::value))> {};

    // ######################################################################
    namespace detail
    {
      //! The number of output serialization functions available
      /*! If specialization is being used, we'll count only those; otherwise we'll count everything */
      template <class T, class OutputArchive>
      struct count_output_serializers : std::integral_constant<int,
        count_specializations<T, OutputArchive>::value ? count_specializations<T, OutputArchive>::value :
        has_member_save<T, OutputArchive>::value +
        has_non_member_save<T, OutputArchive>::value +
        has_member_serialize<T, OutputArchive>::value +
        has_non_member_serialize<T, OutputArchive>::value +
        has_member_save_minimal<T, OutputArchive>::value +
        has_non_member_save_minimal<T, OutputArchive>::value +
        /*-versioned---------------------------------------------------------*/
        has_member_versioned_save<T, OutputArchive>::value +
        has_non_member_versioned_save<T, OutputArchive>::value +
        has_member_versioned_serialize<T, OutputArchive>::value +
        has_non_member_versioned_serialize<T, OutputArchive>::value +
        has_member_versioned_save_minimal<T, OutputArchive>::value +
        has_non_member_versioned_save_minimal<T, OutputArchive>::value> {};
    }

    template <class T, class OutputArchive>
    struct is_output_serializable : std::integral_constant<bool,
      detail::count_output_serializers<T, OutputArchive>::value == 1> {};

    // ######################################################################
    namespace detail
    {
      //! The number of input serialization functions available
      /*! If specialization is being used, we'll count only those; otherwise we'll count everything */
      template <class T, class InputArchive>
      struct count_input_serializers : std::integral_constant<int,
        count_specializations<T, InputArchive>::value ? count_specializations<T, InputArchive>::value :
        has_member_load<T, InputArchive>::value +
        has_non_member_load<T, InputArchive>::value +
        has_member_serialize<T, InputArchive>::value +
        has_non_member_serialize<T, InputArchive>::value +
        has_member_load_minimal<T, InputArchive>::value +
        has_non_member_load_minimal<T, InputArchive>::value +
        /*-versioned---------------------------------------------------------*/
        has_member_versioned_load<T, InputArchive>::value +
        has_non_member_versioned_load<T, InputArchive>::value +
        has_member_versioned_serialize<T, InputArchive>::value +
        has_non_member_versioned_serialize<T, InputArchive>::value +
        has_member_versioned_load_minimal<T, InputArchive>::value +
        has_non_member_versioned_load_minimal<T, InputArchive>::value> {};
    }

    template <class T, class InputArchive>
    struct is_input_serializable : std::integral_constant<bool,
      detail::count_input_serializers<T, InputArchive>::value == 1> {};

    // ######################################################################
    // Base Class Support
    namespace detail
    {
      struct base_class_id
      {
        template<class T>
          base_class_id(T const * const t) :
          type(typeid(T)),
          ptr(t),
          hash(std::hash<std::type_index>()(typeid(T)) ^ (std::hash<void const *>()(t) << 1))
          { }

          bool operator==(base_class_id const & other) const
          { return (type == other.type) && (ptr == other.ptr); }

          std::type_index type;
          void const * ptr;
          size_t hash;
      };
      struct base_class_id_hash { size_t operator()(base_class_id const & id) const { return id.hash; }  };
    } // namespace detail

    namespace detail
    {
      //! Common base type for base class casting
      struct BaseCastBase {};

      template <class>
      struct get_base_class;

      template <template<typename> class Cast, class Base>
      struct get_base_class<Cast<Base>>
      {
        using type = Base;
      };

      //! Base class cast, behave as the test
      template <class Cast, template<class, class> class Test, class Archive,
                bool IsBaseCast = std::is_base_of<BaseCastBase, Cast>::value>
      struct has_minimal_base_class_serialization_impl : Test<typename get_base_class<Cast>::type, Archive>
      { };

      //! Not a base class cast
      template <class Cast, template<class, class> class Test, class Archive>
      struct has_minimal_base_class_serialization_impl<Cast,Test, Archive, false> : std::false_type
      { };
    }

    //! Checks to see if the base class used in a cast has a minimal serialization
    /*! @tparam Cast Either base_class or virtual_base_class wrapped type
        @tparam Test A has_minimal test (for either input or output)
        @tparam Archive The archive to use with the test */
    template <class Cast, template<class, class> class Test, class Archive>
    struct has_minimal_base_class_serialization : detail::has_minimal_base_class_serialization_impl<Cast, Test, Archive>
    { };


    // ######################################################################
    namespace detail
    {
      struct shared_from_this_wrapper
      {
        template <class U>
        static auto (check)( U const & t ) -> decltype( ::cereal::access::shared_from_this(t), std::true_type() );

        static auto (check)( ... ) -> decltype( std::false_type() );

        template <class U>
        static auto get( U const & t ) -> decltype( t.shared_from_this() );
      };
    }

    //! Determine if T or any base class of T has inherited from std::enable_shared_from_this
    template<class T>
    struct has_shared_from_this : decltype((detail::shared_from_this_wrapper::check)(std::declval<T>()))
    { };

    //! Get the type of the base class of T which inherited from std::enable_shared_from_this
    template <class T>
    struct get_shared_from_this_base
    {
      private:
        using PtrType = decltype(detail::shared_from_this_wrapper::get(std::declval<T>()));
      public:
        //! The type of the base of T that inherited from std::enable_shared_from_this
        using type = typename std::decay<typename PtrType::element_type>::type;
    };

    // ######################################################################
    //! Extracts the true type from something possibly wrapped in a cereal NoConvert
    /*! Internally cereal uses some wrapper classes to test the validity of non-member
        minimal load and save functions.  This can interfere with user type traits on
        templated load and save minimal functions.  To get to the correct underlying type,
        users should use strip_minimal when performing any enable_if type type trait checks.

        See the enum serialization in types/common.hpp for an example of using this */
    template <class T, bool IsCerealMinimalTrait = std::is_base_of<detail::NoConvertBase, T>::value>
    struct strip_minimal
    {
      using type = T;
    };

    //! Specialization for types wrapped in a NoConvert
    template <class T>
    struct strip_minimal<T, true>
    {
      using type = typename T::type;
    };

    // ######################################################################
    //! Determines whether the class T can be default constructed by cereal::access
    template <class T>
    struct is_default_constructible
    {
      #ifdef CEREAL_OLDER_GCC
      template <class TT, class SFINAE = void>
      struct test : no {};
      template <class TT>
      struct test<TT, typename detail::Void< decltype( cereal::access::construct<TT>() ) >::type> : yes {};
      static const bool value = test<T>();
      #else // NOT CEREAL_OLDER_GCC =========================================
      template <class TT>
      static auto test(int) -> decltype( cereal::access::construct<TT>(), yes());
      template <class>
      static no test(...);
      static const bool value = std::is_same<decltype(test<T>(0)), yes>::value;
      #endif // NOT CEREAL_OLDER_GCC
    };

    // ######################################################################
    namespace detail
    {
      //! Removes all qualifiers and minimal wrappers from an archive
      template <class A>
      using decay_archive = typename std::decay<typename strip_minimal<A>::type>::type;
    }

    //! Checks if the provided archive type is equal to some cereal archive type
    /*! This automatically does things such as std::decay and removing any other wrappers that may be
        on the Archive template parameter.

        Example use:
        @code{cpp}
        // example use to disable a serialization function
        template <class Archive, EnableIf<cereal::traits::is_same_archive<Archive, cereal::BinaryOutputArchive>::value> = sfinae>
        void save( Archive & ar, MyType const & mt );
        @endcode */
    template <class ArchiveT, class CerealArchiveT>
    struct is_same_archive : std::integral_constant<bool,
      std::is_same<detail::decay_archive<ArchiveT>, CerealArchiveT>::value>
    { };

    // ######################################################################
    //! A macro to use to restrict which types of archives your function will work for.
    /*! This requires you to have a template class parameter named Archive and replaces the void return
        type for your function.

        INTYPE refers to the input archive type you wish to restrict on.
        OUTTYPE refers to the output archive type you wish to restrict on.

        For example, if we want to limit a serialize to only work with binary serialization:

        @code{.cpp}
        template <class Archive>
        CEREAL_ARCHIVE_RESTRICT(BinaryInputArchive, BinaryOutputArchive)
        serialize( Archive & ar, MyCoolType & m )
        {
          ar & m;
        }
        @endcode

        If you need to do more restrictions in your enable_if, you will need to do this by hand.
     */
    #define CEREAL_ARCHIVE_RESTRICT(INTYPE, OUTTYPE) \
    typename std::enable_if<cereal::traits::is_same_archive<Archive, INTYPE>::value || cereal::traits::is_same_archive<Archive, OUTTYPE>::value, void>::type

    //! Type traits only struct used to mark an archive as human readable (text based)
    /*! Archives that wish to identify as text based/human readable should inherit from
        this struct */
    struct TextArchive {};

    //! Checks if an archive is a text archive (human readable)
    template <class A>
    struct is_text_archive : std::integral_constant<bool,
      std::is_base_of<TextArchive, detail::decay_archive<A>>::value>
    { };
  } // namespace traits

  // ######################################################################
  namespace detail
  {
    template <class T, class A,
              bool Member = traits::has_member_load_and_construct<T, A>::value,
              bool MemberVersioned = traits::has_member_versioned_load_and_construct<T, A>::value,
              bool NonMember = traits::has_non_member_load_and_construct<T, A>::value,
              bool NonMemberVersioned = traits::has_non_member_versioned_load_and_construct<T, A>::value>
    struct Construct
    {
      static_assert( cereal::traits::detail::delay_static_assert<T>::value,
        "cereal found more than one compatible load_and_construct function for the provided type and archive combination. \n\n "
        "Types must either have a member load_and_construct function or a non-member specialization of LoadAndConstruct (you may not mix these). \n "
        "In addition, you may not mix versioned with non-versioned load_and_construct functions. \n\n " );
      static T * load_andor_construct( A & /*ar*/, construct<T> & /*construct*/ )
      { return nullptr; }
    };

    // no load and construct case
    template <class T, class A>
    struct Construct<T, A, false, false, false, false>
    {
      static_assert( ::cereal::traits::is_default_constructible<T>::value,
                     "Trying to serialize a an object with no default constructor. \n\n "
                     "Types must either be default constructible or define either a member or non member Construct function. \n "
                     "Construct functions generally have the signature: \n\n "
                     "template <class Archive> \n "
                     "static void load_and_construct(Archive & ar, cereal::construct<T> & construct) \n "
                     "{ \n "
                     "  var a; \n "
                     "  ar( a ) \n "
                     "  construct( a ); \n "
                     "} \n\n" );
      static T * load_andor_construct()
      { return ::cereal::access::construct<T>(); }
    };

    // member non-versioned
    template <class T, class A>
    struct Construct<T, A, true, false, false, false>
    {
      static void load_andor_construct( A & ar, construct<T> & construct )
      {
        access::load_and_construct<T>( ar, construct );
      }
    };

    // member versioned
    template <class T, class A>
    struct Construct<T, A, false, true, false, false>
    {
      static void load_andor_construct( A & ar, construct<T> & construct )
      {
        const auto version = ar.template loadClassVersion<T>();
        access::load_and_construct<T>( ar, construct, version );
      }
    };

    // non-member non-versioned
    template <class T, class A>
    struct Construct<T, A, false, false, true, false>
    {
      static void load_andor_construct( A & ar, construct<T> & construct )
      {
        LoadAndConstruct<T>::load_and_construct( ar, construct );
      }
    };

    // non-member versioned
    template <class T, class A>
    struct Construct<T, A, false, false, false, true>
    {
      static void load_andor_construct( A & ar, construct<T> & construct )
      {
        const auto version = ar.template loadClassVersion<T>();
        LoadAndConstruct<T>::load_and_construct( ar, construct, version );
      }
    };
  } // namespace detail
} // namespace cereal






/*! \file static_object.hpp
    \brief Internal polymorphism static object support
    \ingroup Internal */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#if CEREAL_THREAD_SAFE
#include <mutex>
#endif

//! Prevent link optimization from removing non-referenced static objects
/*! Especially for polymorphic support, we create static objects which
    may not ever be explicitly referenced.  Most linkers will detect this
    and remove the code causing various unpleasant runtime errors.  These
    macros, adopted from Boost (see force_include.hpp) prevent this
    (C) Copyright 2002 Robert Ramey - http://www.rrsd.com .
    Use, modification and distribution is subject to the Boost Software
    License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
    http://www.boost.org/LICENSE_1_0.txt) */

#ifdef _MSC_VER
#   define CEREAL_DLL_EXPORT __declspec(dllexport)
#   define CEREAL_USED
#else // clang or gcc
#   define CEREAL_DLL_EXPORT
#   define CEREAL_USED __attribute__ ((__used__))
#endif

namespace cereal
{
  namespace detail
  {
    //! A static, pre-execution object
    /*! This class will create a single copy (singleton) of some
        type and ensures that merely referencing this type will
        cause it to be instantiated and initialized pre-execution.
        For example, this is used heavily in the polymorphic pointer
        serialization mechanisms to bind various archive types with
        different polymorphic classes */
    template <class T>
    class CEREAL_DLL_EXPORT StaticObject
    {
      private:
        //! Forces instantiation at pre-execution time
        static void instantiate( T const & ) {}

        static T & create()
        {
          static T t;
          instantiate(instance);
          return t;
        }

        StaticObject( StaticObject const & /*other*/ ) {}

      public:
        static T & getInstance()
        {
          return create();
        }

        //! A class that acts like std::lock_guard
        class LockGuard
        {
          #if CEREAL_THREAD_SAFE
          public:
            LockGuard(std::mutex & m) : lock(m) {}
          private:
            std::unique_lock<std::mutex> lock;
          #else
          public:
            ~LockGuard() CEREAL_NOEXCEPT {} // prevents variable not used
          #endif
        };

        //! Attempts to lock this static object for the current scope
        /*! @note This function is a no-op if cereal is not compiled with
                  thread safety enabled (CEREAL_THREAD_SAFE = 1).

            This function returns an object that holds a lock for
            this StaticObject that will release its lock upon destruction. This
            call will block until the lock is available. */
        static LockGuard lock()
        {
          #if CEREAL_THREAD_SAFE
          static std::mutex instanceMutex;
          return LockGuard{instanceMutex};
          #else
          return LockGuard{};
          #endif
        }

      private:
        static T & instance;
    };

    template <class T> T & StaticObject<T>::instance = StaticObject<T>::create();
  } // namespace detail
} // namespace cereal














/*! \file polymorphic_impl_fwd.hpp
    \brief Internal polymorphism support forward declarations
    \ingroup Internal */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* This code is heavily inspired by the boost serialization implementation by the following authors

   (C) Copyright 2002 Robert Ramey - http://www.rrsd.com .
   Use, modification and distribution is subject to the Boost Software
   License, Version 1.0. (See http://www.boost.org/LICENSE_1_0.txt)

    See http://www.boost.org for updates, documentation, and revision history.

   (C) Copyright 2006 David Abrahams - http://www.boost.org.

   See /boost/serialization/export.hpp and /boost/archive/detail/register_archive.hpp for their
   implementation.
*/



namespace cereal
{
  namespace detail
  {
    //! Forward declaration, see polymorphic_impl.hpp for more information
    template <class Base, class Derived>
    struct RegisterPolymorphicCaster;

    //! Forward declaration, see polymorphic_impl.hpp for more information
    struct PolymorphicCasters;

    //! Forward declaration, see polymorphic_impl.hpp for more information
    template <class Base, class Derived>
    struct PolymorphicRelation;
  } // namespace detail
} // namespace cereal






/*! \file base_class.hpp
    \brief Support for base classes (virtual and non-virtual)
    \ingroup OtherTypes */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/




namespace cereal
{
  namespace base_class_detail
  {
    //! Used to register polymorphic relations and avoid the need to include
    //! polymorphic.hpp when no polymorphism is used
    /*! @internal */
    template <class Base, class Derived, bool IsPolymorphic = std::is_polymorphic<Base>::value>
    struct RegisterPolymorphicBaseClass
    {
      static void bind()
      { }
    };

    //! Polymorphic version
    /*! @internal */
    template <class Base, class Derived>
    struct RegisterPolymorphicBaseClass<Base, Derived, true>
    {
      static void bind()
      { detail::RegisterPolymorphicCaster<Base, Derived>::bind(); }
    };
  }

  //! Casts a derived class to its non-virtual base class in a way that safely supports abstract classes
  /*! This should be used in cases when a derived type needs to serialize its base type. This is better than directly
      using static_cast, as it allows for serialization of pure virtual (abstract) base classes.

      This also automatically registers polymorphic relation between the base and derived class, assuming they
      are indeed polymorphic. Note this is not the same as polymorphic type registration. For more information
      see the documentation on polymorphism.

      \sa virtual_base_class

      @code{.cpp}
      struct MyBase
      {
        int x;

        virtual void foo() = 0;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( x );
        }
      };

      struct MyDerived : public MyBase //<-- Note non-virtual inheritance
      {
        int y;

        virtual void foo() {};

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( cereal::base_class<MyBase>(this) );
          ar( y );
        }
      };
      @endcode */
  template<class Base>
    struct base_class : private traits::detail::BaseCastBase
    {
      template<class Derived>
        base_class(Derived const * derived) :
          base_ptr(const_cast<Base*>(static_cast<Base const *>(derived)))
      {
        static_assert( std::is_base_of<Base, Derived>::value, "Can only use base_class on a valid base class" );
        base_class_detail::RegisterPolymorphicBaseClass<Base, Derived>::bind();
      }

        Base * base_ptr;
    };

  //! Casts a derived class to its virtual base class in a way that allows cereal to track inheritance
  /*! This should be used in cases when a derived type features virtual inheritance from some
      base type.  This allows cereal to track the inheritance and to avoid making duplicate copies
      during serialization.

      It is safe to use virtual_base_class in all circumstances for serializing base classes, even in cases
      where virtual inheritance does not take place, though it may be slightly faster to utilize
      cereal::base_class<> if you do not need to worry about virtual inheritance.

      This also automatically registers polymorphic relation between the base and derived class, assuming they
      are indeed polymorphic. Note this is not the same as polymorphic type registration. For more information
      see the documentation on polymorphism.

      \sa base_class

      @code{.cpp}
      struct MyBase
      {
        int x;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( x );
        }
      };

      struct MyLeft : virtual MyBase //<-- Note the virtual inheritance
      {
        int y;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( cereal::virtual_base_class<MyBase>( this ) );
          ar( y );
        }
      };

      struct MyRight : virtual MyBase
      {
        int z;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( cereal::virtual_base_clas<MyBase>( this ) );
          ar( z );
        }
      };

      // diamond virtual inheritance; contains one copy of each base class
      struct MyDerived : virtual MyLeft, virtual MyRight
      {
        int a;

        template <class Archive>
        void serialize( Archive & ar )
        {
          ar( cereal::virtual_base_class<MyLeft>( this ) );  // safely serialize data members in MyLeft
          ar( cereal::virtual_base_class<MyRight>( this ) ); // safely serialize data members in MyRight
          ar( a );

          // Because we used virtual_base_class, cereal will ensure that only one instance of MyBase is
          // serialized as we traverse the inheritance heirarchy. This means that there will be one copy
          // each of the variables x, y, z, and a

          // If we had chosen to use static_cast<> instead, cereal would perform no tracking and
          // assume that every base class should be serialized (in this case leading to a duplicate
          // serialization of MyBase due to diamond inheritance
      };
     }
     @endcode */
  template<class Base>
    struct virtual_base_class : private traits::detail::BaseCastBase
    {
      template<class Derived>
        virtual_base_class(Derived const * derived) :
          base_ptr(const_cast<Base*>(static_cast<Base const *>(derived)))
      {
        static_assert( std::is_base_of<Base, Derived>::value, "Can only use virtual_base_class on a valid base class" );
        base_class_detail::RegisterPolymorphicBaseClass<Base, Derived>::bind();
      }

        Base * base_ptr;
    };

} // namespace cereal









/*! \file cereal.hpp
    \brief Main cereal functionality */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <type_traits>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <cstddef>
#include <cstdint>
#include <functional>



namespace cereal
{
  // ######################################################################
  //! Creates a name value pair
  /*! @relates NameValuePair
      @ingroup Utility */
  template <class T> inline
  NameValuePair<T> make_nvp( std::string const & name, T && value )
  {
    return {name.c_str(), std::forward<T>(value)};
  }

  //! Creates a name value pair
  /*! @relates NameValuePair
      @ingroup Utility */
  template <class T> inline
  NameValuePair<T> make_nvp( const char * name, T && value )
  {
    return {name, std::forward<T>(value)};
  }

  //! Creates a name value pair for the variable T with the same name as the variable
  /*! @relates NameValuePair
      @ingroup Utility */
  #define CEREAL_NVP(T) ::cereal::make_nvp(#T, T)

  // ######################################################################
  //! Convenience function to create binary data for both const and non const pointers
  /*! @param data Pointer to beginning of the data
      @param size The size in bytes of the data
      @relates BinaryData
      @ingroup Utility */
  template <class T> inline
  BinaryData<T> binary_data( T && data, size_t size )
  {
    return {std::forward<T>(data), size};
  }

  // ######################################################################
  //! Creates a size tag from some variable.
  /*! Will normally be used to serialize size (e.g. size()) information for
      variable size containers.  If you have a variable sized container,
      the very first thing it serializes should be its size, wrapped in
      a SizeTag.

      @relates SizeTag
      @ingroup Utility */
  template <class T> inline
  SizeTag<T> make_size_tag( T && sz )
  {
    return {std::forward<T>(sz)};
  }

  // ######################################################################
  //! Called before a type is serialized to set up any special archive state
  //! for processing some type
  /*! If designing a serializer that needs to set up any kind of special
      state or output extra information for a type, specialize this function
      for the archive type and the types that require the extra information.
      @ingroup Internal */
  template <class Archive, class T> inline
  void prologue( Archive & /* archive */, T const & /* data */)
  { }

  //! Called after a type is serialized to tear down any special archive state
  //! for processing some type
  /*! @ingroup Internal */
  template <class Archive, class T> inline
  void epilogue( Archive & /* archive */, T const & /* data */)
  { }

  // ######################################################################
  //! Special flags for archives
  /*! AllowEmptyClassElision
        This allows for empty classes to be serialized even if they do not provide
        a serialization function.  Classes with no data members are considered to be
        empty.  Be warned that if this is enabled and you attempt to serialize an
        empty class with improperly formed serialize or load/save functions, no
        static error will occur - the error will propogate silently and your
        intended serialization functions may not be called.  You can manually
        ensure that your classes that have custom serialization are correct
        by using the traits is_output_serializable and is_input_serializable
        in cereal/details/traits.hpp.
      @ingroup Internal */
  enum Flags { AllowEmptyClassElision = 1 };

  // ######################################################################
  //! Registers a specific Archive type with cereal
  /*! This registration should be done once per archive.  A good place to
      put this is immediately following the definition of your archive.
      Archive registration is only strictly necessary if you wish to
      support pointers to polymorphic data types.  All archives that
      come with cereal are already registered.
      @ingroup Internal */
  #define CEREAL_REGISTER_ARCHIVE(Archive)                              \
  namespace cereal { namespace detail {                                 \
  template <class T, class BindingTag>                                  \
  typename polymorphic_serialization_support<Archive, T>::type          \
  instantiate_polymorphic_binding( T*, Archive*, BindingTag, adl_tag ); \
  } } /* end namespaces */

  // ######################################################################
  //! Defines a class version for some type
  /*! Versioning information is optional and adds some small amount of
      overhead to serialization.  This overhead will occur both in terms of
      space in the archive (the version information for each class will be
      stored exactly once) as well as runtime (versioned serialization functions
      must check to see if they need to load or store version information).

      Versioning is useful if you plan on fundamentally changing the way some
      type is serialized in the future.  Versioned serialization functions
      cannot be used to load non-versioned data.

      By default, all types have an assumed version value of zero.  By
      using this macro, you may change the version number associated with
      some type.  cereal will then use this value as a second parameter
      to your serialization functions.

      The interface for the serialization functions is nearly identical
      to non-versioned serialization with the addition of a second parameter,
      const std::uint32_t version, which will be supplied with the correct
      version number.  Serializing the version number on a save happens
      automatically.

      Versioning cannot be mixed with non-versioned serialization functions.
      Having both types will result result in a compile time error.  Data
      serialized without versioning cannot be loaded by a serialization
      function with added versioning support.

      Example interface for versioning on a non-member serialize function:

      @code{cpp}
      CEREAL_CLASS_VERSION( Mytype, 77 ); // register class version

      template <class Archive>
      void serialize( Archive & ar, Mytype & t, const std::uint32_t version )
      {
        // When performing a load, the version associated with the class
        // is whatever it was when that data was originally serialized
        //
        // When we save, we'll use the version that is defined in the macro

        if( version >= some_number )
          // do this
        else
          // do that
      }
      @endcode

      Interfaces for other forms of serialization functions is similar.  This
      macro should be placed at global scope.
      @ingroup Utility */
  #define CEREAL_CLASS_VERSION(TYPE, VERSION_NUMBER)                             \
  namespace cereal { namespace detail {                                          \
    template <> struct Version<TYPE>                                             \
    {                                                                            \
      static const std::uint32_t version;                                        \
      static std::uint32_t registerVersion()                                     \
      {                                                                          \
        ::cereal::detail::StaticObject<Versions>::getInstance().mapping.emplace( \
             std::type_index(typeid(TYPE)).hash_code(), VERSION_NUMBER );        \
        return VERSION_NUMBER;                                                   \
      }                                                                          \
      static void unused() { (void)version; }                                    \
    }; /* end Version */                                                         \
    const std::uint32_t Version<TYPE>::version =                                 \
      Version<TYPE>::registerVersion();                                          \
  } } // end namespaces

  // ######################################################################
  //! The base output archive class
  /*! This is the base output archive for all output archives.  If you create
      a custom archive class, it should derive from this, passing itself as
      a template parameter for the ArchiveType.

      The base class provides all of the functionality necessary to
      properly forward data to the correct serialization functions.

      Individual archives should use a combination of prologue and
      epilogue functions together with specializations of serialize, save,
      and load to alter the functionality of their serialization.

      @tparam ArchiveType The archive type that derives from OutputArchive
      @tparam Flags Flags to control advanced functionality.  See the Flags
                    enum for more information.
      @ingroup Internal */
  template<class ArchiveType, std::uint32_t Flags = 0>
  class OutputArchive : public detail::OutputArchiveBase
  {
    public:
      //! Construct the output archive
      /*! @param derived A pointer to the derived ArchiveType (pass this from the derived archive) */
      OutputArchive(ArchiveType * const derived) : self(derived), itsCurrentPointerId(1), itsCurrentPolymorphicTypeId(1)
      { }

      OutputArchive & operator=( OutputArchive const & ) = delete;

      //! Serializes all passed in data
      /*! This is the primary interface for serializing data with an archive */
      template <class ... Types> inline
      ArchiveType & operator()( Types && ... args )
      {
        self->process( std::forward<Types>( args )... );
        return *self;
      }

      /*! @name Boost Transition Layer
          Functionality that mirrors the syntax for Boost.  This is useful if you are transitioning
          a large project from Boost to cereal.  The preferred interface for cereal is using operator(). */
      //! @{

      //! Indicates this archive is not intended for loading
      /*! This ensures compatibility with boost archive types.  If you are transitioning
          from boost, you can check this value within a member or external serialize function
          (i.e., Archive::is_loading::value) to disable behavior specific to loading, until 
          you can transition to split save/load or save_minimal/load_minimal functions */
      using is_loading = std::false_type;

      //! Indicates this archive is intended for saving
      /*! This ensures compatibility with boost archive types.  If you are transitioning
          from boost, you can check this value within a member or external serialize function
          (i.e., Archive::is_saving::value) to enable behavior specific to loading, until 
          you can transition to split save/load or save_minimal/load_minimal functions */
      using is_saving = std::true_type;

      //! Serializes passed in data
      /*! This is a boost compatability layer and is not the preferred way of using
          cereal.  If you are transitioning from boost, use this until you can
          transition to the operator() overload */
      template <class T> inline
      ArchiveType & operator&( T && arg )
      {
        self->process( std::forward<T>( arg ) );
        return *self;
      }

      //! Serializes passed in data
      /*! This is a boost compatability layer and is not the preferred way of using
          cereal.  If you are transitioning from boost, use this until you can
          transition to the operator() overload */
      template <class T> inline
      ArchiveType & operator<<( T && arg )
      {
        self->process( std::forward<T>( arg ) );
        return *self;
      }

      //! @}

      //! Registers a shared pointer with the archive
      /*! This function is used to track shared pointer targets to prevent
          unnecessary saves from taking place if multiple shared pointers
          point to the same data.

          @internal
          @param addr The address (see shared_ptr get()) pointed to by the shared pointer
          @return A key that uniquely identifies the pointer */
      inline std::uint32_t registerSharedPointer( void const * addr )
      {
        // Handle null pointers by just returning 0
        if(addr == 0) return 0;

        auto id = itsSharedPointerMap.find( addr );
        if( id == itsSharedPointerMap.end() )
        {
          auto ptrId = itsCurrentPointerId++;
          itsSharedPointerMap.insert( {addr, ptrId} );
          return ptrId | detail::msb_32bit; // mask MSB to be 1
        }
        else
          return id->second;
      }

      //! Registers a polymorphic type name with the archive
      /*! This function is used to track polymorphic types to prevent
          unnecessary saves of identifying strings used by the polymorphic
          support functionality.

          @internal
          @param name The name to associate with a polymorphic type
          @return A key that uniquely identifies the polymorphic type name */
      inline std::uint32_t registerPolymorphicType( char const * name )
      {
        auto id = itsPolymorphicTypeMap.find( name );
        if( id == itsPolymorphicTypeMap.end() )
        {
          auto polyId = itsCurrentPolymorphicTypeId++;
          itsPolymorphicTypeMap.insert( {name, polyId} );
          return polyId | detail::msb_32bit; // mask MSB to be 1
        }
        else
          return id->second;
      }

    private:
      //! Serializes data after calling prologue, then calls epilogue
      template <class T> inline
      void process( T && head )
      {
        prologue( *self, head );
        self->processImpl( head );
        epilogue( *self, head );
      }

      //! Unwinds to process all data
      template <class T, class ... Other> inline
      void process( T && head, Other && ... tail )
      {
        self->process( std::forward<T>( head ) );
        self->process( std::forward<Other>( tail )... );
      }

      //! Serialization of a virtual_base_class wrapper
      /*! \sa virtual_base_class */
      template <class T> inline
      ArchiveType & processImpl(virtual_base_class<T> const & b)
      {
        traits::detail::base_class_id id(b.base_ptr);
        if(itsBaseClassSet.count(id) == 0)
        {
          itsBaseClassSet.insert(id);
          self->processImpl( *b.base_ptr );
        }
        return *self;
      }

      //! Serialization of a base_class wrapper
      /*! \sa base_class */
      template <class T> inline
      ArchiveType & processImpl(base_class<T> const & b)
      {
        self->processImpl( *b.base_ptr );
        return *self;
      }

      //! Helper macro that expands the requirements for activating an overload
      /*! Requirements:
            Has the requested serialization function
            Does not have version and unversioned at the same time
            Is output serializable AND
              is specialized for this type of function OR
              has no specialization at all */
      #define PROCESS_IF(name)                                                             \
      traits::EnableIf<traits::has_##name<T, ArchiveType>::value,                          \
                       !traits::has_invalid_output_versioning<T, ArchiveType>::value,      \
                       (traits::is_output_serializable<T, ArchiveType>::value &&           \
                        (traits::is_specialized_##name<T, ArchiveType>::value ||           \
                         !traits::is_specialized<T, ArchiveType>::value))> = traits::sfinae

      //! Member serialization
      template <class T, PROCESS_IF(member_serialize)> inline
      ArchiveType & processImpl(T const & t)
      {
        access::member_serialize(*self, const_cast<T &>(t));
        return *self;
      }

      //! Non member serialization
      template <class T, PROCESS_IF(non_member_serialize)> inline
      ArchiveType & processImpl(T const & t)
      {
        CEREAL_SERIALIZE_FUNCTION_NAME(*self, const_cast<T &>(t));
        return *self;
      }

      //! Member split (save)
      template <class T, PROCESS_IF(member_save)> inline
      ArchiveType & processImpl(T const & t)
      {
        access::member_save(*self, t);
        return *self;
      }

      //! Non member split (save)
      template <class T, PROCESS_IF(non_member_save)> inline
      ArchiveType & processImpl(T const & t)
      {
        CEREAL_SAVE_FUNCTION_NAME(*self, t);
        return *self;
      }

      //! Member split (save_minimal)
      template <class T, PROCESS_IF(member_save_minimal)> inline
      ArchiveType & processImpl(T const & t)
      {
        self->process( access::member_save_minimal(*self, t) );
        return *self;
      }

      //! Non member split (save_minimal)
      template <class T, PROCESS_IF(non_member_save_minimal)> inline
      ArchiveType & processImpl(T const & t)
      {
        self->process( CEREAL_SAVE_MINIMAL_FUNCTION_NAME(*self, t) );
        return *self;
      }

      //! Empty class specialization
      template <class T, traits::EnableIf<(Flags & AllowEmptyClassElision),
                                          !traits::is_output_serializable<T, ArchiveType>::value,
                                          std::is_empty<T>::value> = traits::sfinae> inline
      ArchiveType & processImpl(T const &)
      {
        return *self;
      }

      //! No matching serialization
      /*! Invalid if we have invalid output versioning or
          we are not output serializable, and either
          don't allow empty class ellision or allow it but are not serializing an empty class */
      template <class T, traits::EnableIf<traits::has_invalid_output_versioning<T, ArchiveType>::value ||
                                          (!traits::is_output_serializable<T, ArchiveType>::value &&
                                           (!(Flags & AllowEmptyClassElision) || ((Flags & AllowEmptyClassElision) && !std::is_empty<T>::value)))> = traits::sfinae> inline
      ArchiveType & processImpl(T const &)
      {
        static_assert(traits::detail::count_output_serializers<T, ArchiveType>::value != 0,
            "cereal could not find any output serialization functions for the provided type and archive combination. \n\n "
            "Types must either have a serialize function, load/save pair, or load_minimal/save_minimal pair (you may not mix these). \n "
            "Serialize functions generally have the following signature: \n\n "
            "template<class Archive> \n "
            "  void serialize(Archive & ar) \n "
            "  { \n "
            "    ar( member1, member2, member3 ); \n "
            "  } \n\n " );

        static_assert(traits::detail::count_output_serializers<T, ArchiveType>::value < 2,
            "cereal found more than one compatible output serialization function for the provided type and archive combination. \n\n "
            "Types must either have a serialize function, load/save pair, or load_minimal/save_minimal pair (you may not mix these). \n "
            "Use specialization (see access.hpp) if you need to disambiguate between serialize vs load/save functions.  \n "
            "Note that serialization functions can be inherited which may lead to the aforementioned ambiguities. \n "
            "In addition, you may not mix versioned with non-versioned serialization functions. \n\n ");

        return *self;
      }

      //! Registers a class version with the archive and serializes it if necessary
      /*! If this is the first time this class has been serialized, we will record its
          version number and serialize that.

          @tparam T The type of the class being serialized
          @param version The version number associated with it */
      template <class T> inline
      std::uint32_t registerClassVersion()
      {
        static const auto hash = std::type_index(typeid(T)).hash_code();
        const auto insertResult = itsVersionedTypes.insert( hash );
        const auto lock = detail::StaticObject<detail::Versions>::lock();
        const auto version =
          detail::StaticObject<detail::Versions>::getInstance().find( hash, detail::Version<T>::version );

        if( insertResult.second ) // insertion took place, serialize the version number
          process( make_nvp<ArchiveType>("cereal_class_version", version) );

        return version;
      }

      //! Member serialization
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_serialize)> inline
      ArchiveType & processImpl(T const & t)
      {
        access::member_serialize(*self, const_cast<T &>(t), registerClassVersion<T>());
        return *self;
      }

      //! Non member serialization
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_serialize)> inline
      ArchiveType & processImpl(T const & t)
      {
        CEREAL_SERIALIZE_FUNCTION_NAME(*self, const_cast<T &>(t), registerClassVersion<T>());
        return *self;
      }

      //! Member split (save)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_save)> inline
      ArchiveType & processImpl(T const & t)
      {
        access::member_save(*self, t, registerClassVersion<T>());
        return *self;
      }

      //! Non member split (save)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_save)> inline
      ArchiveType & processImpl(T const & t)
      {
        CEREAL_SAVE_FUNCTION_NAME(*self, t, registerClassVersion<T>());
        return *self;
      }

      //! Member split (save_minimal)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_save_minimal)> inline
      ArchiveType & processImpl(T const & t)
      {
        self->process( access::member_save_minimal(*self, t, registerClassVersion<T>()) );
        return *self;
      }

      //! Non member split (save_minimal)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_save_minimal)> inline
      ArchiveType & processImpl(T const & t)
      {
        self->process( CEREAL_SAVE_MINIMAL_FUNCTION_NAME(*self, t, registerClassVersion<T>()) );
        return *self;
      }

    #undef PROCESS_IF

    private:
      ArchiveType * const self;

      //! A set of all base classes that have been serialized
      std::unordered_set<traits::detail::base_class_id, traits::detail::base_class_id_hash> itsBaseClassSet;

      //! Maps from addresses to pointer ids
      std::unordered_map<void const *, std::uint32_t> itsSharedPointerMap;

      //! The id to be given to the next pointer
      std::uint32_t itsCurrentPointerId;

      //! Maps from polymorphic type name strings to ids
      std::unordered_map<char const *, std::uint32_t> itsPolymorphicTypeMap;

      //! The id to be given to the next polymorphic type name
      std::uint32_t itsCurrentPolymorphicTypeId;

      //! Keeps track of classes that have versioning information associated with them
      std::unordered_set<size_type> itsVersionedTypes;
  }; // class OutputArchive

  // ######################################################################
  //! The base input archive class
  /*! This is the base input archive for all input archives.  If you create
      a custom archive class, it should derive from this, passing itself as
      a template parameter for the ArchiveType.

      The base class provides all of the functionality necessary to
      properly forward data to the correct serialization functions.

      Individual archives should use a combination of prologue and
      epilogue functions together with specializations of serialize, save,
      and load to alter the functionality of their serialization.

      @tparam ArchiveType The archive type that derives from InputArchive
      @tparam Flags Flags to control advanced functionality.  See the Flags
                    enum for more information.
      @ingroup Internal */
  template<class ArchiveType, std::uint32_t Flags = 0>
  class InputArchive : public detail::InputArchiveBase
  {
    public:
      //! Construct the output archive
      /*! @param derived A pointer to the derived ArchiveType (pass this from the derived archive) */
      InputArchive(ArchiveType * const derived) :
        self(derived),
        itsBaseClassSet(),
        itsSharedPointerMap(),
        itsPolymorphicTypeMap(),
        itsVersionedTypes()
      { }

      InputArchive & operator=( InputArchive const & ) = delete;

      //! Serializes all passed in data
      /*! This is the primary interface for serializing data with an archive */
      template <class ... Types> inline
      ArchiveType & operator()( Types && ... args )
      {
        process( std::forward<Types>( args )... );
        return *self;
      }

      /*! @name Boost Transition Layer
          Functionality that mirrors the syntax for Boost.  This is useful if you are transitioning
          a large project from Boost to cereal.  The preferred interface for cereal is using operator(). */
      //! @{

      //! Indicates this archive is intended for loading
      /*! This ensures compatibility with boost archive types.  If you are transitioning
          from boost, you can check this value within a member or external serialize function
          (i.e., Archive::is_loading::value) to enable behavior specific to loading, until 
          you can transition to split save/load or save_minimal/load_minimal functions */
      using is_loading = std::true_type;

      //! Indicates this archive is not intended for saving
      /*! This ensures compatibility with boost archive types.  If you are transitioning
          from boost, you can check this value within a member or external serialize function
          (i.e., Archive::is_saving::value) to disable behavior specific to loading, until 
          you can transition to split save/load or save_minimal/load_minimal functions */
      using is_saving = std::false_type;

      //! Serializes passed in data
      /*! This is a boost compatability layer and is not the preferred way of using
          cereal.  If you are transitioning from boost, use this until you can
          transition to the operator() overload */
      template <class T> inline
      ArchiveType & operator&( T && arg )
      {
        self->process( std::forward<T>( arg ) );
        return *self;
      }

      //! Serializes passed in data
      /*! This is a boost compatability layer and is not the preferred way of using
          cereal.  If you are transitioning from boost, use this until you can
          transition to the operator() overload */
      template <class T> inline
      ArchiveType & operator>>( T && arg )
      {
        self->process( std::forward<T>( arg ) );
        return *self;
      }

      //! @}

      //! Retrieves a shared pointer given a unique key for it
      /*! This is used to retrieve a previously registered shared_ptr
          which has already been loaded.

          @param id The unique id that was serialized for the pointer
          @return A shared pointer to the data
          @throw Exception if the id does not exist */
      inline std::shared_ptr<void> getSharedPointer(std::uint32_t const id)
      {
        if(id == 0) return std::shared_ptr<void>(nullptr);

        auto iter = itsSharedPointerMap.find( id );
        if(iter == itsSharedPointerMap.end())
          throw Exception("Error while trying to deserialize a smart pointer. Could not find id " + std::to_string(id));

        return iter->second;
      }

      //! Registers a shared pointer to its unique identifier
      /*! After a shared pointer has been allocated for the first time, it should
          be registered with its loaded id for future references to it.

          @param id The unique identifier for the shared pointer
          @param ptr The actual shared pointer */
      inline void registerSharedPointer(std::uint32_t const id, std::shared_ptr<void> ptr)
      {
        std::uint32_t const stripped_id = id & ~detail::msb_32bit;
        itsSharedPointerMap[stripped_id] = ptr;
      }

      //! Retrieves the string for a polymorphic type given a unique key for it
      /*! This is used to retrieve a string previously registered during
          a polymorphic load.

          @param id The unique id that was serialized for the polymorphic type
          @return The string identifier for the tyep */
      inline std::string getPolymorphicName(std::uint32_t const id)
      {
        auto name = itsPolymorphicTypeMap.find( id );
        if(name == itsPolymorphicTypeMap.end())
        {
          throw Exception("Error while trying to deserialize a polymorphic pointer. Could not find type id " + std::to_string(id));
        }
        return name->second;
      }

      //! Registers a polymorphic name string to its unique identifier
      /*! After a polymorphic type has been loaded for the first time, it should
          be registered with its loaded id for future references to it.

          @param id The unique identifier for the polymorphic type
          @param name The name associated with the tyep */
      inline void registerPolymorphicName(std::uint32_t const id, std::string const & name)
      {
        std::uint32_t const stripped_id = id & ~detail::msb_32bit;
        itsPolymorphicTypeMap.insert( {stripped_id, name} );
      }

    private:
      //! Serializes data after calling prologue, then calls epilogue
      template <class T> inline
      void process( T && head )
      {
        prologue( *self, head );
        self->processImpl( head );
        epilogue( *self, head );
      }

      //! Unwinds to process all data
      template <class T, class ... Other> inline
      void process( T && head, Other && ... tail )
      {
        process( std::forward<T>( head ) );
        process( std::forward<Other>( tail )... );
      }

      //! Serialization of a virtual_base_class wrapper
      /*! \sa virtual_base_class */
      template <class T> inline
      ArchiveType & processImpl(virtual_base_class<T> & b)
      {
        traits::detail::base_class_id id(b.base_ptr);
        if(itsBaseClassSet.count(id) == 0)
        {
          itsBaseClassSet.insert(id);
          self->processImpl( *b.base_ptr );
        }
        return *self;
      }

      //! Serialization of a base_class wrapper
      /*! \sa base_class */
      template <class T> inline
      ArchiveType & processImpl(base_class<T> & b)
      {
        self->processImpl( *b.base_ptr );
        return *self;
      }

      //! Helper macro that expands the requirements for activating an overload
      /*! Requirements:
            Has the requested serialization function
            Does not have version and unversioned at the same time
            Is input serializable AND
              is specialized for this type of function OR
              has no specialization at all */
      #define PROCESS_IF(name)                                                              \
      traits::EnableIf<traits::has_##name<T, ArchiveType>::value,                           \
                       !traits::has_invalid_input_versioning<T, ArchiveType>::value,        \
                       (traits::is_input_serializable<T, ArchiveType>::value &&             \
                        (traits::is_specialized_##name<T, ArchiveType>::value ||            \
                         !traits::is_specialized<T, ArchiveType>::value))> = traits::sfinae

      //! Member serialization
      template <class T, PROCESS_IF(member_serialize)> inline
      ArchiveType & processImpl(T & t)
      {
        access::member_serialize(*self, t);
        return *self;
      }

      //! Non member serialization
      template <class T, PROCESS_IF(non_member_serialize)> inline
      ArchiveType & processImpl(T & t)
      {
        CEREAL_SERIALIZE_FUNCTION_NAME(*self, t);
        return *self;
      }

      //! Member split (load)
      template <class T, PROCESS_IF(member_load)> inline
      ArchiveType & processImpl(T & t)
      {
        access::member_load(*self, t);
        return *self;
      }

      //! Non member split (load)
      template <class T, PROCESS_IF(non_member_load)> inline
      ArchiveType & processImpl(T & t)
      {
        CEREAL_LOAD_FUNCTION_NAME(*self, t);
        return *self;
      }

      //! Member split (load_minimal)
      template <class T, PROCESS_IF(member_load_minimal)> inline
      ArchiveType & processImpl(T & t)
      {
        using OutArchiveType = typename traits::detail::get_output_from_input<ArchiveType>::type;
        typename traits::has_member_save_minimal<T, OutArchiveType>::type value;
        self->process( value );
        access::member_load_minimal(*self, t, value);
        return *self;
      }

      //! Non member split (load_minimal)
      template <class T, PROCESS_IF(non_member_load_minimal)> inline
      ArchiveType & processImpl(T & t)
      {
        using OutArchiveType = typename traits::detail::get_output_from_input<ArchiveType>::type;
        typename traits::has_non_member_save_minimal<T, OutArchiveType>::type value;
        self->process( value );
        CEREAL_LOAD_MINIMAL_FUNCTION_NAME(*self, t, value);
        return *self;
      }

      //! Empty class specialization
      template <class T, traits::EnableIf<(Flags & AllowEmptyClassElision),
                                          !traits::is_input_serializable<T, ArchiveType>::value,
                                          std::is_empty<T>::value> = traits::sfinae> inline
      ArchiveType & processImpl(T const &)
      {
        return *self;
      }

      //! No matching serialization
      /*! Invalid if we have invalid input versioning or
          we are not input serializable, and either
          don't allow empty class ellision or allow it but are not serializing an empty class */
      template <class T, traits::EnableIf<traits::has_invalid_input_versioning<T, ArchiveType>::value ||
                                          (!traits::is_input_serializable<T, ArchiveType>::value &&
                                           (!(Flags & AllowEmptyClassElision) || ((Flags & AllowEmptyClassElision) && !std::is_empty<T>::value)))> = traits::sfinae> inline
      ArchiveType & processImpl(T const &)
      {
        static_assert(traits::detail::count_input_serializers<T, ArchiveType>::value != 0,
            "cereal could not find any input serialization functions for the provided type and archive combination. \n\n "
            "Types must either have a serialize function, load/save pair, or load_minimal/save_minimal pair (you may not mix these). \n "
            "Serialize functions generally have the following signature: \n\n "
            "template<class Archive> \n "
            "  void serialize(Archive & ar) \n "
            "  { \n "
            "    ar( member1, member2, member3 ); \n "
            "  } \n\n " );

        static_assert(traits::detail::count_input_serializers<T, ArchiveType>::value < 2,
            "cereal found more than one compatible input serialization function for the provided type and archive combination. \n\n "
            "Types must either have a serialize function, load/save pair, or load_minimal/save_minimal pair (you may not mix these). \n "
            "Use specialization (see access.hpp) if you need to disambiguate between serialize vs load/save functions.  \n "
            "Note that serialization functions can be inherited which may lead to the aforementioned ambiguities. \n "
            "In addition, you may not mix versioned with non-versioned serialization functions. \n\n ");

        return *self;
      }

      //! Befriend for versioning in load_and_construct
      template <class A, class B, bool C, bool D, bool E, bool F> friend struct detail::Construct;

      //! Registers a class version with the archive and serializes it if necessary
      /*! If this is the first time this class has been serialized, we will record its
          version number and serialize that.

          @tparam T The type of the class being serialized
          @param version The version number associated with it */
      template <class T> inline
      std::uint32_t loadClassVersion()
      {
        static const auto hash = std::type_index(typeid(T)).hash_code();
        auto lookupResult = itsVersionedTypes.find( hash );

        if( lookupResult != itsVersionedTypes.end() ) // already exists
          return lookupResult->second;
        else // need to load
        {
          std::uint32_t version;

          process( make_nvp<ArchiveType>("cereal_class_version", version) );
          itsVersionedTypes.emplace_hint( lookupResult, hash, version );

          return version;
        }
      }

      //! Member serialization
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_serialize)> inline
      ArchiveType & processImpl(T & t)
      {
        const auto version = loadClassVersion<T>();
        access::member_serialize(*self, t, version);
        return *self;
      }

      //! Non member serialization
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_serialize)> inline
      ArchiveType & processImpl(T & t)
      {
        const auto version = loadClassVersion<T>();
        CEREAL_SERIALIZE_FUNCTION_NAME(*self, t, version);
        return *self;
      }

      //! Member split (load)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_load)> inline
      ArchiveType & processImpl(T & t)
      {
        const auto version = loadClassVersion<T>();
        access::member_load(*self, t, version);
        return *self;
      }

      //! Non member split (load)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_load)> inline
      ArchiveType & processImpl(T & t)
      {
        const auto version = loadClassVersion<T>();
        CEREAL_LOAD_FUNCTION_NAME(*self, t, version);
        return *self;
      }

      //! Member split (load_minimal)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(member_versioned_load_minimal)> inline
      ArchiveType & processImpl(T & t)
      {
        using OutArchiveType = typename traits::detail::get_output_from_input<ArchiveType>::type;
        const auto version = loadClassVersion<T>();
        typename traits::has_member_versioned_save_minimal<T, OutArchiveType>::type value;
        self->process(value);
        access::member_load_minimal(*self, t, value, version);
        return *self;
      }

      //! Non member split (load_minimal)
      /*! Versioning implementation */
      template <class T, PROCESS_IF(non_member_versioned_load_minimal)> inline
      ArchiveType & processImpl(T & t)
      {
        using OutArchiveType = typename traits::detail::get_output_from_input<ArchiveType>::type;
        const auto version = loadClassVersion<T>();
        typename traits::has_non_member_versioned_save_minimal<T, OutArchiveType>::type value;
        self->process(value);
        CEREAL_LOAD_MINIMAL_FUNCTION_NAME(*self, t, value, version);
        return *self;
      }

      #undef PROCESS_IF

    private:
      ArchiveType * const self;

      //! A set of all base classes that have been serialized
      std::unordered_set<traits::detail::base_class_id, traits::detail::base_class_id_hash> itsBaseClassSet;

      //! Maps from pointer ids to metadata
      std::unordered_map<std::uint32_t, std::shared_ptr<void>> itsSharedPointerMap;

      //! Maps from name ids to names
      std::unordered_map<std::uint32_t, std::string> itsPolymorphicTypeMap;

      //! Maps from type hash codes to version numbers
      std::unordered_map<std::size_t, std::uint32_t> itsVersionedTypes;
  }; // class InputArchive
} // namespace cereal








/*! \file common.hpp
    \brief Support common types - always included automatically
    \ingroup OtherTypes */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace cereal
{
  namespace common_detail
  {
    //! Serialization for arrays if BinaryData is supported and we are arithmetic
    /*! @internal */
    template <class Archive, class T> inline
    void serializeArray( Archive & ar, T & array, std::true_type /* binary_supported */ )
    {
      ar( binary_data( array, sizeof(array) ) );
    }

    //! Serialization for arrays if BinaryData is not supported or we are not arithmetic
    /*! @internal */
    template <class Archive, class T> inline
    void serializeArray( Archive & ar, T & array, std::false_type /* binary_supported */ )
    {
      for( auto & i : array )
        ar( i );
    }

    namespace
    {
      //! Gets the underlying type of an enum
      /*! @internal */
      template <class T, bool IsEnum>
      struct enum_underlying_type : std::false_type {};

      //! Gets the underlying type of an enum
      /*! Specialization for when we actually have an enum
          @internal */
      template <class T>
      struct enum_underlying_type<T, true> { using type = typename std::underlying_type<T>::type; };
    } // anon namespace

    //! Checks if a type is an enum
    /*! This is needed over simply calling std::is_enum because the type
        traits checking at compile time will attempt to call something like
        load_minimal with a special NoConvertRef struct that wraps up the true type.

        This will strip away any of that and also expose the true underlying type.
        @internal */
    template <class T>
    class is_enum
    {
      private:
        using DecayedT  = typename std::decay<T>::type;
        using StrippedT = typename ::cereal::traits::strip_minimal<DecayedT>::type;

      public:
        static const bool value = std::is_enum<StrippedT>::value;
        using type = StrippedT;
        using base_type = typename enum_underlying_type<StrippedT, value>::type;
    };
  }

  //! Saving for enum types
  template <class Archive, class T> inline
  typename std::enable_if<common_detail::is_enum<T>::value,
                          typename common_detail::is_enum<T>::base_type>::type
  CEREAL_SAVE_MINIMAL_FUNCTION_NAME( Archive const &, T const & t )
  {
    return static_cast<typename common_detail::is_enum<T>::base_type>(t);
  }

  //! Loading for enum types
  template <class Archive, class T> inline
  typename std::enable_if<common_detail::is_enum<T>::value, void>::type
  CEREAL_LOAD_MINIMAL_FUNCTION_NAME( Archive const &, T && t,
                                     typename common_detail::is_enum<T>::base_type const & value )
  {
    t = reinterpret_cast<typename common_detail::is_enum<T>::type const &>( value );
  }

  //! Serialization for raw pointers
  /*! This exists only to throw a static_assert to let users know we don't support raw pointers. */
  template <class Archive, class T> inline
  void CEREAL_SERIALIZE_FUNCTION_NAME( Archive &, T * & )
  {
    static_assert(cereal::traits::detail::delay_static_assert<T>::value,
      "Cereal does not support serializing raw pointers - please use a smart pointer");
  }

  //! Serialization for C style arrays
  template <class Archive, class T> inline
  typename std::enable_if<std::is_array<T>::value, void>::type
  CEREAL_SERIALIZE_FUNCTION_NAME(Archive & ar, T & array)
  {
    common_detail::serializeArray( ar, array,
        std::integral_constant<bool, traits::is_output_serializable<BinaryData<T>, Archive>::value &&
                                     std::is_arithmetic<typename std::remove_all_extents<T>::type>::value>() );
  }
} // namespace cereal











/*! \file binary.hpp
    \brief Binary input and output archives */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <sstream>

namespace cereal
{
  // ######################################################################
  //! An output archive designed to save data in a compact binary representation
  /*! This archive outputs data to a stream in an extremely compact binary
      representation with as little extra metadata as possible.

      This archive does nothing to ensure that the endianness of the saved
      and loaded data is the same.  If you need to have portability over
      architectures with different endianness, use PortableBinaryOutputArchive.

      When using a binary archive and a file stream, you must use the
      std::ios::binary format flag to avoid having your data altered
      inadvertently.

      \ingroup Archives */
  class BinaryOutputArchive : public OutputArchive<BinaryOutputArchive, AllowEmptyClassElision>
  {
    public:
      //! Construct, outputting to the provided stream
      /*! @param stream The stream to output to.  Can be a stringstream, a file stream, or
                        even cout! */
      BinaryOutputArchive(std::ostream & stream) :
        OutputArchive<BinaryOutputArchive, AllowEmptyClassElision>(this),
        itsStream(stream)
      { }

      ~BinaryOutputArchive() CEREAL_NOEXCEPT = default;

      //! Writes size bytes of data to the output stream
      void saveBinary( const void * data, std::size_t size )
      {
        auto const writtenSize = static_cast<std::size_t>( itsStream.rdbuf()->sputn( reinterpret_cast<const char*>( data ), size ) );

        if(writtenSize != size)
          throw Exception("Failed to write " + std::to_string(size) + " bytes to output stream! Wrote " + std::to_string(writtenSize));
      }

    private:
      std::ostream & itsStream;
  };

  // ######################################################################
  //! An input archive designed to load data saved using BinaryOutputArchive
  /*  This archive does nothing to ensure that the endianness of the saved
      and loaded data is the same.  If you need to have portability over
      architectures with different endianness, use PortableBinaryOutputArchive.

      When using a binary archive and a file stream, you must use the
      std::ios::binary format flag to avoid having your data altered
      inadvertently.

      \ingroup Archives */
  class BinaryInputArchive : public InputArchive<BinaryInputArchive, AllowEmptyClassElision>
  {
    public:
      //! Construct, loading from the provided stream
      BinaryInputArchive(std::istream & stream) :
        InputArchive<BinaryInputArchive, AllowEmptyClassElision>(this),
        itsStream(stream)
      { }

      ~BinaryInputArchive() CEREAL_NOEXCEPT = default;

      //! Reads size bytes of data from the input stream
      void loadBinary( void * const data, std::size_t size )
      {
        auto const readSize = static_cast<std::size_t>( itsStream.rdbuf()->sgetn( reinterpret_cast<char*>( data ), size ) );

        if(readSize != size)
          throw Exception("Failed to read " + std::to_string(size) + " bytes from input stream! Read " + std::to_string(readSize));
      }

    private:
      std::istream & itsStream;
  };

  // ######################################################################
  // Common BinaryArchive serialization functions

  //! Saving for POD types to binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME(BinaryOutputArchive & ar, T const & t)
  {
    ar.saveBinary(std::addressof(t), sizeof(t));
  }

  //! Loading for POD types from binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME(BinaryInputArchive & ar, T & t)
  {
    ar.loadBinary(std::addressof(t), sizeof(t));
  }

  //! Serializing NVP types to binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(BinaryInputArchive, BinaryOutputArchive)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, NameValuePair<T> & t )
  {
    ar( t.value );
  }

  //! Serializing SizeTags to binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(BinaryInputArchive, BinaryOutputArchive)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, SizeTag<T> & t )
  {
    ar( t.size );
  }

  //! Saving binary data
  template <class T> inline
  void CEREAL_SAVE_FUNCTION_NAME(BinaryOutputArchive & ar, BinaryData<T> const & bd)
  {
    ar.saveBinary( bd.data, static_cast<std::size_t>( bd.size ) );
  }

  //! Loading binary data
  template <class T> inline
  void CEREAL_LOAD_FUNCTION_NAME(BinaryInputArchive & ar, BinaryData<T> & bd)
  {
    ar.loadBinary(bd.data, static_cast<std::size_t>(bd.size));
  }
} // namespace cereal

// register archives for polymorphic support
CEREAL_REGISTER_ARCHIVE(cereal::BinaryOutputArchive)
CEREAL_REGISTER_ARCHIVE(cereal::BinaryInputArchive)

// tie input and output archives together
CEREAL_SETUP_ARCHIVE_TRAITS(cereal::BinaryInputArchive, cereal::BinaryOutputArchive)






/*! \file portable_binary.hpp
    \brief Binary input and output archives */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <sstream>
#include <limits>

namespace cereal
{
  namespace portable_binary_detail
  {
    //! Returns true if the current machine is little endian
    /*! @ingroup Internal */
    inline std::uint8_t is_little_endian()
    {
      static std::int32_t test = 1;
      return *reinterpret_cast<std::int8_t*>( &test ) == 1;
    }

    //! Swaps the order of bytes for some chunk of memory
    /*! @param data The data as a uint8_t pointer
        @tparam DataSize The true size of the data
        @ingroup Internal */
    template <std::size_t DataSize>
    inline void swap_bytes( std::uint8_t * data )
    {
      for( std::size_t i = 0, end = DataSize / 2; i < end; ++i )
        std::swap( data[i], data[DataSize - i - 1] );
    }
  } // end namespace portable_binary_detail

  // ######################################################################
  //! An output archive designed to save data in a compact binary representation portable over different architectures
  /*! This archive outputs data to a stream in an extremely compact binary
      representation with as little extra metadata as possible.

      This archive will record the endianness of the data as well as the desired in/out endianness
      and assuming that the user takes care of ensuring serialized types are the same size
      across machines, is portable over different architectures.

      When using a binary archive and a file stream, you must use the
      std::ios::binary format flag to avoid having your data altered
      inadvertently.

      \warning This archive has not been thoroughly tested across different architectures.
               Please report any issues, optimizations, or feature requests at
               <a href="www.github.com/USCiLab/cereal">the project github</a>.

    \ingroup Archives */
  class PortableBinaryOutputArchive : public OutputArchive<PortableBinaryOutputArchive, AllowEmptyClassElision>
  {
    public:
      //! A class containing various advanced options for the PortableBinaryOutput archive
      class Options
      {
        public:
          //! Represents desired endianness
          enum class Endianness : std::uint8_t
          { big, little };

          //! Default options, preserve system endianness
          static Options Default(){ return Options(); }

          //! Save as little endian
          static Options LittleEndian(){ return Options( Endianness::little ); }

          //! Save as big endian
          static Options BigEndian(){ return Options( Endianness::big ); }

          //! Specify specific options for the PortableBinaryOutputArchive
          /*! @param outputEndian The desired endianness of saved (output) data */
          explicit Options( Endianness outputEndian = getEndianness() ) :
            itsOutputEndianness( outputEndian ) { }

        private:
          //! Gets the endianness of the system
          inline static Endianness getEndianness()
          { return portable_binary_detail::is_little_endian() ? Endianness::little : Endianness::big; }

          //! Checks if Options is set for little endian
          inline std::uint8_t is_little_endian() const
          { return itsOutputEndianness == Endianness::little; }

          friend class PortableBinaryOutputArchive;
          Endianness itsOutputEndianness;
      };

      //! Construct, outputting to the provided stream
      /*! @param stream The stream to output to. Should be opened with std::ios::binary flag.
          @param options The PortableBinary specific options to use.  See the Options struct
                         for the values of default parameters */
      PortableBinaryOutputArchive(std::ostream & stream, Options const & options = Options::Default()) :
        OutputArchive<PortableBinaryOutputArchive, AllowEmptyClassElision>(this),
        itsStream(stream),
        itsConvertEndianness( portable_binary_detail::is_little_endian() ^ options.is_little_endian() )
      {
        this->operator()( options.is_little_endian() );
      }

      ~PortableBinaryOutputArchive() CEREAL_NOEXCEPT = default;

      //! Writes size bytes of data to the output stream
      template <std::size_t DataSize> inline
      void saveBinary( const void * data, std::size_t size )
      {
        std::size_t writtenSize = 0;

        if( itsConvertEndianness )
        {
          for( std::size_t i = 0; i < size; i += DataSize )
            for( std::size_t j = 0; j < DataSize; ++j )
              writtenSize += static_cast<std::size_t>( itsStream.rdbuf()->sputn( reinterpret_cast<const char*>( data ) + DataSize - j - 1 + i, 1 ) );
        }
        else
          writtenSize = static_cast<std::size_t>( itsStream.rdbuf()->sputn( reinterpret_cast<const char*>( data ), size ) );

        if(writtenSize != size)
          throw Exception("Failed to write " + std::to_string(size) + " bytes to output stream! Wrote " + std::to_string(writtenSize));
      }

    private:
      std::ostream & itsStream;
      const uint8_t itsConvertEndianness; //!< If set to true, we will need to swap bytes upon saving
  };

  // ######################################################################
  //! An input archive designed to load data saved using PortableBinaryOutputArchive
  /*! This archive outputs data to a stream in an extremely compact binary
      representation with as little extra metadata as possible.

      This archive will load the endianness of the serialized data and
      if necessary transform it to match that of the local machine.  This comes
      at a significant performance cost compared to non portable archives if
      the transformation is necessary, and also causes a small performance hit
      even if it is not necessary.

      It is recommended to use portable archives only if you know that you will
      be sending binary data to machines with different endianness.

      The archive will do nothing to ensure types are the same size - that is
      the responsibility of the user.

      When using a binary archive and a file stream, you must use the
      std::ios::binary format flag to avoid having your data altered
      inadvertently.

      \warning This archive has not been thoroughly tested across different architectures.
               Please report any issues, optimizations, or feature requests at
               <a href="www.github.com/USCiLab/cereal">the project github</a>.

    \ingroup Archives */
  class PortableBinaryInputArchive : public InputArchive<PortableBinaryInputArchive, AllowEmptyClassElision>
  {
    public:
      //! A class containing various advanced options for the PortableBinaryInput archive
      class Options
      {
        public:
          //! Represents desired endianness
          enum class Endianness : std::uint8_t
          { big, little };

          //! Default options, preserve system endianness
          static Options Default(){ return Options(); }

          //! Load into little endian
          static Options LittleEndian(){ return Options( Endianness::little ); }

          //! Load into big endian
          static Options BigEndian(){ return Options( Endianness::big ); }

          //! Specify specific options for the PortableBinaryInputArchive
          /*! @param inputEndian The desired endianness of loaded (input) data */
          explicit Options( Endianness inputEndian = getEndianness() ) :
            itsInputEndianness( inputEndian ) { }

        private:
          //! Gets the endianness of the system
          inline static Endianness getEndianness()
          { return portable_binary_detail::is_little_endian() ? Endianness::little : Endianness::big; }

          //! Checks if Options is set for little endian
          inline std::uint8_t is_little_endian() const
          { return itsInputEndianness == Endianness::little; }

          friend class PortableBinaryInputArchive;
          Endianness itsInputEndianness;
      };

      //! Construct, loading from the provided stream
      /*! @param stream The stream to read from. Should be opened with std::ios::binary flag.
          @param options The PortableBinary specific options to use.  See the Options struct
                         for the values of default parameters */
      PortableBinaryInputArchive(std::istream & stream, Options const & options = Options::Default()) :
        InputArchive<PortableBinaryInputArchive, AllowEmptyClassElision>(this),
        itsStream(stream),
        itsConvertEndianness( false )
      {
        uint8_t streamLittleEndian;
        this->operator()( streamLittleEndian );
        itsConvertEndianness = options.is_little_endian() ^ streamLittleEndian;
      }

      ~PortableBinaryInputArchive() CEREAL_NOEXCEPT = default;

      //! Reads size bytes of data from the input stream
      /*! @param data The data to save
          @param size The number of bytes in the data
          @tparam DataSize T The size of the actual type of the data elements being loaded */
      template <std::size_t DataSize> inline
      void loadBinary( void * const data, std::size_t size )
      {
        // load data
        auto const readSize = static_cast<std::size_t>( itsStream.rdbuf()->sgetn( reinterpret_cast<char*>( data ), size ) );

        if(readSize != size)
          throw Exception("Failed to read " + std::to_string(size) + " bytes from input stream! Read " + std::to_string(readSize));

        // flip bits if needed
        if( itsConvertEndianness )
        {
          std::uint8_t * ptr = reinterpret_cast<std::uint8_t*>( data );
          for( std::size_t i = 0; i < size; i += DataSize )
            portable_binary_detail::swap_bytes<DataSize>( ptr + i );
        }
      }

    private:
      std::istream & itsStream;
      uint8_t itsConvertEndianness; //!< If set to true, we will need to swap bytes upon loading
  };

  // ######################################################################
  // Common BinaryArchive serialization functions

  //! Saving for POD types to portable binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME(PortableBinaryOutputArchive & ar, T const & t)
  {
    static_assert( !std::is_floating_point<T>::value ||
                   (std::is_floating_point<T>::value && std::numeric_limits<T>::is_iec559),
                   "Portable binary only supports IEEE 754 standardized floating point" );
    ar.template saveBinary<sizeof(T)>(std::addressof(t), sizeof(t));
  }

  //! Loading for POD types from portable binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME(PortableBinaryInputArchive & ar, T & t)
  {
    static_assert( !std::is_floating_point<T>::value ||
                   (std::is_floating_point<T>::value && std::numeric_limits<T>::is_iec559),
                   "Portable binary only supports IEEE 754 standardized floating point" );
    ar.template loadBinary<sizeof(T)>(std::addressof(t), sizeof(t));
  }

  //! Serializing NVP types to portable binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(PortableBinaryInputArchive, PortableBinaryOutputArchive)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, NameValuePair<T> & t )
  {
    ar( t.value );
  }

  //! Serializing SizeTags to portable binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(PortableBinaryInputArchive, PortableBinaryOutputArchive)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, SizeTag<T> & t )
  {
    ar( t.size );
  }

  //! Saving binary data to portable binary
  template <class T> inline
  void CEREAL_SAVE_FUNCTION_NAME(PortableBinaryOutputArchive & ar, BinaryData<T> const & bd)
  {
    typedef typename std::remove_pointer<T>::type TT;
    static_assert( !std::is_floating_point<TT>::value ||
                   (std::is_floating_point<TT>::value && std::numeric_limits<TT>::is_iec559),
                   "Portable binary only supports IEEE 754 standardized floating point" );

    ar.template saveBinary<sizeof(TT)>( bd.data, static_cast<std::size_t>( bd.size ) );
  }

  //! Loading binary data from portable binary
  template <class T> inline
  void CEREAL_LOAD_FUNCTION_NAME(PortableBinaryInputArchive & ar, BinaryData<T> & bd)
  {
    typedef typename std::remove_pointer<T>::type TT;
    static_assert( !std::is_floating_point<TT>::value ||
                   (std::is_floating_point<TT>::value && std::numeric_limits<TT>::is_iec559),
                   "Portable binary only supports IEEE 754 standardized floating point" );

    ar.template loadBinary<sizeof(TT)>( bd.data, static_cast<std::size_t>( bd.size ) );
  }
} // namespace cereal

// register archives for polymorphic support
CEREAL_REGISTER_ARCHIVE(cereal::PortableBinaryOutputArchive)
CEREAL_REGISTER_ARCHIVE(cereal::PortableBinaryInputArchive)

// tie input and output archives together
CEREAL_SETUP_ARCHIVE_TRAITS(cereal::PortableBinaryInputArchive, cereal::PortableBinaryOutputArchive)














/*! \file pair_associative_container.hpp
    \brief Support for the PairAssociativeContainer refinement of the
    AssociativeContainer concept.
    \ingroup TypeConcepts */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace cereal
{
  //! Saving for std-like pair associative containers
  template <class Archive, template <typename...> class Map, typename... Args, typename = typename Map<Args...>::mapped_type> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, Map<Args...> const & map )
  {
    ar( make_size_tag( static_cast<size_type>(map.size()) ) );

    for( const auto & i : map )
      ar( make_map_item(i.first, i.second) );
  }

  //! Loading for std-like pair associative containers
  template <class Archive, template <typename...> class Map, typename... Args, typename = typename Map<Args...>::mapped_type> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, Map<Args...> & map )
  {
    size_type size;
    ar( make_size_tag( size ) );

    map.clear();

    auto hint = map.begin();
    for( size_t i = 0; i < size; ++i )
    {
      typename Map<Args...>::key_type key;
      typename Map<Args...>::mapped_type value;

      ar( make_map_item(key, value) );
      #ifdef CEREAL_OLDER_GCC
      hint = map.insert( hint, std::make_pair(std::move(key), std::move(value)) );
      #else // NOT CEREAL_OLDER_GCC
      hint = map.emplace_hint( hint, std::move( key ), std::move( value ) );
      #endif // NOT CEREAL_OLDER_GCC
    }
  }
} // namespace cereal







/*! \file map.hpp
    \brief Support for types found in \<map\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <map>





/*! \file vector.hpp
    \brief Support for types found in \<vector\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <vector>

namespace cereal
{
  //! Serialization for std::vectors of arithmetic (but not bool) using binary serialization, if supported
  template <class Archive, class T, class A> inline
  typename std::enable_if<traits::is_output_serializable<BinaryData<T>, Archive>::value
                          && std::is_arithmetic<T>::value && !std::is_same<T, bool>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::vector<T, A> const & vector )
  {
    ar( make_size_tag( static_cast<size_type>(vector.size()) ) ); // number of elements
    ar( binary_data( vector.data(), vector.size() * sizeof(T) ) );
  }

  //! Serialization for std::vectors of arithmetic (but not bool) using binary serialization, if supported
  template <class Archive, class T, class A> inline
  typename std::enable_if<traits::is_input_serializable<BinaryData<T>, Archive>::value
                          && std::is_arithmetic<T>::value && !std::is_same<T, bool>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::vector<T, A> & vector )
  {
    size_type vectorSize;
    ar( make_size_tag( vectorSize ) );

    vector.resize( static_cast<std::size_t>( vectorSize ) );
    ar( binary_data( vector.data(), static_cast<std::size_t>( vectorSize ) * sizeof(T) ) );
  }

  //! Serialization for non-arithmetic vector types
  template <class Archive, class T, class A> inline
  typename std::enable_if<!traits::is_output_serializable<BinaryData<T>, Archive>::value
                          || !std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::vector<T, A> const & vector )
  {
    ar( make_size_tag( static_cast<size_type>(vector.size()) ) ); // number of elements
    for(auto && v : vector)
      ar( v );
  }

  //! Serialization for non-arithmetic vector types
  template <class Archive, class T, class A> inline
  typename std::enable_if<!traits::is_input_serializable<BinaryData<T>, Archive>::value
                          || !std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::vector<T, A> & vector )
  {
    size_type size;
    ar( make_size_tag( size ) );

    vector.resize( static_cast<std::size_t>( size ) );
    for(auto && v : vector)
      ar( v );
  }

  //! Serialization for bool vector types
  template <class Archive, class A> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::vector<bool, A> const & vector )
  {
    ar( make_size_tag( static_cast<size_type>(vector.size()) ) ); // number of elements
    for(auto && v : vector)
      ar( static_cast<bool>(v) );
  }

  //! Serialization for bool vector types
  template <class Archive, class A> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::vector<bool, A> & vector )
  {
    size_type size;
    ar( make_size_tag( size ) );

    vector.resize( static_cast<std::size_t>( size ) );
    for(auto && v : vector)
    {
      bool b;
      ar( b );
      v = b;
    }
  }
} // namespace cereal







/*! \file string.hpp
    \brief Support for types found in \<string\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string>

namespace cereal
{
  //! Serialization for basic_string types, if binary data is supported
  template<class Archive, class CharT, class Traits, class Alloc> inline
  typename std::enable_if<traits::is_output_serializable<BinaryData<CharT>, Archive>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME(Archive & ar, std::basic_string<CharT, Traits, Alloc> const & str)
  {
    // Save number of chars + the data
    ar( make_size_tag( static_cast<size_type>(str.size()) ) );
    ar( binary_data( str.data(), str.size() * sizeof(CharT) ) );
  }

  //! Serialization for basic_string types, if binary data is supported
  template<class Archive, class CharT, class Traits, class Alloc> inline
  typename std::enable_if<traits::is_input_serializable<BinaryData<CharT>, Archive>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME(Archive & ar, std::basic_string<CharT, Traits, Alloc> & str)
  {
    size_type size;
    ar( make_size_tag( size ) );
    str.resize(static_cast<std::size_t>(size));
    ar( binary_data( const_cast<CharT *>( str.data() ), static_cast<std::size_t>(size) * sizeof(CharT) ) );
  }
} // namespace cereal







/*! \file complex.hpp
    \brief Support for types found in \<complex\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <complex>

namespace cereal
{
  //! Serializing (save) for std::complex
  template <class Archive, class T> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::complex<T> const & comp )
  {
    ar( CEREAL_NVP_("real", comp.real()),
        CEREAL_NVP_("imag", comp.imag()) );
  }

  //! Serializing (load) for std::complex
  template <class Archive, class T> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::complex<T> & bits )
  {
    T real, imag;
    ar( CEREAL_NVP_("real", real),
        CEREAL_NVP_("imag", imag) );
    bits = {real, imag};
  }
} // namespace cereal






/*! \file array.hpp
    \brief Support for types found in \<array\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <array>

namespace cereal
{
  //! Saving for std::array primitive types
  //! using binary serialization, if supported
  template <class Archive, class T, size_t N> inline
  typename std::enable_if<traits::is_output_serializable<BinaryData<T>, Archive>::value
                          && std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::array<T, N> const & array )
  {
    ar( binary_data( array.data(), sizeof(array) ) );
  }

  //! Loading for std::array primitive types
  //! using binary serialization, if supported
  template <class Archive, class T, size_t N> inline
  typename std::enable_if<traits::is_input_serializable<BinaryData<T>, Archive>::value
                          && std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::array<T, N> & array )
  {
    ar( binary_data( array.data(), sizeof(array) ) );
  }

  //! Saving for std::array all other types
  template <class Archive, class T, size_t N> inline
  typename std::enable_if<!traits::is_output_serializable<BinaryData<T>, Archive>::value
                          || !std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::array<T, N> const & array )
  {
    for( auto const & i : array )
      ar( i );
  }

  //! Loading for std::array all other types
  template <class Archive, class T, size_t N> inline
  typename std::enable_if<!traits::is_input_serializable<BinaryData<T>, Archive>::value
                          || !std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::array<T, N> & array )
  {
    for( auto & i : array )
      ar( i );
  }
} // namespace cereal






/*! \file unordered_map.hpp
    \brief Support for types found in \<unordered_map\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <unordered_map>






/*! \file utility.hpp
    \brief Support for types found in \<utility\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <utility>

namespace cereal
{
  //! Serializing for std::pair
  template <class Archive, class T1, class T2> inline
  void CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, std::pair<T1, T2> & pair )
  {
    ar( CEREAL_NVP_("first",  pair.first),
        CEREAL_NVP_("second", pair.second) );
  }
} // namespace cereal





/*! \file tuple.hpp
    \brief Support for types found in \<tuple\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <tuple>

namespace cereal
{
  namespace tuple_detail
  {
    //! Creates a c string from a sequence of characters
    /*! The c string created will alwas be prefixed by "tuple_element"
        Based on code from: http://stackoverflow/a/20973438/710791
        @internal */
    template<char...Cs>
    struct char_seq_to_c_str
    {
      static const int size = 14;// Size of array for the word: tuple_element
      typedef const char (&arr_type)[sizeof...(Cs) + size];
      static const char str[sizeof...(Cs) + size];
    };

    // the word tuple_element plus a number
    //! @internal
    template<char...Cs>
    const char char_seq_to_c_str<Cs...>::str[sizeof...(Cs) + size] =
      {'t','u','p','l','e','_','e','l','e','m','e','n','t', Cs..., '\0'};

    //! Converts a number into a sequence of characters
    /*! @tparam Q The quotient of dividing the original number by 10
        @tparam R The remainder of dividing the original number by 10
        @tparam C The sequence built so far
        @internal */
    template <size_t Q, size_t R, char ... C>
    struct to_string_impl
    {
      using type = typename to_string_impl<Q/10, Q%10, R+'0', C...>::type;
    };

    //! Base case with no quotient
    /*! @internal */
    template <size_t R, char ... C>
    struct to_string_impl<0, R, C...>
    {
      using type = char_seq_to_c_str<R+'0', C...>;
    };

    //! Generates a c string for a given index of a tuple
    /*! Example use:
        @code{cpp}
        tuple_element_name<3>::c_str();// returns "tuple_element3"
        @endcode
        @internal */
    template<size_t T>
    struct tuple_element_name
    {
      using type = typename to_string_impl<T/10, T%10>::type;
      static const typename type::arr_type c_str(){ return type::str; };
    };

    // unwinds a tuple to save it
    //! @internal
    template <size_t Height>
    struct serialize
    {
      template <class Archive, class ... Types> inline
      static void apply( Archive & ar, std::tuple<Types...> & tuple )
      {
        serialize<Height - 1>::template apply( ar, tuple );
        ar( CEREAL_NVP_(tuple_element_name<Height - 1>::c_str(),
            std::get<Height - 1>( tuple )) );
      }
    };

    // Zero height specialization - nothing to do here
    //! @internal
    template <>
    struct serialize<0>
    {
      template <class Archive, class ... Types> inline
      static void apply( Archive &, std::tuple<Types...> & )
      { }
    };
  }

  //! Serializing for std::tuple
  template <class Archive, class ... Types> inline
  void CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, std::tuple<Types...> & tuple )
  {
    tuple_detail::serialize<std::tuple_size<std::tuple<Types...>>::value>::template apply( ar, tuple );
  }
} // namespace cereal





/*! \file deque.hpp
    \brief Support for types found in \<deque\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <deque>

namespace cereal
{
  //! Saving for std::deque
  template <class Archive, class T, class A> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::deque<T, A> const & deque )
  {
    ar( make_size_tag( static_cast<size_type>(deque.size()) ) );

    for( auto const & i : deque )
      ar( i );
  }

  //! Loading for std::deque
  template <class Archive, class T, class A> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::deque<T, A> & deque )
  {
    size_type size;
    ar( make_size_tag( size ) );

    deque.resize( static_cast<size_t>( size ) );

    for( auto & i : deque )
      ar( i );
  }
} // namespace cereal






/*! \file list.hpp
    \brief Support for types found in \<list\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <list>

namespace cereal
{
  //! Saving for std::list
  template <class Archive, class T, class A> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::list<T, A> const & list )
  {
    ar( make_size_tag( static_cast<size_type>(list.size()) ) );

    for( auto const & i : list )
      ar( i );
  }

  //! Loading for std::list
  template <class Archive, class T, class A> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::list<T, A> & list )
  {
    size_type size;
    ar( make_size_tag( size ) );

    list.resize( static_cast<size_t>( size ) );

    for( auto & i : list )
      ar( i );
  }
} // namespace cereal




/*! \file set.hpp
    \brief Support for types found in \<set\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <set>

namespace cereal
{
  namespace set_detail
  {
    //! @internal
    template <class Archive, class SetT> inline
    void save( Archive & ar, SetT const & set )
    {
      ar( make_size_tag( static_cast<size_type>(set.size()) ) );

      for( const auto & i : set )
        ar( i );
    }

    //! @internal
    template <class Archive, class SetT> inline
    void load( Archive & ar, SetT & set )
    {
      size_type size;
      ar( make_size_tag( size ) );

      set.clear();

      auto hint = set.begin();
      for( size_type i = 0; i < size; ++i )
      {
        typename SetT::key_type key;

        ar( key );
        #ifdef CEREAL_OLDER_GCC
        hint = set.insert( hint, std::move( key ) );
        #else // NOT CEREAL_OLDER_GCC
        hint = set.emplace_hint( hint, std::move( key ) );
        #endif // NOT CEREAL_OLDER_GCC
      }
    }
  }

  //! Saving for std::set
  template <class Archive, class K, class C, class A> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::set<K, C, A> const & set )
  {
    set_detail::save( ar, set );
  }

  //! Loading for std::set
  template <class Archive, class K, class C, class A> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::set<K, C, A> & set )
  {
    set_detail::load( ar, set );
  }

  //! Saving for std::multiset
  template <class Archive, class K, class C, class A> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::multiset<K, C, A> const & multiset )
  {
    set_detail::save( ar, multiset );
  }

  //! Loading for std::multiset
  template <class Archive, class K, class C, class A> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::multiset<K, C, A> & multiset )
  {
    set_detail::load( ar, multiset );
  }
} // namespace cereal






/*! \file stack.hpp
    \brief Support for types found in \<stack\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stack>


namespace cereal
{
  namespace stack_detail
  {
    //! Allows access to the protected container in stack
    template <class T, class C> inline
    C const & container( std::stack<T, C> const & stack )
    {
      struct H : public std::stack<T, C>
      {
        static C const & get( std::stack<T, C> const & s )
        {
          return s.*(&H::c);
        }
      };

      return H::get( stack );
    }
  }

  //! Saving for std::stack
  template <class Archive, class T, class C> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::stack<T, C> const & stack )
  {
    ar( CEREAL_NVP_("container", stack_detail::container( stack )) );
  }

  //! Loading for std::stack
  template <class Archive, class T, class C> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::stack<T, C> & stack )
  {
    C container;
    ar( CEREAL_NVP_("container", container) );
    stack = std::stack<T, C>( std::move( container ) );
  }
} // namespace cereal






/*! \file functional.hpp
    \brief Support for types found in \<functional\>
    \ingroup STLSupport */
/*
  Copyright (c) 2016, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <functional>

namespace cereal
{
  //! Saving for std::less
  template <class Archive, class T> inline
  void serialize( Archive &, std::less<T> & )
  { }
} // namespace cereal







/*! \file queue.hpp
    \brief Support for types found in \<queue\>
    \ingroup STLSupport */
/*
  Copyright (c) 2014, Randolph Voorhies, Shane Grant
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of cereal nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL RANDOLPH VOORHIES OR SHANE GRANT BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <queue>


namespace cereal
{
  namespace queue_detail
  {
    //! Allows access to the protected container in queue
    /*! @internal */
    template <class T, class C> inline
    C const & container( std::queue<T, C> const & queue )
    {
      struct H : public std::queue<T, C>
      {
        static C const & get( std::queue<T, C> const & q )
        {
          return q.*(&H::c);
        }
      };

      return H::get( queue );
    }

    //! Allows access to the protected container in priority queue
    /*! @internal */
    template <class T, class C, class Comp> inline
    C const & container( std::priority_queue<T, C, Comp> const & priority_queue )
    {
      struct H : public std::priority_queue<T, C, Comp>
      {
        static C const & get( std::priority_queue<T, C, Comp> const & pq )
        {
          return pq.*(&H::c);
        }
      };

      return H::get( priority_queue );
    }

    //! Allows access to the protected comparator in priority queue
    /*! @internal */
    template <class T, class C, class Comp> inline
    Comp const & comparator( std::priority_queue<T, C, Comp> const & priority_queue )
    {
      struct H : public std::priority_queue<T, C, Comp>
      {
        static Comp const & get( std::priority_queue<T, C, Comp> const & pq )
        {
          return pq.*(&H::comp);
        }
      };

      return H::get( priority_queue );
    }
  }

  //! Saving for std::queue
  template <class Archive, class T, class C> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::queue<T, C> const & queue )
  {
    ar( CEREAL_NVP_("container", queue_detail::container( queue )) );
  }

  //! Loading for std::queue
  template <class Archive, class T, class C> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::queue<T, C> & queue )
  {
    C container;
    ar( CEREAL_NVP_("container", container) );
    queue = std::queue<T, C>( std::move( container ) );
  }

  //! Saving for std::priority_queue
  template <class Archive, class T, class C, class Comp> inline
  void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, std::priority_queue<T, C, Comp> const & priority_queue )
  {
    ar( CEREAL_NVP_("comparator", queue_detail::comparator( priority_queue )) );
    ar( CEREAL_NVP_("container", queue_detail::container( priority_queue )) );
  }

  //! Loading for std::priority_queue
  template <class Archive, class T, class C, class Comp> inline
  void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, std::priority_queue<T, C, Comp> & priority_queue )
  {
    Comp comparator;
    ar( CEREAL_NVP_("comparator", comparator) );

    C container;
    ar( CEREAL_NVP_("container", container) );

    priority_queue = std::priority_queue<T, C, Comp>( comparator, std::move( container ) );
  }
} // namespace cereal




#endif // end of __CEREAL_IN_ONE_H__

