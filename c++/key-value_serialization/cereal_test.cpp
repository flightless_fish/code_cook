
#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#include "cereal.h"

class Stuff
{
public:
    Stuff() = default;

    void fillData()
    {
        data = { {"real", { {1.0f, 0},
                            {2.2f, 0},
                            {3.3f, 0} }},
                 {"imaginary", { {0, -1.0f},
                                 {0, -2.9932f},
                                 {0, -3.5f} }} };
    }

private:
    std::map<std::string, std::vector<std::complex<float>>> data;
    
    friend class cereal::access;

    template <class Archive>
    void serialize( Archive & ar )
    {
        ar( CEREAL_NVP(data) );
    }
};

int test1()
{
    std::ofstream fs("test.bin", std::ofstream::out);

    //cereal::JSONOutputArchive output(std::cout); // stream to cout
    cereal::BinaryOutputArchive output(fs);

    Stuff myStuff;
    myStuff.fillData();

    output( cereal::make_nvp("best data ever", myStuff) );
    fs.close();

    return 0;
}

int test2()
{
    std::map<std::string, std::vector<std::complex<float>>> data;

    data = { {"real", { {1.0f, 0},
                        {2.2f, 0},
                        {3.3f, 0} }},
             {"imaginary", { {0, -1.0f},
                             {0, -2.9932f},
                             {0, -3.5f} }} };

    std::ofstream fs("test.bin", std::ofstream::out);
    cereal::BinaryOutputArchive output(fs);
    output( cereal::make_nvp("test", data) );
    fs.close();

    return 0;
}

int test2_read()
{
    std::map<std::string, std::vector<std::complex<float>>> data;

    std::ifstream fs("test.bin", std::ofstream::in);
    cereal::BinaryInputArchive bia(fs);

    bia(data);
    fs.close();

    for(auto it : data)
    {
        printf("name: %s\n", it.first.c_str());
    }

    return 0;
}

int test3()
{
    std::map<std::string, std::vector<uint8_t>> data;

    for(int i=0; i<100; i++) {
        char vname[16];
        sprintf(vname, "value_%06d", i);

        std::vector<uint8_t> da;
        da.resize(100+i*2);

        data.insert(std::make_pair(vname, da));
    }

    std::ofstream fs("test3.bin", std::ofstream::out);
    cereal::PortableBinaryOutputArchive output(fs);
    output( cereal::make_nvp("test3", data) );
    fs.close();
}

int test3_read()
{
    std::map<std::string, std::vector<uint8_t>> data;

    std::ifstream fs("test3.bin", std::ofstream::in);
    cereal::PortableBinaryInputArchive bia(fs);

    bia(data);
    fs.close();

    int i=0;
    for(auto it : data)
    {
        printf("item [%4d]\n", i++);
        printf("name    : %s\n", it.first.c_str());
        printf("buf size: %zd\n", it.second.size());
        printf("\n");
    }

}

int main()
{
//    return test2_read();

    test3();
    test3_read();

    return 0;
}
