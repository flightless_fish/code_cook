/*
 * SQLite simple encrypt VFS
 */

#include "sqlite3.h"
#include <assert.h>
#include <string.h>


/*
 * Encrypt / Decrypt code
 */
static unsigned char enc_key[256] = {
    0x34, 0xe7, 0x41, 0x3d, 0xbf, 0x1c, 0x91, 0xe5, 0xce, 0xcc, 0x9f, 0xc4, 0x64, 0xb5, 0x29, 0xdb,
    0x37, 0x5c, 0x31, 0x92, 0xbe, 0xa6, 0x47, 0xd0, 0xae, 0x4d, 0x54, 0xbc, 0xb1, 0x51, 0x6e, 0x4e,
    0x90, 0x3e, 0x2f, 0x9e, 0x0c, 0xef, 0x00, 0x1f, 0x98, 0x42, 0x8e, 0xa5, 0x11, 0x2e, 0x1d, 0x44,
    0xe0, 0x21, 0xa8, 0x6b, 0x39, 0x74, 0xc6, 0x79, 0xa2, 0x6d, 0x71, 0xfa, 0x15, 0x57, 0x53, 0xf3,
    0x87, 0x63, 0x5f, 0xa9, 0x05, 0x12, 0x08, 0xe8, 0x4a, 0x94, 0x76, 0x26, 0x32, 0xf1, 0x7d, 0xf2,
    0x3c, 0x83, 0x04, 0x7a, 0x8a, 0xd1, 0x59, 0x78, 0x1e, 0x33, 0x8d, 0x14, 0x0d, 0x2a, 0xa4, 0x1b,
    0x35, 0xd2, 0xf5, 0xbb, 0x93, 0xfc, 0x62, 0xe9, 0xf9, 0xc5, 0x22, 0x17, 0x89, 0x5b, 0x4b, 0x40,
    0x36, 0xc0, 0xcf, 0x45, 0x95, 0x82, 0xc9, 0x27, 0xe1, 0x99, 0xcd, 0x73, 0x9a, 0xe6, 0x0a, 0xee,
    0x06, 0x13, 0xc3, 0x84, 0xad, 0x7b, 0x85, 0x4f, 0x0b, 0xa7, 0x7e, 0xcb, 0x61, 0xb3, 0x16, 0x96,
    0xf8, 0xdc, 0xa0, 0xe4, 0x20, 0xb8, 0x3b, 0xbd, 0x9c, 0xa1, 0x43, 0x8c, 0xd3, 0x58, 0x70, 0xfb,
    0x6c, 0xca, 0xd8, 0xf7, 0x72, 0xb4, 0x1a, 0xdf, 0xba, 0x86, 0x23, 0xc1, 0xff, 0xb2, 0xa3, 0x28,
    0x18, 0xc8, 0xda, 0x46, 0x2d, 0xab, 0x0f, 0x9b, 0x49, 0xd7, 0xfd, 0xf4, 0x01, 0x02, 0x7c, 0xdd,
    0x77, 0x19, 0xaf, 0x6a, 0x55, 0xaa, 0x10, 0x69, 0x88, 0x65, 0xde, 0x0e, 0x9d, 0xb0, 0x75, 0x2b,
    0x3f, 0xd6, 0xc2, 0x24, 0x07, 0x50, 0xb7, 0xb9, 0x81, 0x5a, 0xe2, 0x3a, 0x4c, 0x2c, 0x5e, 0xb6,
    0x80, 0x52, 0xf6, 0xec, 0x60, 0x8b, 0x56, 0x48, 0xed, 0x25, 0x30, 0xfe, 0x67, 0xf0, 0x09, 0xeb,
    0x8f, 0x03, 0x38, 0xd5, 0x7f, 0x68, 0x6f, 0x5d, 0xc7, 0xd4, 0xd9, 0x97, 0xac, 0x66, 0xe3, 0xea,
};

static unsigned char dec_key[256] = {
    0x26, 0xbc, 0xbd, 0xf1, 0x52, 0x44, 0x80, 0xd4, 0x46, 0xee, 0x7e, 0x88, 0x24, 0x5c, 0xcb, 0xb6,
    0xc6, 0x2c, 0x45, 0x81, 0x5b, 0x3c, 0x8e, 0x6b, 0xb0, 0xc1, 0xa6, 0x5f, 0x05, 0x2e, 0x58, 0x27,
    0x94, 0x31, 0x6a, 0xaa, 0xd3, 0xe9, 0x4b, 0x77, 0xaf, 0x0e, 0x5d, 0xcf, 0xdd, 0xb4, 0x2d, 0x22,
    0xea, 0x12, 0x4c, 0x59, 0x00, 0x60, 0x70, 0x10, 0xf2, 0x34, 0xdb, 0x96, 0x50, 0x03, 0x21, 0xd0,
    0x6f, 0x02, 0x29, 0x9a, 0x2f, 0x73, 0xb3, 0x16, 0xe7, 0xb8, 0x48, 0x6e, 0xdc, 0x19, 0x1f, 0x87,
    0xd5, 0x1d, 0xe1, 0x3e, 0x1a, 0xc4, 0xe6, 0x3d, 0x9d, 0x56, 0xd9, 0x6d, 0x11, 0xf7, 0xde, 0x42,
    0xe4, 0x8c, 0x66, 0x41, 0x0c, 0xc9, 0xfd, 0xec, 0xf5, 0xc7, 0xc3, 0x33, 0xa0, 0x39, 0x1e, 0xf6,
    0x9e, 0x3a, 0xa4, 0x7b, 0x35, 0xce, 0x4a, 0xc0, 0x57, 0x37, 0x53, 0x85, 0xbe, 0x4e, 0x8a, 0xf4,
    0xe0, 0xd8, 0x75, 0x51, 0x83, 0x86, 0xa9, 0x40, 0xc8, 0x6c, 0x54, 0xe5, 0x9b, 0x5a, 0x2a, 0xf0,
    0x20, 0x06, 0x13, 0x64, 0x49, 0x74, 0x8f, 0xfb, 0x28, 0x79, 0x7c, 0xb7, 0x98, 0xcc, 0x23, 0x0a,
    0x92, 0x99, 0x38, 0xae, 0x5e, 0x2b, 0x15, 0x89, 0x32, 0x43, 0xc5, 0xb5, 0xfc, 0x84, 0x18, 0xc2,
    0xcd, 0x1c, 0xad, 0x8d, 0xa5, 0x0d, 0xdf, 0xd6, 0x95, 0xd7, 0xa8, 0x63, 0x1b, 0x97, 0x14, 0x04,
    0x71, 0xab, 0xd2, 0x82, 0x0b, 0x69, 0x36, 0xf8, 0xb1, 0x76, 0xa1, 0x8b, 0x09, 0x7a, 0x08, 0x72,
    0x17, 0x55, 0x61, 0x9c, 0xf9, 0xf3, 0xd1, 0xb9, 0xa2, 0xfa, 0xb2, 0x0f, 0x91, 0xbf, 0xca, 0xa7,
    0x30, 0x78, 0xda, 0xfe, 0x93, 0x07, 0x7d, 0x01, 0x47, 0x67, 0xff, 0xef, 0xe3, 0xe8, 0x7f, 0x25,
    0xed, 0x4d, 0x4f, 0x3f, 0xbb, 0x62, 0xe2, 0xa3, 0x90, 0x68, 0x3b, 0x9f, 0x65, 0xba, 0xeb, 0xac,
};




/*
** Name used to identify this VFS.
*/
#define FS_VFS_NAME "ENCRYPT_VFS"

/*
** Method declarations for fs_file.
*/
static int fsClose(sqlite3_file*);
static int fsRead(sqlite3_file*, void*, int iAmt, sqlite3_int64 iOfst);
static int fsWrite(sqlite3_file*, const void*, int iAmt, sqlite3_int64 iOfst);
static int fsTruncate(sqlite3_file*, sqlite3_int64 size);
static int fsSync(sqlite3_file*, int flags);
static int fsFileSize(sqlite3_file*, sqlite3_int64 *pSize);
static int fsLock(sqlite3_file*, int);
static int fsUnlock(sqlite3_file*, int);
static int fsCheckReservedLock(sqlite3_file*, int *pResOut);
static int fsFileControl(sqlite3_file*, int op, void *pArg);
static int fsSectorSize(sqlite3_file*);
static int fsDeviceCharacteristics(sqlite3_file*);
static int fsShmMap(sqlite3_file*, int iPg, int pgsz, int po, void volatile**);
static int fsShmLock(sqlite3_file*, int offset, int n, int flags);
static void fsShmBarrier(sqlite3_file*);
static int fsShmUnmap(sqlite3_file*, int deleteFlag);
static int fsFetch(sqlite3_file*, sqlite3_int64 iOfst, int iAmt, void **pp);
static int fsUnfetch(sqlite3_file*, sqlite3_int64 iOfst, void *p);



/*
** Method declarations for fs_vfs.
*/
static int fsOpen(sqlite3_vfs*, const char *, sqlite3_file*, int , int *);
static int fsDelete(sqlite3_vfs*, const char *zName, int syncDir);
static int fsAccess(sqlite3_vfs*, const char *zName, int flags, int *);
static int fsFullPathname(sqlite3_vfs*, const char *zName, int nOut,char *zOut);
static void *fsDlOpen(sqlite3_vfs*, const char *zFilename);
static void fsDlError(sqlite3_vfs*, int nByte, char *zErrMsg);
static void (*fsDlSym(sqlite3_vfs*,void*, const char *zSymbol))(void);
static void fsDlClose(sqlite3_vfs*, void*);
static int fsRandomness(sqlite3_vfs*, int nByte, char *zOut);
static int fsSleep(sqlite3_vfs*, int microseconds);
static int fsCurrentTime(sqlite3_vfs*, double*pTimeOut);
static int fsGetLastError(sqlite3_vfs *pVfs, int nb, char *str);
static int fsCurrentTimeInt64(sqlite3_vfs*, sqlite3_int64 *pTimeOut);
static int fsSetSystemCall(sqlite3_vfs*, const char *zName, sqlite3_syscall_ptr sc);
static sqlite3_syscall_ptr fsGetSystemCall(sqlite3_vfs*, const char *zName);
static const char* fsNextSystemCall(sqlite3_vfs*, const char *zName);

typedef struct fs_vfs_t fs_vfs_t;
struct fs_vfs_t {
    sqlite3_vfs         base;
    sqlite3_vfs         *pParent;
};


typedef struct encrypt_file_t encrypt_file_t;
struct encrypt_file_t {
    sqlite3_file        base;
    sqlite3_file*       parent;
};


static fs_vfs_t fs_vfs = {
    {
        1,                                          /* iVersion */
        0,                                          /* szOsFile */
        0,                                          /* mxPathname */
        0,                                          /* pNext */
        FS_VFS_NAME,                                /* zName */
        0,                                          /* pAppData */
        fsOpen,                                     /* xOpen */
        fsDelete,                                   /* xDelete */
        fsAccess,                                   /* xAccess */
        fsFullPathname,                             /* xFullPathname */
        fsDlOpen,                                   /* xDlOpen */
        fsDlError,                                  /* xDlError */
        fsDlSym,                                    /* xDlSym */
        fsDlClose,                                  /* xDlClose */
        fsRandomness,                               /* xRandomness */
        fsSleep,                                    /* xSleep */
        fsCurrentTime,                              /* xCurrentTime */
        fsGetLastError,

        fsCurrentTimeInt64,

        fsSetSystemCall,
        fsGetSystemCall,
        fsNextSystemCall
    },
    0,                                            /* pParent */
};

static sqlite3_io_methods fs_io_methods = {
    1,                            /* iVersion */
    fsClose,                      /* xClose */
    fsRead,                       /* xRead */
    fsWrite,                      /* xWrite */
    fsTruncate,                   /* xTruncate */
    fsSync,                       /* xSync */
    fsFileSize,                   /* xFileSize */
    fsLock,                       /* xLock */
    fsUnlock,                     /* xUnlock */
    fsCheckReservedLock,          /* xCheckReservedLock */
    fsFileControl,                /* xFileControl */
    fsSectorSize,                 /* xSectorSize */
    fsDeviceCharacteristics,      /* xDeviceCharacteristics */
    fsShmMap,                     /* xShmMap */
    fsShmLock,                    /* xShmLock */
    fsShmBarrier,                 /* xShmBarrier */
    fsShmUnmap,                   /* xShmUnmap */
    fsFetch,
    fsUnfetch
};



/*
** Close an fs-file.
*/
static int fsClose(sqlite3_file *pFile)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    int r = p->parent->pMethods->xClose(p->parent);
    sqlite3_free(p->parent);
    p->parent = NULL;

    return r;
}


/*
** Read data from an fs-file.
*/
static int fsRead(
        sqlite3_file *pFile,
        void *zBuf,
        int iAmt,
        sqlite_int64 iOfst
        )
{
    int rc = SQLITE_OK;
    int i;
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    unsigned char *pbuf, *buf;
    pbuf = zBuf;
    buf = (unsigned char*) sqlite3_malloc(iAmt);
    rc = p->parent->pMethods->xRead(p->parent, buf, iAmt, iOfst);
    for(i=0; i<iAmt; i++) pbuf[i] = dec_key[buf[i]];
    sqlite3_free(buf);

    return rc;
}

/*
** Write data to an fs-file.
*/
static int fsWrite(
        sqlite3_file *pFile,
        const void *zBuf,
        int iAmt,
        sqlite_int64 iOfst
        )
{
    int rc = SQLITE_OK;
    int i;
    unsigned char *pbuf, *buf;
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    pbuf = (unsigned char*) zBuf;
    buf = (unsigned char*) sqlite3_malloc(iAmt);
    for(i=0; i<iAmt; i++) buf[i] = enc_key[pbuf[i]];

    rc = p->parent->pMethods->xWrite(p->parent, buf, iAmt, iOfst);

    sqlite3_free(buf);

    return rc;
}

/*
** Truncate an fs-file.
*/
static int fsTruncate(sqlite3_file *pFile, sqlite_int64 size)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xTruncate(p->parent, size);
}

/*
** Sync an fs-file.
*/
static int fsSync(sqlite3_file *pFile, int flags)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xSync(p->parent, flags);
}

/*
** Return the current file-size of an fs-file.
*/
static int fsFileSize(sqlite3_file *pFile, sqlite_int64 *pSize)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xFileSize(p->parent, pSize);
}

/*
** Lock an fs-file.
*/
static int fsLock(sqlite3_file *pFile, int eLock)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xLock(p->parent, eLock);
}

/*
** Unlock an fs-file.
*/
static int fsUnlock(sqlite3_file *pFile, int eLock)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xUnlock(p->parent, eLock);
}

/*
** Check if another file-handle holds a RESERVED lock on an fs-file.
*/
static int fsCheckReservedLock(sqlite3_file *pFile, int *pResOut)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xCheckReservedLock(p->parent, pResOut);
}

/*
** File control method. For custom operations on an fs-file.
*/
static int fsFileControl(sqlite3_file *pFile, int op, void *pArg)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xFileControl(p->parent, op, pArg);
}

/*
** Return the sector-size in bytes for an fs-file.
*/
static int fsSectorSize(sqlite3_file *pFile)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xSectorSize(p->parent);
}

/*
** Return the device characteristic flags supported by an fs-file.
*/
static int fsDeviceCharacteristics(sqlite3_file *pFile)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xDeviceCharacteristics(p->parent);
}


static int fsShmMap(sqlite3_file *pFile, int iPg, int pgsz, int po, void volatile** c)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xShmMap(p->parent, iPg, pgsz, po, c);
}

static int fsShmLock(sqlite3_file *pFile, int offset, int n, int flags)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xShmLock(p->parent, offset, n, flags);
}

static void fsShmBarrier(sqlite3_file *pFile)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xShmBarrier(p->parent);
}

static int fsShmUnmap(sqlite3_file* pFile, int deleteFlag)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xShmUnmap(p->parent, deleteFlag);
}

static int fsFetch(sqlite3_file* pFile, sqlite3_int64 iOfst, int iAmt, void **pp)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xFetch(p->parent, iOfst, iAmt, pp);
}

static int fsUnfetch(sqlite3_file* pFile, sqlite3_int64 iOfst, void *pb)
{
    encrypt_file_t *p = (encrypt_file_t *)pFile;

    return p->parent->pMethods->xUnfetch(p->parent, iOfst, pb);
}





/*
** Open an fs file handle.
*/
static int fsOpen(
        sqlite3_vfs *pVfs,
        const char *zName,
        sqlite3_file *pFile,
        int flags,
        int *pOutFlags
        )
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    encrypt_file_t *pFileEnc = (encrypt_file_t *)pFile;
    int rc = SQLITE_OK;

    sqlite3_file *parFile = (sqlite3_file*) sqlite3_malloc(pParent->szOsFile);
    rc = pParent->xOpen(pParent, zName, parFile, flags, pOutFlags);

    pFileEnc->base.pMethods = &fs_io_methods;
    pFileEnc->parent = parFile;

    return rc;
}

/*
** Delete the file located at zPath. If the dirSync argument is true,
** ensure the file-system modifications are synced to disk before
** returning.
*/
static int fsDelete(sqlite3_vfs *pVfs, const char *zPath, int dirSync)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xDelete(pParent, zPath, dirSync);
}


/*
** Test for access permissions. Return true if the requested permission
** is available, or false otherwise.
*/
static int fsAccess(
        sqlite3_vfs *pVfs,
        const char *zPath,
        int flags,
        int *pResOut
        )
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xAccess(pParent, zPath, flags, pResOut);
}

/*
** Populate buffer zOut with the full canonical pathname corresponding
** to the pathname in zPath. zOut is guaranteed to point to a buffer
** of at least (FS_MAX_PATHNAME+1) bytes.
*/
static int fsFullPathname(
        sqlite3_vfs *pVfs,            /* Pointer to vfs object */
        const char *zPath,            /* Possibly relative input path */
        int nOut,                     /* Size of output buffer in bytes */
        char *zOut                    /* Output buffer */
        )
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xFullPathname(pParent, zPath, nOut, zOut);
}

/*
** Open the dynamic library located at zPath and return a handle.
*/
static void *fsDlOpen(sqlite3_vfs *pVfs, const char *zPath)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xDlOpen(pParent, zPath);
}

/*
** Populate the buffer zErrMsg (size nByte bytes) with a human readable
** utf-8 string describing the most recent error encountered associated
** with dynamic libraries.
*/
static void fsDlError(sqlite3_vfs *pVfs, int nByte, char *zErrMsg)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xDlError(pParent, nByte, zErrMsg);
}

/*
** Return a pointer to the symbol zSymbol in the dynamic library pHandle.
*/
static void (*fsDlSym(sqlite3_vfs *pVfs, void *pH, const char *zSym))(void)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xDlSym(pParent, pH, zSym);
}

/*
** Close the dynamic library handle pHandle.
*/
static void fsDlClose(sqlite3_vfs *pVfs, void *pHandle)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xDlClose(pParent, pHandle);
}

/*
** Populate the buffer pointed to by zBufOut with nByte bytes of
** random data.
*/
static int fsRandomness(sqlite3_vfs *pVfs, int nByte, char *zBufOut)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xRandomness(pParent, nByte, zBufOut);
}

/*
** Sleep for nMicro microseconds. Return the number of microseconds
** actually slept.
*/
static int fsSleep(sqlite3_vfs *pVfs, int nMicro)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xSleep(pParent, nMicro);
}

/*
** Return the current time as a Julian Day number in *pTimeOut.
*/
static int fsCurrentTime(sqlite3_vfs *pVfs, double *pTimeOut)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xCurrentTime(pParent, pTimeOut);
}

static int fsGetLastError(sqlite3_vfs *pVfs, int nb, char *str)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xGetLastError(pParent, nb, str);
}

static int fsCurrentTimeInt64(sqlite3_vfs *pVfs, sqlite3_int64 *pTimeOut)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xCurrentTimeInt64(pParent, pTimeOut);
}

static int fsSetSystemCall(sqlite3_vfs *pVfs, const char *zName, sqlite3_syscall_ptr sc)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xSetSystemCall(pParent, zName, sc);
}

static sqlite3_syscall_ptr fsGetSystemCall(sqlite3_vfs *pVfs, const char *zName)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xGetSystemCall(pParent, zName);
}

static const char* fsNextSystemCall(sqlite3_vfs *pVfs, const char *zName)
{
    sqlite3_vfs *pParent = fs_vfs.pParent;
    return pParent->xNextSystemCall(pParent, zName);
}



/*
** This procedure registers the fs vfs with SQLite. If the argument is
** true, the fs vfs becomes the new default vfs. It is the only publicly
** available function in this file.
*/
int vfs_encrypt_register(void)
{
    if( fs_vfs.pParent ) return SQLITE_OK;

    fs_vfs.pParent = sqlite3_vfs_find(0);
    fs_vfs.base.mxPathname = fs_vfs.pParent->mxPathname;
    fs_vfs.base.szOsFile = sizeof(encrypt_file_t);

    return sqlite3_vfs_register(&fs_vfs.base, 1);
}

