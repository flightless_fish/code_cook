#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <iostream>

#include "GSLAM/core/Svar.h"

#include "sqlite3pp.h"

using namespace std;

extern "C" {
    int vfs_encrypt_register(void);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int t_drop_table(void)
{
    int ret = 0;

    sqlite3pp::database db("test.db");
    ret = db.execute("DROP TABLE contacts;");

    printf("ret = %d\n", ret);

    return 0;
}

int t_create_table(void)
{
    int ret = 0;

    sqlite3pp::database db("test.db");
    ret = db.execute("CREATE TABLE contacts ("
                     "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                     "name VARCHAR(255), "
                     "phone VARCHAR(255));");

    printf("ret = %d\n", ret);

    return 0;
}

int t_insert_data(void)
{
    int ret = 0;

    sqlite3pp::database db("test.db");

    // insert method 1
    {
        sqlite3pp::command cmd(db,
                               "INSERT INTO contacts (name, phone) "
                               "VALUES(?, ?)");
        cmd.binder() << "adfasdf" << "12323423";
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }

    // insert method 2
    {
        sqlite3pp::command cmd(db,
                               "INSERT INTO contacts (name, phone) "
                               "VALUES(?, ?)");
        cmd.bind(1, "name_xx", sqlite3pp::nocopy);
        cmd.bind(2, "444-5234234", sqlite3pp::nocopy);
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }

    // insert method 3
    {
        sqlite3pp::command cmd(db,
                               "INSERT INTO contacts (name, phone) "
                               "VALUES (?100, ?101)");
        cmd.bind(100, "Mike", sqlite3pp::nocopy);
        cmd.bind(101, "555-1234", sqlite3pp::nocopy);
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }

    // insert method 4
    {
        sqlite3pp::command cmd(
                    db, "INSERT INTO contacts (name, phone) VALUES (:user, :phone)");
        cmd.bind(":user", "Mike", sqlite3pp::nocopy);
        cmd.bind(":phone", "555-1234", sqlite3pp::nocopy);
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }

    // insert method 5
    {
        sqlite3pp::command cmd(
                    db,
                    "INSERT INTO contacts (name, phone) VALUES (:user, '555-0000');"
                    "INSERT INTO contacts (name, phone) VALUES (:user, '555-1111');"
                    "INSERT INTO contacts (name, phone) VALUES (:user, '555-2222')");
        cmd.bind(":user", "Mike", sqlite3pp::nocopy);
        ret = cmd.execute_all();
        printf("ret = %d\n", ret);
    }

    return 0;
}

int t_transaction(void)
{
    sqlite3pp::database db("test.db");

    sqlite3pp::transaction xct(db);
    {
        sqlite3pp::command cmd(
                    db, "INSERT INTO contacts (name, phone) VALUES (:user, :phone)");
        cmd.bind(":user", "Mike", sqlite3pp::nocopy);
        cmd.bind(":phone", "555-1234", sqlite3pp::nocopy);
        cmd.execute();
    }
    xct.rollback();

    return 0;
}

int t_query(void)
{
    sqlite3pp::database db("test.db");

    // method 1
    {
        sqlite3pp::query qry(db, "SELECT id, name, phone FROM contacts");

        for (int i = 0; i < qry.column_count(); ++i) {
            cout << qry.column_name(i) << "\t";
        }
        cout << "\n";

        for (sqlite3pp::query::iterator i = qry.begin(); i != qry.end(); ++i) {
            for (int j = 0; j < qry.column_count(); ++j) {
                cout << (*i).get<char const*>(j) << "\t";
            }
            cout << endl;
        }
    }

    cout << "\n\n";

    // method 2
    {
        sqlite3pp::query qry(db, "SELECT id, name, phone FROM contacts");

        for (int i = 0; i < qry.column_count(); ++i) {
            cout << qry.column_name(i) << "\t";
        }
        cout << "\n";

        for (sqlite3pp::query::iterator i = qry.begin(); i != qry.end(); ++i) {
            int id;
            char const* name, *phone;
            std::tie(id, name, phone) =
                    (*i).get_columns<int, char const*, char const*>(0, 1, 2);
            cout << id << "\t" << name << "\t" << phone << endl;
        }
    }

    cout << "\n\n";

    // method 3
    {
        sqlite3pp::query qry(db, "SELECT id, name, phone FROM contacts");

        for (int i = 0; i < qry.column_count(); ++i) {
            cout << qry.column_name(i) << "\t";
        }
        cout << "\n";

        for (sqlite3pp::query::iterator i = qry.begin(); i != qry.end(); ++i) {
            string name, phone;
            (*i).getter() >> sqlite3pp::ignore >> name >> phone;
            cout << "\t" << name << "\t" << phone << endl;
        }
    }

    cout << "\n\n";

    // method 4
    {
        sqlite3pp::query qry(db, "SELECT id, name, phone FROM contacts");

        for (int i = 0; i < qry.column_count(); ++i) {
            cout << qry.column_name(i) << "\t";
        }
        cout << "\n";

        for (auto v : qry) {
            string name, phone;
            v.getter() >> sqlite3pp::ignore >> name >> phone;
            cout << "\t" << name << "\t" << phone << endl;
        }
    }
}

int t_update_data(void)
{
    int ret = 0;

    sqlite3pp::database db("test.db");

    // update method 1
    {
        sqlite3pp::command cmd(db,
                               "UPDATE contacts SET name=?, phone=? WHERE id=1;");
        cmd.bind(1, "upd_n1", sqlite3pp::nocopy);
        cmd.bind(2, "666-0000", sqlite3pp::nocopy);
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }

    // update method 2
    {
        sqlite3pp::command cmd(db,
                               "UPDATE contacts SET name=?100, phone=?101 WHERE id=?110;");
        cmd.bind(100, "upd_n2", sqlite3pp::nocopy);
        cmd.bind(101, "666-0010", sqlite3pp::nocopy);
        cmd.bind(110, "3", sqlite3pp::nocopy);
        ret = cmd.execute();
        printf("ret = %d\n", ret);
    }
}

int t_vfs(void)
{
    vfs_encrypt_register();

    t_create_table();
    t_insert_data();
    t_transaction();
    t_query();

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "t_create_table");

    #define DEF_TEST(f) if ( act == #f ) return f();

    DEF_TEST(t_create_table);
    DEF_TEST(t_insert_data);
    DEF_TEST(t_transaction);
    DEF_TEST(t_query);
    DEF_TEST(t_update_data);
    DEF_TEST(t_vfs);

    return 0;
}
