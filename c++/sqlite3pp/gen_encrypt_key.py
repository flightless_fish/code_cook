 
import random

def print_array(a, vname):
    print("unsigned char %s[256] = {" % vname)
    for i in range(16):
        b = a[i*16:(i+1)*16]
        s = ["0x%02x" % j for j in b]
        ss = ", ".join(s)
        print("    %s," % ss)
    print("};")
    
a = [i for i in range(256)]
for i in range(10): random.shuffle(a)

enc_key = a
dec_key = [i for i in range(256)]
for i in enc_key:
    dec_key[enc_key[i]] = i
    
print_array(enc_key, "enc_key")
print_array(dec_key, "dec_key")
