#include <stdio.h>
#include <stdlib.h>

#include <vector>

#include "GSLAM/core/Svar.h"

#include "argparse.hpp"
#include "OptionParser.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int t_argparse()
{
    int argc = svar.GetInt("argc", 1);
    char **argv = (char**) svar.GetPointer("argv", NULL);

    ArgumentParser ap;

    ap.addArgument("-a", "--action", 1);

    ap.parse(argc, (const char**)argv);
    int act = ap.retrieve<int>("action");

    if( act == 1 ) printf("hello act = 1\n");

    return 0;
}

int t_optparse()
{
    int argc = svar.GetInt("argc", 1);
    char **argv = (char**) svar.GetPointer("argv", NULL);

    using optparse::OptionParser;

    OptionParser op = OptionParser().description("Test of OptionParser");
    op.add_option("-a", "--action").dest("action").help("Action type").metavar("NUM").set_default(0);

    optparse::Values opts = op.parse_args(argc, argv);
    std::vector<std::string> args = op.args();

    // check opt is set
    printf("is_set('action')         = %d\n", opts.is_set("action"));
    printf("is_set_by_user('action') = %d\n", opts.is_set_by_user("action"));
    printf("\n");

    int a = opts.get("action");
    printf("act = %d\n", a);
    printf("args_n = %d\n", args.size());
    for(int i=0; i<args.size(); i++) printf("    [%2d] %s\n", i, args[i].c_str());

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.GetInt("argc", 1) = argc;
    svar.GetPointer("argv", NULL) = argv;

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "t_argparse");

    DEF_TEST(t_argparse);
    DEF_TEST(t_optparse);

    return 0;
}
