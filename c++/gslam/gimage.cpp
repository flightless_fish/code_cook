
#include <stdio.h>
#include <stdlib.h>

#include "GSLAM/core/GImage.h"
#include "GSLAM/core/Glog.h"
#include "GSLAM/core/Svar.h"

#include "GImageIO/GImage_IO.h"
#include "utils.h"

int test_gimage_io(const std::string& fname)
{
    GSLAM::GImage img;

    img = GSLAM::imread(fname.c_str());
    if( img.empty() ) {
        LOG(ERROR) << "Can not open file: " << fname;
        return -1;
    }

    std::string fnBase, fnExt, fnOut;
    fnBase = path_getFileBase(fname);
    fnExt = path_getFileExt(fname);

    fnOut = fnBase + "_out." + fnExt;
    printf("read %s -> write %s\n", fname.c_str(), fnOut.c_str());

    GSLAM::imwrite(fnOut.c_str(), img);

    return 0;
}

int gimage_io()
{
    std::string fnBase = svar.GetString("fname", "../data/test");

    test_gimage_io(fnBase + ".bmp");
    test_gimage_io(fnBase + ".jpg");
    test_gimage_io(fnBase + ".png");

    return 0;
}



////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "gimage_io");

    DEF_TEST(gimage_io);
}
