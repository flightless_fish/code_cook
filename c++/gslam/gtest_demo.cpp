#include <iostream>

#include "gtest.h"


/**
    Demo usage of gest

    References:
        http://www.cnblogs.com/coderzh/archive/2009/04/06/1430364.html
**/

TEST(Demo1, CheckTypeSize)
{
    EXPECT_TRUE(sizeof(int) == 4);
    EXPECT_TRUE(sizeof(float) == 4);

    EXPECT_EQ(sizeof(double), 8);
}


int Foo(int a, int b)
{
    if (a == 0 || b == 0)
    {
        throw "don't do that";
    }
    int c = a % b;
    if (c == 0)
        return b;
    return Foo(b, c);
}

TEST(FooTest, HandleZeroInput)
{
    EXPECT_ANY_THROW(Foo(10, 0));
    EXPECT_THROW(Foo(0, 5), char*);
}


// Initialize test framework
int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, (char**) argv);
    int ret = RUN_ALL_TESTS();

    return ret;
}

