#ifndef __UTILS_FS_H__
#define __UTILS_FS_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <string>
#include <algorithm>
#include <vector>


namespace utils_fs {

// string array type
typedef std::vector<std::string> StringArray;



////////////////////////////////////////////////////////////////////////////////
/// string utils
////////////////////////////////////////////////////////////////////////////////

inline int char_is_delims(char c, char d)
{
    if( c == d )
        return 1;
    else
        return 0;
}

inline StringArray split_text(const std::string &intext, const std::string &delims)
{
    StringArray         r;

    int                 st;
    int                 n, nd, i, j, k, dd;
    char                *buf;
    const char          *pb, *pd;


    n = intext.size();
    nd = delims.size();

    pb = intext.c_str();
    pd = delims.c_str();

    buf = new char[n+10];

    st = 0;
    i = 0;
    k = 0;
    buf[0] = 0;

    while( i<n ) {
        for(dd = 0, j=0; j<nd; j++) dd += char_is_delims(pb[i], pd[j]);

        if( dd > 0 ) {
            buf[k] = 0;
            r.push_back(buf);

            k = 0;
            st = 1;
        } else {
            buf[k++] = pb[i];
            st = 0;
        }

        i++;
    }

    // process last character
    if( st == 0 ) {
        buf[k] = 0;
        r.push_back(buf);
    } else {
        buf[0] = 0;
        r.push_back(buf);
    }

    delete [] buf;

    return r;
}

inline std::string join_text(const StringArray& sa, const std::string& delims)
{
    std::string s;

    for(int i=0; i<sa.size(); i++) {
        if( i == 0 ) s = sa[i];
        else         s = s + delims + sa[i];
    }

    return s;
}


inline std::string& str_toupper(std::string &s)
{
    for(size_t i=0; i < s.size(); i++) {
        s[i] = toupper(s[i]);
    }

    return s;
}

inline std::string& str_tolower(std::string &s)
{
    for(size_t i=0; i < s.size(); i++) {
        s[i] = tolower(s[i]);
    }

    return s;
}


inline std::string trim(const std::string &s)
{
    std::string              delims = " \t\n\r",
                             r;
    std::string::size_type   i, j;

    i = s.find_first_not_of(delims);
    j = s.find_last_not_of(delims);

    if( i == std::string::npos ) {
        r = "";
        return r;
    }

    if( j == std::string::npos ) {
        r = "";
        return r;
    }

    r = s.substr(i, j-i+1);
    return r;
}



inline std::string& trim2(std::string& s)
{
    if (s.empty()) {
        return s;
    }

    std::string& r = s;
    std::string::iterator c;

    // Erase whitespace before the string
    for (c = r.begin(); c != r.end() && isspace(*c++););
    r.erase(r.begin(), --c);

    // Erase whitespace after the string
    for (c = r.end(); c != r.begin() && isspace(*--c););
    r.erase(++c, r.end());

    return r;
}


// string trim functions
inline std::string ltrim(const std::string &s)
{
    std::string             delims = " \t\n\r",
                            r;
    std::string::size_type  i;

    i = s.find_first_not_of(delims);
    if( i == std::string::npos )
        r = "";
    else
        r = s.substr(i, s.size() - i);

    return r;
}


inline std::string rtrim(const std::string &s)
{
    std::string             delims = " \t\n\r",
                            r;
    std::string::size_type  i;

    i = s.find_last_not_of(delims);
    if( i == std::string::npos )
        r = "";
    else
        r = s.substr(0, i+1);

    return r;
}


inline int str_to_int(const std::string &s)
{
    return atoi(s.c_str());
}

inline float str_to_float(const std::string &s)
{
    return atof(s.c_str());
}

inline double str_to_double(const std::string &s)
{
    return atof(s.c_str());
}


////////////////////////////////////////////////////////////////////////////////
/// path utils
////////////////////////////////////////////////////////////////////////////////

#if _WIN32 // WIN

#include <windows.h>
#include <io.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

inline bool path_exist(const std::string& fnpath)
{
    if( fnpath.size() == 0 ) return false;

    DWORD attr = GetFileAttributesA(fnpath.c_str());
    if (attr == INVALID_FILE_ATTRIBUTES)
    {
        switch (GetLastError())
        {
        case ERROR_FILE_NOT_FOUND:
        case ERROR_PATH_NOT_FOUND:
        case ERROR_NOT_READY:
        case ERROR_INVALID_DRIVE:
            return false;
        default:
            //handleLastErrorImpl(_path);
            return false;
        }
    }

    return true;
}

inline int _mkdir(const std::string& fnpath)
{
    if ( CreateDirectoryA(fnpath.c_str(), 0) )
        return 1;
    else
        return 0;
}

inline int path_mkdir(const std::string& path)
{
    if( path.size() == 0 ) return false;

    StringArray sa = split_text(path, "/\\");
    int n = sa.size();
    if( n < 1 ) return false;

    // try to create dir
    std::string p = sa[0];
    if( !path_exist(p) ) {
        if( !_mkdir(p) )
            return -1;
    }

    for(int i=1; i<n; i++) {
        p = p + "\\" + sa[i];

        if( path_exist(p) )
            continue;
        else {
            if( !_mkdir(p) ) {
                return -1;
            }
        }
    }

    return 0;
}

inline BOOL IsDots(const char* str)
{
    if(strcmp(str,".") && strcmp(str,"..")) return FALSE;
    return TRUE;
}

inline BOOL DeleteDirectory(const char* sPath)
{
    HANDLE hFind;  // file handle
    WIN32_FIND_DATA FindFileData;

    char DirPath[MAX_PATH];
    char FileName[MAX_PATH];

    strcpy(DirPath,sPath);
    strcat(DirPath,"\\*");    // searching all files
    strcpy(FileName,sPath);
    strcat(FileName,"\\");

    hFind = FindFirstFileA(DirPath,&FindFileData); // find the first file
    if(hFind == INVALID_HANDLE_VALUE) return FALSE;
    strcpy(DirPath,FileName);

    bool bSearch = true;
    while(bSearch) { // until we finds an entry
        if(FindNextFileA(hFind,&FindFileData)) {
            if(IsDots(FindFileData.cFileName)) continue;
            strcat(FileName,FindFileData.cFileName);
            if((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {

                // we have found a directory, recurse
                if(!DeleteDirectory(FileName)) {
                    FindClose(hFind);
                    return FALSE; // directory couldn't be deleted
                }
                RemoveDirectoryA(FileName); // remove the empty directory
                strcpy(FileName,DirPath);
            }
            else {
                if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
                    _chmod(FileName, _S_IWRITE); // change read-only file mode
                if(!DeleteFileA(FileName)) {  // delete the file
                    FindClose(hFind);
                    return FALSE;
                }
                strcpy(FileName,DirPath);
            }
        }
        else {
            if(GetLastError() == ERROR_NO_MORE_FILES) // no more files there
            bSearch = false;
            else {
                // some error occured, close the handle and return FALSE
                FindClose(hFind);
                return FALSE;
            }
        }
    }
    FindClose(hFind);  // closing file handle

    return RemoveDirectory(sPath); // remove the empty directory
}

inline int path_rmdir(const std::string &path)
{
    if( TRUE == DeleteDirectory(path.c_str()) )
        return 0;
    else
        return -1;
}

inline int path_rmfile(const std::string& path)
{
    // remove a file object
    if ( !DeleteFileA(path.c_str()) ) {
        fprintf(stderr, "Can`t remove a file: %s\n", path.c_str());
        return -1;
    }

    return 0;
}

inline int path_isdir(const std::string &p)
{
    struct _stat    st;
    int             ret;

    ret = _stat(p.c_str(), &st);
    if( ret == -1 ) {
        fprintf(stderr, "ERR: Failed at stat: %s", p.c_str());
        return 0;
    }

    if ( (st.st_mode & S_IFMT) == S_IFDIR )
        return 1;
    else
        return 0;
}

inline int path_isfile(const std::string &p)
{
    struct _stat    st;
    int             ret;

    ret = _stat(p.c_str(), &st);
    if( ret == -1 ) {
        fprintf(stderr, "ERR: Failed at stat: %s", p.c_str());
        return 0;
    }

    if ( (st.st_mode & S_IFMT) == S_IFREG )
        return 1;
    else
        return 0;
}

inline int path_lsdir(const std::string &dir_name, StringArray &dl, int sortFiles)
{
    HANDLE hFind;  // file handle
    WIN32_FIND_DATA FindFileData;

    hFind = FindFirstFileA(dir_name.c_str(), &FindFileData);  // find the first file
    if(hFind == INVALID_HANDLE_VALUE) return -1;

    dl.clear();

    while( 1 ) {
        if(FindNextFileA(hFind, &FindFileData)) {
            if(IsDots(FindFileData.cFileName)) continue;

            dl.push_back(FindFileData.cFileName);
        } else {
            break;
        }
    }

    FindClose(hFind);  // closing file handle

    // sort all file name
    if( sortFiles ) std::sort(dl.begin(), dl.end());

    return 0;
}


#else // UNIX

#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

inline bool path_exist(const std::string& fnpath)
{
    struct stat sb;

    if( fnpath.size() == 0 ) return false;

    if( 0 == stat(fnpath.c_str(), &sb) && S_ISDIR(sb.st_mode) ) {
        return true;
    } else {
        return false;
    }
}

inline int path_mkdir_(const std::string& path)
{
    char cmds[2048];

    sprintf(cmds, "mkdir -p '%s'", path.c_str());
    return system(cmds)==0?0:-1;
}

inline int mkpath(std::string s, mode_t mode=0755)
{
    size_t pre=0,pos;
    std::string dir;
    int mdret;

    if(s[s.size()-1]!='/') {
        // force trailing / so we can handle everything in loop
        s+='/';
    }

    while((pos=s.find_first_of('/',pre))!=std::string::npos) {
        dir=s.substr(0,pos++);
        pre=pos;
        if(dir.size()==0) continue; // if leading / first time is 0 length
        if((mdret=mkdir(dir.c_str(),mode)) && errno!=EEXIST) {
            return mdret;
        }
    }
    return mdret;
}

inline int path_mkdir(const std::string& path)
{
    std::string p = path;

    return mkpath(p);
}

inline int path_rmdir(const std::string& path)
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    // stat for the path
    stat(path.c_str(), &stat_path);

    // if path does not exists or is not dir - exit with status -1
    if (S_ISDIR(stat_path.st_mode) == 0) {
        fprintf(stderr, "ERR: Is not directory: %s\n", path.c_str());
        return -1;
    }

    // if not possible to read the directory for this user
    if ((dir = opendir(path.c_str())) == NULL) {
        fprintf(stderr, "ERR: Can`t open directory: %s\n", path.c_str());
        return -2;
    }

    // the length of the path
    path_len = path.size();

    // iteration through entries in the directory
    while ((entry = readdir(dir)) != NULL) {

        // skip entries "." and ".."
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        // determinate a full path of an entry
        full_path = (char*) calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path.c_str());
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        // stat for the entry
        stat(full_path, &stat_entry);

        // recursively remove a nested directory
        if (S_ISDIR(stat_entry.st_mode) != 0) {
            path_rmdir(full_path);
            continue;
        }

        // remove a file object
        if (unlink(full_path) != 0)
            fprintf(stderr, "Can`t remove a file: %s\n", full_path);
    }

    // remove the devastated directory and close the object of it
    if (rmdir(path.c_str()) != 0)
        fprintf(stderr,"ERR: Can`t remove a directory: %s\n", path.c_str());

    closedir(dir);

    return 0;
}

inline int path_rmfile(const std::string& path)
{
    // remove a file object
    if ( unlink(path.c_str()) != 0 ) {
        fprintf(stderr, "Can`t remove a file: %s\n", path.c_str());
        return -1;
    }

    return 0;
}


inline int path_isdir(const std::string &p)
{
    struct stat     st;
    int             ret;

    ret = stat(p.c_str(), &st);
    if( ret == -1 ) {
        fprintf(stderr, "ERR: Failed at stat: %s", p.c_str());
        return 0;
    }

    if ( (st.st_mode & S_IFMT) == S_IFDIR )
        return 1;
    else
        return 0;
}

inline int path_isfile(const std::string &p)
{
    struct stat     st;
    int             ret;

    ret = stat(p.c_str(), &st);
    if( ret == -1 ) {
        fprintf(stderr, "ERR: Failed at stat: %s", p.c_str());
        return 0;
    }

    if ( (st.st_mode & S_IFMT) == S_IFREG )
        return 1;
    else
        return 0;
}


inline int path_lsdir(const std::string &dir_name, StringArray &dl, int sortFiles)
{
    DIR             *dir;
    struct dirent   *dp;

    // open directory
    dir = opendir(dir_name.c_str());
    if( dir == NULL ) {
        fprintf(stderr, "Failed to open dir: %s\n", dir_name.c_str());
        return -1;
    }

    // get each items
    dl.clear();
    for(dp=readdir(dir); dp!=NULL; dp=readdir(dir)) {
        // skip .
        if( strlen(dp->d_name) == 1 && dp->d_name[0] == '.' )
            continue;

        // skip ..
        if( strlen(dp->d_name) == 2 && dp->d_name[0] == '.' && dp->d_name[1] == '.' )
            continue;

        // add to list
        dl.push_back(dp->d_name);
    }

    closedir(dir);

    // sort all file name
    if( sortFiles ) std::sort(dl.begin(), dl.end());

    return 0;
}

#endif // end of _WIN32



inline std::string path_getFileName(const std::string& fname)
{
    size_t found;
    found = fname.find_last_of("/\\");
    if( found == std::string::npos ) return fname;
    return fname.substr(found+1);
}

inline std::string path_getPathName(const std::string& fname)
{
    size_t found;
    found = fname.find_last_of("/\\");
    if( found == std::string::npos ) return "";
    else                             return fname.substr(0,found);
}

inline std::string path_getFileBase(const std::string& fname)
{
    size_t found = fname.find_last_of(".");
    if( found != std::string::npos ) {
        if( found == 0 ) return "";
        else return fname.substr(0, found);
    } else {
        return fname;
    }
}

inline std::string path_getFileExt(const std::string& fname)
{
    size_t found = fname.find_last_of(".");
    if( found != std::string::npos ) {
        return fname.substr(found+1);
    } else {
        return "";
    }
}


inline StringArray path_split(const std::string &fname)
{
    size_t      found = -1;
    StringArray r;

    r.clear();

    /* find / or \ */
    found = fname.find_last_of("/\\");

    if( found == std::string::npos ) {
        r.push_back("");
        r.push_back(fname);
        return r;
    }

    // folder
    r.push_back(fname.substr(0, found));
    // file
    r.push_back(fname.substr(found+1));

    return r;
}

inline StringArray path_splitext(const std::string &fname)
{
    size_t      found;
    StringArray r;

    r.clear();

    // find .
    found = fname.find_last_of(".");
    if( found == std::string::npos ) {
        r.push_back(fname);
        r.push_back("");
        return r;
    }

    // filename
    r.push_back(fname.substr(0, found));
    // extname
    r.push_back(fname.substr(found));

    return r;
}

inline std::string path_join(const std::string &p1, const std::string &p2)
{
    std::string p;
    int         l;

    p = p1;

    l = p.size();
    if( p[l-1] == '/' || p[l-1] == '\\' )
        p = p.substr(0, l-1);

    p = p + "/" + p2;
    return p;
}

inline std::string path_join(const std::string &p1, const std::string &p2, const std::string &p3)
{
    std::string p;

    p = path_join(p1, p2);
    return path_join(p, p3);
}


inline std::string path_join(const StringArray &p)
{
    int         i, l;
    std::string p_all;

    p_all = "";
    for(i=0; i<p.size(); i++) {
        l = p_all.size();
        if( l>0 ) {
            if( p_all[l-1] == '/' || p_all[l-1] == '\\' )
                p_all = p_all.substr(0, l-1);
        }

        p_all = p_all + "/" + p[i];
    }

    return p_all;
}

} // end of namespace utils_fs

#endif // end of __UTILS_FS_H__



