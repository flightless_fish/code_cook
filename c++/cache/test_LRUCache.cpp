// tests LRUCache with vectors as key

#include <vector>
#include <map>
#include <cassert>
#include <iostream>
#include <string>
#include <sstream>
#include <memory>

#if 0
#include "LRUCache11.hpp"


using namespace std;
using namespace lru11;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// types for our cache
using KeyT = vector<int>;
using ValueT = vector<unsigned int>;
using KVMap = map<KeyT, ValueT>;

int test_vector_key(void)
{
    lru11::Cache<KeyT, ValueT, mutex, KVMap> cache;

    const KeyT key1{1,2,3}; //must be const for LRUCache(?)
    const ValueT val1{0,0,1};
    cache.insert(key1, val1);

    auto ret = cache.get(key1);
    assert (ret == val1);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace lru
{
    void test()
    {
        lru11::Cache<std::string, std::string> cache(3,0);
        cache.insert("hello", "world");
        cache.insert("foo", "bar");

        std::cout<<"checking refresh : "<<cache.get("hello")<<std::endl;
        cache.insert("hello1", "world1");
        cache.insert("foo1", "bar1");

    lru11::Cache<int, int> ic(2);
    ic.insert(1, 10);
    ic.insert(2, 20);
    int i = ic.getCopy(1);
    std::cout << "value : "<< i << std::endl;
    ic.insert(3, 30);
    std::cout << "value (old) : "<< i << std::endl;
    }
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef Cache<std::string, int32_t> KVCache;
typedef KVCache::node_type KVNode;
typedef KVCache::list_type KVList;

// test the vanilla version of the cache
void testNoLock()
{

    // with no lock
    auto cachePrint =
            [&] (const KVCache& c) {
                std::cout << "Cache (size: "<<c.size()<<") (max="<<c.getMaxSize()<<") (e="<<c.getElasticity()<<") (allowed:" << c.getMaxAllowedSize()<<")"<< std::endl;
                size_t index = 0;
                auto nodePrint = [&] (const KVNode& n) {
                    std::cout << " ... [" << ++index <<"] " << n.key << " => " << n.value << std::endl;
                };
                c.cwalk(nodePrint);
            };
    KVCache c(5, 2);
    c.insert("hello", 1);
    c.insert("world", 2);
    c.insert("foo", 3);
    c.insert("bar", 4);
    c.insert("blanga", 5);
    cachePrint(c);
    c.insert("toodloo", 6);
    cachePrint(c);
    c.insert("wagamama", 7);
    cachePrint(c);
    c.get("blanga");
    std::cout << "... blanga should move to the top..." << std::endl;
    cachePrint(c);
    c.get("foo");
    std::cout << "... foo should move to the top..." << std::endl;
    cachePrint(c);
}

// Test a thread-safe version of the cache with a std::mutex
void testWithLock()
{

    using LCache = Cache<std::string, std::string, std::mutex>;
    auto cachePrint2 =
                [&] (const LCache& c) {
                std::cout << "Cache (size: "<<c.size()<<") (max="<<c.getMaxSize()<<") (e="<<c.getElasticity()<<") (allowed:" << c.getMaxAllowedSize()<<")"<< std::endl;
                size_t index = 0;
                auto nodePrint = [&] (const LCache::node_type& n) {
                    std::cout << " ... [" << ++index <<"] " << n.key << " => " << n.value << std::endl;
                };
                c.cwalk(nodePrint);
    };
    // with a lock
    LCache lc(25,2);
    auto worker = [&] () {
        std::ostringstream os;
        os << std::this_thread::get_id();
        std::string id = os.str();

        for (int i = 0; i < 10; i++) {
            std::ostringstream os2;
            os2 << "id:"<<id<<":"<<i;
            lc.insert(os2.str(), id);

        }
    };
    std::vector<std::unique_ptr<std::thread>> workers;
    workers.reserve(100);
    for (int i = 0; i < 100; i++) {
        workers.push_back(std::unique_ptr<std::thread>(
                new std::thread(worker)));
    }

    for (const auto& w : workers) {
        w->join();
    }
    std::cout << "... workers finished!" << std::endl;
    cachePrint2(lc);
}

#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main()
{
    return 0;
}
