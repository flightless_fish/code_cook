cmake_minimum_required(VERSION 2.8)

pi_add_target(test_LRUCache    BIN test_LRUCache.cpp    REQUIRED Utils System)
pi_add_target(test_OfflineFIFO BIN test_OfflineFIFO.cpp utils_lz4.c REQUIRED Utils System)
