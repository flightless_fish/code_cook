
#include <GSLAM/core/Svar.h>
#include <GSLAM/core/Timer.h>

#include "utils_debug.h"
#include "OfflineFIFO.h"



int test_fifo(void)
{
    std::string path = svar.GetString("path", "testf");
    int write_n = svar.GetInt("write_n", 10);
    int nloop = svar.GetInt("nloop", 1000);
    GSLAM::Rate r(30);

    OfflineFifo buf(path, write_n);

    int32_t *pidx;
    int idx=0;

    for(int i=0; i<nloop; i++)
    {
        {
            uint64_t len = 1920*1080*3;
            uint8_t* data = (uint8_t*) malloc(len);
            pidx = (int32_t*) data;
            *pidx = idx ++;

            buf.push(data, len);
        }

        {
            uint64_t len = 1920*1080*3;
            uint8_t* data = (uint8_t*) malloc(len);
            pidx = (int32_t*) data;
            *pidx = idx ++;

            buf.push(data, len);
        }

        printf("buff size = %d\n", buf.size());

        {
            uint8_t* data = NULL;
            uint64_t len;

            if( 0 == buf.pop(&data, &len) )
            {
                pidx = (int32_t*) data;
                printf("get item [%09d] p=%09x\n", *pidx, data);

                if( data != NULL) free(data);
            }
        }

        r.sleep();
    }

    while( 1 )
    {
        if( buf.size() > 0 )
        {
            uint8_t* data = NULL;
            uint64_t len;

            if( 0 == buf.pop(&data, &len) )
            {
                pidx = (int32_t*) data;
                printf("get item [%09d] p=%09x\n", *pidx, data);

                if( data != NULL) free(data);
            }
        }

        r.sleep();
    }


    return 0;
}


/**
 * @brief test_deque_block
 * @return
 *
 * Note:
 *  Use SPtr to store vector<unsigned char> data block, the meory can not be released
 *
 *  FIXME: why memory can not released?
 */
int test_deque_block(void)
{
    struct BuffItem
    {
        std::vector<uint8_t>    data;
        uint64_t                id;
    };
    typedef SPtr<BuffItem> BufType;

    std::deque<BufType> buf, buf2;

    int nloop = svar.GetInt("nloop", 1000);
    GSLAM::Rate r(30);

    int32_t *pidx;
    int idx=0;

    for(int i=0; i<nloop; i++)
    {
        {
            BufType bi(new BuffItem);
            bi->data.resize(1920*1080*3);
            pidx = (int32_t*) bi->data.data();
            *pidx = idx++;
            buf.push_back(bi);
            buf2.push_back(bi);
        }

        {
            BufType bi(new BuffItem);
            bi->data.resize(1920*1080*3);
            pidx = (int32_t*) bi->data.data();
            *pidx = idx++;
            buf.push_back(bi);
            buf2.push_back(bi);
        }

        printf("buff size = %d\n", buf.size());

        BufType bi3 = buf.front();
        buf.pop_front();
        printf(">>> get item %09d\n", bi3->id);

        buf2.pop_front();
        buf2.pop_front();

        r.sleep();
    }

    while( buf.size() > 0 )
    {
        BufType bi3 = buf.front();
        buf.pop_front();
        printf(">>> get item %09d\n", bi3->id);

        r.sleep();
    }

    while( 1 ) {
        r.sleep();
    }

    return 0;
}

int test_deque_memory(void)
{

    struct BufBlock
    {
        unsigned char   *buf;
        uint64_t        len;
        uint64_t        id;
    };
    typedef SPtr<BufBlock> PBufBlock;

    std::deque<SPtr<BufBlock> > buf, buf2;

    int nloop = svar.GetInt("nloop", 1000);
    GSLAM::Rate r(30);

    int idx=0;

    for(int i=0; i<nloop; i++)
    {
        {
            PBufBlock b1(new BufBlock);

            b1->len = 1920*1080*3;
            b1->buf = (unsigned char*) malloc(b1->len);
            b1->id = idx ++;

            buf.push_back(b1);
            buf2.push_back(b1);
        }

        if( 1 )
        {
            PBufBlock b1(new BufBlock);

            b1->len = 1920*1080*3;
            b1->buf = (unsigned char*) malloc(b1->len);
            b1->id = idx ++;

            buf.push_back(b1);
            buf2.push_back(b1);
        }

        printf("buff size = %d\n", buf.size());

        PBufBlock bi3 = buf.front();
        buf.pop_front();
        printf(">>> get item %09x id: %09d\n", bi3->buf, bi3->id);
        free(bi3->buf);
        bi3->buf = NULL;

        while( buf2.size() > 0 ) {
            buf2.pop_front();
        }

        r.sleep();
    }

    while( buf.size() > 0 )
    {
        PBufBlock bi3 = buf.front();
        buf.pop_front();
        printf(">>> get item %09x id: %09d\n", bi3->buf, bi3->id);

        free(bi3->buf);
        bi3->buf = NULL;

        r.sleep();
    }

    buf.clear();
    buf2.clear();

    while( 1 ) {
        r.sleep();
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    dbg_stacktrace_setup();

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "test_fifo");

    #define DEF_TEST(f) if ( act == #f ) return f();

    DEF_TEST(test_fifo);
    DEF_TEST(test_deque_block);
    DEF_TEST(test_deque_memory);


    return 0;
}
