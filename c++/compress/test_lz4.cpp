#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GSLAM/core/Svar.h>

#include "utils_debug.h"

#include "lz4.h"



int test_lz4(void)
{
    char        *buf_src, *buf_compressed;
    int         len_src, len_compressed, bufsize;

    len_src = 1920*1080*3;
    buf_src = (char*) malloc(len_src);

    len_compressed = len_src;
    buf_compressed = (char*) malloc(len_compressed);

    // compress
    bufsize = LZ4_compress_default(buf_src, buf_compressed, len_src, len_compressed);
    printf("len = %d, compressed size = %d\n", len_src, bufsize);

    // decompress
    char        *buf_decom;
    int         len_decom;

    buf_decom = (char*) malloc(len_src);
    len_decom = LZ4_decompress_safe(buf_compressed, buf_decom, bufsize, len_src);
    printf("compressed len = %d, decompressed size = %d\n", bufsize, len_decom);

    free(buf_src);
    free(buf_compressed);
    free(buf_decom);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    dbg_stacktrace_setup();

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "test_lz4");

    #define DEF_TEST(f) if ( act == #f ) return f();

    DEF_TEST(test_lz4);


    return 0;
}
