#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <vector>
#include <deque>
#include <map>
#include <algorithm>

#include "GSLAM/core/Svar.h"
#include "utils.h"
#include "TinyEXIF.h"

using namespace std;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int test_exif()
{
    TinyEXIF::EXIF exifReader;
    GSLAM::Svar var;
    std::string fname = svar.GetString("fname", "../data/baboon.jpg");

    exifReader.parseFile(fname, var);
    var.dumpAllVars();

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "test_exif");

    DEF_TEST(test_exif);
}

