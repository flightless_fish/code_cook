
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <regex>

#include "GSLAM/core/Svar.h"

#include "pcre/RegularExpression.h"


int test_regexp()
{
    pi::RegularExpression re_mac("([0-9a-fA-F]{2}[:-]){5}[0-9a-fA-F]{2}");
    pi::RegularExpression::Match m;
    
    std::string mac = "c8:5b:76:f4:54:6b";
    std::string mac_fake = "fu-5b:76:f4:54:6b";
    
    int r = re_mac.match(mac, 0, m);
    if( m.offset != std::string::npos && m.length > 0 ) {
        printf("%s is a MAC address\n", mac.c_str());
    } else {
        printf("%s is not a MAC address\n", mac.c_str());
    }
    

    r = re_mac.match(mac_fake, 0, m);
    if( m.offset != std::string::npos && m.length > 0 ) {
        printf("%s is a MAC address\n", mac_fake.c_str());
    } else {
        printf("%s is not a MAC address\n", mac_fake.c_str());
    }
    
    return 0;
}


int test_std_regexp()
{
    if (std::regex_match ("subject", std::regex("(sub)(.*)") ))
        std::cout << "string literal matched\n";

    const char cstr[] = "subject";
    std::string s ("subject");
    std::regex e ("(sub)(.*)");

    if (std::regex_match (s,e))
        std::cout << "string object matched\n";

    if ( std::regex_match ( s.begin(), s.end(), e ) )
        std::cout << "range matched\n";

    std::cmatch cm;    // same as std::match_results<const char*> cm;
    std::regex_match (cstr,cm,e);
    std::cout << "string literal with " << cm.size() << " matches\n";

    std::smatch sm;    // same as std::match_results<string::const_iterator> sm;
    std::regex_match (s,sm,e);
    std::cout << "string object with " << sm.size() << " matches\n";

    std::regex_match ( s.cbegin(), s.cend(), sm, e);
    std::cout << "range with " << sm.size() << " matches\n";

    // using explicit flags:
    std::regex_match ( cstr, cm, e, std::regex_constants::match_default );

    std::cout << "the matches were: ";
    for (unsigned i=0; i<cm.size(); ++i) {
        std::cout << "[" << cm[i] << "] ";
    }

    std::cout << std::endl;

    return 0;
}

int test_std_regex_search()
{
    std::string s ("this subject has a submarine as a subsequence");
    std::smatch m;
    std::regex e ("\\b(sub)([^ ]*)");   // matches words beginning by "sub"

    std::cout << "Target sequence: " << s << std::endl;
    std::cout << "Regular expression: /\\b(sub)([^ ]*)/" << std::endl;
    std::cout << "The following matches and submatches were found:" << std::endl;

    while (std::regex_search (s, m, e)) {
        for (auto x:m) std::cout << x << " ";
        std::cout << std::endl;
        s = m.suffix().str();
    }

    return 0;
}


//
// References:
//  https://en.cppreference.com/w/cpp/regex
//  https://solarianprogrammer.com/2011/10/12/cpp-11-regex-tutorial/
//  https://solarianprogrammer.com/2011/10/20/cpp-11-regex-tutorial-part-2/
//  http://www.cplusplus.com/reference/regex/ECMAScript/
//
int test_regexp_usage()
{
    std::string s = "Some people, when confronted with a problem, think "
                    "\"I know, I'll use regular expressions.\" "
                    "Now they have two problems.";

    std::regex self_regex("REGULAR EXPRESSIONS",
                          std::regex_constants::ECMAScript | std::regex_constants::icase);
    if (std::regex_search(s, self_regex)) {
        std::cout << "Text contains the phrase 'regular expressions'\n";
    }

    std::regex word_regex("(\\S+)");
    auto words_begin =
            std::sregex_iterator(s.begin(), s.end(), word_regex);
    auto words_end = std::sregex_iterator();

    std::cout << "Found "
              << std::distance(words_begin, words_end)
              << " words\n";

    const int N = 6;
    std::cout << "Words longer than " << N << " characters:\n";
    for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
        std::smatch match = *i;
        std::string match_str = match.str();
        if (match_str.size() > N) {
            std::cout << "  " << match_str << '\n';
        }
    }

    std::regex long_word_regex("(\\w{7,})");
    std::string new_s = std::regex_replace(s, long_word_regex, "[$&]");
    std::cout << new_s << '\n';
}


////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "test_regexp");

    DEF_TEST(test_regexp);
    DEF_TEST(test_std_regexp);
    DEF_TEST(test_std_regex_search);
}
