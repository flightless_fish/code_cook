
#include <stdio.h>
#include <stdlib.h>

#include <string>

#include "GSLAM/core/Svar.h"

using namespace std;

string trim(const string &s)
{
    string              delims = " \t\n\r",
                        r;
    string::size_type   i, j;

    i = s.find_first_not_of(delims);
    j = s.find_last_not_of(delims);

    if( i == string::npos ) {
        r = "";
        return r;
    }

    if( j == string::npos ) {
        r = "";
        return r;
    }

    r = s.substr(i, j-i+1);
    return r;
}

string trim2(const std::string& s)
{
    string r = s;

    if (r.empty()) {
        return r;
    }

    std::string::iterator c;

    // Erase whitespace before the string
    for (c = r.begin(); c != r.end() && isspace(*c++););
    r.erase(r.begin(), --c);

    // Erase whitespace after the string
    for (c = r.end(); c != r.begin() && isspace(*--c););
    r.erase(++c, r.end());

    return r;
}

int t_trim()
{
    std::string s1 = "  asdf   dfasdf a sdf  ";

    std::string r1 = trim(s1);
    std::string r2 = trim2(s1);

    printf("input string: %s\n", s1.c_str());
    printf("trim1       : %s\n", r1.c_str());
    printf("trim2       : %s\n", r2.c_str());

    return 0;
}


int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "t_trim");

    DEF_TEST(t_trim);
}

