#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <vector>
#include <deque>
#include <map>
#include <algorithm>

#include "GSLAM/core/Svar.h"
#include "GSLAM/core/SPtr.h"
#include "GSLAM/core/Timer.h"

#include "utils.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int stl_map()
{
    std::map<std::string,int> mymap = {
        { "alpha", 0 },
        { "beta", 0 },
        { "gamma", 0 } };

    mymap.at("alpha") = 10;
    mymap.at("beta") = 20;
    mymap.at("gamma") = 30;

    std::map<std::string,int>::iterator it;
    for (it=mymap.begin(); it!=mymap.end(); it++) {
        std::cout << it->first << ": " << it->second << '\n';
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct TestStruct
{
    int     ix, iy, iz;

    void print(void) {
        printf("ix, iy, iz = %d,%d,%d, ", ix, iy, iz);
    }
};

struct Point3f
{
    float   x, y, z;
};

struct Vec3b
{
    unsigned char val[3];

    Vec3b() {
        val[0] = 0;
        val[1] = 0;
        val[2] = 0;
    }

    Vec3b(unsigned char b1, unsigned char b2, unsigned char b3) {
        val[0] = b1;
        val[1] = b2;
        val[2] = b3;
    }

    unsigned char operator ()(int b) {
        return val[b];
    }
};

struct CloudPoint
{
    Point3f         pt;
    Vec3b           rgb;

    map<int, int>   frame2kp;

    int             *p;

    TestStruct      s;

public:

    CloudPoint() {
        p = new int;
        *p = 0;
    }

    /*
    ~CloudPoint() {
        delete p;
        p = NULL;
    }
    */

    /*
    CloudPoint(const CloudPoint &cp) {
        //printf("CloudPoint(const CloudPoint &cp)\n");

        pt = cp.pt;
        rgb = cp.rgb;
        frame2kp = cp.frame2kp;

        p = new int;
        *p = *(cp.p);
    }

    CloudPoint& operator = (const CloudPoint &cp) {
        //printf("CloudPoint operator = \n");

        pt = cp.pt;
        rgb = cp.rgb;
        frame2kp = cp.frame2kp;

        if( p != NULL ) delete p;
        p = new int;
        *p = *(cp.p);
    }
    */

public:
    void print(void) {
        map<int,int>::iterator it;

        printf("p_add = %llx, s = %llx, pt_add = %llx, rgb_addr = %llx, frame2kp = %llx\n",
               p, &s, &pt, &rgb, &frame2kp);

        printf("    ");
        printf("pt: (%9f,%9f,%9f), rgb: (%3d,%3d,%3d), frame2kp: ",
               pt.x, pt.y, pt.z, rgb(0), rgb(1), rgb(2));
        for(it=frame2kp.begin(); it!=frame2kp.end(); it++) {
            printf("%d->%d ", it->first, it->second);
        }
        s.print();

        printf("\n");
    }
};

int test_stl_vector()
{
    vector<CloudPoint>      arrCP, arrCP2, arrCP3;
    CloudPoint              p;
    CloudPoint              p1, p2, *pp;

    int                     i, j, n;

    // test struct copy
    pp = &p1;
    *(pp->p) = 10;
    pp->pt.x = (rand() % 100)*1.0/100.0;
    pp->pt.y = (rand() % 100)*1.0/100.0;
    pp->pt.z = (rand() % 100)*1.0/100.0;
    pp->rgb = Vec3b(rand()%255, rand()%255, rand()%255);

    pp->frame2kp.clear();
    pp->frame2kp.insert(make_pair(rand()%100, rand()%500));

    pp = &p2;
    *(pp->p) = 20;
    pp->pt.x = (rand() % 100)*1.0/100.0;
    pp->pt.y = (rand() % 100)*1.0/100.0;
    pp->pt.z = (rand() % 100)*1.0/100.0;
    pp->rgb = Vec3b(rand()%255, rand()%255, rand()%255);

    pp->frame2kp.clear();
    pp->frame2kp.insert(make_pair(rand()%100, rand()%500));

    printf("p1: "); p1.print();
    printf("p2: "); p2.print();
    p2 = p1;
    printf("p2: "); p2.print();

    printf("\n");


    // crate vector array1
    n = 10;
    for(i=0; i<n; i++) {
        p.pt.x = (rand() % 100)*1.0/100.0;
        p.pt.y = (rand() % 100)*1.0/100.0;
        p.pt.z = (rand() % 100)*1.0/100.0;
        p.rgb = Vec3b(rand()%255, rand()%255, rand()%255);

        p.frame2kp.clear();
        p.frame2kp.insert(make_pair(rand()%100, rand()%500));

        arrCP.push_back(p);
    }

    arrCP2 = arrCP;

    printf("arrCP1.size = %d, arrCP2.size = %d\n", arrCP.size(), arrCP2.size());

    // delete some items
    vector<int>     arrDel;

    arrDel.resize(n, 1);
    for(i=0; i<2; i++) {
        j = rand()%n;
        arrDel[j] = 0;
    }

    arrCP3.reserve(n);
    for(i=0; i<n; i++) {
        if( arrDel[i] )
            arrCP3.push_back(arrCP[i]);
        else
            arrCP[i].frame2kp.insert(make_pair(rand()%100, rand()%500));
    }

    // print 3 vector array
    printf("arrCP1.size = %d, arrCP2.size = %d, arrCP3.size = %d\n",
           arrCP.size(), arrCP2.size(), arrCP3.size());

    printf("\n");
    for(i=0; i<arrCP.size(); i++) {
        printf("[%4d] ", i);
        arrCP[i].print();
    }

    printf("\n");
    for(i=0; i<arrCP2.size(); i++) {
        printf("[%4d] ", i);
        arrCP2[i].print();
    }

    printf("\n");
    for(i=0; i<arrCP3.size(); i++) {
        printf("[%4d] ", i);
        arrCP3[i].print();
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
 *  STL vector with SPtr will case memory leackage
 *
 *  Ref:
 *      https://stackoverflow.com/questions/24374456/memory-leaks-when-using-shared-pointers-in-a-vector
 *      https://stackoverflow.com/questions/38298008/possible-memory-leaks-with-smart-pointers
 *      https://stackoverflow.com/questions/67174/find-memory-leaks-caused-by-smart-pointers
 *      https://en.cppreference.com/w/cpp/memory
 */

struct Tile
{
public:
    Tile(int x, int y, int z)
    {
        _x = x;
        _y = y;
        _z = z;
    }
    virtual ~Tile()
    {

    }

    int load(void)
    {
        int imgW, imgH;
        imgW = 256;
        imgH = 256;
        _img.resize(imgW*imgH*4);
    }

    int save(void)
    {
        _img.clear();
    }


    int                     _x, _y, _z;
    std::vector<uint8_t>    _img;
};


int test_stl_sptr()
{
    std::deque<SPtr<Tile> > tileDB;

    int nTest = svar.GetInt("n", 1000);
    for(int i=0; i<nTest; i++)
    {
        SPtr<Tile>  t(new Tile(i, i*10, i*13));
        t->load();
        tileDB.push_back(t);

        printf("insert tile: %8d\n", i);
    }

    printf("After insert tiles\n");
    GSLAM::Rate::sleep(20);

    tileDB.clear();

    printf("After tileDB clear\n");
    GSLAM::Rate::sleep(20);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int test_iterator_range(void)
{
    std::vector<int> arr;

    for(int i=0; i<10; i++) arr.push_back(i);

    std::vector<int> arr1(arr.begin()+0, arr.begin()+5);
    std::vector<int> arr2(arr.begin()+5, arr.begin()+10);

    printf("arr1: ");
    for(auto i : arr1) printf("%d ", i);
    printf("\n");

    printf("arr2: ");
    for(auto i : arr2) printf("%d ", i);
    printf("\n");

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "stl_map");

    DEF_TEST(stl_map);
    DEF_TEST(test_stl_vector);
    DEF_TEST(test_stl_sptr);
    DEF_TEST(test_iterator_range);
}

