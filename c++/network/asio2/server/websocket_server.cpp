#include <asio2/http/ws_server.hpp>
#include "ws_server.h"
#include "http_server.h"
#include "EasySocket.hpp"

int main()
{
    std::string host = "0.0.0.0";
    std::string ws_port = "3000";
    std::string http_port = "8000";

    PI::WSServer ws_server(host,ws_port);
    PI::HttpServer http_server(host,http_port);
    ws_server.start();
    http_server.start();

    masesk::EasySocket socket;
    socket.socketListenUDP("pose",1000u,[&ws_server](std::string msg){
        ws_server.async_send(msg);
        std::cout<<msg<<std::endl;
    });
	while (std::getchar() != '\n');

	return 0;
}
