#ifndef __HTTP_SERVER_H__
#define __HTTP_SERVER_H__
#include <asio2/http/http_server.hpp>
#include <iostream>
namespace PI {
    class HttpServer
    {
    public:
        HttpServer(std::string host, std::string port):host(host),port(port) {
            std::filesystem::path root = std::filesystem::current_path();
            server.set_root_directory(std::move(root));
            server.bind_recv([&](http::web_request& req, http::web_response& rep)
            {
                asio2::ignore_unused(req, rep);
                // all http and websocket request will goto here first.
                std::cout << req.path() << req.query() << std::endl;
            }).bind_connect([](auto & session_ptr)
            {
                printf("client enter : %s %u %s %u\n",
                    session_ptr->remote_address().c_str(), session_ptr->remote_port(),
                    session_ptr->local_address().c_str(), session_ptr->local_port());
                //session_ptr->set_response_mode(asio2::response_mode::manual);
            }).bind_disconnect([](auto & session_ptr)
            {
                printf("client leave : %s %u %s\n",
                    session_ptr->remote_address().c_str(), session_ptr->remote_port(),
                    asio2::last_error_msg().c_str());
            }).bind_start([&]()
            {
                printf("start http server : %s %u %d %s\n",
                    server.listen_address().c_str(), server.listen_port(),
                    asio2::last_error_val(), asio2::last_error_msg().c_str());
            }).bind_stop([&]()
            {
                printf("stop http server : %d %s\n",
                    asio2::last_error_val(), asio2::last_error_msg().c_str());
            });

            server.bind<http::verb::get, http::verb::post>("/", [](http::web_request& req, http::web_response& rep)
            {
                asio2::ignore_unused(req, rep);

                rep.fill_file("index.html");
                rep.chunked(true);

            });

            // If no method is specified, GET and POST are both enabled by default.
            server.bind("*", [](http::web_request& req, http::web_response& rep)
            {
                rep.fill_file(req.target());
            });
        }

        bool start(){
            return server.start(host,port);
        }

        std::string host;
        std::string port;
        asio2::http_server server;
    };




} // namespace PI

#endif
