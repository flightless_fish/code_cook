 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <string>
#include <iostream>

#include "Socket++.h"
#include "utils_network.h"


using namespace std;
using namespace pi;



#ifdef _WIN32

#include <windows.h>

inline double tm_getTimeStamp(void)
{
    FILETIME        t;
    uint64_t        t_ret;
    double          ts;

    // get UTC time
    GetSystemTimeAsFileTime(&t);

    t_ret = 0;

    t_ret |= t.dwHighDateTime;
    t_ret <<= 32;
    t_ret |= t.dwLowDateTime;

    // convert 100 ns to second
    ts = 1.0 * t_ret / 1e7;
    return ts;
}

inline void tm_sleep(uint32_t t)
{
    Sleep(t);
}

#else
#include <sys/time.h>
#include <sys/timeb.h>

inline double tm_getTimeStamp(void)
{
    struct timeval  tm_val;
    double          v;
    int             ret;

    ret = gettimeofday(&tm_val, NULL);

    v = tm_val.tv_sec + 1.0*tm_val.tv_usec/1e6;
    return v;
}

inline void tm_sleep(uint32_t t)
{
    struct timespec tp;

    tp.tv_sec  = t / 1000;
    tp.tv_nsec = ( t % 1000 ) * 1000000;

    while( nanosleep(&tp, &tp) );
}

#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


int test_server()
{
    int         imsg=0;
    string      msg;
    RSocket     server;
    int         ret;

    int         port = 30000;


    // start socket server
    std::cout << "running....\n";
    server.startServer(port);

    while(1) {
        RSocket new_socket;

        if( 0 != server.accept(new_socket) ) {
            printf("server.accept failed!");
            continue;
        }

        RSocketAddress ca;
        new_socket.getClientAddress(ca);

        printf("\n");
        printf("accept a new connection! client: %s (%d)\n",
               ca.addr_str, ca.port);

        while(1) {
            ret = new_socket.recv(msg);

            if( ret < 0 ) break;
            else if( ret == 0 ) {
                tm_sleep(10);
                continue;
            }

            new_socket.send(msg);

            printf("[%3d] %s\n", imsg++, msg.c_str());

            if( msg == "quit" ) {
                goto SERVER_QUIT;
            }
        }

        tm_sleep(10);
    }

SERVER_QUIT:
    server.close();

    return 0;
}


int test_client()
{
    char        msg[4096], buff[4096];
    string      msg_s, msg_r;

    RSocket     client;
    int         ret1, ret2;
    string      addr;
    int         port;

    // parse input arguments
    addr = "127.0.0.1";
    port = 30000;

    // generate default message
    sprintf(buff, "Test message! from client");


    // begin socket
    if( 0 != client.startClient(addr, port) ) {
        printf("client.startClient failed!");
        return -1;
    }

    printf("client started!\n");

    for(int i=0; i<1000;i++) {
        sprintf(msg, "%s [%5d]", buff, i);
        msg_s = msg;

        ret1 = client.send(msg_s);
        ret2 = client.recv(msg_r);

        printf("receive message from sever: %s (%d), ret = %d, %d\n",
               msg_r.c_str(), msg_r.size(),
               ret1, ret2);

        tm_sleep(500);
    }

    client.close();

    return 0;
}


int test_hostname(void)
{
    std::string hn;
    int ret = net_gethostname(hn);

    printf("hostname: %s\n", hn.c_str());

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    int act;

    if( argc > 1 ) act = atoi(argv[1]);

    if( act == 0 )          test_server();
    else if( act == 1)      test_client();
    else if( act == 2)      test_hostname();

    return 0;
}

