/******************************************************************************

  Robot Toolkit ++ (RTK++)

  Copyright (c) 2007-2013 Shuhui Bu <bushuhui@nwpu.edu.cn>
  http://www.adv-ci.com

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


#ifdef _WIN32
#include <winsock2.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#endif


#include "Socket++.h"


#define dbg_pe printf


namespace pi {



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class RSocket_PrivateData
{
public:
    RSocket_PrivateData() {
        memset(&m_addr, 0, sizeof(sockaddr_in));
        memset(&m_addrClient, 0, sizeof(sockaddr_in));
    }

    ~RSocket_PrivateData() {
        memset(&m_addr, 0, sizeof(sockaddr_in));
        memset(&m_addrClient, 0, sizeof(sockaddr_in));
    }

public:
    sockaddr_in     m_addr;
    sockaddr_in     m_addrClient;
};



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int RSocket::m_bInitialized = 0;


RSocket::RSocket() : m_sock ( -1 )
{
    if( !m_bInitialized ) {
        #ifdef _WIN32
           WORD versionWanted = MAKEWORD(1, 1);
           WSADATA wsaData;
           WSAStartup(versionWanted, &wsaData);

           m_bInitialized = 1;
        #endif
    }

    RSocket_PrivateData *pd = new RSocket_PrivateData;
    m_priData = pd;

    m_socketType = SOCKET_TCP;
    m_server = 0;

    m_maxConnections = 500;
    m_maxHostname    = 1024;
}

RSocket::~RSocket()
{
    if ( isOpened() )
        ::close(m_sock);

    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;
    delete pd;
    m_priData = NULL;
}

int RSocket::create(void)
{
    // create socket
    if( m_socketType == SOCKET_TCP )
        m_sock = socket(AF_INET, SOCK_STREAM, 0);
    else
        m_sock = socket(AF_INET, SOCK_DGRAM, 0);

    // check socket created or not
    if ( !isOpened() )
        return -1;

    // set SO_REUSEADDR on
    if( m_socketType == SOCKET_TCP ||
        //(m_socketType == SOCKET_UDP_MULTICAST && !m_server) )  {
        m_socketType == SOCKET_UDP_MULTICAST ) {
        setReuseAddr(1);
    }

    return 0;
}

int RSocket::close(void)
{
    if ( isOpened() )
        ::close(m_sock);

    m_sock = -1;
    m_server = 0;

    return 0;
}

int RSocket::bind(int port)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

    if ( !isOpened() ) {
        return -1;
    }

    pd->m_addr.sin_family       = AF_INET;
    pd->m_addr.sin_addr.s_addr  = htonl(INADDR_ANY);
    pd->m_addr.sin_port         = htons(port);

    int ret = ::bind(m_sock, (struct sockaddr*) &(pd->m_addr), sizeof(pd->m_addr));
    if ( ret == -1 ) {
        return -2;
    }

    return 0;
}


int RSocket::listen(void)
{
    if ( !isOpened() ) {
        return -1;
    }

    int listen_return = ::listen(m_sock, m_maxConnections);

    if ( listen_return == -1 ) {
        return -2;
    }

    return 0;
}


int RSocket::accept(RSocket& new_socket)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) new_socket.m_priData;

    int addr_length = sizeof(pd->m_addrClient);
    new_socket.m_sock = ::accept(m_sock,
                                 (sockaddr*) &(pd->m_addrClient),
                                 (socklen_t*) &addr_length);

    if ( new_socket.m_sock <= 0 )
        return -1;
    else
        return 0;
}



int RSocket::send(uint8_t *dat, int len)
{
    if( m_socketType == SOCKET_TCP ) {
        return ::send(m_sock, dat, len, MSG_NOSIGNAL);
    } else if( m_socketType == SOCKET_UDP ) {
        RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

        if( m_server ) {
            return ::sendto(m_sock, dat, len, 0,
                          (struct sockaddr *) &(pd->m_addrClient), sizeof(pd->m_addrClient));
        } else {
            return ::sendto(m_sock, dat, len, 0,
                          (struct sockaddr *) &(pd->m_addr), sizeof(pd->m_addr));
        }
    } else if( m_socketType == SOCKET_UDP_MULTICAST ) {
        RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

        return ::sendto(m_sock, dat, len, 0,
                        (struct sockaddr *) &(pd->m_addr), sizeof(pd->m_addr));
    }
}

int RSocket::send (std::string &s)
{
    return send((uint8_t*)s.c_str(), s.size());
}


int RSocket::recv(uint8_t *dat, int len)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

    if( m_socketType == SOCKET_TCP )
        return ::recv(m_sock, dat, len, 0);
    else if( m_socketType == SOCKET_UDP ) {
        if( m_server ) {
            socklen_t addrLen = sizeof(pd->m_addrClient);
            return ::recvfrom(m_sock, dat, len, 0,
                              (struct sockaddr *) &(pd->m_addrClient), &addrLen);
        } else {
            return ::recvfrom(m_sock, dat, len, 0, NULL, NULL);
        }
    } else if( m_socketType == SOCKET_UDP_MULTICAST ) {
        socklen_t addrLen = sizeof(pd->m_addr);
        return ::recvfrom(m_sock, dat, len, 0,
                          (struct sockaddr *) &(pd->m_addr), &addrLen);
    }
}

int RSocket::recv(std::string& s, int maxLen)
{
    char *buf;
    int status = 0;

    buf = new char[maxLen + 1];
    memset(buf, 0, maxLen + 1);
    s = "";

    status = recv((uint8_t*) buf, maxLen);
    if ( status > 0 ) s = buf;

    delete [] buf;
    
    return status;
}



int RSocket::recv_until(uint8_t *dat, int len)
{
    uint8_t     *p;
    int         ret, read, readed = 0;

    p    = dat;
    read = len;

    while(1) {
        ret = recv(p, read);
        if( ret < 0 ) return ret;

        readed += ret;
        p      += ret;

        if( readed >= len ) return readed;

        read = len - readed;
    }

    return -1;
}


int RSocket::connect(std::string host, int port)
{
    uint32_t addr;

    inet_pton(AF_INET, host.c_str(), &addr);
    if ( errno == EAFNOSUPPORT )
        return -1;

    return connect(addr, port);
}


int RSocket::connect(uint32_t addr, int port)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

    if ( ! isOpened() ) return -1;

    pd->m_addr.sin_family       = AF_INET;
    pd->m_addr.sin_port         = htons(port);
    pd->m_addr.sin_addr.s_addr  = addr;

    if( m_socketType == SOCKET_TCP ) {
        int status = ::connect(m_sock, (sockaddr *) &(pd->m_addr), sizeof(pd->m_addr) );

        if ( status == 0 )
            return 0;
        else
            return -3;
    } else {
        return 0;
    }
}

int RSocket::startServer(int port, RSocketType t)
{
    m_socketType = t;
    m_server = 1;

    if ( 0 != create() ) {
        return -1;
    }

    if ( 0 != bind (port) ) {
        return -2;
    }

    if( m_socketType == SOCKET_TCP ) {
        if ( 0 != listen() ) return -3;
    }

    return 0;
}

int RSocket::startServer(const std::string& addr, int port, RSocketType t)
{
    m_socketType = t;
    m_server = 1;

    if ( 0 != create() ) {
        return -1;
    }

    if( t != SOCKET_UDP_MULTICAST ) {
        if ( 0 != bind (port) ) {
            return -2;
        }
    } else {
        RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

        memset(&(pd->m_addr), 0, sizeof(pd->m_addr));

        pd->m_addr.sin_family       = AF_INET;
        pd->m_addr.sin_addr.s_addr  = inet_addr(addr.c_str());
        pd->m_addr.sin_port         = htons(port);
    }

    if( m_socketType == SOCKET_TCP ) {
        if ( 0 != listen() ) return -3;
    }

    return 0;
}


int RSocket::startClient(const std::string& host, int port, RSocketType t)
{
    uint32_t addr;

    inet_pton(AF_INET, host.c_str(), &addr);
    if ( errno == EAFNOSUPPORT )
        return -1;

    addr = inet_addr(host.c_str());

    return startClient(addr, port, t);
}

int RSocket::startClient(uint32_t addr, int port, RSocketType t)
{
    m_socketType = t;
    m_server = 0;

    if ( 0 != create() ) {
        return -1;
    }

    if( t == SOCKET_UDP_MULTICAST ) {
        RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

        memset(&(pd->m_addr), 0, sizeof(pd->m_addr));

        pd->m_addr.sin_family       = AF_INET;
        pd->m_addr.sin_addr.s_addr  = htonl(INADDR_ANY);
        pd->m_addr.sin_port         = htons(port);

        // bind to receive address
        if ( ::bind(m_sock, (struct sockaddr *) &(pd->m_addr), sizeof(pd->m_addr)) < 0 ) {
            dbg_pe("Failed to bind UDP port [%d]", port);
            return -2;
        }

        // use setsockopt() to request that the kernel join a multicast group
        struct ip_mreq mreq;
        mreq.imr_multiaddr.s_addr = addr;
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);
        if (setsockopt(m_sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                       &mreq, sizeof(mreq)) < 0) {
            dbg_pe("Failed to setsockopt for setting join a multicast group");
            return -3;
        }
    } else {
        if ( 0 != connect(addr, port) ) {
            return -2;
        }
    }

    return 0;
}


int RSocket::getMyAddress(RSocketAddress &a)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

    a.port      = ntohs(pd->m_addr.sin_port);
    a.addr_inet = ntohl(pd->m_addr.sin_addr.s_addr);
    inet_ntop(AF_INET, &(pd->m_addr.sin_addr),
              a.addr_str, INET_ADDRSTRLEN);
    a.type = m_socketType;

    return 0;
}

int RSocket::getClientAddress(RSocketAddress &a)
{
    RSocket_PrivateData *pd = (RSocket_PrivateData*) m_priData;

    a.port      = ntohs(pd->m_addrClient.sin_port);
    a.addr_inet = ntohl(pd->m_addrClient.sin_addr.s_addr);
    inet_ntop(AF_INET, &(pd->m_addrClient.sin_addr),
              a.addr_str, INET_ADDRSTRLEN);
    a.type = m_socketType;

    return 0;
}

int RSocket::setNonBlocking(int nb)
{
    int opts;

    opts = fcntl(m_sock, F_GETFL);

    if ( opts < 0 ) {
        return -1;
    }

    if ( nb )
        opts = opts | O_NONBLOCK;
    else
        opts = opts & ~O_NONBLOCK;

    fcntl(m_sock, F_SETFL, opts);

    return 0;
}

int RSocket::setReuseAddr(uint32_t reuse)
{
    int ret = setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR,
                         (const char*) &reuse, sizeof(reuse));
    if( ret == -1 ) {
        dbg_pe("setsockopt failed");
    }

    return ret;
}


} // end of namespace pi
