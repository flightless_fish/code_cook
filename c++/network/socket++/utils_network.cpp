#include "utils_network.h"

#ifdef _WIN32
    #include <winsock2.h>
#else
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <arpa/inet.h>
    #include <fcntl.h>
#endif


int net_gethostname(std::string& hn)
{
#ifdef _WIN32
    WORD versionWanted = MAKEWORD(1, 1);
    WSADATA wsaData;
    WSAStartup(versionWanted, &wsaData);
#endif

    char hostname[1024];
    int ret = gethostname(hostname, 1024);
    hn = hostname;

    return ret;
}

