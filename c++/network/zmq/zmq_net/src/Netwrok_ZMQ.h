#pragma once

#ifndef __NETWROK_ZMQ_H__
#define __NETWROK_ZMQ_H__

#include <iostream>
#include "zmq.hpp"
#include "message_cereal.hpp"
//#include "Svar.h"


/**
 * @brief The Network_ZMQ class for single socket management
 * @note this objbect should only manage 1 socket and currently only supports pub-sub mode
 * @todo some test is required for stability & correctness of the code
 */
class Network_ZMQ // FIXME: add support to other type of sockets(by inheritance?)
{
public:
    /**
     * @brief default construction: PUB-SUB mode, subscribe to all and do synchronization
     */
    Network_ZMQ(zmq::context_t& context);
    virtual ~Network_ZMQ();

    virtual std::string type()const {return "Network_ZMQ";}

    /**
     * @brief openDevice - set com type
     * @note this class manages many sockets, so this function won't set IP address
     * @param comType - use zqm::socket_type to sepecify the communication type(pub-sub, req-rep,...)
     * @return error code, 0 - good, else - error
     */
    int openDevice(const std::string& addr, int isSync, int comType = ZMQ_PUB, std::string subscribeStr = "");
    //int openDevice(Svar& conf);

    int write(std::string& data);
    int write(std::shared_ptr<Msg_base> msgPack);
    int write(std::vector<uint8_t>& buf);

    int read(std::string& msg);
    int read(std::shared_ptr<Msg_base> msgPack);
    int read(std::vector<uint8_t>& buf);

    int changeAddress(const std::string& addr);
    int changeSubscribeStr(const std::string& subscribeStr);
    /**
     * @brief changeExpectListener
     * @param listenerNumber
     * @note change before openDeivce to take effect
     * @return
     */
    int changeExpectListener(int listenerNumber);

protected:
    static int syncWrite(zmq::context_t& context, const std::string& addr, int expectListener);
    static int syncRead(zmq::context_t& context, const std::string& addr);

private:
    zmq::context_t*     context; /**< the context */
    zmq::socket_t*      socket; /**< the socket */
    int                 comType; /**< use zmq::socket_type to determine*/
    std::string         address; /**< the address receiving from/ sending to*/
    std::string         subscribeStr; /**< subscribe string for ZMQ_SUB socket*/
    int                 expectListener; /**< sync wait connecting endpoint number for ZMQ_PUB socket*/
    int                 isSync; /**< whether do synchronization */
//  void*               data; /**< last pack data */
};




#endif // __NETWROK_ZMQ_H__
