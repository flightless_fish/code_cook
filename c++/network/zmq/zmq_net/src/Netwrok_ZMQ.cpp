#include "zhelpers.hpp"

// cereal includes
#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/types/string.hpp"

#include "utils.h"

#include "Netwrok_ZMQ.h"

#define MAXLEN 10240

Network_ZMQ::Network_ZMQ(zmq::context_t& _context)
{
    context = &_context;
    comType = ZMQ_PUB;
    isSync = 1;
    subscribeStr = "";
    expectListener = 1;
}

Network_ZMQ::~Network_ZMQ()
{
    socket->close();
    // context.close();
}

int Network_ZMQ::openDevice(const std::string& addr, int _isSync, int _comType, std::string _subscribeStr)
{
    int ret = 0;
    address = addr;
    isSync = _isSync;
    comType = _comType;
    subscribeStr = _subscribeStr;

    *socket = zmq::socket_t(*context,comType);
    if( comType == ZMQ_PUB )
    {
        socket->bind(address);
        syncWrite(*context, address, expectListener);
    }
    else
    {
        socket->connect(addr);
        socket->setsockopt(ZMQ_SUBSCRIBE, &subscribeStr, subscribeStr.size());
        syncRead(*context, address);
    }
    return ret;
}

//int ComDealer_zmq::openDevice(Svar& conf)
//{
    //remianed
//    int _comType = conf.GetInt("comType_zmq", 1);
//
//}

int Network_ZMQ::write(std::string& data)
{
    if( s_send(*socket, data) ) return 0;
    else return -1;
}
int Network_ZMQ::write(std::shared_ptr<Msg_base> msgPack)
{
    int ret;

    // generate frame 1
    std::string frame1 = msgPack->genGuideFrame();

    //generate frame 2
    std::stringstream ss;
    {
        cereal::BinaryOutputArchive boar(ss);
        boar(msgPack);
    }
    // access the stream data and its length
    char data[MAXLEN];
    int len;
    ss.read(data, MAXLEN);
    len = ss.gcount();
    zmq::message_t msg(len);
    memcpy (msg.data(), data, len);

    // send frames out
    if( s_sendmore(*socket, "") ) ret = 0;//std::cout << "[Server]: Frame 0 send success" << std::endl;
    if( s_sendmore(*socket, frame1) ) ret = 0;//std::cout << "[Server]: Frame 1 send success" << std::endl;
    if( socket->send(msg) ) return ret;//std::cout << "[Server]: Frame 2 send success" << std::endl;

    return -1;
}
int Network_ZMQ::write(std::vector<uint8_t>& buf)
{
    zmq::message_t msg;
    // FIXME:possible error
    memcpy(msg.data(), &buf[0], buf.size()*sizeof(buf[0]));

    if( socket->send(msg) ) return 0;
    else return -1;
}
int Network_ZMQ::read(std::string& msg)
{
    if( s_recv(*socket, msg) ) return 0;
    else return -1;
}
int Network_ZMQ::read(std::shared_ptr<Msg_base> msgPack)
{
    int ret;

    std::string frame0;
    std::string frame1;
    // recv frame 0
    s_recv(*socket,frame0);
    // recv frame 1
    s_recv(*socket,frame1);
    // recv frame 2
    std::stringstream ss;
    zmq::message_t msg;
    ret = socket->recv(&msg);
    char str[msg.size()];
    memcpy(&str[0], msg.data(), msg.size());
    ss.write(str, msg.size());
    {
        cereal::BinaryInputArchive biar(ss);
        biar(msgPack); // Caution: cereal will create a new object to replace it
    }
    msgPack->parseGuideFrame(frame1);

    if( ret ) return 0;
    else return -1;
}
int Network_ZMQ::read(std::vector<uint8_t>& buf)
{
    int ret;

    zmq::message_t msg;
    ret = socket->recv(&msg);
    // FIXME:possible error
    memcpy(&buf[0], msg.data(), msg.size());
    if( ret ) return 0;
    else return -1;
}

int Network_ZMQ::changeAddress(const std::string& addr)
{
    if( comType == ZMQ_PUB )
    {
        socket->unbind(address);
        socket->bind(addr);
    }
    else
    {
        socket->disconnect(address);
        socket->connect(addr);
    }

    address = addr;

    return 0;
}
int Network_ZMQ::changeSubscribeStr(const std::string& _subscribeStr)
{
    subscribeStr = _subscribeStr;
    if( comType == ZMQ_SUB ) socket->setsockopt(ZMQ_SUBSCRIBE, &subscribeStr, subscribeStr.size());

    return 0;
}

int Network_ZMQ::changeExpectListener(int listenerNumber)
{
    expectListener = listenerNumber;
}

int Network_ZMQ::syncWrite(zmq::context_t& context, const std::string& addr, int expectListener)
{
    int ret = 0;
    StringArray sa = split_text(addr, ":");
    if( sa.size() != 2 ) std::cout << "[Network_ZMQ]: sync error due to addr error" << std::endl;
    std::string Saddr = sa[0] + std::to_string(std::atoi(sa[1].c_str())+1000);

    zmq::socket_t syncRep(context, ZMQ_REP);
    syncRep.bind(Saddr);

    // For multiple listeners
    int subNum = 0;
    while( subNum < expectListener )
    {
        // FIXME: abort mechanism for no-response listener

        // receive sync request from client and reply to it
        s_recv(syncRep);
        s_send(syncRep, "");
        subNum++;
    }

    if( ret ) return 0;
    else return -1;
}
int Network_ZMQ::syncRead(zmq::context_t& context, const std::string& addr)
{
    int ret;
    std::string srep;
    StringArray sa = split_text(addr, ":");
    if( sa.size() != 2 ) std::cout << "[Network_ZMQ]: sync error due to addr error" << std::endl;
    std::string Saddr = sa[0] + std::to_string(std::atoi(sa[1].c_str())+1000);

    zmq::socket_t syncReq(context, ZMQ_REQ);
    syncReq.connect(Saddr);

    // send sync req
    s_send(syncReq, "");

    // wait for sync rep
    ret = s_recv(syncReq, srep);

    if( ret ) return 0;
    else return -1;
}
