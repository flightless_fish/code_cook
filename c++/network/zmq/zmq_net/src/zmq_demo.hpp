#ifndef DEMO_HPP
#define DEMO_HPP

#include <fstream>
#include <sstream>
#include <iostream>

// zmq includes
#include <zmq.hpp>
#include "zhelpers.hpp"

// cereal includes
#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/types/string.hpp"

#include "message_cereal.hpp"

////////////////////////////////////////////////////////////////////////////////
/// in proc demos

#define MAXLOOP 10
#define MAXLEN 10240
#define INIPC_EXPECTSUB 1
void* inprocServer(void* context)
{
    /// communication carrier preparation
    // create context for zmq, this is a thread-safe variant
    zmq::context_t* mcontext = static_cast<zmq::context_t*>(context);
    // create socket from context and assign socket type
    // Note: pair socket requires server(receiver) to be bind before client(sender) to be connected
    // yet it is the fastest for communication
    zmq::socket_t socket(*mcontext, zmq::socket_type::pair);
    std::string addr = "inproc://inproc1";
    socket.bind(addr); // conventionally use bind for server


    /// receive msg and deal with it
    int loopNum = 0;
    std::string arr_str[MAXLOOP];
    int         arr_i[MAXLOOP];

    while( loopNum < MAXLOOP )
    {
        zmq::message_t msg;
        if(  socket.recv(&msg) )
        {
            std::stringstream ss;
            char str[msg.size()];
            std::string cstr;
            int         cint;

            // get msg data
            memcpy(&str[0], msg.data(),msg.size());

            ss.write(str,msg.size());
            // cereal part: read from sstream
            // Note: it is better to add a scope for cereal serialization part to avoid problems
            {
                cereal::BinaryInputArchive biar(ss);
                biar(cstr, cint);
            }

            arr_str[loopNum] = cstr;
            arr_i[loopNum] = cint;
            loopNum++;
        }
    }

    // put off printing to see clearly the msg content
    tm_sleep(1000);
    for( int i = 0; i < MAXLOOP; ++i )
    {
        std::cout << "[Server] msg receieved, content: " << arr_str[i] << ", " << arr_i[i] << ". count: " << i+1 << std::endl;
    }

    return (NULL);
}

void* inprocClient(void* context)
{
    /// communication carrier preparation
    // create context for zmq, this is a thread-safe variant
    zmq::context_t* mcontext = static_cast<zmq::context_t*>(context);
    // create socket from context and assign socket type
    // Note: pair socket requires server(receiver) to be bind before client(sender) to be connected
    zmq::socket_t socket(*mcontext, zmq::socket_type::pair);
    std::string addr = "inproc://inproc1";
    socket.connect(addr); // conventionally use connect for client

    int loopNum = MAXLOOP;
    while(loopNum)
    {
        loopNum--;

        /// message content preparation
        std::stringstream ss;
        std::string cstr    = "TestData";
        int cint            = 1;
        // cereal part
        // Serilize and save data into stringstream
        // Caution: since we use binary stream here, be careful dealing with the data in the stream;
        // functions like strlen() won't work properly due to
        // there are many `\0`(terminating null character) in binary stream.
        // However, using JSON or XML may help since these two are string stream,
        // though these two will decrease process efficiency
        {
            cereal::BinaryOutputArchive boar(ss);
            boar(cstr, cint);
        }

        // access the stream data and its length
        char data[MAXLEN];
        int len;
        ss.read(data, MAXLEN);
        len = ss.gcount();

        /// pack up and set out via zmq socket
        // save data into zmq msg for sending
        zmq::message_t msg(len);
        memcpy (msg.data(), data, len);

        if( socket.send (msg) )
        {
            std::cout << "[Client]msg sent, count left: " << loopNum << std::endl;
        }
    }

    return (NULL);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// in ipc demos

void inIpc(const std::string &role, void* context, const std::string& addr, const std::string& syncAddr)
{
    zmq::context_t* mcontext = static_cast<zmq::context_t*>(context);
    std::shared_ptr<Msg_base> msgPack = std::make_shared<Msg_command>();

    if( role == "client" ) // client part
    {
        zmq::socket_t socket(*mcontext, zmq::socket_type::sub);
        socket.connect(addr);
        socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);

        /// build synchronizing mechanism
        // use req-rep socket pair to generate a sync service for preventing message loss
        // due to connection-establish time
        zmq::socket_t syncReq(*mcontext, zmq::socket_type::req);
        syncReq.connect(syncAddr);

        std::cout << "[Client]: begin receiving msgs" << std::endl;

        // send sync req
        s_send(syncReq, "");

        // wait for sync rep
        s_recv(syncReq);

        /// Main sub-pub task dealing
        int count = 0;

        while( 1 )
        {
            count ++;
            int frameCount = 0;
            std::string frame1;
            while(1)
            {
                int more;
                size_t moresize = sizeof (more);
                zmq::message_t msg;

                socket.recv(&msg);
                std::cout << "[Client]: Frame " << frameCount << " received" << std::endl;
                socket.getsockopt(ZMQ_RCVMORE, &more, &moresize);

                /// deal with received msg
                // since frame 0 is just the guidance, we don't deal with it

                if( frameCount == 1 )
                {
                    frame1 = std::string(static_cast<char*>(msg.data()),msg.size());
                    //printf("[Client]: frame1: %s\n", frame1.c_str());
                }
                else if( frameCount == 2 )
                {
                    std::stringstream ss;
                    char str[msg.size()];
                    memcpy(&str[0], msg.data(), msg.size());
                    ss.write(str, msg.size());

                    {
                        cereal::BinaryInputArchive biar(ss);
                        biar(msgPack); // Caution: cereal will create a new object to replace it
                    }
                }

                frameCount++;

                if( !more )
                {
                    msgPack->parseGuideFrame(frame1); // parse to frame 1 is delayed due to cereal's recreation of object
                    break;
                }
            }
            std::cout << "[Client]: count " << count << std::endl;

            // give an exit
            if( count == 10)
            {
                // give a print to check data
                std::cout << "[Client]: Here is the pack we received at the last time" << std::endl;
                printf("ID:%d, gcsID:%u, msgType:%u, totalFrame:%d, uavType:%u, timeStamp:%lu, crc:%d, "
                       "cmdType:%u, cmdLng:%.5f, cmdLat:%.5f, cmdAlt:%.5f, cmdHeading:%.8f, cmdVa:%.5f\n",\
                       msgPack->ID, msgPack->gcsID, msgPack->msgType, msgPack->totalFrame, msgPack->uavType, msgPack->timeStamp, msgPack->crc,\
                       *(uint8_t*)msgPack->getData("cmdType"), *(double*)msgPack->getData("cmdLng"), *(double*)msgPack->getData("cmdLat"), \
                       *(double*)msgPack->getData("cmdAlt"), *(double*)msgPack->getData("cmdHeading"), *(double*)msgPack->getData("cmdVa"));
                break;
            }
        }

    }
    else // server part
    {
        zmq::socket_t socket(*mcontext, zmq::socket_type::pub);
        socket.bind(addr);
        std::cout << "[Server]: begin sending msgs" << std::endl;

        /// build synchronizing mechanism
        zmq::socket_t syncRep(*mcontext, zmq::socket_type::rep);
        syncRep.bind(syncAddr);

        int subNum = 0;
        while( subNum < INIPC_EXPECTSUB)
        {
            // receive sync request from client and reply to it
            s_recv(syncRep);
            s_send(syncRep, "");
            subNum++;
        }

        int loopNum = MAXLOOP;

        while(loopNum)
        {
            loopNum--;

            // set frame 1
            msgPack->ID = 0;
            msgPack->gcsID = 1;
            msgPack->msgType = MSG_COMMAND;
            msgPack->totalFrame = 1;

            // set frame 2
            msgPack->uavType = 1;
            msgPack->timeStamp = tm_get_ms();

            CommandType cmdType = COMMAND_MANUALPOINT;
            msgPack->setData("cmdType", &cmdType);
            double lng = 109;
            double lat = 35;
            double alt = 1000;
            msgPack->setData("cmdLng", &lng);
            msgPack->setData("cmdLat", &lat);
            msgPack->setData("cmdAlt", &alt);
            double cmdHeading = 90;
            double cmdVa = 50;
            msgPack->setData("cmdHeading", &cmdHeading);
            msgPack->setData("cmdVa", &cmdVa);

            msgPack->crc = 1; // FIXME: can add a method to calculate CRC


            // generate frame 1
            std::string frame1 = msgPack->genGuideFrame();

            //generate frame 2
            std::stringstream ss;
            {
                cereal::BinaryOutputArchive boar(ss);
                boar(msgPack);
            }
            // access the stream data and its length
            char data[MAXLEN];
            int len;
            ss.read(data, MAXLEN);
            len = ss.gcount();
            zmq::message_t msg(len);
            memcpy (msg.data(), data, len);

            // send frames out
            if( s_sendmore(socket, " ") ) std::cout << "[Server]: Frame 0 send success" << std::endl;
            if( s_sendmore(socket, frame1) ) std::cout << "[Server]: Frame 1 send success" << std::endl;
            if( socket.send(msg) ) std::cout << "[Server]: Frame 2 send success, count left: " << loopNum << std::endl;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// remote communication demos

void remote(const std::string& role, void* context, const std::string& addr)
{
    zmq::context_t* mcontext = static_cast<zmq::context_t*>(context);
    std::shared_ptr<Msg_base> msgPack = std::make_shared<Msg_command>();

    if( role == "client") // client part
    {

    }
    else // server part
    {

    }
}
////////////////////////////////////////////////////////////////////////////////
#endif // DEMO_HPP
