#include <thread>

#include "zmq_demo.hpp"


using namespace std;

int inprocCom()
{
    cout << "To be noted: inproc communications can't work on Windows as the guide told" << endl;

    // create context for zmq
    // Here 1 refers to io_thread number, socket number can be assigned to context too
    zmq::context_t context(1);

    // create receiver thread
    thread tRecver(inprocServer, &context);

    tm_sleep(1000); // To ensure receiver has really been created, wait enough long time

    // create sender thread
    thread tSender(inprocClient, &context);

    tSender.join();
    tRecver.join();
    return 0;
}

int inIPCCom(const string& role)
{
    cout << "We also add message struct here, along with the sync service" << endl;
    zmq::context_t context(1);
    std::string addr = "tcp://192.168.1.66:6177";
    std::string syncAddr = "tcp://192.168.1.66:6178";
    inIpc(role, &context, addr, syncAddr);

    return 0;
}

int remoteCom(const string& role)
{
    cout << "remote communication is almost the same as in IPC communication, yet here we may introduce more features of zmq" << endl;
    zmq::context_t context(1);
    std::string addr = "tcp://192.168.1.66:6277";
    remote(role, &context, addr);

    return 0;
}


int main(int argc, char* argv[])
{
    std::string role;

    // do a parse for client, server
    if( argc <= 1 )
    {
        cout << "Please specify role: client, server." << endl;

        return -1;
    }
    else
    {
        char* parser = argv[1];
        if( strcmp(parser, "client") == 0 || strcmp(parser, "server") == 0 ) role = parser;
        else
        {
            cout << "Error, unknown option: " << parser << endl;
            cout << "Options: [server/client] " << endl;

            return -1;
        }
    }

    // Note: for serialization part, refer to cereal demo first
    // inproc communication
    cout << "First we have in process communications." << endl;
    inprocCom();
    cout << "-------------------------------------------" << endl << endl;

    // in ipc communication
    cout << "Then we have in ipc( among processes ) communications" << endl;
    inIPCCom(role);
    cout << "-------------------------------------------" << endl << endl;

    // remote communication
    cout << "Finally we have remote communication( among IPCs ) communications" << endl;
    remoteCom(role);
    cout << "-------------------------------------------" << endl << endl;

    return 0;
}
