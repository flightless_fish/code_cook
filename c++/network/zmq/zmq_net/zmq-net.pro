SOURCES += \
    src/Netwrok_ZMQ.cpp \
    src/zmq_demo.cpp \
    backward/backward.cpp \
    src/utils.cpp

HEADERS += \
    src/Netwrok_ZMQ.h \
    src/message_cereal.hpp \
    src/zmq_demo.hpp \
    src/zhelpers.hpp \
    backward/backward.hpp \
    src/utils.h

DEPENDPATH += \
    /usr/local/lib # zmq installation address

INCLUDEPATH += \
    src \
    backward \
    cereal

LIBS += \
    -g -rdynamic -ldl -lunwind -lbfd -lzmq -lzmqpp -lpthread


