# ZeroMQ Demo

> If possible, [Official manual](https://zeromq.org/) is recommended

## Installation

On Ubuntu 18.04, please install following packages:

```
sudo apt install libzmq3-dev libzmqpp-dev
```

## Compile Simple Demos

And check installation by compiling easiest [client](simple_demo/zmq-helloWorld-client.cpp) and [server](simple_demo/zmq-helloWorld-server.cpp).

```sh
cd simple_demo
g++ zmq-helloWorld-server.cpp  -lzmq -lzmqpp
g++ zmq-helloWorld-server.cpp -lzmq -lzmqpp
```

## Compile ZMQ_Net Demo

Open project file at `zmq_net/zmq-net.pro`, and then compile it in QtCreator. Or make the program by following commands:

```
cd zmq_net
mkdir build
cd build
qmake ../zmq-net.pro
make
```
