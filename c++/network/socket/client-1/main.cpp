#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int sockcl;
    sockcl=socket(AF_INET,SOCK_DGRAM,0);
    if(sockcl==-1)
    {
        cout<<"SOCKET created is false"<<endl;
        return -1;
    }

    struct sockaddr_in client1_addr;
    client1_addr.sin_family=AF_INET;
    client1_addr.sin_port=htons(5001);
    client1_addr.sin_addr.s_addr=inet_addr("192.168.1.63");
    if(bind(sockcl,(const struct sockaddr *)(&client1_addr),sizeof(client1_addr))==-1)
    {
        cout<<"binded is false"<<endl;
        return -2;
    }
    char buf[128];
    struct sockaddr_in server_addr;
    server_addr.sin_family=AF_INET;
    server_addr.sin_port=htons(5000);
    server_addr.sin_addr.s_addr=inet_addr("192.168.1.63");
    socklen_t fromlen=sizeof(server_addr);
    while(1)
    {
        recvfrom(sockcl,buf,128,0,(struct sockaddr *)(&server_addr),&fromlen);
        cout<<"the message is from server"<<endl;
        cout<<"the message received is "<<buf<<endl;
        buf[1]=buf[0];
        sendto(sockcl,buf,128,0,(struct sockaddr *)(&server_addr),128);
        cout<<"send the message "<<buf<<" to server"<<endl;
        sleep(1);

    }
    close(sockcl);
    return 0;
}
