
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#include <iostream>

#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;
using namespace Eigen;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct SA {
    int a;
    uint8_t b;

};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


int test_matrix(Matrix4f &a)
{
    a(0, 0) = 1;
    a(0, 1) = 2;

    return 0;
}

int eigen3_matrix4f()
{
    Matrix4f a;

    test_matrix(a);
    cout << a;

    return 0;
}

int matrix_directAccess()
{
    Matrix3d x;

    x <<    1, 2, 3,
            4, 5, 6,
            7, 8, 9;

    std::cout << "x = \n" << x << "\n\n";

    double *p = x.data();
    printf("x = \n");
    for(int j=0; j<3; j++) {
        for(int i=0; i<3; i++) {
            printf("%6f ", p[j*3+i]);
        }
        printf("\n");
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int MatrixXd_setValue()
{
    MatrixXd m(2,2);
    m(0,0) = 3;
    m(1,0) = 2.5;
    m(0,1) = -1;
    m(1,1) = m(1,0) + m(0,1);
    std::cout << "m=\n" << m << std::endl;
}

int MatrixXd_setValue_fromArray()
{
    float   _R[9];

    _R[0] = 1; _R[1] = 2; _R[2] = 3;
    _R[3] = 4; _R[4] = 5; _R[5] = 6;
    _R[6] = 7; _R[7] = 8; _R[8] = 9;

    Eigen::Matrix3f R(_R);

    cout << "R = \n" << R;

    return 0;
}

int MatrixXd_random()
{
    MatrixXd m = MatrixXd::Random(3,3);
    m = (m + MatrixXd::Constant(3,3,1.2)) * 50;
    cout << "m =" << endl << m << endl;
    VectorXd v(3);

    v << 1, 2, 3;
    cout << "m * v =" << endl << m * v << endl;

    cout << "v.rows() = " << v.rows() << endl;
    cout << "v.cols() = " << v.cols() << endl;
}

int Matrix_constant()
{
    Matrix3d m = Matrix3d::Random();
    m = (m + Matrix3d::Constant(1.2)) * 50;
    cout << "m =\n" << endl << m << endl;
    Vector3d v(1,2,3);
    cout << "m * v =\n" << endl << m * v << endl;
}

int Matrix_Array()
{
    VectorXf a(50), b(50), c(50), d(50);
    a = 3*b + 4*c + 5*d;


    MatrixXd m = MatrixXd::Random(10,10);
    cout << "m = \n" << m << endl;

    m = ((m.array()+1.0)/2.0).matrix();

    cout << "m = \n" << m << "\n\n";
}

int Matrix_streamInit()
{
    Matrix3f m;
    m << 1, 2, 3,
         4, 5, 6,
         7, 8, 9;
    std::cout << m;
}

int Matrix_resize()
{
    MatrixXd m(2,5);

    m = MatrixXd::Random(2, 5);
    cout << "m (before resizing) = \n" << m << endl;
    m.resize(4,3);
    cout << "m (after resizing) = \n" << m << "\n\n";

    std::cout << "The matrix m is of size "
              << m.rows() << "x" << m.cols() << "\n";
    std::cout << "It has " << m.size() << " coefficients" << "\n\n";

    m.conservativeResize(4, 4);
    cout << "\nm (after conservativeResize) = \n" << m << "\n\n";


    VectorXd v(2);

    v = VectorXd::Random(2);
    cout << "v (before resizing) = \n" << v << endl;
    v.resize(5);
    cout << "v (after resizing) = \n" << v << "\n\n";
    std::cout << "The vector v is of size " << v.size() << std::endl;
    std::cout << "As a matrix, v is of size "
              << v.rows() << "x" << v.cols() << std::endl;

    v.conservativeResize(10);
    std::cout << "v (after conservativeResize) = \n" << v << "\n\n";
}

int Matrix_assignment_resize()
{
    MatrixXf a(2,2);
    std::cout << "a is of size " << a.rows() << "x" << a.cols() << std::endl;
    MatrixXf b(3,3);
    a = b;
    std::cout << "a is now of size " << a.rows() << "x" << a.cols() << std::endl;
}

int vector_operators()
{
    Vector3d    v1, v2, v3, v4;

    v1 << 1, 2, 3;
    v2 << 2, 3, 4;
    v3 << 4, 5, 6;
    v4 = (v1+v2+v3)/3.0;
    v4.normalized();

    cout << "v1 = \n" << v1 << "\n";
    cout << "v2 = \n" << v2 << "\n";
    cout << "v3 = \n" << v3 << "\n";
    cout << "v4 = \n" << v4 << "\n";

    v4 = v4 * 2;
    cout << "v4 = \n" << v4 << "\n";


    Matrix3d cov;

    cov = v4 * v4.transpose();
    cout << "cov = \n" << cov << "\n";

    cov += v1 * v1.transpose();
    cout << "cov = \n" << cov << "\n";

    return 0;
}

int Matrix_inv_det()
{
    MatrixXd        m(3, 3);

    m << 10, 0, 0,
         0, 20, 0,
         0,  0, 5;

    cout << "m.inverse() = \n" << m.inverse() << endl;
    cout << "m.determinant() = \n" << m.determinant() << endl;

    return 0;
}

int Matrix_block()
{
    MatrixXf    a(3, 3), b;

    a.setZero(4, 4);
    a << 1, 2, 3, 4,
         5, 6, 7, 8,
         9, 10, 11, 12,
         13, 14, 15, 16;
    cout << "a = \n" << a << "\n\n";

    b = a;

    a.setZero(6, 6);
    a.block(0,0,4,4) = b;
    cout << "a = \n" << a << "\n\n";

    a.conservativeResize(8, 8);
    a.block(6, 6, 2, 2) << 9, 8, 7, 6;
    a.block(6, 0, 2, 6) << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12;
    a.block(0, 6, 6, 2) = a.block(6, 0, 2, 6).transpose();
    cout << "a = \n" << a << "\n\n";


    VectorXf    x(4), x_;
    x << 1, 2, 3, 4;

    x_ = x.block(0,0,2,1);
    cout << "x  = \n" << x << "\n\n";
    cout << "x_ = \n" << x_ << "\n\n";
}

int Matrix_block2()
{
    MatrixXf    a(3, 3), b;

    a.setZero(4, 4);
    a << 1, 2, 3, 4,
         5, 6, 7, 8,
         9, 10, 11, 12,
         13, 14, 15, 16;
    cout << "a = \n" << a << "\n\n";

    b = a.block(0, 0, 1, 4);
    cout << "b = \n" << b << "\n\n";

    VectorXf    v1, v2;

    v1 = a.block(0, 0, 4, 1);
    v2 = a.block(0, 0, 1, 4).transpose();
    cout << "v1 = \n" << v1 << "\n\n";
    cout << "v2 = \n" << v2 << "\n\n";

    return 0;
}

int add_sub()
{
    Matrix2d a;
    a << 1, 2,
         3, 4;
    MatrixXd b(2,2);
    b << 2, 3,
         1, 4;
    std::cout << "a + b =\n" << a + b << std::endl;
    std::cout << "a - b =\n" << a - b << std::endl;
    std::cout << "Doing a += b;" << std::endl;
    a += b;
    std::cout << "Now a =\n" << a << std::endl;
    Vector3d v(1,2,3);
    Vector3d w(1,0,0);
    std::cout << "-v + w - v =\n" << -v + w - v << std::endl;
}


int Vector_dot_cross_product()
{
    Vector3d    v1, v2, v3;
    double      dp;

    v1 << 0, 0, 1;
    v2 << 1, 0, 0;

    dp = v1.dot(v2);
    v3 = v1.cross(v2);

    cout << "v1 = \n" << v1 << "\n";
    cout << "v2 = \n" << v2 << "\n";
    cout << "v1 . v2 = " << dp << "\n";
    cout << "v1 x v2 = " << v3  << "\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int trans_conj()
{
    MatrixXcf a = MatrixXcf::Random(2,2);
    cout << "Here is the matrix a\n" << a << endl;
    cout << "Here is the matrix a^T\n" << a.transpose() << endl;
    cout << "Here is the conjugate of a\n" << a.conjugate() << endl;
    cout << "Here is the matrix a^*\n" << a.adjoint() << endl;
}

int test_chol()
{
    MatrixXd A(3,3);
    A << 4,-1,2, -1,6,0, 2,0,5;
    cout << "The matrix A is" << endl << A << endl;
    LLT<MatrixXd> lltOfA(A); // compute the Cholesky decomposition of A
    MatrixXd L = lltOfA.matrixL(); // retrieve factor L in the decomposition
    // The previous two lines can also be written as "L = A.llt().matrixL()"
    cout << "The Cholesky factor L is" << endl << L << endl;
    cout << "To check this, let us compute L * L.transpose()" << endl;
    cout << L * L.transpose() << endl;
    cout << "This should equal the matrix A" << endl;

    cout << "\n\n";

    MatrixXd B=A.llt().matrixU();
    cout << "chol(B) = \n" << B << "\n\n";
}


int test_sqrtm()
{
    const double pi = std::acos(-1.0);
    MatrixXf A(2,2);
    A << cos(pi/3), -sin(pi/3),
            sin(pi/3), cos(pi/3);
    std::cout << "The matrix A is:\n" << A << "\n\n";
    std::cout << "The matrix square root of A is:\n" << A.sqrt() << "\n\n";
    std::cout << "The square of the last matrix is:\n" << A.sqrt() * A.sqrt() << "\n";
}


int test_expm()
{
    const double pi = std::acos(-1.0);
    MatrixXd A(2,2);
    A << cos(pi/3), -sin(pi/3),
            sin(pi/3), cos(pi/3);
    std::cout << "The matrix A is:\n" << A << "\n\n";
    std::cout << "The matrix exp of A is:\n" << A.exp() << "\n\n";
}

int test_svd()
{
    MatrixXf m = MatrixXf::Random(3,2);
    cout << "Here is the matrix m:" << endl << m << endl;
    JacobiSVD<MatrixXf> svd(m, ComputeThinU | ComputeThinV);
    cout << "Its singular values are:" << endl << svd.singularValues() << endl;
    cout << "Its left singular vectors are the columns of the thin U matrix:" << endl << svd.matrixU() << endl;
    cout << "Its right singular vectors are the columns of the thin V matrix:" << endl << svd.matrixV() << endl;

    MatrixXf    u, v;

    u = svd.matrixU();
    v = svd.matrixV();


    Vector3f rhs(1, 0, 0);
    cout << "Now consider this rhs vector:" << endl << rhs << endl;
    cout << "A least-squares solution of m*x = rhs is:" << endl << svd.solve(rhs) << endl;

}

int test_svd2()
{
    MatrixXf m(3, 3);

    m <<      8.1047,    0.2812,   -8.3859,
              0.2812,    8.3794,   -8.6606,
             -8.3859,   -8.6606,   17.0465;

    cout << "Here is the matrix m:" << endl << m << endl;
    JacobiSVD<MatrixXf> svd(m, ComputeThinU | ComputeThinV);
    cout << "Its singular values are:" << endl << svd.singularValues() << endl;

    cout << "Its left singular vectors are the columns of the thin U matrix:" << endl << svd.matrixU() << endl;
    cout << "Its right singular vectors are the columns of the thin V matrix:" << endl << svd.matrixV() << endl;

    MatrixXf    u, v;

    u = svd.matrixU();
    v = svd.matrixV();

    Vector3f    a1, a2, a3;

    a1 = u.col(0).normalized();
    a2 = u.col(1).normalized();
    a3 = u.col(2).normalized();

    cout << "\n";
    cout << "a1 = \n" << a1 << "\n";
    cout << "a2 = \n" << a2 << "\n";
    cout << "a3 = \n" << a3 << "\n\n";


    Vector3f rhs(1, 0, 0);
    cout << "Now consider this rhs vector:" << endl << rhs << endl;
    cout << "A least-squares solution of m*x = rhs is:" << endl << svd.solve(rhs) << endl;

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

VectorXf multivariate_gauss(VectorXf &x, MatrixXf &P, int n);

namespace nRandMat{
    MatrixXf randn(int m, int n);
    MatrixXf rand(int m, int n);
}

VectorXf multivariate_gauss(VectorXf &x, MatrixXf &P, int n)
{
    int len = x.size();

    //choleksy decomposition
    MatrixXf S = P.llt().matrixL();
    MatrixXf X = nRandMat::randn(len,n);

    return S*X + x;
}

//http://moby.ihme.washington.edu/bradbell/mat2cpp/randn.cpp.xml
MatrixXf nRandMat::randn(int m, int n)
{
    // use formula 30.3 of Statistical Distributions (3rd ed)
    // Merran Evans, Nicholas Hastings, and Brian Peacock
    int urows = m * n + 1;
    VectorXf u(urows);

    //u is a random matrix
#if 1
    for (int r=0; r<urows; r++) {
        // FIXME: better way?
        u(r) = std::rand() * 1.0/RAND_MAX;
    }
#else
    u = ( (VectorXf::Random(urows).array() + 1.0)/2.0 ).matrix();
#endif


    MatrixXf x(m,n);

    int     i, j, k;
    float   square, amp, angle;

    k = 0;
    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++) {
            if( k % 2 == 0 ) {
                square = - 2. * std::log( u(k) );
                if( square < 0. )
                    square = 0.;
                amp = sqrt(square);
                angle = 2. * M_PI * u(k+1);
                x(i, j) = amp * std::sin( angle );
            }
            else
                x(i, j) = amp * std::cos( angle );

            k++;
        }
    }

    return x;
}

MatrixXf nRandMat::rand(int m, int n)
{
    MatrixXf x(m,n);
    int i, j;
    float rand_max = float(RAND_MAX);

    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++)
            x(i, j) = float(std::rand()) / rand_max;
    }
    return x;
}

int test_randn()
{
    MatrixXf    r;
    int         n;

    n = 10;
    r.resize(n, n);
    r = nRandMat::randn(n, n);
    cout << "r = \n" << r << endl;
}


int test_multivariate_gauss()
{
    VectorXf x(2);
    MatrixXf P(2, 2);
    int n;

    VectorXf    res;

    x(0) = 3.000000000000000;
    x(1) = -0.008726646259972;

    P(0, 0) = 0.090000000000000; P(0, 1) = 0.0;
    P(1, 0) = 0.0;               P(1, 1) = 0.002741556778080;

    n = 1;


    int len = x.size();
    //choleksy decomposition
    MatrixXf S = P.llt().matrixL();
    MatrixXf X;

    X = nRandMat::randn(len,n);

    cout << "S = \n" << S << "\n\n";
    cout << "X = \n" << X << "\n\n";

    //VectorXf ones = //VectorXf::Ones(n).transpose();
    MatrixXf ones = MatrixXf::Ones(1,n);

    res = S*X + x;

    cout << "res = \n" << res << "\n\n";

    VectorXf res2;

    res2 = multivariate_gauss(x, P, n);
    cout << "res2 = \n" << res2 << "\n\n";
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "t_trim");

    DEF_TEST(eigen3_matrix4f);
    DEF_TEST(MatrixXd_setValue);
    DEF_TEST(MatrixXd_setValue_fromArray);

    DEF_TEST(matrix_directAccess);

    DEF_TEST(MatrixXd_random);
    DEF_TEST(Matrix_constant);
    DEF_TEST(Matrix_Array);
    DEF_TEST(Matrix_streamInit);
    DEF_TEST(Matrix_resize);
    DEF_TEST(Matrix_assignment_resize);

    DEF_TEST(vector_operators);
    DEF_TEST(Matrix_inv_det);

    DEF_TEST(Matrix_block);
    DEF_TEST(Matrix_block2);

    DEF_TEST(add_sub);
    DEF_TEST(Vector_dot_cross_product);
    DEF_TEST(trans_conj);

    DEF_TEST(test_chol);
    DEF_TEST(test_sqrtm);
    DEF_TEST(test_expm);

    DEF_TEST(test_svd);
    DEF_TEST(test_svd2);

    DEF_TEST(test_randn);
    DEF_TEST(test_multivariate_gauss);

}
