/**
  install boost under Ubuntu
    sudo apt-get install libboost-all-dev
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/thread.hpp>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct Vertex
{
    float x, y, z;
};

bool operator==(const Vertex& lhs, const Vertex& rhs)
{
    return lhs.x==rhs.x && lhs.y==rhs.y && lhs.z==rhs.z;
}

namespace boost { namespace serialization {
    template<class Archive>
    void serialize(Archive & ar, Vertex& v, const unsigned int version)
    {
        ar & v.x; ar & v.y; ar & v.z;
    }
} }

typedef vector<Vertex> VertexList;

// http://www.boost.org/doc/libs/1_54_0/libs/serialization/doc/tutorial.html#stl
int boost_serialization()
{
    // Create a list for testing
    const Vertex v[] = {
        {1.0f, 2.0f,   3.0f},
        {2.0f, 100.0f, 3.0f},
        {3.0f, 200.0f, 3.0f},
        {4.0f, 300.0f, 3.0f}
    };
    VertexList list(v, v + (sizeof(v) / sizeof(v[0])));

    // Write out a list to a disk file
    {
        ofstream os("boost_data.dat", ios::binary);
        boost::archive::binary_oarchive oar(os);
        oar << list;
    }

    // Read it back in
    VertexList list2;

    {
        ifstream is("boost_data.dat", ios::binary);
        boost::archive::binary_iarchive iar(is);
        iar >> list2;
    }

    // Check if vertex lists are equal
    assert(list == list2);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class bus_stop
{
public:
    bus_stop() {}
    virtual ~bus_stop() {}

    void print(void) {
        printf("lat, lng = %f %f\n", lat, lng);
    }

    double  lat, lng;

private:
    friend class boost::serialization::access;
    template<class Archive> void serialize(Archive &ar, const unsigned int version) {
        ar & lat;
        ar & lng;
    };

};

int boost_serialization_2()
{
    bus_stop        bs, bs2;

    bs.lat = 10;
    bs.lng = 20;

    bs2.lat = 0;
    bs2.lng = 0;

    {
        ofstream os("boost_data2.dat", ios::binary);
        boost::archive::binary_oarchive oar(os);

        oar << bs;
    }

    {
        ifstream is("boost_data2.dat", ios::binary);
        boost::archive::binary_iarchive iar(is);

        iar >> bs2;
    }

    bs.print();
    bs2.print();

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int boost_serialization_3()
{
    vector<int>     list, list2;
    int             i;

    list.push_back(0);
    list.push_back(10);
    list.push_back(3);

    {
        ofstream os("boost_data3.dat", ios::binary);
        boost::archive::binary_oarchive oar(os);

        oar << list;
    }

    {
        ifstream is("boost_data3.dat", ios::binary);
        boost::archive::binary_iarchive iar(is);

        iar >> list2;
    }

    for(i=0; i<list.size(); i++) {
        printf("[%d] %d\n", i, list[i]);
    }

    for(i=0; i<list2.size(); i++) {
        printf("[%d] %d\n", i, list2[i]);
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void boost_thread_func(void *dat)
{
    int     *pi = (int*) dat;
    int     i;

    printf("Thread begin: %d\n", *pi);

    i = 0;
    while(1) {
        printf("[%5d]\n", i);

        i++;

        tm_sleep(100);
    }
}

int boost_thread()
{
    boost::thread   *t = NULL;
    int             a, i;

    a = 10;
    t = new boost::thread(boost_thread_func, &a);
    if( t == NULL ) {
        fprintf(stderr, "ERR: Can not create boost thread!\n");
        return -1;
    }

    i = 0;
    while(1) {
        printf("<%05d>\n", i++);

        tm_sleep(50);
    }

    t->join();

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// main function
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    #define DEF_TEST(f) if( act == #f ) return f()

    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "boost_serialization");

    DEF_TEST(boost_serialization);
    DEF_TEST(boost_serialization_2);
    DEF_TEST(boost_serialization_3);

    DEF_TEST(boost_thread);
}
