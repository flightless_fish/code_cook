# Linux程序打包

**AppImage** 是一种把应用打包成单一文件的格式，允许在各种不同的目标系统（基础系统(Debian、RHEL等)，发行版(Ubuntu、Deepin等)）上运行，无需进一步修改。

## References

* [关于创建AppImage应用方面的介绍](https://doc.appimage.cn/docs/creating-appimages/)

