
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <errno.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/poll.h>

static inline int __get_chars(int *key_buf)
{
    int key;
    int last_key=0;

    for (;;) {
        key = getchar();

        if( key == -1 ) {
            if( last_key > 0 ) {
                key_buf[last_key] = 0;
                return last_key;
            }

            usleep(20000);
            continue;
        } else {
            key_buf[last_key++] = key;
        }
    }

    return 0;
}


int demo_detect_keys(void)
{
	int				fd;
    struct termios 	origTermios, newTermios;

    int             key_len;
    int             key_buf[100];

    int             key_code;

	/* get current termios settings */
	fd = fileno(stdin);
    if (tcgetattr(fd, &origTermios) < 0) {
        printf("_osa_ev_receiver: failed at tcgetattr!\n");
		return -1;
	}

    /* set termios flags */
    memcpy(&newTermios, &origTermios, sizeof(struct termios));
    newTermios.c_lflag &= ~(ECHO|ICANON);
    newTermios.c_cc[VTIME] = 0;
    newTermios.c_cc[VMIN] = 0;

	if (tcsetattr(fd, TCSANOW, &newTermios) < 0) {
        printf("_osa_ev_receiver: Failed at tcsetattr!\n");
		return -1;
	}
	
	
    /* loop for ever */
	while(1) {
        // get key sequence
        key_len = __get_chars(key_buf);

        if( key_len < 1 ) continue;

        printf("key: (%2d)  ", key_len);
        for(int i=0; i<key_len; i++)
            printf("%3d(0x%02x) ", key_buf[i], key_buf[i]);
        printf("\n");

        // convert key
        if( key_len == 1 ) {
            key_code = key_buf[0];
        } else if ( key_len == 3 ) {
            key_code = key_buf[2];
        } else
            continue;
	}

	return 0;
}


int main(int argc, char *argv[])
{
    demo_detect_keys();

    return 0;
}
