#include <iostream>

#include "GSLAM/core/Svar.h"
#include "utils.h"

using namespace std;


////////////////////////////////////////////////////////////////////////////////
/// time utils
////////////////////////////////////////////////////////////////////////////////

int test_tm_getTimeStamp(void)
{
    for(int i=0; i<10; i++) {
        double ts = tm_getTimeStamp();

        printf("[%4d] %f\n", i, ts);

        tm_sleep(500);
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// path utils
////////////////////////////////////////////////////////////////////////////////

int test_path_exist(void)
{
    std::string path = svar.GetString("path", "../test/dir_a/dir_1");

    bool r = path_exist(path);
    cout << "path: " << path << " : r = " << r << "\n";

    return 0;
}

int test_path_mkdir(void)
{
    std::string path = svar.GetString("path", "../test/dir_b/dir_2");

    int r = path_mkdir(path);
    cout << "path: " << path << " : r = " << r << "\n";

    return 0;
}

int test_path_rmdir(void)
{
    std::string path = svar.GetString("path", "../test/dir_b");

    int r = path_rmdir(path);
    cout << "path: " << path << " : r = " << r << "\n";

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "test_path_exist");

    #define DEF_TEST(f) if ( act == #f ) return f();

    DEF_TEST(test_tm_getTimeStamp);

    DEF_TEST(test_path_exist);
    DEF_TEST(test_path_mkdir);
    DEF_TEST(test_path_rmdir);

    return 0;
}
