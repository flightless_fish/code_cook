cmake_minimum_required(VERSION 2.8)


pi_add_target(c++_basic_types BIN basic_types.cpp REQUIRED Utils System)
pi_add_target(test_utils BIN test_utils.cpp REQUIRED Utils System)
