#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "GSLAM/core/Svar.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int basic_types(void)
{
    printf("sizeof(int)            = %d\n", sizeof(int));
    printf("sizeof(short)          = %d\n", sizeof(short));
    printf("sizeof(long)           = %d\n", sizeof(long));
    printf("sizeof(unsigned int)   = %d\n", sizeof(unsigned int));
    printf("sizeof(unsigned short) = %d\n", sizeof(unsigned short));
    printf("sizeof(unsigned long)  = %d\n", sizeof(unsigned long));

    printf("\n==================================================\n\n");

    printf("sizeof(uint8_t)        = %d\n", sizeof(uint8_t));
    printf("sizeof(uint16_t)       = %d\n", sizeof(uint16_t));
    printf("sizeof(uint32_t)       = %d\n", sizeof(uint32_t));
    printf("sizeof(uint64_t)       = %d\n", sizeof(uint64_t));

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    svar.ParseMain(argc, argv);
    std::string act = svar.GetString("Act", "basic_types");

    #define DEF_TEST(f) if ( act == #f ) return f();

    DEF_TEST(basic_types);

    return 0;
}
