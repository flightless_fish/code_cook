# 示例项目

在代码片段的基础上，通过参考、学习多个小型的项目，能够更进一步提高软件设计、架构设计、编码、调试、集成等多方面的能力。下面列了一些可以参考的项目，按照类别划分，大家可以根据自己的需要找到最相近的项目作为参考。在阅读、理解的基础进行修改，并最终将项目代码变成自己的。


## 1. C++编程
C++在计算机视觉、机器学习领域有非常多的应用，如果有比较好的C++编程基础，则能够显著提高研究过程试错的速度。

* [Svar](https://gitee.com/pi-lab/Svar) : 一个模块化的C++11库，提供了参数读取、调用、JSON、Python绑定、插件架构
* [简单的路径规划模拟系统](https://gitee.com/pi-lab/DynamicPathPlanning) ： 提供了一个简单的C++模拟系统，支持地图编辑、路径规划、可视化等功能
* [C++的2D SLAM演示系统](https://gitee.com/pi-lab/fastslam) : 一个简单的2D EKF/Fast SLAM演示系统，通过这个演示系统，能够学会如何将算法、Qt GUI结合起来
* [GSLAM](https://gitee.com/pi-lab/GSLAM) : 提供了一套SLAM的插件化设计接口、库函数



## 2. Qt编程
Qt是一个非常便捷的跨平台GUI、可视化编程框架，通过可视化能够提高自己研究成果的显示度、并提高Debug的效率等等。

* [C++的2D SLAM演示系统](https://gitee.com/pi-lab/fastslam) : 一个简单的2D EKF/Fast SLAM演示系统，通过这个演示系统，能够学会如何将算法、Qt GUI结合起来
* [SimpGCS](https://gitee.com/pi-lab/SimpGCS) : 一个简单的地面站程序，通过解析MAVLINK协议，将无人机的位置等信息可视化在Qt的地图控件上
* [opmapcontrol_ex](https://gitee.com/pi-lab/opmapcontrol_ex) : 一个卫星地图控件，了解如何编写复杂的Qt GUI程序，并了解如何将复杂的功能进行包装
* [简单的路径规划模拟系统](https://gitee.com/pi-lab/DynamicPathPlanning) ： 提供了一个简单的C++模拟系统，支持地图编辑、路径规划、可视化等功能
* [FastGCS无人机地面站](https://gitee.com/pi-lab/FastGCS)：能够支持航线飞行、定点飞行的无人机地面站，整合了无人机实时地图、无人机飞行仿真



## 3. 仿真
* [C++的2D SLAM演示系统](https://gitee.com/pi-lab/fastslam) : 一个简单的2D EKF/Fast SLAM演示系统，通过这个演示系统，能够学会如何将算法、Qt GUI结合起来
* [简单的路径规划模拟系统](https://gitee.com/pi-lab/DynamicPathPlanning) ： 提供了一个简单的C++模拟系统，支持地图编辑、路径规划、可视化等功能
* [基于视觉Marker的无人机自动着陆系统](https://gitee.com/pi-lab/uav-autolanding) ： 提供了一套无人机视觉引导降落的系统



## 4. 计算机视觉
* [GSLAM](https://gitee.com/pi-lab/GSLAM) : 提供了一套SLAM的插件化设计接口、库函数
* [PI-SLAM](https://gitee.com/pi-lab/pi-slam) : 智能系统实验室开发的插件化SLAM系统，能够学习视觉SLAM编程
* [Map2DFusion](https://gitee.com/pi-lab/Map2DFusion) ： 实时二维地图拼接，通过视觉SLAM解算位姿，并增量式拼接二维地图
* [序列化图像的SfM重建](https://gitee.com/pi-lab/SequenceSfM) : 本实例程序演示了如何编写基本的SfM程序，通过利用序列化数据的特点加快重建的速度



## 5. 机器学习
* [航拍图像的物体检测](https://gitee.com/pi-lab/pi-det)：演示如何使用YOLO+PyTorch实现物体检测与在线增量学
* [基于像素对应的差异检测](https://gitee.com/pi-lab/Mask-CDNet) : 演示了如何通过像素级对应实现差异的检测
* [PyTorch版本的NetVLAD](https://gitee.com/hu_jinsong/pytorch_-net-vlad)：基于PyTorch的NetVLAD代码，实现场景认识、重定位

* 目标识别与跟踪
	- [静态目标识别—yolov4_pytorch实现](https://gitee.com/hu_jinsong/yolov4_-py-torch)
	- [人工画框后跟踪—pysot实现](https://gitee.com/hu_jinsong/py-sot_-tracking-after-artificial-frame)
	- [自动识别跟踪—deepsort_yolov4实现](https://gitee.com/hu_jinsong/deepsort_yolov4)