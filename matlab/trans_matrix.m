function p = trans_matrix(r1, r2, r3, t1, t2, t3)

a1 = [1 0 0; 0 cos(r1) sin(r1); 0 -sin(r1) cos(r1)];
a2 = [cos(r2) 0 -sin(r2); 0 1 0; -sin(r2) 0 cos(r2)];
a3 = [cos(r3) sin(r3) 0; -sin(r3) cos(r3) 0; 0 0 1];

p = zeros(3, 4);
p(1:3, 1:3) = a1*a2*a3;
p(:, 4) = [t1; t2; t3];

