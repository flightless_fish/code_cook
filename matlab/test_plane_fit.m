function test_plane_fit()

p = [1 1 1 1];

pts1 = generate_samples(p, 500, 0.000001);
%pts2 = generate_samples(p, 100, 10);
%pts3 = generate_samples(p, 10,  200);
%pts = [pts1; pts2; pts3];

pts = pts1;

p_m = mean(pts);
p_c = cov(pts);

[u, s, v] = svd(p_c);

a1 = u(:, 1);
a2 = u(:, 2);
a3 = u(:, 3);



function pts = generate_samples(p, np, n)
%
% generate plane data 
% 
% Arguments:
%   p       plane equation
%   np      point number
%   n       point noise

pts = zeros(np, 3);

for i=1:np
    x = (rand()-0.5) * 10;
    y = (rand()-0.5) * 10;
    
    z = -p(1)*x - p(2)*y - p(4) + randn*n;
    
    pts(i, :) = [x, y, z];
end
