
p(:, 1) = [1, 2, 0, 1]';
p(:, 2) = [2, 2, 0, 1]';
p(:, 3) = [2, 1, 0, 1]';

a = trans_matrix(0*pi/180, 0*pi/180, 30*pi/180, ...
                 0, 0, 0);
pn = a*p;

lp  = [p, p(:,1)];
lpn = [pn, pn(:,1)];
clf;
line(lp(1, :), lp(2,:), lp(3,:), 'Color', 'r');
line(lpn(1, :), lpn(2,:), lpn(3,:), 'Color', 'b');
axis equal;
xlabel('x'); ylabel('y'); zlabel('z');
grid on;