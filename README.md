# Code Cook for Quick Programming - 编程快速参考实例集

![code cook](images/code_cook.jpg)

这个工程包含了常见软件库的实例代码，通过使用这些代码能够快速的学会如何利用这些库来加速程序的编写。当中也包含了一些使用方法的文档（`./doc`目录下）

编程非常重要的一个技能就是`照葫芦画瓢`，在没有比较深厚的背景知识和编程技能的情况下，通过学习已有的代码片段、小项目来作为自己编程、做项目的开始，是一个非常好的方式。在做的过程发现自己的不足，通过查资料、查解决办法，不断提高自己的能力。


本仓库整理并罗列了一些功能代码片段和[《示例项目》](DemoProjects.md)，能够为大家提供编程学习的参考资料，请大家认真查阅并借鉴学习。



## 1. 安装依赖
建议在Ununtu，LinuxMint环境下编译程序
```
sudo apt-get install build-essential g++ cmake git
sudo apt-get install freeglut3-dev libxmu-dev libxi-dev
sudo apt-get install libqt4-dev libqt4-opengl-dev
sudo apt-get install libopencv-dev 
sudo apt-get install libboost-all-dev
sudo apt-get install libgoogle-perftools-dev google-perftools
```

## 2. 编译
按照下述命令进行编译，建议使用qtcreator打开`CMakeLists.txt`来对项目文件进行编辑。
```
mkdir build
cd build
cmake ..
make
```

编译之后，可执行程序在项目的`bin`目录下，例如执行
```
../bin/c++_basic_types
```

## 3. 包含的程序列表

|实例模块|基本描述|
| -------------------------------- |:-------------------------------------------------------|
| `PICMake`                        | 本实例程序所使用的编译方法，具体的cmake模块位于`./cmake`目录下 |
|||
| `C++`           | C++以及多种C++库的实例，具体的代码位于`c++`目录下 |
| `C++/c++basic`  | C++的基本用法，以及常用的`utils`库                |
| `C++/stl`       | C++ STL的基本用法                                 |
| `C++/boost`     | boost库的使用例子                                 |
| `C++/eigen3`    | Eigen3 矩阵运算库的使用例子                       |
| `C++/argparse`  | argparse命令行参数读取的使用例子                  |
| `C++/exif`      | TinyEXIF的使用例子，读取JPEG图片的标签信息        |
| `C++/key-value` | unqlite的key-value数据库的使用例子                |
| `C++/nanoflann` | nanoflann的使用例子，头文件的快速最近距离计算库   |
| `C++/regexp`    | PCRE正则表达式的例子                              |
| `C++/sqlite3pp` | SQLite的实例代码                                  |
| `C++/uart`      | 串口设备读取的例子                                |
| `C++/xml`       | tinyxml2的使用例子                                |
| `C++/cache`     | OfflineFIFO等缓冲库                               |
|||
| `cv`                             | OpenCV的使用方法 |
| `cv/cv_mat.cpp`                  | OpenCV中Mat的使用方法 |
| `cv/cv_image.cpp`                | OpenCV中图像处理的使用方法 |
| `cv/cv_higui.cpp`                | OpenCV中图像读取、显式，视频读取、显示的使用方法 |
| `cv/cv_calib3d.cpp`              | OpenCV中相机标定的使用方法 |
|||
| `gslam`                          | GSLAM中的一些例子程序 |
| `gslam/gimage.cpp`               | GSLAM中图像读取的使用方法 |
| `gslam/gtest_demo.cpp`           | GTEST的使用方法 |
|||
| `gui/qt`                         | QT的使用例子 |
| `gui/qt/1_simple_draw`           | 最简单的2D绘图示例程序 |
| `gui/qt/2_draw_map`              | 利用QGraphicsView显示二维网格图的例子 |
| `gui/qt/3_elasticnodes`            | 利用QGraphicsView/QGraphicsItem显示二维弹簧网络的例子 |
| `gui/qt/4_pathplane_2d`            | 利用QGraphicsView展示二维网格地图的路径规划 |
| `gui/qt/5-qglviewer`               | QGLViewer的例子 |
| `gui/qt/6_qt_controls`             | Qt的一些控件 |
| `gui/sfml`                       | 轻量化GUI库sfml的例子程序 |
|||
| `python`                         | Python使用例子 |
| `python/basic`                   | Python常用的函数、用法等 |
| `python/pconf`                   | Python如何加载配置（json/yaml)的例子 |
| `python/modules`                 | Python模块化编程的例子 |
|||
| `docker`                         | Docker使用说明，文档 |
|||
| `thirdparty`                     | 常用的第三方库 |


## 4. 相关学习资料参考

更多的学习资料可以参考：


1. [《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
2. 飞行器智能感知与控制实验室 - 培训教程与作业
    - [《飞行器智能感知与控制实验室-暑期培训教程》](https://gitee.com/pi-lab/SummerCamp)
    - [《飞行器智能感知与控制实验室-暑期培训作业》](https://gitee.com/pi-lab/SummerCampHomework)
3. 机器学习教程与作业
    - [《机器学习教程》](https://gitee.com/pi-lab/machinelearning_notebook)
    - [《机器学习课程作业》](https://gitee.com/pi-lab/machinelearning_homework)
4. [《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)
5. [《一步一步学ROS》](https://gitee.com/pi-lab/learn_ros)
6. [飞行器智能感知与控制实验室-研究课题](https://gitee.com/pi-lab/pilab_research_fields)


### 4.1 工具的使用教程等

* [Linux](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/linux/README.md)
* [Markdown](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/markdown/README.md)
* [Git](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/README.md)
* [CMake](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/cmake/README.md)

