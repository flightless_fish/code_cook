
## 如何执行 sh/bash
```
docker run -it --rm pytorch/pytorch /bin/bash
```

## 如何生成一个新的IP地址，并链接到docker

生成一个新的IP地址
```
sudo ifconfig wlp3s0:0 192.168.3.2 netmask 255.255.255.0 up
```

运行docker
```
docker run \
    -it --rm \
    --link yapi_mongodb:mongo \
    -p 192.168.3.2:80:8081 \
    --name yapi_mongo_express \
    mongo-express
```

## 如何显示docker的log
```
docker logs -f gitlab
```


## 如何快速将一台机器上的docker image配置到另外一台电脑
可以通过`docker save`将镜像进行导出，同时进行压缩，从而减少传输过程中的文件大小。这里使用`zstd`，比较好的均衡了压缩速度和压缩率。

install tools
```
sudo apt-get install zstd
```

compress docker image to file
```
docker save pytorch/pytorch | zstdmt > pytorch.tar.zst
```

restore compressed image to docker
```
cat pytorch.tar.zst | zstdmt -d | docker load
```


## 如何在docker build时候时候主机的网络 (目前已经无用，新版的程序修复了访问网络的问题)
```
docker build --net=host -f tools/docker/Dockerfile .
```


## docker volume
```
$ docker volume ls | grep mysql

local               vagrant_mysql


$ docker volume inspect vagrant_mysql

[

   {

       "Name": "vagrant_mysql",

       "Driver": "local",

       "Mountpoint": "/var/lib/docker/volumes/vagrant_mysql/_data"

   }

]
```


## Web management

Docker web-based management tool. Install instructions:
```
$ docker volume create portainer_data
$ docker run -d -p 8000:8000 -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```
