#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")


# make dirs
if [ ! -e $CUR_DIR/gitlab-data ]; then
    mkdir $CUR_DIR/gitlab-data

    mkdir $CUR_DIR/gitlab-data/config
    mkdir $CUR_DIR/gitlab-data/logs
    mkdir $CUR_DIR/gitlab-data/data
fi


#
# bind a new IP address
#   https://www.garron.me/en/linux/add-secondary-ip-linux.html
#
N_DEV='wlp3s0'
N_IP='192.168.3.3'

un=`whoami`
if [ "$un" == "root" ]; then
    ifconfig $N_DEV:0 $N_IP netmask 255.255.255.0 up
else
    sudo ifconfig $N_DEV:0 $N_IP netmask 255.255.255.0 up
fi


#
# gitlab
#   https://docs.gitlab.com/omnibus/docker
#
docker run --detach \
    --hostname gitlab.example.com \
    --publish $N_IP:443:443 --publish $N_IP:80:80 --publish $N_IP:22:22 \
    --name gitlab \
    --restart always \
    --volume $CUR_DIR/gitlab-data/config:/etc/gitlab \
    --volume $CUR_DIR/gitlab-data/logs:/var/log/gitlab \
    --volume $CUR_DIR/gitlab-data/data:/var/opt/gitlab \
    gitlab/gitlab-ce
