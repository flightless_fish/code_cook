#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")

# vsftp
#   https://github.com/fauria/docker-vsftpd
#   https://www.jianshu.com/p/e51fe1975c29

docker run -d -v $CUR_DIR/vsftp-data:/home/vsftpd \
    -p 21:21 -p 21100-21110:21100-21110 \
    -e FTP_USER=test -e FTP_PASS=test \
    --restart always \
    --name vsftpd fauria/vsftpd


# References
#   https://github.com/fauria/docker-vsftpd
#   https://www.jianshu.com/p/e51fe1975c29
