# Docker - Nginx

创建
* `config`: stroe configure
* `www`: the static resource
* `logs`: log storage


```
docker run \
  --name nginx \
  -d -p 6800:80 \
  -v `pwd`/config:/etc/nginx/conf.d \
  -v `pwd`/www:/www \
  nginx
```


## Reference
* 在Docker下部署Nginx http://blog.shiqichan.com/Deploying-Nginx-with-Docker/
* Docker部署nginx并修改配置文件 https://blog.csdn.net/wangfei0904306/article/details/77623400
