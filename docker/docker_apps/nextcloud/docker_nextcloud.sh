#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")


# https://github.com/docker-library/docs/blob/master/nextcloud/README.md

docker run -d \
    --name nextcloud \
    --restart always \
    -p 8080:80 \
    -v $CUR_DIR/nextcloud_data:/var/www/html \
    -v $CUR_DIR/nextcloud_db:/var/lib/mysql \
    nextcloud

