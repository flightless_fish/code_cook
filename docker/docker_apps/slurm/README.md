## References

* [How to quickly set up Slurm on Ubuntu 20.04 for single node workload scheduling](https://drtailor.medium.com/how-to-setup-slurm-on-ubuntu-20-04-for-single-node-work-scheduling-6cc909574365)
* [如何使用SLURM？](https://zhuanlan.zhihu.com/p/345387783)
* [Slurm超算集群跑深度学习代码教程](https://blog.csdn.net/weixin_43723625/article/details/125060527)
* [Slurm快速入门用户指南](https://docs.slurm.cn/users/kuai-su-ru-men-yong-hu-zhi-nan)
* [通过 slurm 系统使用 GPU 资源](https://co1lin.github.io/AIR-Server-Doc/gpu/)

