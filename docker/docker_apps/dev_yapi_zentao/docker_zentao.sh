#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")

docker run -d \
        -p 8005:80 -p 8306:3306 \
        --restart always \
        -e USER="root" -e PASSWD="adDf32424Jf#jfs" \
        -e BIND_ADDRESS="false" \
        -e SMTP_HOST="163.177.90.125 smtp.exmail.qq.com" \
        -v $CUR_DIR/zbox_data/:/opt/zbox/ \
        --name zentao-server \
        idoop/zentao
        

#
# Reference:
#   https://github.com/idoop/zentao
#