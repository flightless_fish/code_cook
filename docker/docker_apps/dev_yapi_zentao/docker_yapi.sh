#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")

# account
#   初始化管理员账号成功,账号名："admin@admin.com"，密码："ymfe.org"
#   用户名是注册时候的邮箱


docker pull registry.cn-hangzhou.aliyuncs.com/anoy/yapi
docker tag  registry.cn-hangzhou.aliyuncs.com/anoy/yapi yapi

# start mongodb
docker run -d \
    --name yapi_mongodb \
    --restart always \
    -v $CUR_DIR/mongo_db:/data/db \
    mongo


# setup db
docker run -it --rm \
  --name yapi_web \
  --link yapi_mongodb:mongo \
  --entrypoint npm \
  --workdir /api/vendors \
  yapi \
  run install-server

# start yapi
docker run -d \
  --restart always \
  --name yapi_web \
  --link yapi_mongodb:mongo \
  --workdir /api/vendors \
  -p 3000:3000 \
  yapi \
  server/app.js


