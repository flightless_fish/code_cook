## Torrent settings
torrent search:
* https://1337x.to/home/
* https://torrentz2.eu/

torrent client:
* qbittorrent
* transmission
* utorrent

## qbittorrent
https://hub.docker.com/r/linuxserver/qbittorrent/

```
docker run \
  --name=qbittorrent \
  -e PUID=1001 -e PGID=1001 \
  -e TZ=Europe/London \
  -e UMASK_SET=022 \
  -e WEBUI_PORT=8080 \
  -p 6881:6881 \
  -p 6881:6881/udp \
  -p 8080:8080 \
  -v /home/bushuhui/download/bt/config:/config \
  -v /home/bushuhui/download/bt/downloads:/downloads \
  --restart unless-stopped \
  linuxserver/qbittorrent
```

## uTorrent
https://hub.docker.com/r/ekho/utorrent

```
docker run \
    --name utorrent \
    -v /home/bushuhui/download/ut/download:/utorrent/data \
    -v /home/bushuhui/download/ut/settings:/utorrent/settings \
    -e HOST_UID=1001 -e HOST_GID=1001 \
    -p 8080:8080 \
    -p 6881:6881 \
    ekho/utorrent:latest
```

default username and password:
* username: admin
* password: <empty>

## Transimssion setting
* user proxy for tracker: https://askubuntu.com/questions/551278/how-to-use-proxy-for-transmission

```
export http_proxy=socks5://192.168.1.4:1080
export https_proxy=socks5://192.168.1.4:1080
```

```
docker run \
  --name=transmission \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e TRANSMISSION_WEB_HOME=/combustion-release/ \
  -e USER=username \
  -e PASS=password \
  -p 9091:9091 \
  -p 51413:51413 \
  -p 51413:51413/udp \
  -v /home/bushuhui/downloads/amule/transmission/config:/config \
  -v /home/bushuhui/downloads/amule/transmission/downloads:/downloads \
  -v /home/bushuhui/downloads/amule/transmission/watch:/watch \
  --restart unless-stopped \
  linuxserver/transmission
```

## Tracker list
torrent tracker:
* https://github.com/ngosang/trackerslist
* https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all_http.txt
* https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all_https.txt

```
https://tracker.lelux.fi:443/announce

https://tracker.gbitt.info:443/announce

https://tracker.nanoha.org:443/announce

https://tracker.sloppyta.co:443/announce

https://tracker.nitrix.me:443/announce

https://tracker.tamersunion.org:443/announce

https://tracker.parrotlinux.org:443/announce

https://tracker.hama3.net:443/announce

https://t3.leech.ie:443/announce

https://t2.leech.ie:443/announce

https://t1.leech.ie:443/announce

https://aaa.army:8866/announce

https://1337.abcvg.info:443/announce

http://tracker.opentrackr.org:1337/announce

http://p4p.arenabg.com:1337/announce

http://tracker3.itzmx.com:6961/announce

http://tracker1.itzmx.com:8080/announce

http://tracker.nyap2p.com:8080/announce

http://explodie.org:6969/announce

http://tracker.zerobytes.xyz:1337/announce

http://h4.trakx.nibba.trade:80/announce

http://vps02.net.orel.ru:80/announce

http://tracker.lelux.fi:80/announce

http://tracker.kamigami.org:2710/announce

http://tracker.gbitt.info:80/announce

http://opentracker.i2p.rocks:6969/announce

http://t.overflow.biz:6969/announce

http://tracker.bt4g.com:2095/announce

http://trun.tom.ru:80/announce

http://tracker.yoshi210.com:6969/announce

http://tracker.ygsub.com:6969/announce

http://tracker.skyts.net:6969/announce

http://tracker.dler.org:6969/announce

http://t.nyaatracker.com:80/announce

http://retracker.sevstar.net:2710/announce

http://pow7.com:80/announce

http://open.acgnxtracker.com:80/announce

http://mail2.zelenaya.net:80/announce

http://highteahop.top:6960/announce

http://aaa.army:8866/announce

http://tracker4.itzmx.com:2710/announce

http://tracker2.itzmx.com:6961/announce

http://tracker.gcvchp.com:2710/announce

http://tracker.acgnx.se:80/announce

http://t.acg.rip:6699/announce
```
