#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
CUR_DIR=$(dirname "$SCRIPT")

mkdir -p $CUR_DIR/ldap
mkdir -p $CUR_DIR/slap.d

# LDAP
docker run -it \
    -p 389:389 \
    -v $CUR_DIR/ldap:/var/lib/ldap \
    -v $CUR_DIR/slap.d:/etc/ldap/slapd.d \
    --name ldap \
    --env LDAP_TLS=false \
    --env LDAP_ORGANISATION="PI-LAB" \
    --env LDAP_DOMAIN="adv-ci.com" \
    --env LDAP_ADMIN_PASSWORD="123456" \
    --env LDAP_CONFIG_PASSWORD="123456" \
    --restart always \
    --detach osixia/openldap
    
# phpldapadmin
docker run -it \
    -p 19999:80 \
    --link ldap \
    --name ldap_mgr \
    --env PHPLDAPADMIN_HTTPS=false \
    --env PHPLDAPADMIN_LDAP_HOSTS=ldap \
    --restart always \
    --detach osixia/phpldapadmin
