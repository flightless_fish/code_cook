## Capture Screen

`Shutter` is a useful tools for capture screen.

```
sudo apt install shutter
```



Then add global key, 

Step1: Open system setting

![system setting](images/Shutter1_LinuxMintMenu.png)



Step2: Open keyboard setting

![system setting](images/Shutter2_Settings.png)



Step3: Open `Application Shotcuts`

![system setting](images/Shutter3_ApplicationShortcuts.png)



Step 4: Add `shutter` command `shutter -s`

![system setting](images/Shutter4_Command.png)



## Screen Movie Capture

```
sudo apt install kazam
```

