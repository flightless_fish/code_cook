# 如何在Ubuntu 18.04里增加`rc.local`

* 参考 https://oldtang.com/2463.html

## 一、创建 rc-local.service 配置文件
这个配置文件默认在 /lib/systemd/system/ 这个目录下，我们只需将它拷贝到对应目录：
```
cp /lib/systemd/system/rc-local.service /etc/systemd/system/rc-local.service
```

然后编辑这个文件：
```
vim /etc/systemd/system/rc-local.service
```

在最后加上这些内容：
```
[Install]
WantedBy=multi-user.target
Alias=rc-local.service
```

修改后的 rc-local.service 文件完整如下：
```
#  SPDX-License-Identifier: LGPL-2.1+
#
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

# This unit gets pulled automatically into multi-user.target by
# systemd-rc-local-generator if /etc/rc.local is executable.
[Unit]
Description=/etc/rc.local Compatibility
Documentation=man:systemd-rc-local-generator(8)
ConditionFileIsExecutable=/etc/rc.local
After=network.target

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
RemainAfterExit=yes
GuessMainPID=no

[Install]
WantedBy=multi-user.target
Alias=rc-local.service
```

## 二、创建 rc.local 文件
配置完成后，还需要创建一个 rc.local 文件：
```
sudo vim /etc/rc.local
```
在这个文件里写入我们想要开机自动启动的命令即可。

然后给这个文件加上执行权限：
```
sudo chmod +x /etc/rc.local
```

编辑以下内容到`/etc/rc.local`并重启测试
```
#!/bin/bash

echo "test rc " > /var/test.log
```


至此，就已经完成了 rc.local 的配置，重启之后会自动启动对应想启动的脚本。

如果发现没有生效，可以尝试：
```
sudo systemctl enable rc-local
```