# RTMapperMobile

This software currently support Android platform. You can use this software to capture photo or video by defined area or waypoints. 




## User interface

This APP has following GUI components:

1. **UAV status pannel** (on top-left): Display and control UAV settings.
2. **Setup and Flight button** (on top-right): Setup the system parameters or start/stop mission.
3. **Mission pannel** (right): Add/Edit/Delete mission, and setting mission parameters.
4. **Video viewport** (bottom-left): Showing received vide stream.

![App GUI](images/rtmapper-mobile/app_gui.jpg)

## Missions

This APP will greatly reduce the workload for capture given area's images. Currently, there are two types of mission supported:

1. **Area mission**: You can draw a region, and the waypoints will be automatically generated to cover the region. 
2. **Waypoints mission**: You can draw waypoints in the map, the UAV will follow the waypoint to take photos or video.

![mission type](images/realtime-map/02_Mission_type.png)


## Mission parameters

* `Camera Model`: The camera currently mounted. To match your UAV model.
* `Camera Orientation`: The pitch angle of camera
* `Photo Mode`: The photo capture mode
  * `Equal time interval` capture images in same time interval
  * `Isometric interval` caputure images in same distance interval
  * `No photo` do not take photo, please use this mode for real-time map creation
* `Flight Height`: Control the UAV height for recording video
* `Flight Speed`: Control the horizontal flight speed 
* `Interline Overlap Rate`: Control the width's overlap ratio
* `Main Route Angle`: Generated route's angle to North-Sourths

![mission setup1](images/realtime-map/03_Setup1.jpg)

![mission setup2](images/realtime-map/03_Setup2.jpg)


## Start mission

When you mission has been setted and checked. You can click paper-plane icon (top-right of screen) to start the mission. It is very important to confirm the UAV's status and mission data are correct before start the mission.

![mission confirm](images/rtmapper-mobile/mission-confirm.jpg)
