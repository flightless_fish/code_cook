# About 

This software are developed by [Pilot intelligent laboratory](http://www.adv-ci.com) and [MaperStar Co. Ltd.](http://www.rtmapper.com). Our mission is to develop leading technologies of SLAM and AI. 


- Software website: [http://www.rtmapper.com](http://www.rtmapper.com)
- Technical service e-mail: rtmapper@163.com


![logo](images/RTMapper_logo.png)
