# Changelog of RTMapper

## 3.2.3 (2018-04-25)
* RTMapperDesktop:
    - Support local character for filename, path on Windows
    - Fix crash when project finished
    - Fix GUI for better user experience
    - Add online manual
    - Fix GPS fitting problems, improve fitting robustness
* RTMapperMobile: 
    - Add area mission type
    - Improve user interface, mission settings, waypoint editing


## 3.2.2 (2018-03-05)
* Fix some license bugs
* Use Qt5 and VisualStudio for Windows platform


## 3.2.0 (2018-02-15)
* New RTMapperSDK, and RTMapper architecture
* Support RTMapperMobile to realize real-time mapping using DJI drones
* Provide RTMapperSDK (https://github.com/zdzhaoyong/RTMapperSDK) for professional development


## 3.1.0 (2018-01-10)
* Add globalOptionWidget
* Improve opmapcontrol for displaying generated tile
* Imrpove dbNet for supporting network datastream transfer


## 2.1.1 (2017-06-01)
* First public release of RTMapper


