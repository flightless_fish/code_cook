# Fast reconstruction from photos
In this mode, the software use our develop image processing algorithms to compute precise camera's position and attitude, meanwhile the estimated values and stored GPS inforation are jointly optimized for stitching photos to map. 

The main operation include flowing steps: create project, select photos, set camera parameters, computation, export results. 

## 1. Create project

1. Click menu `File->New` or `Ctrl-N` to open new project wizard.
2. Enter `project name` and select project folder.
3. Select project type to: `Ortho`, Oblique is not support now.
4. Choose data source to `Images`.
5. Then click `Next` to next wizard page.

![create project](images/fast-recon/01_create_project.png)

## 2. Select photos

1. Click `Add Images` to add images to the project. If some of the photos are not good, you can select them and then click `Delete Image` to remove them.
2. When images are loaded, and confirm they are correct, then click `Next`.

![slecting photos](images/fast-recon/02_select_photos.png)

## 3. Set camera parameters

1. In this window, you can see each image's properties: file name, captured date/time, longitude/latitude, altitude, and so on. 
2. Set camera parameters if they are not exist in EXIF, or they are not correct. 

The meaning of camera parameters can be view at [here](RTMapperDesktop.md#camera-parameters).


![set camera](images/fast-recon/03_camera_parameters.png)


## 4. Set reconstruction options
After the camera parameter setting, you need to set [project options](RTMapperDesktop.md#project-options).


![project option1](images/fast-recon/04_project_option1.png)

![project option2](images/fast-recon/05_project_option2.png)


## 5. Computation

1. Press the `Run` button (Right direction triangle) to begin the execution.
2. The processing logs are displayed at bottom of the window. 
3. The processing status are displayed at the status panel.

Click the start button (Right direction triangle) on toolbar pannel to start the computation. 
![start project](images/fast-recon/06_start_project.jpg)

The 2D result viewing window, showing generated map.
![2D window](images/fast-recon/07_2D_window.jpg)

The 3D result viewing window, showing camea and point cloud.
![3D window](images/fast-recon/07_3D_window.jpg)


## 6. Export results

1. Press menu `File` -> `Export` to export results.
2. Select which type data you want to export.

Currently, this software support three types of export:

* Sparse point cloud: The system computed point cloud
* Orthoimage (.kml): Export PNG image with kml description
* Orthoimage (.tif): Geometric information are embedded in the TIFF image.

![export](images/fast-recon/08_export.png)
