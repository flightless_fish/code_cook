# License register
This software provide free trial function, you can use to process 30 images freely. However, if you want to process more images, please register it.

## 1. Open license window
1. Click menu `Help` -> `Register`
2. Input your email address
3. Press `Generate Product ID` to generate and save the product ID

![register window](images/license/01_input_email.png)

## 2. Generate you computer ID
1. Select the folder for saving the product ID file.
2. Please the contact us for buying the license through rtmapper@163.com. Please attach the product ID file in the mail.

![save productID](images/license/02_save_pid.png)


## 3. Setup license
1. Receive the license file from us.
2. Click `Set License` to select the saved license file.
3. Confirm the license.


Click `Set License` to set the license
![set license](images/license/03_set_license.png)

Select the received license file
![select license file](images/license/04_select_licenseFile.png)

Confirm the license period
![confirm license](images/license/05_confirm_license.png)
