

# Real-time processing by using RTMapperMobile

In this mode, you need a [DJI drone](https://www.dji.com) and an Android phone/tablet to capture video in real-time. You need install [RTMapperMobile](http://www.rtmapper.com/download/RTMapperMobile-latest.apk) to perform data transfer. 

If you want to use your own UAV, you can write a program based on [RTMapperSDK](https://github.com/zdzhaoyong/RTMapperSDK). Detailed development support can be found at our [SDK website](https://github.com/zdzhaoyong/RTMapperSDK).

The map creation are running on a desktop/laptop computer running [RTMapperDesktop](RTMapperDesktop.md) software. 


## 1. Requirement
1. Download [RTMapperMobile](RTMapperMobile.md) and install into you Android phone/tablet. The download address is: [http://www.rtmapper.com/download/RTMapperMobile-latest.apk](http://www.rtmapper.com/download/RTMapperMobile-latest.apk)
2. You need a DJI drone. Support models: Mavic, Mavic Air, Mavic Pro, Spark, Phantom 4 Pro, Phantom 4 Advanced, Inspire 2.


## 2. Setup [RTMapperMobile](RTMapperMobile.md)

1. Turn on `WiFi Hotspot` of your phone/tablet.
2. Open [RTMapperMobile](RTMapperMobile.md) APP on you phone.
3. Open setup panel by touching 2nd icon (gear shape) on the top-right screen.
4. On the pannel, select data transfer setting (The 3rd icon).
5. Turn `Transmit Data with Wifi`. And remember the IP address and port. 
6. If you want to save the datastream to you phone's SD card, you can turn `Record Data to File` to open.

![setup](images/realtime-map/01_Setup.png)

## 3. Create Mission
1. Click `New` on the *Missions* panel.

![create mission](images/realtime-map/02_Create_mission.jpg)

2. Select mission type: `AreaMission` or `WayPointMission`

![mission type](images/realtime-map/02_Mission_type.jpg)


## 4. Setup mission
1. Moving the anchor point to modify mission's area or waypoints, press the `+` to add control point.

2. Set [mission parameters](RTMapperMobile.md#mission-parameters)

![mission setup1](images/realtime-map/03_Setup1.jpg)

![mission setup2](images/realtime-map/03_Setup2.jpg)

Some important parameters are listed at [here](RTMapperMobile.md#mission-parameters). 


## 5. Start [RTMapperDesktop](RTMapperDesktop.md)

1. Click menu `File->New` or `Ctrl-N`
2. Enter `project name` and select project folder
3. Select project type to: `Ortho`, Oblique is not support now.
4. Choose data source to `IP TCP/UDP`
![create project](images/realtime-map/04_CreateProject.jpg)


5. Setup IP address and port to previously setted WiFi hotspot's IP address and port.
![create project](images/realtime-map/04_Setup_IP_Port.jpg)


## 6. Set reconstruction options
After the camera parameter setting, you need to set [project options](RTMapperDesktop.md#project-options).

![project option1](images/fast-recon/04_project_option1.png)

![project option2](images/fast-recon/05_project_option2.png)

## 7. Computation

1. Press the `Flight` button (Top-right paper airplane icon) on the [RTMapperMobile](RTMapperMobile.md) App. Then the flight mission is start if everything is OK.
![mission setup1](images/realtime-map/03_Setup1.jpg)

2. When the UAV reach a the height above 50 m or your desired condition, press the `Run` button on [RTMapperDesktop](RTMapperDesktop.md) (Right direction triangle) to begin the execution.
![start project](images/realtime-map/05_run.jpg)

3. The processing logs are displayed at bottom of the window. 
4. The processing status are displayed at the status panel.


## 8. Export results
When the flight task is finished, you can export the results.

1. Press menu `File` -> `Export` to export results.
2. Select which type data you want to export.

There are three types of export:

* Sparse point cloud: The system computed point cloud
* Orthoimage (.kml): Export PNG image with kml description
* Orthoimage (.tif): Geometric information are embedded in the TIFF image.

![export](images/fast-recon/08_export.png)
