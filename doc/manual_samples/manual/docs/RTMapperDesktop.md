# RTMapperDesktop

This program running on desktop/laptop computer for fast/real-time processing captured photos and video. 

## Main window

This software has following GUI components:

1. **Menu bar**: control the operation
2. **Tool bar**: quick access the operation
3. **Log window**: display the running status
4. **Side bar**: control the visualization options, and current images
5. **Status pannel**: display processing status

![main window](images/rtmapper-desktop/mainwindow.jpg)


## Project options

When creating the project or load the project, you can set the compution options.

***Options for SLAM:***

* `nFeature`: Extracted feature number for each frame, default value is 1000.
* `MaxOverlap`: Maximum overlap between consequative frames, default value is 0.95.
* `LostRestart`: Determin how long the SLAM restart when lost tracking, default value is 10 seconds.

![project option1](images/fast-recon/04_project_option1.png)


***Options for Map2DFusion:***

* `Coordinate`: aviable settings include 
    - `GCJ`: Google, Gaode map's coordinate (default)
    - `GPS`: WGS84 coordinate
    - `BAIDU`: Baidu map's coordinate
* `WarpMode`: 
    - `DLT`: Direct linear transform method (default)
    - `MDLT`: Multi-DLT method 
* `Blender`:
    - `Multiband`: Use adaptive weighted multiband to fuse images to photo (default)
    - `Simple`: Simple fusing alogirhm.
    - `SeamMultiband`: Use seam and multiband algorithm.
* `MaxZoom`: Maximum zoom level for generating map. Default value is 19 (resolution is 0.2986 m/pixel), and maximum value is 21 (resolution is 0.0746 m/pixel). 
* `CacheMB`: How man tile cache can be used, the unit is MB. Default value is 1000 MB.
* `FittingThresho`: Threshold for fitting ground plane, default value is 20 m.

![project option2](images/fast-recon/05_project_option2.png)


## Camera parameters

This software use [OpenCV's camera model](https://docs.opencv.org/2.4/doc/tutorials/calib3d/camera_calibration/camera_calibration.html). For more detailed description you can found them at [here](https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html).

* `width`, `height`: The pixel size in column and row.
* `fx`, `fy`: The focal length in x and y direction.
* `cx`, `cy`: The principle center in x and y direction.
* `k1`, `k2`, `p0`, `p1`, `k3`: radial distortion parameters.

![set camera](images/fast-recon/03_camera_parameters.png)
