# RTMapper Manual

## 1. Introduction

![logo](images/RTMapper_logo.png)

[RTMapper](http://www.rtmapper.com) is a software based on computer vision and deep learning for real-time and online creating/processing digital map. It provides full sets of solutions for map creation, difference/object detection. Through [RTMapperMobile](RTMapperMobile.md) the full task can be accomplished just one-click by DJI drone, in addtion our technology can be integrated into you UAV system by using [RTMapperSDK](https://github.com/zdzhaoyong/RTMapperSDK) to make your system has the capability of real-time processing. 


## 2. Quick start
One of the significant advantages of this software is easy to use, you do not need to set boring parameter and wait to do complex processing steps. You can reconstuct the map just select you photo and wait it fished in minutes. There are two working modes:

1. [Fast reconstruction from photos](FastReconstructionFromPhoto.md): The software accepts UAV captured photo's and compute camera's position and attitude with fast speed, the map are fused incremental. 

2. [Real-time processing by using RTMapperMobile](RealtimeReconstruction.md): This software receive UAV transfered video stream, and processing them in real-time, while the map creation is also incremental. 


## 3. [License register](LicenseRegister.md)

This software provide free trial function, you can use to process 30 images freely. However, if you want to process more images, please register it.


## 4. Features

The software are based on SLAM technology, therefore, it offers fast or real-time processing speed. The features are concludes as:

* Fast and real-time map creation, the map creation just in minutes.
* The map are incremental fused, you do not wait too long.
* Each component is implement as plugin, easy to be integrated into your system.
* Support multiply operation systems, including Windows and Linux.
* Support phone/tablet (current just support Android). 
* Support cloud computation (will be provided soon).
* Support GPU acceleration.
* 2D image and 3D point cloud exportation.
* Multi-type data input support: Video and Images.
* Provide SDK for development.
* Dense point cloud and 3D mesh reconstruction (Will be provided soon)


## 5. [Changelog](CHANGELOG.md)

The detail of changes of the software can be found at [Changelog](CHANGELOG.md).

## 6. [About](About.md)

- Software website: [http://www.rtmapper.com](http://www.rtmapper.com)
- Technical service e-mail: rtmapper@163.com

