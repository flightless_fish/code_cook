# Document of RTMapper

## 1. MkDocs

1. Install software [MkDocs](http://www.mkdocs.org)
```
pip install mkdocs
pip install mkdocs-windmill
pip install mkdocs-cinder
```

2. Write the `mkdocs.yml` to describe the project

3. Run the live server
```
cd manual
mkdocs serve
```

If you save the edited contents, the brower will automatically refresh to new contents.

4. Build the document
```
mkdocs build
```

The contents are stored in `site` directory.



