# 关于我们

本软件由[派勒智能实验室](http://www.adv-ci.com)和[西安拇指星智能科技有限责任公司](http://www.rtmapper.com)共同开发。我们的使命是引领SLAM和AI的科技潮流。

- 软件官方网站：http://www.rtmapper.com
- 技术服务联系方式：rtmapper@163.com

![logo](images/RTMapper_logo.png)