# RTMapper桌面版

本软件主要功能为快速或实时的处理获取到的图片和视频序列，需在台式机或笔记本电脑上运行。

## 主窗口

本软件的GUI组件包括如下内容：

1. **菜单栏**：
2. **工具栏**：
3. **输出窗口**：显示运行结果。
4. **侧边栏**：控制可视化选项，以及对当前图片的一些处理。
5. **状态面板**：显示运行状态。

![main window](images/rtmapper-desktop/mainwindow.jpg)

## 工程选项

当创建新的工程或载入已有工程时，您需要进行计算相关的选项设置。

***SLAM相关选项***

* `特征数量`：每一帧提取的特征数量，默认值为1000。
* `最大重叠率`：相邻帧的最大重叠比率，默认值为0.95。
* `重启时间`：当跟踪丢失后，多久重启SLAM计算，默认值为10秒。

![project option1](images/fast-recon/04_project_option1.png)

***Map2DFusion选项***

* `坐标系`：可选设置包括
    - `GCJ`：谷歌、高德地图使用的坐标系。（默认设置)
    - `GPS`：WGS84坐标系。
    - `BAIDU`：百度地图使用的坐标系。
* `Warp模式`：
    - `DLT`：直接线性变换方法。（默认设置）
    - `MDLT`：多重直接线性变化方法。
* `Blender`：
    - `多波段`：使用自适应权重的多波段算法，来将图片融合成地图。（默认设置）
    - `朴素`：朴素融合算法。
    - `接合多波段`：使用接合和多波段算法。
* `最大缩放级别`：生成的地图的最大缩放级别。默认值是19（分辨率为0.2986米/像素），最大值是21（分辨率为0.0746米/像素）。
* `缓存空间`：地图瓦片可使用的缓存大小，单位为Mb。默认值是1000Mb。
* `拟合阈值`：用于拟合大地平面的阈值参数。默认值为20m。

![project option2](images/fast-recon/05_project_option2.png)

## 相机参数

本软件使用 [OpenCV相机模型](https://docs.opencv.org/2.4/doc/tutorials/calib3d/camera_calibration/camera_calibration.html). 更多详细的信息，请参照[此处](https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html).

* `宽度`, `高度`: 以像素为单位，图片行和列的数目。
* `fx`, `fy`: x方向和y方向的焦距。
* `cx`, `cy`: 像素坐标系上原点的平移量。
* `k1`, `k2`, `p0`, `p1`, `k3`: 径向畸变。

![设置相机](images/fast-recon/03_camera_parameters.png)