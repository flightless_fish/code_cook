# RTMapper 手册


## 1.介绍

![logo](images/RTMapper_logo.png)

[RTMapper](http://www.rtmapper.com) 是基于计算机视觉和深度学习技术的软件，用于实时和在线创建、处理数字地图。它提供了地图创建、差异检测、目标检测等全套的解决方案。借助[RTMapperMobile](RTMapperMobile.md)，您仅需轻轻一点，即可通过大疆DJI的无人机，完成整个任务。同时我们也提供[RTMapperSDK](https://github.com/zdzhaoyong/RTMapperSDK)，您可以将其集成在您的无人机系统中，从而使其具备实时处理的能力。

## 2.快速上手

本软件有一个明显的优势，即非常易于使用。您无需在枯燥的参数设置上花费时间，也不用进行复杂的处理工作。构建地图只需要您选取图片，并等待几分钟即可完成。这里有两种工作模式：

1. [从图片快速重建](FastReconstructionFromPhoto.md)：软件利用无人机拍摄的照片，快速地计算相机位姿，并以增量更新的方式重建地图。
2. [通过RTMapperMobile实时处理](RealtimeReconstruction.md)：软件接收无人机传输的视频流，并实时地处理数据，同时以增量更新的方式重建地图。

## 3.[许可注册](LicenseRegister.md)

本软件提供免费的试用功能，在试用期间，您可以处理30张图片。如果您需要处理更多的图片，请注册软件许可。

## 4.特性

本软件基于SLAM技术，因此具备快速、实时处理的能力。其包含以下特性：

* 快速、实时的地图创建，立谈之间即可完成。
* 地图是增量更新融合的，您无需等待太久。
* 每个组件均以插件化的形式实现，可以轻松集成进您的系统中。
* 支持多个操作系统，包括Windows和Linux。
* 支持手机、平板操作。（当前仅支持Android)
* 支持云端计算。（即将推出）
* 支持显卡（GPU）加速。
* 您可选择输出2D图像或是3D点云信息。
* 支持多种数据输入格式，如图像或视频。
* 提供软件配套的SDK供您进行开发。
* 输出稠密点云及3D网状结构重建。（即将推出）


## 5.[版本信息](CHANGELOG.md)

软件的版本变动细节可以参照[版本信息](CHANGELOG.md)。

## 6.[关于](About.md)

- 软件官方网址：[http://www.rtmapper.com](http://www.rtmapper.com)
- 技术服务邮箱：rtmapper@163.com
