
# 利用图片进行快速重建
在这个模式下，软件使用我们开发的图像处理算法计算精确的相机位置及姿态，位姿估计的数据和已存储的GPS信息将进行联合优化，从而使图片转化为地图。

主要操作包括下列步骤：创建工程、选择图片、设置相机参数、计算、输出结果。

## 1.创建工程

1. 点击菜单 `文件->新建` 或 `Ctrl-N` 开启一个新的工程向导。
2. 点击 `工程名称` 并选择工程文件夹。
3. 选择工程类型为 `Ortho`， 目前暂不支持Oblique。
4. 数据来源选择 `图片`。
5. 点击 `下一步` 来进入下一向导页。

## 2.选取图片

1. 点击 `添加图片` 来向工程中添加图片。如果有质量不好的图片或您不满意的图片，您可以勾选并点击 `删除图片` 来移除它们。
2. 当图片完全加载成功，且确认无误后，点击 `下一步`。

![slecting photos](images/fast-recon/02_select_photos.png)

## 3.设置相机参数

1. 在当前窗口中，您可以看到每一项图片属性：文件名、拍摄日期/时间、经度/纬度、高度等等。
2. 如果相机参数有误或EXIF信息中并未包含相机参数，请自行设置相机参数。
3. 各个相机参数的意义可以参照[此链接](RTMapperDesktop.md#camera-parameters)。

![set camera](images/fast-recon/03_camera_parameters.png)

## 4.设置重建选项

在设置完相机参数后，您需要设置[工程选项](RTMapperDesktop.md#project-options)。

![project option1](images/fast-recon/04_project_option1.png)

![project option2](images/fast-recon/05_project_option2.png)

## 5.运行过程

1. 点击 `执行` 按钮（向右三角型箭头） 来开始运行。
2. 运行过程的输出将显示在窗口底部。
3. 运行状态将显示在状态面板上。

点击工具栏上的开始按钮以执行计算过程。
![start project](images/fast-recon/06_start_project.jpg)

2D运行结果视窗显示生成的地图。
![2D window](images/fast-recon/07_2D_window.jpg)

3D运行结果视窗显示相机位姿及点云。
![3D window](images/fast-recon/07_3D_window.jpg)

## 6.输出结果

1.点击菜单 `文件` -> `输出` 来输出结果。
2.选择您需要输出的文件格式。

目前本软件支持以下三种输出格式。
* 稀疏点云（.ply）：系统运行过程中计算的点云数据。
* 正射影像（.kml）：具有kml描述的PNG图像。
* 正射影像（.tif）：嵌入了几何信息的TIFF图像。
