# 许可注册
本软件提供免费的试用功能，在试用期间，您可以处理30张图片。如果您需要处理更多的图片，请注册软件许可。

## 1.打开许可证窗口
1. 点击菜单 `帮助` -> `注册`。
2. 输入您的邮箱地址。
3. 点击 `生成产品ID` 来产生并保存产品ID。

![register window](images/license/01_input_email.png)

## 2. 生成您的计算机ID
1. 选择你要存储产品ID文件的路径。
2. 请通过rtmapper@163.com联系我们，购买软件许可。请将您的产品ID文件附在邮件里。

![save productID](images/license/02_save_pid.png)

## 3. 设置产品许可
1. 接收我们发送给您的许可证。
2. 点击 `设置许可` 并选择已有的许可文件。
3. 确认许可成功。

点击 `设置许可` 按钮。
![set license](images/license/03_set_license.png)

选择收到的许可文件。
![select license file](images/license/04_select_licenseFile.png)

确认许可的使用年限。
![confirm license](images/license/05_confirm_license.png)
