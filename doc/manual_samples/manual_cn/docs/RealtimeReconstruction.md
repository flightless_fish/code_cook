# 使用RTMapperMobile进行实时处理

在本模式下，您需要有一台[大疆DJI无人机](https://www.dji.com)以及一部安卓手机或平板来实时获取视频。您需要安装[RTMapperMobile](http://www.rtmapper.com/download/RTMapperMobile-latest.apk)来执行数据传输。

如果您想使用自己的无人机，您可以利用[RTMapperSDK](https://github.com/zdzhaoyong/RTMapperSDK)来自行编写程序。更多开发相关支持，详情请见[SDK网址](https://github.com/zdzhaoyong/RTMapperSDK).

整个地图创建的过程是运行在一台装有[RTMapperDesktop](RTMapperDesktop.md)软件的台式机或笔记本上的。

## 1. 需求

1. 下载[RTMapperMobile](RTMapperMobile.md)，并在您的安卓手机或平板上安装该软件。下载地址为[http://www.rtmapper.com/download/RTMapperMobile-latest.apk](http://www.rtmapper.com/download/RTMapperMobile-latest.apk)。

2. 您需要一台大疆DJI无人机。支持的型号有Mavic、Mavic Air、Mavic Pro、Spark、Phantom 4 Pro、Phantom 4 Advanced、Inspire 2。

## 2.设置[RTMapperMobile](RTMapperMobile.md)

1. 打开您手机或平板上的WiFi热点。
2. 打开[RTMapperMobile](RTMapperMobile.md)应用。
3. 触摸屏幕右上角齿轮形状的图标，打开设置面板。
4. 在面板上选择数据传输设置。（面板左边栏第三个标志）
5. 打开 `使用WiFi传输数据` 选项，并记住IP地址及端口号。
6. 如果您希望将数据流保存在手机的内存卡中，您可以打开 `Record Data to File` 选项。

![setup](images/realtime-map/01_Setup.png)

## 3.创建任务

1. 在 *任务* 面板上点击 `新建`。

![create mission](images/realtime-map/02_Create_mission.jpg)

2. 选择任务类型：`区域任务` 或 `路点任务`。

![mission type](images/realtime-map/02_Mission_type.jpg)


## 4.设置任务

1. 移动锚点来创建任务区域或路点，点击 `+` 按钮添加新的控制点。

2. 设置[任务参数](RTMapperMobile.md#mission-parameters)， 一些重要的参数可以参见[这里](RTMapperMobile.md#mission-parameters)。

![mission setup1](images/realtime-map/03_Setup1.jpg)

![mission setup2](images/realtime-map/03_Setup2.jpg)

## 5.启动[RTMapperDesktop](RTMapperDesktop.md)

1. 点击菜单 `文件->新建` 或 `Ctrl-N` 开启一个新的工程向导。
2. 点击 `工程名称` 并选择工程文件夹。
3. 选择工程类型为 `Ortho`， 目前暂不支持Oblique。
4. 数据来源选择 `IP TCP/UDP`。
![create project](images/realtime-map/04_CreateProject.jpg)
5. 参照先前设置菜单里的IP和端口号依次进行填写。
![create project](images/realtime-map/04_Setup_IP_Port.jpg)


## 6.设置重建选项

在设置完相机参数后，您需要设置[工程选项](RTMapperDesktop.md#project-options)。

![project option1](images/fast-recon/04_project_option1.png)

![project option2](images/fast-recon/05_project_option2.png)

## 7.计算

1. 点击[RTMapperMobile](RTMapperMobile.md)应用上的 `飞行` 按钮（右上角纸飞机状图标）。如果一切准备就绪，将启动飞行任务。
![mission setup1](images/realtime-map/03_Setup1.jpg)

2. 当无人机达到默认高度（50m)或您自己设置的条件时，在[RTMapperDesktop](RTMapperDesktop.md) 软件上点击 `运行` 按钮（向右方向箭头）来执行飞行任务。
![start project](images/realtime-map/05_run.jpg)

3. 运行过程的输出将显示在窗口底部。
4. 运行状态将显示在状态面板上。

## 8.输出结果

当飞行任务结束后，您可以输出结果。

1.点击菜单 `文件` -> `输出` 来输出结果。
2.选择您需要输出的文件格式。

目前本软件支持以下三种输出格式。

* 稀疏点云（.ply）：系统运行过程中计算的点云数据。
* 正射影像（.kml）：具有kml描述的PNG图像。
* 正射影像（.tif）：嵌入了几何信息的TIFF图像。

![export](images/fast-recon/08_export.png)