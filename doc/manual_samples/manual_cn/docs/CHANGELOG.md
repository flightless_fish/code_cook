# 软件变更日志

## 3.2.3 (2018-04-25)
* RTMapper桌面版：
    - 在Windows平台上支持文件名、路径使用本地字符。(Question !!!)
    - 修复在工程结束是软件崩溃的bug。
    - 改动GUI以期更好的用户体验。
    - 增加线上用户手册。
    - 修复GPS拟合问题，提升拟合的鲁棒性。
* RTMapper移动版：
    - 增加区域任务模式。
    - 改进用户界面、任务设置及路点编辑功能。

## 3.2.2 (2018-03-05)
* 修复License相关bug。
* 在Windows平台上使用了QT5和Visual Studio进行编译。 (Question !!!)

## 3.2.0 (2018-02-15)
* 新的RTMapperSDK及RTMapper软件架构。
* 在RTMapperMobile上支持使用大疆DJI无人机进行实时地图测绘。
* 为专业开发人员提供[RTMapperSDK](https://github.com/zdzhaoyong/RTMapperSDK)开发环境。

## 3.1.0 (2018-01-10)
* 增加全局设置菜单。
* 改进opmapcontrol以显示生成的瓦片地图。
* 改进dbNet以支持网络数据流传输。

## 2.1.1 (2017-06-01)
* 首个RTMapper的公开发行版本。