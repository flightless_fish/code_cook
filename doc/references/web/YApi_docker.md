# YApi setup by docker

## 1. start mongodb
```
docker run -d \
    --name yapi_mongodb \
    --restart always \
    -v `pwd`/mongo/db:/data/db \
    mongo
```

## 2. pull yapi
```
docker pull registry.cn-hangzhou.aliyuncs.com/anoy/yapi
docker tag registry.cn-hangzhou.aliyuncs.com/anoy/yapi yapi
```

## 3. run interactive
```
docker run -it --rm \
    --name yapi_web \
    --link yapi_mongodb:mongo \
    -p 3000:3000 \
    --entrypoint /bin/sh \
    yapi
```

run commands:
```
cd /api/vendors
npm run install-server
node server/app.js
```

## install
```
docker run -it --rm \
  --name yapi_web \
  --link yapi_mongodb:mongo \
  --entrypoint npm \
  --workdir /api/vendors \
  yapi \
  run install-server
```

## start yapi
```
docker run -d \
  --restart always \
  --name yapi_web \
  --link yapi_mongodb:mongo \
  --workdir /api/vendors \
  -p 3000:3000 \
  yapi \
  server/app.js
```

## YApi docker usage:
* https://www.jianshu.com/p/a97d2efb23c5
* https://github.com/Ryan-Miao/docker-yapi

