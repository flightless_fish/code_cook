# Web development


## 1. System design

采用前后端设计模式，简化各个部分的设计、实现的难度
* frontend, UI： 负责各种终端的显示，从后端调用数据并渲染，负责UI的操作逻辑

* Backend： 负责数据的操作、业务逻辑，只提供REST API给外部，从而简化设计

* 软件架构探索 
	- https://icyfenix.cn/
	- https://github.com/fenixsoft/awesome-fenix

## 2. Libraries

### Framework
Streamlit - 适合快速机器学习应用开发
* https://m.toutiaocdn.com/i6847105285861933575

Flask
* flask-restful (https://github.com/flask-restful/flask-restful)
* [使用 Python 和 Flask 设计 RESTful API](http://www.pythondoc.com/flask-restful/first.html)

API design demo
* JQData ApiDoc https://dataapi.joinquant.com/docs

API document management
* YApi 
    - [YApi docker setup](YApi_docker.md)
* showdoc

Celery - Distributed Task Queue
* [在 Flask 中使用 Celery](http://www.pythondoc.com/flask-celery/first.html)

Nameko - A microservices framework for Python
* https://nameko.readthedocs.io/en/stable/
* A wrapper for using nameko services with Flask (https://github.com/jessepollak/flask-nameko)

REST request
* python requests
* C++ REST SDK (https://github.com/Microsoft/cpprestsdk)
* C++ wrapper around libcURL (https://github.com/jpbarrette/curlpp)
* 好用的http client库CPP REST SDK (https://www.cnblogs.com/highway-9/p/6021238.html)
* How do you make a HTTP request with C++? (https://stackoverflow.com/questions/1011339/how-do-you-make-a-http-request-with-c)



### UI，Frontend
vue https://github.com/vuejs/vue
* [A curated list of awesome things related to Vue.js](https://github.com/vuejs/awesome-vue)
* [基于 vue2 + vuex 构建一个具有 45 个页面的大型单页面应用](https://github.com/bailicangdu/vue2-elm)
* [A magical vue admin](https://github.com/PanJiaChen/vue-element-admin)

Bootstrap
* Gentelella admin template powered by Flask (https://github.com/afourmy/flask-gentelella)
* https://github.com/bootstrap-vue/bootstrap-vue

[Electron - Build cross platform desktop apps](https://electronjs.org/)
* [Useful resources for creating apps with Electron - awesome](https://github.com/sindresorhus/awesome-electron)

### Web utils
* selenium 
* Pyppeteer
* 怎样利用python打开一个网页并实现自动登录 https://www.wukong.com/answer/6712692983575085325/


## H5/APP
* https://www.dcloud.io/hbuilderx.html
* https://www.jianshu.com/p/26c84a6aec57