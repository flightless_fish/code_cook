
## Qt

* Qt Widgets https://gitee.com/feiyangqingyun/QWidgetDemo


## Game
* [Python 游戏编程之实现飞机大战(含源代码)](https://mp.weixin.qq.com/s?__biz=MzA4MjEyNTA5Mw==&mid=2652573892&idx=1&sn=e9645a68fde7792410d0827f7e0469e3&chksm=8465388eb312b1989c7a998a60041acc99b50361d55e182b7e8e78ea2ecdf6a6535a45871365&&xtrack=1&scene=90&subscene=93&sessionid=1605357146&clicktime=1605357188&enterid=1605357188#rd), https://github.com/yangshangqi/The-Python-code-implements-aircraft-warfare


## 3D Map Visualization
* Cesium (3D Map visualization framework based on JS)
    - https://www.cesium.com/
    - https://zhuanlan.zhihu.com/p/34217817
* osgearth
* FlightGear
* Gazebo