

## Utils
* mypy: static type checker for Python
    - https://www.toutiao.com/a6737217897191440904
    - http://mypy-lang.org/

## References

* [Python 算法集](https://gitee.com/TheAlgorithms/Python) 
* [scikit-opt](https://gitee.com/guofei9987/scikit-opt) :  遗传算法、粒子群算法、模拟退火、蚁群算法、免疫优化算法、鱼群算法，旅行商问题等