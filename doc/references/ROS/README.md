
## rosdep

`rosdep` is a command-line tool for installing system dependencies.



```
sudo rosdep init
```

ERROR: cannot download default sources list from:
https://raw.githubusercontent.com/ros/rosdistro/master/rosdep/sources.list.d/20-default.list

need to setup HTTP proxy


```
# install http proxy
sudo apt install polipo
sudo service polipo stop

# start polipo socks5->http proxy
sudo polipo socksParentProxy=192.168.1.4:1080

# set environment
export http_proxy=http://127.0.0.1:8123
export https_proxy=https://127.0.0.1:8123

# do rosdep init / update
sudo su
export http_proxy=http://127.0.0.1:8123
export https_proxy=https://127.0.0.1:8123

rosdep init
exit

rosdep update
```


## ROS commands

### rosrun
运行一个package内部的可执行程序

### roslunch
打开一组node

### rosnode

```
rosnode list                # 列出当前运行的node信息
rosnode info node_name      # 显示出node的详细信息
rosnode kill node_name      # 结束某个node
rosnode ping                # 测试连接节点的延时
rosnode machine             # 列出在特定机器或者列表机器上运行的节点
rosnode cleanup             # 清除不可到达节点的注册信息
```

### rospack
列出所有的package和所在目录


## VirtualBox graphics driver
在虚拟机里面装ROS，gazebo，可能由于虚拟机的显卡不对，所以显示不出来画面


## ROS mirror
https://mirrors.tuna.tsinghua.edu.cn/help/ros/

ROS下的Gazebo安装: 如果你当初安装ROS是使用的full全部安装模式，那么恭喜你，你不需要在安装了。

如果不是，只装了部分，请使用这个命令（注意啊，如果是kinetic版本的ros，用这个，其它版本把对应版本的名字替换掉indigo部分就可以了，注意多使用tab键，有的版本后缀的模拟器名字不同的）
    sudo apt-get install ros-kinetic-simulators

在终端中启动Gazebo：
    gazebo


    ## Ubuntu 16.04
```
sudo apt install gazebo7
```

默认的不能运行
https://www.linuxidc.com/Linux/2017-03/141505.htm


## Docker
* https://registry.hub.docker.com/_/gazebo/


## Tutorial
* Gazebo创建机器人教程（一，二） https://www.jianshu.com/p/2807eed3276f
* Dronecode & Gazebo https://dev.px4.io/v1.9.0/en/simulation/gazebo.html

* ROS入门教程 https://www.ncnynl.com/archives/201608/496.html
* ROS Tutorial: http://wiki.ros.org/cn/ROS/Tutorials