## Programming
* Processing is a flexible software sketchbook and a language for learning how to code within the context of the visual arts. 
    - https://processing.org/
    - The Nature of Code https://natureofcode.com/
    - [Processing Gallery](gui/Processing_Gallery.md)
    - [Processing是干嘛的？艺术家学编程能做什么？](https://zhuanlan.zhihu.com/p/35940282)
    - [想学创意编程？这15款实用工具你知道吗？](https://zhuanlan.zhihu.com/p/33137467)

## Numerical Calculation
* Blitz++

## Graphics & Game
* Box2D: A 2D physics engine for games https://box2d.org/
* [raylib: A simple and easy-to-use library to enjoy videogames programming](https://www.raylib.com/)
* [openFrameworks: C++ toolkit for creative coding](https://github.com/openframeworks/openFrameworks)


## Task / Job Manager
* [A General-purpose Parallel and Heterogeneous Task Programming System](https://github.com/taskflow/taskflow) (C++)

## Media / video

* [ZLMediaKit](https://gitee.com/xia-chu/ZLMediaKit) - 实现RTSP/RTMP/HLS/HTTP协议的轻量级流媒体框架，支持大并发连接请求

## Type training
* tuxtype
* klavaro, ktouch, gtypist


## Math & Geometry
* 几何画板
* kig
* 超级画板, GeoGebra

## Office

[pandoc](https://pandoc.org/index.html) - a universal document converter