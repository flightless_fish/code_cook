# Memory leakage 


## References
* C++ 如何避免内存泄漏 (Arena, Coroutine, RAII, gperftools) https://www.toutiao.com/a6689285387723997707/
* 线上内存泄漏的正确查找姿势 https://zhuanlan.zhihu.com/p/56162782
* C++ 如何避免内存泄漏 https://zhuanlan.zhihu.com/p/51898119
* 学会用Clang来进行内存泄露分析 http://www.cnblogs.com/rickyk/p/3794523.html
* 内存泄露，越界，数据竞争检测 http://www.cnblogs.com/LyndonYoung/articles/5702470.html
* 内存问题的排查工具和方法– Clang的AddressSanitizer https://blog.csdn.net/gnnulzy/article/details/78725001
* 现代 C++ 救不了程序员！ https://www.toutiao.com/a6683745165284213251


## Linux memory profile tools:
* Linux中的常用内存问题检测工具 https://blog.csdn.net/jinzhuojun/article/details/46659155
* Linux下内存检测工具：asan https://blog.csdn.net/biqioso/article/details/82875310


## TCMalloc
* glibc内存泄露以及TCmalloc 简单分析 https://www.cnblogs.com/raymondshiquan/archive/2011/06/25/tcmalloc_configuration_analysis.html
* C dynamic memory allocation https://en.wikipedia.org/wiki/C_dynamic_memory_allocation
* 内存优化总结:ptmalloc、tcmalloc和jemalloc https://blog.csdn.net/junlon2006/article/details/77854898

* 深入了解tcmalloc（一）：windows环境下无缝拦截技术初探 https://blog.csdn.net/sharemyfree/article/details/47339497
* TcMalloc的介绍以及Windows下安装使用 https://www.cnblogs.com/xsln/p/Introduction_TcMalloc.html
* 使用tcmalloc检测内存泄漏 https://www.jianshu.com/p/c6096adca7c0
* vs2015编译tcmalloc(gperftools2.4) https://blog.csdn.net/10km/article/details/50127697

* how to use tcmalloc in programs with dynamic libraries https://stackoverflow.com/questions/7064120/how-to-use-tcmalloc-in-programs-with-dynamic-libraries

* https://github.com/google/leveldb/issues/634
* https://news.ycombinator.com/item?id=5260690


## gperftools:
* https://github.com/gperftools/gperftools/wiki
* https://github.com/gperftools/gperftools
* google/pprof: https://github.com/google/pprof

* How To Use TCMalloc? https://stackoverflow.com/questions/29205141/how-to-use-tcmalloc


### pprof:
basic usage:
```
export HEAPCHECK=normal
./you_exe
```

simple text display:
```
pprof -text -lines  /tmp/test_stl_sptr.12061._main_-end.heap
```

graphics display:
```
pprof -web -inuse_objects -lines  -edgefraction=1e-10  /tmp/test_stl_sptr.12061._main_-end.heap
```


## Valgrind:
* https://www.ibm.com/developerworks/cn/linux/l-cn-valgrind/index.html
* https://www.ibm.com/developerworks/cn/aix/library/au-memorytechniques.html
* https://www.ibm.com/developerworks/cn/linux/l-mleak/


## memory-profiler:
* https://github.com/nokia/memory-profiler
* https://www.toutiao.com/a6693423291643724300


## LeakSanitizer:
* AddressSanitizerLeakSanitizer https://github.com/google/sanitizers/wiki/AddressSanitizerLeakSanitizer


## jemalloc
JeMalloc 是一款内存分配器，与其它内存分配器相比，它最大的优势在于多线程情况下的高性能以及内存碎片的减少。

* https://github.com/jemalloc/jemalloc
* https://zhuanlan.zhihu.com/p/48957114
* https://www.cnblogs.com/gaoxing/p/4253833.html